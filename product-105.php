<?php include("header.php"); ?>

<div id="breadcrumb">
	<a class="back" href="javascript:history.back();"><i class="fa fa-chevron-left"></i></a>
	<ul>
		<li><a href="">Dashboard</a>
		</li><li><a href="">Product 105</a></li>
	</ul>
</div>




<div id="content" class="prod-map">

	<div class="page-header">
		<div class="title">
			<span>Product 105</span>
			<h1>HIDTA HIFCA Risk Index</h1>						
		</div>
		<ul class="exports">
			<li><a target="_blank" href="data/105(a) - HIDTA HIFCA Risk Index.xlsx">EXPORT RISK INDEX</a></li>
			<li><a target="_blank" href="data/HIDTA.pdf">HIDTA SOURCE FILES</a></li>
			<li><a target="_blank" href="data/HIFCA.pdf">HIFCA SOURCE FILES</a></li>
		</ul>
	</div>

	<div class="inner">
		<div class="col-1">
				<div class="row">
					<div class="col-sm-12">
						<div class="panel">
							<div id="select-filters">
								<div class="inner">
									<h4>COUNTY EXPLORER</h4>
									<div class="search select2box">
										<form>
											<select id="country-search105"></select>
										</form>						
									</div>


								</div>
							</div>									
							<div id="product105"></div>
						</div>				
					</div>
				</div>
			</div>
		<div class="col-2">
		
			<div id="select-a-card">
				<div class="inner">
					<i class="fa fa-search-plus"></i>
					<p>Select a county on the map to view more information</p>
				</div>
			</div>				
			<div id="county-card"  class="data-selection" style="display:none;">
			</div>
		</div>	
	</div>
</div>	


<?php include("footer.php"); ?>

