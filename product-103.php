
							
<?php include("header.php"); ?>

<div id="breadcrumb">
  <a class="back" href="javascript:history.back();"><i class="fa fa-chevron-left"></i></a>
  <ul>
    <li><a href="">Dashboard</a>
    </li><li><a href="">Product 103</a></li>
  </ul>
</div>

<div id="content" class="prod-104">

  <div class="page-header">
    <div class="title">
      <span>Product 103</span>
      <h1>US Regulatory Guidance (FinCEN)</h1>              
    </div>
  </div>

  <div id="prod-104-filters">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel">
          <div id="filters" class="pad">

            <span class="small-cap">FILTERS</span>

            <select class="option-set clearfix"  data-filter-group="institution" style="float:left !important;">
              <option value="*" data-filter-value="" class="selected">All FinCEN</option>
              <option value="" data-filter-value="">Advisories</option>
              <option value="" data-filter-value="">Guidance</option>
              <option value="" data-filter-value="">311 Special Measures</option>
              <option value="" data-filter-value="">Administrative Rulings</option>
            </select>

            <select class="filter option-set clearfix"  data-filter-group="bsa" style="float:left !important;">
              <option value="*" data-filter-value="" class="selected">All BSA Program Areas</option>
              <option value="" data-filter-value="">KYC</option>
              <option value="" data-filter-value="">SAR</option>
              <option value="" data-filter-value="">CTR</option>
              <option value="" data-filter-value="">WLM</option>
            </select>

            <select class="filter option-set clearfix"  data-filter-group="subarea" style="float:left !important;">
                <option value="*" data-filter-value="" class="selected">All BSA Sub-Program Area</option>
                <option value="" data-filter-value=".cip">Customer Identification Program</option>
                <option value="" data-filter-value=".cdd">Customer Due Diligence</option>
                <option value="" data-filter-value=".crr">Customer Risk Rating</option>
                <option value="" data-filter-value=".edd">Enhanced Due Diligence</option>
                <option value="" data-filter-value=".tranmon">Transaction Monitoring</option>
                <option value="" data-filter-value=".sarfile">SAR Filings</option>
                <option value="" data-filter-value=".ctrfile">CTR Filings</option>
                <option value="" data-filter-value=".redflag">Red Flags</option>
                <option value="" data-filter-value=".recordkeeping">Recordkeeping</option>
                <option value="" data-filter-value=".314a">314(a)</option>
                <option value="" data-filter-value=".314b">314(b)</option>
                <option value="" data-filter-value=".sanctions">Sanctions</option>
                <option value="" data-filter-value=".riskassesment">Risk Assessment</option>
                <option value="" data-filter-value=".coraccs">Correspondent Accounts</option>
            </select>

            <select class="filter option-set clearfix"  data-filter-group="bsa" style="float:left !important;">
              <option value="*" data-filter-value="" class="selected">All Issuances</option>
              <option value="" data-filter-value="">Advisory</option>
              <option value="" data-filter-value="">Update</option>
              <option value="" data-filter-value="">Widthdrawal</option>
              <option value="" data-filter-value="">NPRM</option>
              <option value="" data-filter-value="">Final Ruling</option>
              <option value="" data-filter-value="">Finding</option>
              <option value="" data-filter-value="">Supplement</option>
            </select>

            <div class="search select2box" style="float:left !important;">
              <form>
                <select id="country-search101"></select>
              </form>           
            </div>

            <div class="date-range">
              <span>Dataset Range</span>
              <select name="" id="gtDate">
              </select>
              -
              <select name="" id="ltDate">
              </select>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

    
  <div class="inner">

    <div class="col-2" style="">
      <div class="inner">
        <div class="row">

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product103-guide'></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product103-bsa'></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product103-sub'></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product103-issuance'></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product103-country'></div>
            </div>
          </div>

        </div>
      </div>  
    </div>

    <div class="col-1"  style="min-height:2500px !important;">
      <div class="row">
        <div class="col-sm-12">
          <div class="panel">
            <div id="sort">         
              SORT BY
              <select class="button-group sort-by-button-group">
                <option data-sort-by="year">Most Recent</option>
              </select>
            </div>  
            <h3 style="margin-top:10px;">DATASET - FinCEN ADVISORIES (154)</h3>
            <div class="grid"> </div>
          </div>
        </div>    
      </div>
    </div>

    <div id="inspector">
      <div id="report-card" class="data-selection ea-1">

      </div>
    </div>


  </div>
  
</div>

<?php include("footer.php"); ?>

