<?php include("header.php"); ?>

<div id="breadcrumb">
	<a class="back" href="javascript:history.back();"><i class="fa fa-chevron-left"></i></a>
	<ul>
		<li><a href="">Dashboard</a>
		</li><li><a href="">Search</a>
		</li><li><a href="">Advisory on the FATF-Identified Jurisdictions with AML/CFT Deficiencies</a></li>
	</ul>
</div>

<div id="content">
	<div class="col-1">

	</div>	
	<div class="col-2">
					<div class="page-header-preview">
						<div class="title">
							<span>PDF</span>
						<h1>Advisory on the FATF-Identified Jurisdictions with AML/CFT Deficiencies</h1>	
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, vero ipsa dolore odio nemo voluptate sint ducimus consequatur sapiente magnam, rerum sed. Molestias reiciendis, enim.</p>
						<a href="" class="btn"><i class="fa fa-download"></i> Download</a>		
						<a href="" class="btn"><i class="fa fa-bookmark"></i> Bookmark</a>	
						<ul>
							<li>Advisory</li>
							<li>Financial</li>
							<li>Politics</li>
							<li>Corruption</li>
							<li>Laws</li>
							<li>FinCEN</li>
						</ul>
	
						</div>

					</div>
	</div>
</div>


<?php include("footer.php"); ?>

