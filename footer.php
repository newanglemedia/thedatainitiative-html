<footer>
	<span><a target="_blank" href="http://www.thedatainitiative.com">The Data  Initiative</a> © 2018. All Rights Reserved.</span>
	<ul id="legal">
		<li>
			<a href="">Privacy Policy</a>
			<a href="">Terms of Use</a>
		</li>
	</ul>
<!--
	<ul id="social">
		<li><a href="https://www.facebook.com/datainitiative/"><i class="fa fa-facebook"></i></a></li>
		<li><a href="https://twitter.com/data_initiative"><i class="fa fa-twitter"></i></a></li>
		<li><a href="https://www.linkedin.com/company/the-data-initiative/"><i class="fa fa-linkedin"></i></a></li>
	</ul>
-->
	
	
</footer>


<script src="js/jquery-1.11.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="http://www.jsviews.com/download/jsrender.js"></script>

<script src="js/jquery.sticky.js"></script>




<!--<script src="https://code.highcharts.com/maps/highmaps.js"></script>-->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script> 
<script src="https://code.highcharts.com/maps/modules/data.js"></script>
<script src="http://code.highcharts.com/maps/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/mapdata/custom/world-continents.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/us/us-all-all.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/us/us-all-all-highres.js"></script>
<script src="https://code.highcharts.com/mapdata/custom/world-palestine.js"></script>      

<script src="js/isotope.pkgd.min.js"></script>


<script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.js"></script>
<script type="text/javascript">
Array.prototype.unique = function() {
      return this.filter(function (value, index, self) {
        return self.indexOf(value) === index;
      });
    };
</script>
<script id="card101isotmpl" type="text/x-jsrender">
  <div data-id="{{:id}}" class="grid-item">
    <div class="inner">
      <p class="year">{{:year}}</p>
      <ul class="tags">
        <li>{{:issue_abbr}}</li>
      </ul>
      <h3 class="name">{{:title}}</h3>
      <div class="state-tag">{{:issue_abbr}}</div>
    </div>
  </div>
</script>
<script id="card101tmpl" type="text/x-jsrender">
    <div class="inner">
      <i class="fa fa-times close-inspector"></i>
      <h2>{{:title}}</h2>
      <table>
        <tr>
          <td>Source</td>
          <td>{{:source}}</td>
        </tr>
        <tr>
          <td>Coverage Year</td>
          <td>{{:coverage_year}}</td>
        </tr>
        <tr>
          <td>Pages</td>
          <td>{{:pages}}</td>
        </tr>
        <tr>
          <td>Issue</td>
          <td>{{:issue_type}}</td>
        </tr>
      </table>
      <div class="moreinfo">
        <h3 class="head">Executive Summary</h3>
        <p>{{:executive_summary}}</p>
      </div>
    </div>
</script>
<script id="card102tmpl" type="text/x-jsrender">
      <div class="inner">
        <h2 id="country-card-country">{{:country}}</h2>
        <h6 id="country-card-capital">{{:capital}}</h6>
        <h6 id="country-card-region">{{:region}}</h6>
        <table>
          <tr>
            <td>Anti-Money Laundering Laws</td>
            <td>{{:aml_laws}}</td>
          </tr>
          <tr>
            <td>Terrorism Financing Laws</td>
            <td>{{:tf_laws}}</td>
          </tr>
        </table>
        <hr>
        <h5>Laws and Regulations</h5>
          <div class="expander">
            <h3 class="head">Anti-Money Laundering Laws</h3>
            <ul>
              {{for aml_docs}}
                  <li><a href="">{{:name}}</a></li>
              {{/for}}
            </ul>
          </div>
          <div class="expander">
            <h3 class="head">Terrorism Financing Laws</h3>
            <ul>
              {{for tf_docs}}
                  <li><a href="">{{:name}}</a></li>
              {{/for}}
            </ul>
          </div>
          <div class="expander">
            <h3 class="head">Program Areas</h3>
            <ul>
              {{for pa_docs}}
                  <li><a href="">{{:name}}</a></li>
              {{/for}}
            </ul>
          </div>
          <div class="expander">
            <h3 class="head">Financial Institution Types</h3>
            <ul>
              {{for fi_docs}}
                  <li><a href="">{{:name}}</a></li>
              {{/for}}
            </ul>
          </div>
          <div class="expander">
            <h3 class="head">Illicit Acts</h3>
            <ul>
              {{for ia_docs}}
                  <li><a href="">{{:name}}</a></li>
              {{/for}}
            </ul>
          </div>
          <div class="expander">
            <h3 class="head">OFAC Sanctions</h3>
            <ul>
              {{for ofac_docs}}
                  <li><a href="">{{:name}}</a></li>
              {{/for}}
            </ul>
          </div>
          <div class="expander">
            <h3 class="head">311 Special Measures</h3>
            <ul>
              {{for sm_docs}}
                  <li><a href="">{{:name}}</a></li>
              {{/for}}
            </ul>
          </div>

        <div class="moreinfo">
          <span class="btn">106 - RISK INDEX</span><br/>
        </div>
      </div>
</script>
<script id="card103tmpl" type="text/x-jsrender">
    <div class="inner">
      <i class="fa fa-times close-inspector"></i>
      <h2>{{:title}}</h2>
      <table>
        <tr>
          <td>Source</td>
          <td>{{:source}}</td>
        </tr>
        <tr>
          <td>Advisory Year</td>
          <td>{{:year}}</td>
        </tr>
        <tr>
          <td>Issuance</td>
          <td>{{:issuance_type}}</td>
        </tr>
        <tr>
          <td>Countries Implicated</td>
          <td>{{:countries_implicated}}</td>
        </tr>
        <tr>
          <td>BSA Program Area(s) Involved</td>
          <td>{{:bsa_programs}}</td>
        </tr>
      </table>
      <div class="moreinfo">
        <h3 class="head">Executive Summary</h3>
        <p>{{:executive_summary}}</p>
      </div>
    </div>
</script>
<script id="card103isotmpl" type="text/x-jsrender">
  <div data-id="{{:id}}" class="grid-item">
    <div class="inner">
      <p class="year">{{:year}}</p>
    

    <ul class="tags">
      <li>{{:issuance_abbr}}</li>
    </ul>
      <h3 class="name">{{:title}}</h3>
      <div class="state-tag">{{:issuance_type_abbr}}</div>
    </div>
  </div>
</script>
<script id="card104isotmpl" type="text/x-jsrender">
  <div data-matter-id="{{:matter_number}}" class="grid-item {{:entity_iso_class}} {{:pvs_iso_class}} {{:bsa_iso_class}} {{:sub_iso_class}} {{:assetsize_iso_class}} {{:penalty_iso_class}} {{:state_abbr}}">
    <div class="inner">
      <p class="year">{{:issuance_date}}</p>
	  

	  <ul class="tags">
			<li>{{:entity_acro}}</li>
	  </ul>
      <h3 class="name">{{:entity_name}}</h3>
      <div class="state-tag">{{:state_abbr}}</div>
      <!--<p>$<span class="penalty">{{:fine_penalty}}</span></p>-->
    </div>
  </div>
</script>
<script id="card104tmpl" type="text/x-jsrender">
<div class="inner2" data-simplebar>

  <div class="inner">
    <i class="fa fa-times close-inspector"></i>
    <h2>{{:enforcement_action_legal_name}}</h2>
    <table>
      <tr>
        <td>Entity Type</td>
        <td>{{:financial_institution}}</td>
      </tr>
      <tr>
        <td>Asset Size</td>
        <td>{{:asset_size}}</td>
      </tr>
      <tr>
        <td>State</td>
        <td>{{:state}}</td>
      </tr>
      <tr>
        <td>Penalty Amount</td>
        <td>${{:fine_penalty}}</td>
      </tr>
      <tr>
        <td>Pillar Violated</td>
        <td>{{:aml_pillars_violated}}</td>
      </tr>
      <tr>
        <td>BSA Program Area</td>
        <td>{{:bsa_program_area}}</td>
      </tr>
      <tr>
        <td>BSA Sub-Program Area</td>
        <td>{{:sub_areas}}</td>
      </tr>

    </table>
    <div class="moreinfo">
      <h3 class="head">Specific Conduct</h3>
      <p>{{:specific_conduct}}</p>
      <h3 class="head">Entity Name(s)</h3>
      <p>{{:entity_name}}</p>
    </div>
	</div>
  </div>
</script>
<script id="card105tmpl" type="text/x-jsrender">
        <div class="inner">
          <h2 class="data-select-title">{{:county}}</h2>
          <h6>{{:latLong}}</h6>
          <h6>{{:region}}</h6>
          <table>
            <tr>
              <td>HIDTA Risk</td>
              <td style="color:#d65527;">{{:hidtaRisk}}</td>
            </tr>
            <tr>
              <td>HIFCA Risk</td>
              <td style="color:#d65527;">{{:hifcaRisk}}</td>
            </tr>
            <tr>
              <td>HIDTA/HIFCA Risk</td>
              <td style="color:#d65527;">{{:bothRisk}}</td>
            </tr>
            <tr>
              <td>No HIDTA/HIFCA Risk</td>
              <td style="color:#d65527;">{{:noRisk}}</td>
            </tr>
          </table>
          <div class="moreinfo">
            <h3 class="head">Zip Code(s)</h3>
            <p style="max-height:250px;" data-simplebar>{{:zipCode}}</p>
            <h3 class="head">Special Note</h3>
            <p>{{:specialNote}}</p>
          </div>
        </div>
</script>

<script id="card106tmpl" type="text/x-jsrender">
      <div class="inner">
        <h2 id="country-card-country">{{:country}}</h2>
        <h6 id="country-card-capital">{{:capital}}</h6>
        <h6 id="country-card-region">{{:region}}</h6>

        <span class="{{:riskClass}}">{{:riskText}} <span style="float:right;">{{:riskIndex}}/100</span></span>

        <table>
          <tr>
            <td>Criminalized Drug Money Laundering</td>
            <td style="color:#d65527;">{{:cdml}}</td>
          </tr>
          <tr>
            <td>Know-Your-Customer Provisions</td>
            <td style="color:#d65527;">{{:kyc}}</td>
          </tr>
          <tr>
            <td>Report Suspicious Transactions</td>
            <td style="color:#d65527;">{{:rst}}</td>
          </tr>
          <tr>
            <td>FIU Member of Egmont Group</td>
            <td style="color:#d65527;">{{:fiu}}</td>
          </tr>
          <tr>
            <td>State Sponsor of Terrorism</td>
            <td style="color:#d65527;">{{:ssot}}</td>
          </tr>
        </table>

        <div class="moreinfo">
          <h3 class="head">PRIMARY Source(s) of Illicit Proceeds</h3>
          <p>
            <ul style="list-style:disc;padding-left: 25px;">
              {{for illicit_activities}}
                  <li>{{:name}}</li>
              {{/for}}
            </ul>
          </p>

          <h3 class="head">FATF Membership</h3>
          <p>{{:fatf}}</p>

          <h3 class="head">Last Mutual Evaluation</h3>
          <p>{{:last_eval}}</p>

          <h3 class="head">Mutual Evaluation Results</h3>
          <p>{{:eval_results}}</p>
        </div>
      </div>
</script>


<script>
	
$('#filters select').on('change', function() {
	if ($(this).val() == '*') {
		$(this).removeClass('active');

	}
	else {
    $(this).addClass('active');
	}
});	
		
	
	
var console_data;

var card101tmpl = $.templates("#card101tmpl");
var card101isotmpl = $.templates("#card101isotmpl");
var card103tmpl = $.templates("#card103tmpl");
var card103isotmpl = $.templates("#card103isotmpl");
var card102tmpl = $.templates("#card102tmpl");  
var card104tmpl = $.templates("#card104tmpl");
var card104isotmpl = $.templates("#card104isotmpl");
var card105tmpl = $.templates("#card105tmpl");
var card106tmpl = $.templates("#card106tmpl");	


function tdiCC(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function tdiRC(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}


$(document).ready(function() {
	
		$(".data-selection").sticky({topSpacing:0, bottomSpacing:75});

	// LOGIN SCRIPT	
    $('.submit').click(function() {

        event.preventDefault(); // prevent PageReLoad
        var ValidEmail = $('#username').val() === 'demo'; // User validate
		var ValidPassword = $('#password').val() === 'demo'; // Password validate
        if (ValidEmail === true && ValidPassword === true) { // if ValidEmail & ValidPassword
            window.location = window.location.href+"dashboard.php";
        }
        else {
            $('.error').css('display', 'block'); // show error msg
        }
    });
	
	//SEARCH SCRIPT
	$('#search form .submit').click(function() {
            window.location = window.location.href.substring(0,window.location.href.lastIndexOf('/'))+"/search.php";
	});	

	//
	$(document).on('click','.expander',function(e) {
		$(this).toggleClass('active');
		$(this).find('h3').next().slideToggle();
		$(this).siblings().find('h3').next().slideUp();
		$(this).siblings().removeClass('active');
		e.stopPropagation();

    });

	//SEARCH SUGGESTIONS
	var prod101 = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.whitespace,
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  prefetch: './data/search_suggestions101.json'
	});
    var prod104 = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: './data/search_suggestions104.json'
    });
	$('#search-input').typeahead({
	  hint: true,
	  highlight: true,
	  minLength: 1
	},
	{
	  name: 'product101',
	  source: prod101,
	  templates: {
	  	header: '<h3 class="product-name">Product 101</h3>',
	  }
	},
    {
      name: 'product104',
      source: prod104,
      templates: {
        header: '<h3 class="product-name">Product 104</h3>',

      }
    });
if(window.location.href.split('/').pop() == "dashboard.php"){

  if(tdiRC('tdi-welcome-msg') === null){
    $('#welcome-dashboard').fadeIn();
  }
	
	
	
 $(document).on('click','#welcome-dashboard .checkbox', function(){
  $('#welcome-dashboard').fadeOut();
  tdiCC('tdi-welcome-msg', true, 365);
 });
}

if(window.location.href.split('/').pop() == "product-101.php"){

  function getItemById(id){
    for (var i = 0; i < report_data.length; i++) {
      if(report_data[i].id == id){
        return report_data[i];
      }
    };
  }

  var p1010data = [{"id":"","orig":"","text":""},{"id":"AF","orig":"Afghanistan","text":"Afghanistan"},{"id":"AL","orig":"Albania","text":"Albania"},{"id":"DZ","orig":"Algeria","text":"Algeria"},{"id":"AS","orig":"American Samoa","text":"American Samoa"},{"id":"AD","orig":"Andorra","text":"Andorra"},{"id":"AO","orig":"Angola","text":"Angola"},{"id":"AG","orig":"Antigua and Barbuda","text":"Antigua and Barbuda"},{"id":"AR","orig":"Argentina","text":"Argentina"},{"id":"AM","orig":"Armenia","text":"Armenia"},{"id":"AU","orig":"Australia","text":"Australia"},{"id":"AT","orig":"Austria","text":"Austria"},{"id":"AZ","orig":"Azerbaijan","text":"Azerbaijan"},{"id":"BH","orig":"Bahrain","text":"Bahrain"},{"id":"BU","orig":"Bajo Nuevo Bank (Petrel Is.)","text":"Bajo Nuevo Bank (Petrel Is.)"},{"id":"BD","orig":"Bangladesh","text":"Bangladesh"},{"id":"BB","orig":"Barbados","text":"Barbados"},{"id":"BY","orig":"Belarus","text":"Belarus"},{"id":"BE","orig":"Belgium","text":"Belgium"},{"id":"BZ","orig":"Belize","text":"Belize"},{"id":"BJ","orig":"Benin","text":"Benin"},{"id":"BT","orig":"Bhutan","text":"Bhutan"},{"id":"BO","orig":"Bolivia","text":"Bolivia"},{"id":"BA","orig":"Bosnia and Herzegovina","text":"Bosnia and Herzegovina"},{"id":"BW","orig":"Botswana","text":"Botswana"},{"id":"BR","orig":"Brazil","text":"Brazil"},{"id":"BN","orig":"Brunei","text":"Brunei"},{"id":"BG","orig":"Bulgaria","text":"Bulgaria"},{"id":"BF","orig":"Burkina Faso","text":"Burkina Faso"},{"id":"BI","orig":"Burundi","text":"Burundi"},{"id":"KH","orig":"Cambodia","text":"Cambodia"},{"id":"CM","orig":"Cameroon","text":"Cameroon"},{"id":"CA","orig":"Canada","text":"Canada"},{"id":"CV","orig":"Cape Verde","text":"Cape Verde"},{"id":"CF","orig":"Central African Republic","text":"Central African Republic"},{"id":"TD","orig":"Chad","text":"Chad"},{"id":"CL","orig":"Chile","text":"Chile"},{"id":"CN","orig":"China","text":"China"},{"id":"CO","orig":"Colombia","text":"Colombia"},{"id":"KM","orig":"Comoros","text":"Comoros"},{"id":"CR","orig":"Costa Rica","text":"Costa Rica"},{"id":"HR","orig":"Croatia","text":"Croatia"},{"id":"CU","orig":"Cuba","text":"Cuba"},{"id":"CY","orig":"Cyprus","text":"Cyprus"},{"id":null,"orig":"Cyprus No Mans Area","text":"Cyprus No Mans Area"},{"id":"CZ","orig":"Czech Republic","text":"Czech Republic"},{"id":"CD","orig":"Democratic Republic of the Congo","text":"Democratic Republic of the Congo"},{"id":"DK","orig":"Denmark","text":"Denmark"},{"id":"DJ","orig":"Djibouti","text":"Djibouti"},{"id":"DM","orig":"Dominica","text":"Dominica"},{"id":"DO","orig":"Dominican Republic","text":"Dominican Republic"},{"id":"TL","orig":"East Timor","text":"East Timor"},{"id":"EC","orig":"Ecuador","text":"Ecuador"},{"id":"EG","orig":"Egypt","text":"Egypt"},{"id":"SV","orig":"El Salvador","text":"El Salvador"},{"id":"GQ","orig":"Equatorial Guinea","text":"Equatorial Guinea"},{"id":"ER","orig":"Eritrea","text":"Eritrea"},{"id":"EE","orig":"Estonia","text":"Estonia"},{"id":"ET","orig":"Ethiopia","text":"Ethiopia"},{"id":"FO","orig":"Faroe Islands","text":"Faroe Islands"},{"id":"FM","orig":"Federated States of Micronesia","text":"Federated States of Micronesia"},{"id":"FJ","orig":"Fiji","text":"Fiji"},{"id":"FI","orig":"Finland","text":"Finland"},{"id":"FR","orig":"France","text":"France"},{"id":"GA","orig":"Gabon","text":"Gabon"},{"id":"GM","orig":"Gambia","text":"Gambia"},{"id":"GZ","orig":"Gaza Strip","text":"Gaza Strip"},{"id":"GE","orig":"Georgia","text":"Georgia"},{"id":"DE","orig":"Germany","text":"Germany"},{"id":"GH","orig":"Ghana","text":"Ghana"},{"id":"GR","orig":"Greece","text":"Greece"},{"id":"GL","orig":"Greenland","text":"Greenland"},{"id":"GD","orig":"Grenada","text":"Grenada"},{"id":"GU","orig":"Guam","text":"Guam"},{"id":"GT","orig":"Guatemala","text":"Guatemala"},{"id":"GN","orig":"Guinea","text":"Guinea"},{"id":"GW","orig":"Guinea Bissau","text":"Guinea Bissau"},{"id":"GY","orig":"Guyana","text":"Guyana"},{"id":"HT","orig":"Haiti","text":"Haiti"},{"id":"HN","orig":"Honduras","text":"Honduras"},{"id":"HU","orig":"Hungary","text":"Hungary"},{"id":"IS","orig":"Iceland","text":"Iceland"},{"id":"IN","orig":"India","text":"India"},{"id":"ID","orig":"Indonesia","text":"Indonesia"},{"id":"IR","orig":"Iran","text":"Iran"},{"id":"IQ","orig":"Iraq","text":"Iraq"},{"id":"IE","orig":"Ireland","text":"Ireland"},{"id":"IL","orig":"Israel","text":"Israel"},{"id":"IT","orig":"Italy","text":"Italy"},{"id":"CI","orig":"Ivory Coast","text":"Ivory Coast"},{"id":"JM","orig":"Jamaica","text":"Jamaica"},{"id":"JP","orig":"Japan","text":"Japan"},{"id":"JO","orig":"Jordan","text":"Jordan"},{"id":"KZ","orig":"Kazakhstan","text":"Kazakhstan"},{"id":"KE","orig":"Kenya","text":"Kenya"},{"id":"KI","orig":"Kiribati","text":"Kiribati"},{"id":"KV","orig":"Kosovo","text":"Kosovo"},{"id":"KW","orig":"Kuwait","text":"Kuwait"},{"id":"KG","orig":"Kyrgyzstan","text":"Kyrgyzstan"},{"id":"LA","orig":"Laos","text":"Laos"},{"id":"LV","orig":"Latvia","text":"Latvia"},{"id":"LB","orig":"Lebanon","text":"Lebanon"},{"id":"LS","orig":"Lesotho","text":"Lesotho"},{"id":"LR","orig":"Liberia","text":"Liberia"},{"id":"LY","orig":"Libya","text":"Libya"},{"id":"LI","orig":"Liechtenstein","text":"Liechtenstein"},{"id":"LT","orig":"Lithuania","text":"Lithuania"},{"id":"LU","orig":"Luxembourg","text":"Luxembourg"},{"id":"MK","orig":"Macedonia","text":"Macedonia"},{"id":"MG","orig":"Madagascar","text":"Madagascar"},{"id":"MW","orig":"Malawi","text":"Malawi"},{"id":"MY","orig":"Malaysia","text":"Malaysia"},{"id":"MV","orig":"Maldives","text":"Maldives"},{"id":"ML","orig":"Mali","text":"Mali"},{"id":"MT","orig":"Malta","text":"Malta"},{"id":"MH","orig":"Marshall Islands","text":"Marshall Islands"},{"id":"MR","orig":"Mauritania","text":"Mauritania"},{"id":"MU","orig":"Mauritius","text":"Mauritius"},{"id":"MX","orig":"Mexico","text":"Mexico"},{"id":"MD","orig":"Moldova","text":"Moldova"},{"id":"MC","orig":"Monaco","text":"Monaco"},{"id":"MN","orig":"Mongolia","text":"Mongolia"},{"id":"ME","orig":"Montenegro","text":"Montenegro"},{"id":"MA","orig":"Morocco","text":"Morocco"},{"id":"MZ","orig":"Mozambique","text":"Mozambique"},{"id":"MM","orig":"Myanmar","text":"Myanmar"},{"id":"NA","orig":"Namibia","text":"Namibia"},{"id":"NR","orig":"Nauru","text":"Nauru"},{"id":"NP","orig":"Nepal","text":"Nepal"},{"id":"NL","orig":"Netherlands","text":"Netherlands"},{"id":"NZ","orig":"New Zealand","text":"New Zealand"},{"id":"NI","orig":"Nicaragua","text":"Nicaragua"},{"id":"NE","orig":"Niger","text":"Niger"},{"id":"NG","orig":"Nigeria","text":"Nigeria"},{"id":"KP","orig":"North Korea","text":"North Korea"},{"id":"NC","orig":"Northern Cyprus","text":"Northern Cyprus"},{"id":"MP","orig":"Northern Mariana Islands","text":"Northern Mariana Islands"},{"id":"NO","orig":"Norway","text":"Norway"},{"id":"OM","orig":"Oman","text":"Oman"},{"id":"PK","orig":"Pakistan","text":"Pakistan"},{"id":"PW","orig":"Palau","text":"Palau"},{"id":"PA","orig":"Panama","text":"Panama"},{"id":"PG","orig":"Papua New Guinea","text":"Papua New Guinea"},{"id":"PY","orig":"Paraguay","text":"Paraguay"},{"id":"PE","orig":"Peru","text":"Peru"},{"id":"PH","orig":"Philippines","text":"Philippines"},{"id":"PL","orig":"Poland","text":"Poland"},{"id":"PT","orig":"Portugal","text":"Portugal"},{"id":"PR","orig":"Puerto Rico","text":"Puerto Rico"},{"id":"QA","orig":"Qatar","text":"Qatar"},{"id":"CG","orig":"Republic of Congo","text":"Republic of Congo"},{"id":"RS","orig":"Republic of Serbia","text":"Republic of Serbia"},{"id":"RO","orig":"Romania","text":"Romania"},{"id":"RU","orig":"Russia","text":"Russia"},{"id":"RW","orig":"Rwanda","text":"Rwanda"},{"id":"KN","orig":"Saint Kitts and Nevis","text":"Saint Kitts and Nevis"},{"id":"LC","orig":"Saint Lucia","text":"Saint Lucia"},{"id":"VC","orig":"Saint Vincent and the Grenadines","text":"Saint Vincent and the Grenadines"},{"id":"WS","orig":"Samoa","text":"Samoa"},{"id":"SM","orig":"San Marino","text":"San Marino"},{"id":"ST","orig":"Sao Tome and Principe","text":"Sao Tome and Principe"},{"id":"SA","orig":"Saudi Arabia","text":"Saudi Arabia"},{"id":"SH","orig":"Scarborough Reef","text":"Scarborough Reef"},{"id":"SN","orig":"Senegal","text":"Senegal"},{"id":"SW","orig":"Serranilla Bank","text":"Serranilla Bank"},{"id":"SC","orig":"Seychelles","text":"Seychelles"},{"id":"JK","orig":"Siachen Glacier","text":"Siachen Glacier"},{"id":"SL","orig":"Sierra Leone","text":"Sierra Leone"},{"id":"SG","orig":"Singapore","text":"Singapore"},{"id":"SK","orig":"Slovakia","text":"Slovakia"},{"id":"SI","orig":"Slovenia","text":"Slovenia"},{"id":"SB","orig":"Solomon Islands","text":"Solomon Islands"},{"id":"SO","orig":"Somalia","text":"Somalia"},{"id":"SX","orig":"Somaliland","text":"Somaliland"},{"id":"ZA","orig":"South Africa","text":"South Africa"},{"id":"KR","orig":"South Korea","text":"South Korea"},{"id":"SS","orig":"South Sudan","text":"South Sudan"},{"id":"ES","orig":"Spain","text":"Spain"},{"id":"SP","orig":"Spratly Islands","text":"Spratly Islands"},{"id":"LK","orig":"Sri Lanka","text":"Sri Lanka"},{"id":"SD","orig":"Sudan","text":"Sudan"},{"id":"SR","orig":"Suriname","text":"Suriname"},{"id":"SZ","orig":"Swaziland","text":"Swaziland"},{"id":"SE","orig":"Sweden","text":"Sweden"},{"id":"CH","orig":"Switzerland","text":"Switzerland"},{"id":"SY","orig":"Syria","text":"Syria"},{"id":"TW","orig":"Taiwan","text":"Taiwan"},{"id":"TJ","orig":"Tajikistan","text":"Tajikistan"},{"id":"TH","orig":"Thailand","text":"Thailand"},{"id":"BS","orig":"The Bahamas","text":"The Bahamas"},{"id":"TG","orig":"Togo","text":"Togo"},{"id":"TO","orig":"Tonga","text":"Tonga"},{"id":"TT","orig":"Trinidad and Tobago","text":"Trinidad and Tobago"},{"id":"TN","orig":"Tunisia","text":"Tunisia"},{"id":"TR","orig":"Turkey","text":"Turkey"},{"id":"TM","orig":"Turkmenistan","text":"Turkmenistan"},{"id":"TV","orig":"Tuvalu","text":"Tuvalu"},{"id":"UG","orig":"Uganda","text":"Uganda"},{"id":"UA","orig":"Ukraine","text":"Ukraine"},{"id":"AE","orig":"United Arab Emirates","text":"United Arab Emirates"},{"id":"GB","orig":"United Kingdom","text":"United Kingdom"},{"id":"TZ","orig":"United Republic of Tanzania","text":"United Republic of Tanzania"},{"id":"UM","orig":"United States Minor Outlying Islands","text":"United States Minor Outlying Islands"},{"id":"VI","orig":"United States Virgin Islands","text":"United States Virgin Islands"},{"id":"US","orig":"United States of America","text":"United States of America"},{"id":"UY","orig":"Uruguay","text":"Uruguay"},{"id":"UZ","orig":"Uzbekistan","text":"Uzbekistan"},{"id":"VU","orig":"Vanuatu","text":"Vanuatu"},{"id":"VA","orig":"Vatican","text":"Vatican"},{"id":"VE","orig":"Venezuela","text":"Venezuela"},{"id":"VN","orig":"Vietnam","text":"Vietnam"},{"id":"WE","orig":"West Bank","text":"West Bank"},{"id":"EH","orig":"Western Sahara","text":"Western Sahara"},{"id":"YE","orig":"Yemen","text":"Yemen"},{"id":"ZM","orig":"Zambia","text":"Zambia"},{"id":"ZW","orig":"Zimbabwe","text":"Zimbabwe"}];
  $("#country-search101").select2({
      placeholder: "Select a country",
      data: p1010data
  });

  var report_data = null;
  $.getJSON('data/product101.json',  function(data){

    var dummyCount = 57-data.length;

    for (var i = 0; i < dummyCount; i++) {
      var minYear = Math.floor(Math.random() * (2000 - 1970 + 1)) + 1970;
      var maxYear = Math.floor(Math.random() * (2014 - 2000 + 1)) + 2000;
      var pages = Math.floor(Math.random() * (1000 - 100 + 1)) + 100;
      data.push({
        "id":i+6,
        "title":"Report Title",
        "issue_type":"Issue Type",
        "issue_abbr":"IT",
        "year":maxYear,
        "coverage_year":minYear+"-"+maxYear,
        "source":"U.S. Department of State",
        "pages":pages, 
        "executive_summary":"Suspendisse aliquam eleifend magna eu euismod. Vivamus lacus mi, consectetur non facilisis a, tristique dictum dolor. Aenean mattis luctus sollicitudin. Proin et ex mollis, dapibus magna in, iaculis massa. Vivamus fermentum eleifend leo sed pulvinar. Aliquam dictum ex sit amet nibh feugiat, eget congue ipsum ultricies. Ut in felis id tellus suscipit tempor. Proin non leo vestibulum, convallis turpis a, consectetur magna. Donec eu risus vehicula, posuere ipsum et, accumsan enim."
      })
    };
    report_data = data;

    var tileHtml = card101isotmpl.render(data);
    $('.grid').html(tileHtml);

    var $container = $('.grid');
    var $grid = $container.isotope({
      itemSelector : '.grid-item',
      getSortData: {
        year: '.year',
        name: '.name'
      },
      sortBy : 'year',
      sortAscending: {
        year: false
      },
    });

    var currentYear = new Date().getFullYear(), years = [];
    var startYear = 1970;
    var yearOpts = "";
    while ( startYear <= currentYear ) {
        var year = startYear++;
        yearOpts += "<option value="+year+">"+year+"</option>";
    } 
    $('#gtDate').html(yearOpts);
    $('#ltDate').html(yearOpts);
    $('#ltDate').val('2018');


    $(document).on('click', '.grid-item', function(e){ 
      var actionid = $(this).attr('data-id');

      $('.grid-item').removeClass('active');

        var cardHtml = card101tmpl.render(getItemById(actionid));
        $("#report-card").html(cardHtml);


        $('#content > .col-2').addClass('show');
        $('.prod-104').addClass('show');
        $('.ea-1').show();
        $(this).addClass('active');
      e.stopPropagation(); 

      
    })

    $(document).on('click', '.close-inspector', function() {
      $('#content > .col-2').removeClass('show');
      $('.prod-104').removeClass('show');
      $('.grid-item').removeClass('active');

        $('.ea-1').hide();
    });



  });
	
	
Highcharts.chart('product101-gml', {

      chart: {
		  backgroundColor: '#222222',
		  borderColor:'#335cad',
		  plotBorderColor:'#335cad',
          height:350,
          margin:75,
		  marginBottom:70,
		  marginTop:100,
          plotBorderWidth: null,
          plotShadow: false,        
          type: 'arearange',
          style: {
              fontFamily: 'monospace',
              color: "#fff"
          },
      },
	  plotOptions: {

		  line: {	  
			  color:'#555',
			  borderColor: '#555'
		  },
		  area : {
			  color:'#ffffff',
			  fillColor: '#ffffff',
		  },
		  series: {
		  	borderColor:'#ffffff',
			color:'#D65527',			  
			  
		  },
		  colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],

	  },

      title: {
          text: 'Global Money Laundering in USD Billions (2010-2017)',
		  y:40,
		  style: {color: '#ffffff'
			 }
      },
      credits: false,
      yAxis: {
		  gridLineColor:'#555',
		  lineColor:'#555',
		  minorGridLineColor:'#555',
		  tickColor:'#555',

          title: {
              text: null,
			  color:'#fff'
          }
      },

      legend: {
          enabled: false
      },
      tooltip: {
          enabled: false
      },

      series: [{
          name: '',
          data: [
            [2010,1319,1979],
            [2011,1466,2199],
            [2012,1499,2249],
            [2013,1541,2312],
            [2014,1583,2374],
            [2015,1497,2245],
            [2016,1519,2278],
            [2017,1614,2421],
          ]
      }]
  });
Highcharts.chart('product101-iff', {
	
      chart: {
		  type: 'column',
		  backgroundColor: '#222222',
		  borderColor:'#335cad',
		  plotBorderColor:'#335cad',
          height:400,
          margin:75,
		  marginBottom:70,
		  marginTop:100,
          plotBorderWidth: null,
          plotShadow: false,        
          style: {
              fontFamily: 'monospace',
              color: "#fff"
          },
      },
      title: {
        text: 'Global Illicit Financial Flows (2005-2014)',
		  y:40,
		  style: {color: '#ffffff'
			 }
      },
      credits: false,
      yAxis: {
		  gridLineColor:'#555',
		  lineColor:'#555',
		  minorGridLineColor:'#555',
		  tickColor:'#555',

          title: {
              text: null,
			  color:'#fff'
          }
      },	
	  plotOptions: {

		  line: {	  
			  color:'#555',
			  borderColor: '#555'
		  },
		  area : {
			  color:'#ffffff',
			  fillColor: '#ffffff',
		  },
		  series: {
		  	borderColor:'#555',
			color:'#0be881',	
			negativeColor:'#f53b57'

			  
		  },
		  colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],

		  
	  column: {
            stacking: 'normal',
            dataLabels: {
                enabled: false
            }
        },

		  
	  },



    xAxis: {
        categories: [2005,2006,2007,2008,2009,2010,2011,2012,2013,2014]
    },

    credits: false,
    legend: {
        enabled: false
    },
    tooltip: {
        enabled: false
    },
 

    series: [{
		borderWidth:0,
		colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
        name: 'Global IFF Inflows (in USD millions)',
        data: [996696,1261591,1477066,1829461,1461260,1919955,2364900,2386520,2613990,2553021]
    },{
		colors: ['#0fbcf9', '#D65527',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
		borderWidth:0,
        name: 'Global IFF Outflows (in USD millions)',
        data: [-480703,-550363,-675617,-837435,-767959,-845554,-908460,-956841,-975083,-969733]
    }]
});
Highcharts.chart('product101-ht', {
	
      chart: {
		  zoomType: 'xy',
		  backgroundColor: '#222222',
		  borderColor:'#335cad',
		  plotBorderColor:'#335cad',
          height:400,
          margin:75,
		  marginBottom:70,
		  marginTop:100,
          plotBorderWidth: null,
          plotShadow: false,        
          style: {
              fontFamily: 'monospace',
              color: "#fff"
          },
      },
      title: {
			  text: 'Trafficking in Persons Victims Identified',
		  y:40,
		  style: {color: '#ffffff'
			 }
      },
      credits: false,	
	  plotOptions: {

		  line: {	  
			  color:'#555',
			  borderColor: '#555'
		  },
		  area : {
			  color:'#ffffff',
			  fillColor: '#ffffff',
		  },
		  series: {
		  	borderColor:'#555',
			color:'#0fbcf9',
			  
			negativeColor:'#D65527'

			  
		  },
		  colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],

		  
	  column: {
            stacking: 'normal',
            dataLabels: {
                enabled: false
            }
        },

		  
	  },


    xAxis: [{
        categories: [2010,2011,2012,2013,2014,2015,2016],
        crosshair: false
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            //format: '{value}°C',
            style: {
                color: '#555'
            }
        },
        title: {
            text: '',
            style: {
                color: '#fff'
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Victims',
            style: {
                color: '#fff'
            }
        },
        labels: {
            style: {
                color: '#fff'
            }
        },
        opposite: true
    }],
    tooltip: {
        enabled: false
    },
    credits: false,
    legend: {
        enabled: false
    },
    series: [{
        name: 'Victims',
        type: 'column',
        yAxis: 1,
		color:'#0fbcf9',
        data: [33113,42291,46570,44758,44462,77823,66520],
    }, {
        name: 'Trendline',
        type: 'line',
        marker: { enabled: false },
        data: [[0,30000],[6,70000]],
    }]
});	
Highcharts.chart('product101-dt', {
	
      chart: {
		  backgroundColor: '#222222',
		  borderColor:'#335cad',
		  plotBorderColor:'#335cad',
          height:350,
          margin:75,
		  marginBottom:70,
		  marginTop:100,
          plotBorderWidth: null,
          plotShadow: false,        
        type: 'area',
          style: {
              fontFamily: 'monospace',
              color: "#fff"
          },
      },
	  plotOptions: {

		  line: {	  
			  color:'#555',
			  borderColor: '#555'
		  },
		  area : {
			  color:'#ffffff',
			  fillColor: '#D65527',
		  },
		  series: {
		  	borderColor:'#ffffff',
			color:'#D65527',			  
			  
		  },
		  colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],

	  },

      title: {
        text: 'The Afghan Opiate Trade and Africa - A Baseline Assessment 2016',
		  y:40,
		  style: {color: '#ffffff'
			 }
      },
      yAxis: {
		  gridLineColor:'#555',
		  lineColor:'#555',
		  minorGridLineColor:'#555',
		  tickColor:'#555',

          title: {
              text: null,
			  color:'#fff'
          }
      },

	
	

    credits: false,
    legend: {
        enabled: false
    },
    tooltip: {
        enabled: false
    },
    xAxis: [{
        categories: [2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015],
        crosshair: false
    }],
    series: [{
        name: 'Hectares',
		color: '#ffdd59',
        data: [131000,104000,165000,193000,157000,123000,123000,131000,154000,209000,224000,183000]
    }]
});
Highcharts.chart('product101-tc', {
    chart: {
		  backgroundColor: '#222222',
		  borderColor:'#335cad',
		  plotBorderColor:'#335cad',
          height:350,
          margin:75,
		  marginBottom:70,
		  marginLeft:150,
		  marginTop:100,
          plotBorderWidth: null,
          plotShadow: false,        
          type: 'bar',
          style: {
              fontFamily: 'monospace',
              color: "#fff"
	  },
   },
	
	
      title: {
		  text: 'Least and Most Corrupt Countries (2017)',
		  y:40,
		  style: {color: '#ffffff'
			 }
      },
      credits: false,
      yAxis: {

      },
	
	
	
	
	

    xAxis: {
			labels: {
			style: {
				width: '200px'
			},
		},
        categories: ['New Zealand','Denmark','Finland','Norway','Switzerland','Yemen','Afghanistan','Syria','South Sudan','Somalia'],
        title: {
            text: null
        },
				  gridLineColor:'#555',
		  lineColor:'#555',
		  minorGridLineColor:'#555',
		  tickColor:'#555',

          title: {
              text: null,
			  color:'#fff'
          }
    },
    yAxis:{
      title: ''
    },
    tooltip: {
        enabled: false,
    },
    plotOptions: {
        bar: {
			colorByPoint: true,
            dataLabels: {
                enabled: true
            },
			colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],

        },
				  line: {	  
			  color:'#555',
			  borderColor: '#555'
		  },
		  area : {
			  color:'#ffffff',
			  fillColor: '#ffffff',
		  },


		
		
		
		
    },    credits: false,
    legend: {
        enabled: false
    },
    series: [{
		bar: {
								shadow:false,

			datalabels: {
					color:'#fff',
												shadow:false,

			}
		},
      name:'Score',
	borderWidth:0,

      data: [89,88,85,85,85,16,15,14,12,9]
    }]
});
Highcharts.chart('product101-fs', {
    chart: {
		  backgroundColor: '#222222',
		  borderColor:'#335cad',
		  plotBorderColor:'#335cad',
          height:350,
          margin:75,
		  marginBottom:70,
		marginLeft:150,
		  marginTop:100,
          plotBorderWidth: null,
          plotShadow: false,        
          type: 'bar',
          style: {
              fontFamily: 'monospace',
              color: "#fff"
	  },
   },

	
      title: {
        text: 'Least and Most Secret Countries (2018)',
		  y:40,
		  style: {color: '#ffffff'
			 }
      },
      credits: false,
 
	
	
	
	

    xAxis: {
		labels: {
			style: {
				width: '200px'
			},
		},
		useHTML: true,

        categories: ['Vanuatu','Antigua and Barbuda','Bahamas','Paraguay','Brunei','Lithuania','Sweden','Belgium','United Kingdom','Slovenia'],
        title: {
            text: null
        },
				  gridLineColor:'#555',
		  lineColor:'#555',
		  minorGridLineColor:'#555',
		  tickColor:'#555',

          title: {
              text: null,
			  color:'#fff'
          }
    },
    yAxis:{
      title: ''
    },
    tooltip: {
        enabled: false,
    },
    plotOptions: {
        bar: {
			colorByPoint: true,
            dataLabels: {
                enabled: true
            },
			colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],

        },
				  line: {	  
			  color:'#555',
			  borderColor: '#555'
		  },
		  area : {
			  color:'#ffffff',
			  fillColor: '#ffffff',
		  },


		
		
		
		
    },


	
    credits: false,
    legend: {
        enabled: false
    },
    series: [{
		borderWidth:0,

		bar: {
			shadow:false,
  			colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
			datalabels: {
			color:'#fff',
			shadow:false,

			}
		},
		

      name:'Score',
      data: [0.89,0.87,0.85,0.84,0.84,0.47,0.45,0.44,0.42,0.42]
    }]
});

}

if(window.location.href.split('/').pop() == "product-102.php"){
    /*
    var p102Data = [];
    var geop102Data = Highcharts.geojson(Highcharts.maps['custom/world-palestine']);
    for (var i = 0; i < geop102Data.length; i++) {
      p102Data.push({
        id: geop102Data[i].properties["iso-a2"],
        orig: geop102Data[i].properties["name"],
        text: geop102Data[i].properties["name"],
      });
    };
    console_data = sortByKey(p102Data, "orig");
    */

    var p102Data =[{"id":"","orig":"","text":""},{"id":"AF","orig":"Afghanistan","text":"Afghanistan (10)"},{"id":"AL","orig":"Albania","text":"Albania (26)"},{"id":"DZ","orig":"Algeria","text":"Algeria (23)"},{"id":"AS","orig":"American Samoa","text":"American Samoa (29)"},{"id":"AD","orig":"Andorra","text":"Andorra (1)"},{"id":"AO","orig":"Angola","text":"Angola (19)"},{"id":"AG","orig":"Antigua and Barbuda","text":"Antigua and Barbuda (23)"},{"id":"AR","orig":"Argentina","text":"Argentina (13)"},{"id":"AM","orig":"Armenia","text":"Armenia (17)"},{"id":"AU","orig":"Australia","text":"Australia (12)"},{"id":"AT","orig":"Austria","text":"Austria (13)"},{"id":"AZ","orig":"Azerbaijan","text":"Azerbaijan (7)"},{"id":"BH","orig":"Bahrain","text":"Bahrain (29)"},{"id":"BU","orig":"Bajo Nuevo Bank (Petrel Is.)","text":"Bajo Nuevo Bank (Petrel Is.) (15)"},{"id":"BD","orig":"Bangladesh","text":"Bangladesh (17)"},{"id":"BB","orig":"Barbados","text":"Barbados (24)"},{"id":"BY","orig":"Belarus","text":"Belarus (8)"},{"id":"BE","orig":"Belgium","text":"Belgium (26)"},{"id":"BZ","orig":"Belize","text":"Belize (14)"},{"id":"BJ","orig":"Benin","text":"Benin (17)"},{"id":"BT","orig":"Bhutan","text":"Bhutan (20)"},{"id":"BO","orig":"Bolivia","text":"Bolivia (5)"},{"id":"BA","orig":"Bosnia and Herzegovina","text":"Bosnia and Herzegovina (20)"},{"id":"BW","orig":"Botswana","text":"Botswana (20)"},{"id":"BR","orig":"Brazil","text":"Brazil (7)"},{"id":"BN","orig":"Brunei","text":"Brunei (28)"},{"id":"BG","orig":"Bulgaria","text":"Bulgaria (6)"},{"id":"BF","orig":"Burkina Faso","text":"Burkina Faso (9)"},{"id":"BI","orig":"Burundi","text":"Burundi (26)"},{"id":"KH","orig":"Cambodia","text":"Cambodia (15)"},{"id":"CM","orig":"Cameroon","text":"Cameroon (3)"},{"id":"CA","orig":"Canada","text":"Canada (13)"},{"id":"CV","orig":"Cape Verde","text":"Cape Verde (17)"},{"id":"CF","orig":"Central African Republic","text":"Central African Republic (27)"},{"id":"TD","orig":"Chad","text":"Chad (4)"},{"id":"CL","orig":"Chile","text":"Chile (27)"},{"id":"CN","orig":"China","text":"China (5)"},{"id":"CO","orig":"Colombia","text":"Colombia (13)"},{"id":"KM","orig":"Comoros","text":"Comoros (7)"},{"id":"CR","orig":"Costa Rica","text":"Costa Rica (27)"},{"id":"HR","orig":"Croatia","text":"Croatia (15)"},{"id":"CU","orig":"Cuba","text":"Cuba (25)"},{"id":"CY","orig":"Cyprus","text":"Cyprus (15)"},{"id":null,"orig":"Cyprus No Mans Area","text":"Cyprus No Mans Area (28)"},{"id":"CZ","orig":"Czech Republic","text":"Czech Republic (29)"},{"id":"CD","orig":"Democratic Republic of the Congo","text":"Democratic Republic of the Congo (18)"},{"id":"DK","orig":"Denmark","text":"Denmark (9)"},{"id":"DJ","orig":"Djibouti","text":"Djibouti (21)"},{"id":"DM","orig":"Dominica","text":"Dominica (4)"},{"id":"DO","orig":"Dominican Republic","text":"Dominican Republic (11)"},{"id":"TL","orig":"East Timor","text":"East Timor (22)"},{"id":"EC","orig":"Ecuador","text":"Ecuador (3)"},{"id":"EG","orig":"Egypt","text":"Egypt (13)"},{"id":"SV","orig":"El Salvador","text":"El Salvador (21)"},{"id":"GQ","orig":"Equatorial Guinea","text":"Equatorial Guinea (9)"},{"id":"ER","orig":"Eritrea","text":"Eritrea (21)"},{"id":"EE","orig":"Estonia","text":"Estonia (24)"},{"id":"ET","orig":"Ethiopia","text":"Ethiopia (16)"},{"id":"FO","orig":"Faroe Islands","text":"Faroe Islands (0)"},{"id":"FM","orig":"Federated States of Micronesia","text":"Federated States of Micronesia (15)"},{"id":"FJ","orig":"Fiji","text":"Fiji (22)"},{"id":"FI","orig":"Finland","text":"Finland (27)"},{"id":"FR","orig":"France","text":"France (17)"},{"id":"GA","orig":"Gabon","text":"Gabon (4)"},{"id":"GM","orig":"Gambia","text":"Gambia (28)"},{"id":"GZ","orig":"Gaza Strip","text":"Gaza Strip (4)"},{"id":"GE","orig":"Georgia","text":"Georgia (18)"},{"id":"DE","orig":"Germany","text":"Germany (11)"},{"id":"GH","orig":"Ghana","text":"Ghana (26)"},{"id":"GR","orig":"Greece","text":"Greece (9)"},{"id":"GL","orig":"Greenland","text":"Greenland (13)"},{"id":"GD","orig":"Grenada","text":"Grenada (21)"},{"id":"GU","orig":"Guam","text":"Guam (28)"},{"id":"GT","orig":"Guatemala","text":"Guatemala (12)"},{"id":"GN","orig":"Guinea","text":"Guinea (10)"},{"id":"GW","orig":"Guinea Bissau","text":"Guinea Bissau (2)"},{"id":"GY","orig":"Guyana","text":"Guyana (25)"},{"id":"HT","orig":"Haiti","text":"Haiti (1)"},{"id":"HN","orig":"Honduras","text":"Honduras (6)"},{"id":"HU","orig":"Hungary","text":"Hungary (14)"},{"id":"IS","orig":"Iceland","text":"Iceland (29)"},{"id":"IN","orig":"India","text":"India (21)"},{"id":"ID","orig":"Indonesia","text":"Indonesia (4)"},{"id":"IR","orig":"Iran","text":"Iran (3)"},{"id":"IQ","orig":"Iraq","text":"Iraq (25)"},{"id":"IE","orig":"Ireland","text":"Ireland (15)"},{"id":"IL","orig":"Israel","text":"Israel (25)"},{"id":"IT","orig":"Italy","text":"Italy (18)"},{"id":"CI","orig":"Ivory Coast","text":"Ivory Coast (0)"},{"id":"JM","orig":"Jamaica","text":"Jamaica (15)"},{"id":"JP","orig":"Japan","text":"Japan (25)"},{"id":"JO","orig":"Jordan","text":"Jordan (18)"},{"id":"KZ","orig":"Kazakhstan","text":"Kazakhstan (10)"},{"id":"KE","orig":"Kenya","text":"Kenya (25)"},{"id":"KI","orig":"Kiribati","text":"Kiribati (28)"},{"id":"KV","orig":"Kosovo","text":"Kosovo (23)"},{"id":"KW","orig":"Kuwait","text":"Kuwait (9)"},{"id":"KG","orig":"Kyrgyzstan","text":"Kyrgyzstan (15)"},{"id":"LA","orig":"Laos","text":"Laos (20)"},{"id":"LV","orig":"Latvia","text":"Latvia (7)"},{"id":"LB","orig":"Lebanon","text":"Lebanon (19)"},{"id":"LS","orig":"Lesotho","text":"Lesotho (14)"},{"id":"LR","orig":"Liberia","text":"Liberia (24)"},{"id":"LY","orig":"Libya","text":"Libya (12)"},{"id":"LI","orig":"Liechtenstein","text":"Liechtenstein (24)"},{"id":"LT","orig":"Lithuania","text":"Lithuania (14)"},{"id":"LU","orig":"Luxembourg","text":"Luxembourg (8)"},{"id":"MK","orig":"Macedonia","text":"Macedonia (13)"},{"id":"MG","orig":"Madagascar","text":"Madagascar (14)"},{"id":"MW","orig":"Malawi","text":"Malawi (4)"},{"id":"MY","orig":"Malaysia","text":"Malaysia (23)"},{"id":"MV","orig":"Maldives","text":"Maldives (6)"},{"id":"ML","orig":"Mali","text":"Mali (7)"},{"id":"MT","orig":"Malta","text":"Malta (14)"},{"id":"MH","orig":"Marshall Islands","text":"Marshall Islands (17)"},{"id":"MR","orig":"Mauritania","text":"Mauritania (11)"},{"id":"MU","orig":"Mauritius","text":"Mauritius (13)"},{"id":"MX","orig":"Mexico","text":"Mexico (23)"},{"id":"MD","orig":"Moldova","text":"Moldova (1)"},{"id":"MC","orig":"Monaco","text":"Monaco (10)"},{"id":"MN","orig":"Mongolia","text":"Mongolia (21)"},{"id":"ME","orig":"Montenegro","text":"Montenegro (4)"},{"id":"MA","orig":"Morocco","text":"Morocco (16)"},{"id":"MZ","orig":"Mozambique","text":"Mozambique (18)"},{"id":"MM","orig":"Myanmar","text":"Myanmar (16)"},{"id":"NA","orig":"Namibia","text":"Namibia (29)"},{"id":"NR","orig":"Nauru","text":"Nauru (0)"},{"id":"NP","orig":"Nepal","text":"Nepal (6)"},{"id":"NL","orig":"Netherlands","text":"Netherlands (19)"},{"id":"NZ","orig":"New Zealand","text":"New Zealand (22)"},{"id":"NI","orig":"Nicaragua","text":"Nicaragua (27)"},{"id":"NE","orig":"Niger","text":"Niger (4)"},{"id":"NG","orig":"Nigeria","text":"Nigeria (26)"},{"id":"KP","orig":"North Korea","text":"North Korea (10)"},{"id":"NC","orig":"Northern Cyprus","text":"Northern Cyprus (21)"},{"id":"MP","orig":"Northern Mariana Islands","text":"Northern Mariana Islands (22)"},{"id":"NO","orig":"Norway","text":"Norway (9)"},{"id":"OM","orig":"Oman","text":"Oman (18)"},{"id":"PK","orig":"Pakistan","text":"Pakistan (29)"},{"id":"PW","orig":"Palau","text":"Palau (27)"},{"id":"PA","orig":"Panama","text":"Panama (6)"},{"id":"PG","orig":"Papua New Guinea","text":"Papua New Guinea (12)"},{"id":"PY","orig":"Paraguay","text":"Paraguay (17)"},{"id":"PE","orig":"Peru","text":"Peru (20)"},{"id":"PH","orig":"Philippines","text":"Philippines (19)"},{"id":"PL","orig":"Poland","text":"Poland (14)"},{"id":"PT","orig":"Portugal","text":"Portugal (28)"},{"id":"PR","orig":"Puerto Rico","text":"Puerto Rico (16)"},{"id":"QA","orig":"Qatar","text":"Qatar (5)"},{"id":"CG","orig":"Republic of Congo","text":"Republic of Congo (15)"},{"id":"RS","orig":"Republic of Serbia","text":"Republic of Serbia (12)"},{"id":"RO","orig":"Romania","text":"Romania (9)"},{"id":"RU","orig":"Russia","text":"Russia (0)"},{"id":"RW","orig":"Rwanda","text":"Rwanda (8)"},{"id":"KN","orig":"Saint Kitts and Nevis","text":"Saint Kitts and Nevis (18)"},{"id":"LC","orig":"Saint Lucia","text":"Saint Lucia (1)"},{"id":"VC","orig":"Saint Vincent and the Grenadines","text":"Saint Vincent and the Grenadines (21)"},{"id":"WS","orig":"Samoa","text":"Samoa (0)"},{"id":"SM","orig":"San Marino","text":"San Marino (13)"},{"id":"ST","orig":"Sao Tome and Principe","text":"Sao Tome and Principe (18)"},{"id":"SA","orig":"Saudi Arabia","text":"Saudi Arabia (13)"},{"id":"SH","orig":"Scarborough Reef","text":"Scarborough Reef (4)"},{"id":"SN","orig":"Senegal","text":"Senegal (26)"},{"id":"SW","orig":"Serranilla Bank","text":"Serranilla Bank (29)"},{"id":"SC","orig":"Seychelles","text":"Seychelles (26)"},{"id":"JK","orig":"Siachen Glacier","text":"Siachen Glacier (18)"},{"id":"SL","orig":"Sierra Leone","text":"Sierra Leone (5)"},{"id":"SG","orig":"Singapore","text":"Singapore (8)"},{"id":"SK","orig":"Slovakia","text":"Slovakia (6)"},{"id":"SI","orig":"Slovenia","text":"Slovenia (16)"},{"id":"SB","orig":"Solomon Islands","text":"Solomon Islands (17)"},{"id":"SO","orig":"Somalia","text":"Somalia (24)"},{"id":"SX","orig":"Somaliland","text":"Somaliland (27)"},{"id":"ZA","orig":"South Africa","text":"South Africa (1)"},{"id":"KR","orig":"South Korea","text":"South Korea (12)"},{"id":"SS","orig":"South Sudan","text":"South Sudan (9)"},{"id":"ES","orig":"Spain","text":"Spain (3)"},{"id":"SP","orig":"Spratly Islands","text":"Spratly Islands (5)"},{"id":"LK","orig":"Sri Lanka","text":"Sri Lanka (5)"},{"id":"SD","orig":"Sudan","text":"Sudan (9)"},{"id":"SR","orig":"Suriname","text":"Suriname (29)"},{"id":"SZ","orig":"Swaziland","text":"Swaziland (4)"},{"id":"SE","orig":"Sweden","text":"Sweden (14)"},{"id":"CH","orig":"Switzerland","text":"Switzerland (28)"},{"id":"SY","orig":"Syria","text":"Syria (4)"},{"id":"TW","orig":"Taiwan","text":"Taiwan (24)"},{"id":"TJ","orig":"Tajikistan","text":"Tajikistan (26)"},{"id":"TH","orig":"Thailand","text":"Thailand (13)"},{"id":"BS","orig":"The Bahamas","text":"The Bahamas (12)"},{"id":"TG","orig":"Togo","text":"Togo (19)"},{"id":"TO","orig":"Tonga","text":"Tonga (5)"},{"id":"TT","orig":"Trinidad and Tobago","text":"Trinidad and Tobago (8)"},{"id":"TN","orig":"Tunisia","text":"Tunisia (9)"},{"id":"TR","orig":"Turkey","text":"Turkey (8)"},{"id":"TM","orig":"Turkmenistan","text":"Turkmenistan (8)"},{"id":"TV","orig":"Tuvalu","text":"Tuvalu (0)"},{"id":"UG","orig":"Uganda","text":"Uganda (23)"},{"id":"UA","orig":"Ukraine","text":"Ukraine (12)"},{"id":"AE","orig":"United Arab Emirates","text":"United Arab Emirates (28)"},{"id":"GB","orig":"United Kingdom","text":"United Kingdom (24)"},{"id":"TZ","orig":"United Republic of Tanzania","text":"United Republic of Tanzania (6)"},{"id":"UM","orig":"United States Minor Outlying Islands","text":"United States Minor Outlying Islands (24)"},{"id":"VI","orig":"United States Virgin Islands","text":"United States Virgin Islands (7)"},{"id":"US","orig":"United States of America","text":"United States of America (6)"},{"id":"UY","orig":"Uruguay","text":"Uruguay (6)"},{"id":"UZ","orig":"Uzbekistan","text":"Uzbekistan (13)"},{"id":"VU","orig":"Vanuatu","text":"Vanuatu (1)"},{"id":"VA","orig":"Vatican","text":"Vatican (23)"},{"id":"VE","orig":"Venezuela","text":"Venezuela (4)"},{"id":"VN","orig":"Vietnam","text":"Vietnam (13)"},{"id":"WE","orig":"West Bank","text":"West Bank (4)"},{"id":"EH","orig":"Western Sahara","text":"Western Sahara (25)"},{"id":"YE","orig":"Yemen","text":"Yemen (28)"},{"id":"ZM","orig":"Zambia","text":"Zambia (24)"},{"id":"ZW","orig":"Zimbabwe","text":"Zimbabwe (3)"}];

    $("#country-search102").select2({
        placeholder: "Select a country",
        data: p102Data
    }).on('select2:select', function (e) {
        var data = e.params.data;
        for(var i = 0; i <  Highcharts.charts[0].series[0].points.length; i++){

           if(Highcharts.charts[0].series[0].points[i].properties["iso-a2"] == data.id){

              Highcharts.charts[0].series[0].points[i].zoomTo();

              $('#content').addClass('show');

              var cData = {
                country: data.orig,
                region: Highcharts.charts[0].series[0].points[i].properties["region-wb"],
                capital: 'Capital',
                aml_laws: "YES",
                tf_laws: "YES",
                aml_docs: [],
                tf_docs: [],
                pa_docs: [{name:"SAR (3)"},{name:"CTR (4)"},{name:"KYC (5)"},{name:"WLM (6)"}],
                fi_docs: [{name:"Banks (7)"},{name:"Jewelers (8)"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Gambling and Casinos"},{name:"Law Firms"},{name:"Accountants"},{name:"Money Services Businesses"},{name:"Mutual Funds"},{name:"Futures Commission Merchants"},{name:"Brokers in Commodities"},{name:"Credit Card"},{name:"Loan and Finance Companies"},{name:"Housing Government Sponsored Enterprises"},],
                ia_docs: [{name:"Human Trafficking"},{name:"Drug Trafficking"},{name:"Corruption"},{name:"Arms Trafficking"},{name:"Smuggling"}],
                ofac_docs: [],
                sm_docs: [],
              };

              if(data.orig == "Egypt"){ //Middle East and North Africa Financial Action Task Force (MENAFATF)
                cData.capital = "Cairo";
                cData.pa_docs= [{name:"SAR"},{name:"KYC"}];
                cData.fi_docs= [{name:"Banks"},{name:"Jewelers"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Gambling and Casinos"},{name:"Law Firms"},{name:"Accountants"},{name:"Money Services Businesses"},];
                cData.ia_docs= [{name:"Drug Trafficking"},{name:"Corruption"},{name:"Arms Trafficking"}];
              }
              if(data.orig == "Ghana"){ //Inter Governmental Action Group against Money Laundering in West Africa (GIABA)
                cData.capital = "Accra";
                cData.pa_docs= [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                cData.fi_docs= [];
                cData.ia_docs= [{name:"Human Trafficking"},{name:"Drug Trafficking"},{name:"Corruption"},{name:"Arms Trafficking"},{name:"Smuggling"}];
              }
              if(data.orig == "Albania"){ //Committee of Experts on the Evaluation of Anti-Money Laundering Measures (MONEYVAL)
                cData.capital = "Tirana";
                cData.pa_docs= [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                cData.fi_docs= [{name:"Banks"},{name:"Jewelers"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Gambling and Casinos"},{name:"Accountants"},{name:"Loan and Finance Companies"},{name:"Credit Card"}];
                cData.ia_docs= [{name:"Corruption"}];
              }
              if(data.orig == "Australia"){ //Asia/Pacific Group on Money Laundering (APG)     Financial Action Task Force
                cData.capital = "Canberra";
                cData.pa_docs= [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                cData.fi_docs= [{name:"Banks"},{name:"Gambling and Casinos"},{name:"Loan and Finance Companies"},{name:"Brokers and Dealers in Securities"},{name:"Jewelers"},{name:"Insurance"}];
                cData.ia_docs= [{name:"Drug Trafficking"},{name:"Corruption"},{name:"Smuggling"}];
              }
              if(data.orig == "Maldives"){ //Asia/Pacific Group on Money Laundering (APG)
                cData.capital = "Male";
                cData.pa_docs= [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                cData.fi_docs= [{name:"Banks"},{name:"Jewelers"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Law Firms"},{name:"Accountants"},{name:"Money Services Businesses"},{name:"Mutual Funds"},{name:"Brokers in Commodities"},{name:"Credit Card"},{name:"Loan and Finance Companies"}];
              }
              if(data.orig == "United States of America"){ //Asia/Pacific Group on Money Laundering (APG)      Financial Action Task Force
                cData.capital = "Washington, D.C.";
                cData.pa_docs = [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                cData.fi_docs = [{name:"Banks"},{name:"Jewelers"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Gambling and Casinos"},{name:"Law Firms"},{name:"Accountants"},{name:"Money Services Businesses"},{name:"Mutual Funds"},{name:"Futures Commission Merchants"},{name:"Brokers in Commodities"},{name:"Credit Card"},{name:"Loan and Finance Companies"},{name:"Housing Government Sponsored Enterprises"},];
              }


              $('.data-selection').hide();
              var cardHtml = card102tmpl.render(cData);
              $("#country-card").html(cardHtml);
			   
              $('#select-a-card').hide();
			   
              $('.data-selection').fadeIn();

           }
        }
    });






var data = [{"hc-key":"gl","value":63},{"hc-key":"sh","value":96},{"hc-key":"bu","value":57},{"hc-key":"lk","value":21},{"hc-key":"as","value":78},{"hc-key":"dk","value":94},{"hc-key":"fo","value":5},{"hc-key":"gu","value":99},{"hc-key":"mp","value":30},{"hc-key":"pr","value":64},{"hc-key":"um","value":66},{"hc-key":"us","value":82},{"hc-key":"vi","value":5},{"hc-key":"ca","value":0},{"hc-key":"st","value":83},{"hc-key":"jp","value":73},{"hc-key":"cv","value":9},{"hc-key":"dm","value":42},{"hc-key":"sc","value":35},{"hc-key":"jm","value":70},{"hc-key":"ws","value":35},{"hc-key":"om","value":27},{"hc-key":"in","value":93},{"hc-key":"vc","value":36},{"hc-key":"sb","value":36},{"hc-key":"lc","value":49},{"hc-key":"fr","value":68},{"hc-key":"nr","value":66},{"hc-key":"no","value":15},{"hc-key":"fm","value":74},{"hc-key":"kn","value":6},{"hc-key":"cn","value":76},{"hc-key":"bh","value":69},{"hc-key":"to","value":7},{"hc-key":"id","value":35},{"hc-key":"mu","value":14},{"hc-key":"se","value":21},{"hc-key":"tt","value":53},{"hc-key":"sw","value":84},{"hc-key":"bs","value":15},{"hc-key":"pw","value":78},{"hc-key":"ec","value":8},{"hc-key":"au","value":82},{"hc-key":"tv","value":70},{"hc-key":"mh","value":29},{"hc-key":"cl","value":42},{"hc-key":"ki","value":6},{"hc-key":"ph","value":29},{"hc-key":"gd","value":77},{"hc-key":"ee","value":23},{"hc-key":"ag","value":0},{"hc-key":"es","value":42},{"hc-key":"bb","value":57},{"hc-key":"it","value":86},{"hc-key":"mt","value":5},{"hc-key":"mv","value":5},{"hc-key":"sp","value":36},{"hc-key":"pg","value":16},{"hc-key":"vu","value":99},{"hc-key":"sg","value":52},{"hc-key":"gb","value":94},{"hc-key":"cy","value":51},{"hc-key":"gr","value":44},{"hc-key":"km","value":14},{"hc-key":"fj","value":12},{"hc-key":"ru","value":80},{"hc-key":"va","value":11},{"hc-key":"sm","value":66},{"hc-key":"am","value":30},{"hc-key":"az","value":58},{"hc-key":"ls","value":88},{"hc-key":"tj","value":12},{"hc-key":"ml","value":91},{"hc-key":"dz","value":71},{"hc-key":"co","value":55},{"hc-key":"tw","value":42},{"hc-key":"uz","value":12},{"hc-key":"tz","value":55},{"hc-key":"ar","value":47},{"hc-key":"sa","value":61},{"hc-key":"nl","value":39},{"hc-key":"ye","value":33},{"hc-key":"ae","value":97},{"hc-key":"bd","value":13},{"hc-key":"ch","value":6},{"hc-key":"pt","value":0},{"hc-key":"my","value":35},{"hc-key":"vn","value":66},{"hc-key":"br","value":61},{"hc-key":"pa","value":16},{"hc-key":"ng","value":35},{"hc-key":"tr","value":71},{"hc-key":"ir","value":16},{"hc-key":"ht","value":31},{"hc-key":"do","value":21},{"hc-key":"sl","value":11},{"hc-key":"sn","value":78},{"hc-key":"gw","value":28},{"hc-key":"hr","value":92},{"hc-key":"th","value":44},{"hc-key":"mx","value":36},{"hc-key":"tn","value":4},{"hc-key":"kw","value":4},{"hc-key":"de","value":60},{"hc-key":"mm","value":43},{"hc-key":"gq","value":36},{"hc-key":"cnm","value":53},{"hc-key":"nc","value":64},{"hc-key":"ie","value":22},{"hc-key":"kz","value":90},{"hc-key":"pl","value":43},{"hc-key":"lt","value":32},{"hc-key":"eg","value":17},{"hc-key":"ug","value":76},{"hc-key":"cd","value":72},{"hc-key":"mk","value":21},{"hc-key":"al","value":12},{"hc-key":"cm","value":92},{"hc-key":"bj","value":26},{"hc-key":"ge","value":95},{"hc-key":"tl","value":98},{"hc-key":"tm","value":33},{"hc-key":"kh","value":64},{"hc-key":"pe","value":28},{"hc-key":"mw","value":91},{"hc-key":"mn","value":70},{"hc-key":"ao","value":81},{"hc-key":"mz","value":37},{"hc-key":"za","value":79},{"hc-key":"cr","value":15},{"hc-key":"sv","value":39},{"hc-key":"ly","value":12},{"hc-key":"sd","value":75},{"hc-key":"kp","value":17},{"hc-key":"kr","value":13},{"hc-key":"gy","value":26},{"hc-key":"hn","value":37},{"hc-key":"ga","value":88},{"hc-key":"ni","value":21},{"hc-key":"et","value":33},{"hc-key":"so","value":72},{"hc-key":"ke","value":13},{"hc-key":"gh","value":63},{"hc-key":"si","value":23},{"hc-key":"gt","value":68},{"hc-key":"bz","value":44},{"hc-key":"ba","value":6},{"hc-key":"jo","value":79},{"hc-key":"we","value":10},{"hc-key":"il","value":49},{"hc-key":"zm","value":63},{"hc-key":"mc","value":30},{"hc-key":"uy","value":67},{"hc-key":"rw","value":7},{"hc-key":"bo","value":44},{"hc-key":"cg","value":80},{"hc-key":"eh","value":27},{"hc-key":"rs","value":57},{"hc-key":"me","value":93},{"hc-key":"tg","value":63},{"hc-key":"la","value":93},{"hc-key":"af","value":23},{"hc-key":"jk","value":96},{"hc-key":"pk","value":52},{"hc-key":"bg","value":69},{"hc-key":"ua","value":33},{"hc-key":"ro","value":48},{"hc-key":"qa","value":76},{"hc-key":"li","value":73},{"hc-key":"at","value":89},{"hc-key":"sk","value":36},{"hc-key":"sz","value":1},{"hc-key":"hu","value":53},{"hc-key":"ne","value":52},{"hc-key":"lu","value":27},{"hc-key":"ad","value":90},{"hc-key":"ci","value":40},{"hc-key":"lr","value":85},{"hc-key":"bn","value":54},{"hc-key":"mr","value":44},{"hc-key":"be","value":87},{"hc-key":"iq","value":99},{"hc-key":"gm","value":51},{"hc-key":"ma","value":84},{"hc-key":"td","value":6},{"hc-key":"kv","value":63},{"hc-key":"lb","value":6},{"hc-key":"sx","value":36},{"hc-key":"dj","value":53},{"hc-key":"er","value":55},{"hc-key":"bi","value":57},{"hc-key":"sr","value":33},{"hc-key":"gn","value":95},{"hc-key":"zw","value":78},{"hc-key":"py","value":1},{"hc-key":"by","value":42},{"hc-key":"lv","value":16},{"hc-key":"sy","value":87},{"hc-key":"bt","value":84},{"hc-key":"na","value":32},{"hc-key":"bf","value":27},{"hc-key":"cf","value":41},{"hc-key":"md","value":35},{"hc-key":"gz","value":27},{"hc-key":"ss","value":49},{"hc-key":"cz","value":63},{"hc-key":"nz","value":63},{"hc-key":"cu","value":97},{"hc-key":"fi","value":82},{"hc-key":"mg","value":3},{"hc-key":"ve","value":90},{"hc-key":"is","value":51},{"hc-key":"np","value":20},{"hc-key":"kg","value":44},{"hc-key":"bw","value":60}];

$('.data-selection').hide();
// Create the chart
Highcharts.mapChart('product102', {
	    legend: {
		margin:40,
		itemStyle: {
			color: '#ffffff'
		},
		itemHoverStyle: {
			color: '#ffffff'
		},
		useHTML:true,

		
	},

    chart: {
        map: 'custom/world-palestine',
		backgroundColor: '#292929',
		height:600
    },

    title: {
        text: ''
    },
    credits:false,
    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },

    plotOptions: {
        series: {
            point: {
                events: {
                    select: function (a) {
                      a.target.zoomTo();
                      $("#country-search102").val(a.target.properties["iso-a2"]).trigger("change");
                      $('.data-selection').hide();

                      var cData = {
                        country: a.target.name,
                        region: a.target.properties["region-wb"],
                        capital: 'Capital',
                        aml_laws: "YES",
                        tf_laws: "YES",
                        aml_docs: [],
                        tf_docs: [],
                        pa_docs: [{name:"SAR (3)"},{name:"CTR (4)"},{name:"KYC (5)"},{name:"WLM (6)"}],
                        fi_docs: [{name:"Banks (7)"},{name:"Jewelers (8)"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Gambling and Casinos"},{name:"Law Firms"},{name:"Accountants"},{name:"Money Services Businesses"},{name:"Mutual Funds"},{name:"Futures Commission Merchants"},{name:"Brokers in Commodities"},{name:"Credit Card"},{name:"Loan and Finance Companies"},{name:"Housing Government Sponsored Enterprises"},],
                        ia_docs: [{name:"Human Trafficking"},{name:"Drug Trafficking"},{name:"Corruption"},{name:"Arms Trafficking"},{name:"Smuggling"}],
                        ofac_docs: [],
                        sm_docs: [],
                      };

                      if(a.target.name == "Egypt"){
                        cData.capital = "Cairo";
                        cData.pa_docs= [{name:"SAR"},{name:"KYC"}];
                        cData.fi_docs= [{name:"Banks"},{name:"Jewelers"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Gambling and Casinos"},{name:"Law Firms"},{name:"Accountants"},{name:"Money Services Businesses"},];
                        cData.ia_docs= [{name:"Drug Trafficking"},{name:"Corruption"},{name:"Arms Trafficking"}];
                      }
                      if(a.target.name == "Ghana"){
                        cData.capital = "Accra";
                        cData.pa_docs= [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                        cData.fi_docs= [];
                        cData.ia_docs= [{name:"Human Trafficking"},{name:"Drug Trafficking"},{name:"Corruption"},{name:"Arms Trafficking"},{name:"Smuggling"}];
                      }
                      if(a.target.name == "Albania"){
                        cData.capital = "Tirana";
                        cData.pa_docs= [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                        cData.fi_docs= [{name:"Banks"},{name:"Jewelers"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Gambling and Casinos"},{name:"Accountants"},{name:"Loan and Finance Companies"},{name:"Credit Card"}];
                        cData.ia_docs= [{name:"Corruption"}];
                      }
                      if(a.target.name == "Australia"){
                        cData.capital = "Canberra";
                        cData.pa_docs= [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                        cData.fi_docs= [{name:"Banks"},{name:"Gambling and Casinos"},{name:"Loan and Finance Companies"},{name:"Brokers and Dealers in Securities"},{name:"Jewelers"},{name:"Insurance"}];
                        cData.ia_docs= [{name:"Drug Trafficking"},{name:"Corruption"},{name:"Smuggling"}];
                      }
                      if(a.target.name == "Maldives"){
                        cData.capital = "Male";
                        cData.pa_docs= [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                        cData.fi_docs= [{name:"Banks"},{name:"Jewelers"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Law Firms"},{name:"Accountants"},{name:"Money Services Businesses"},{name:"Mutual Funds"},{name:"Brokers in Commodities"},{name:"Credit Card"},{name:"Loan and Finance Companies"}];
                      }
                      if(a.target.name == "United States of America"){
                        cData.capital = "Washington, D.C.";
                        cData.pa_docs = [{name:"SAR"},{name:"KYC"},{name:"CTR"}];
                        cData.fi_docs = [{name:"Banks"},{name:"Jewelers"},{name:"Real Estate"},{name:"Brokers and Dealers in Securities"},{name:"Attorneys"},{name:"Insurance"},{name:"Gambling and Casinos"},{name:"Law Firms"},{name:"Accountants"},{name:"Money Services Businesses"},{name:"Mutual Funds"},{name:"Futures Commission Merchants"},{name:"Brokers in Commodities"},{name:"Credit Card"},{name:"Loan and Finance Companies"},{name:"Housing Government Sponsored Enterprises"},];
                      }


                      var cardHtml = card102tmpl.render(cData);
                      $("#country-card").html(cardHtml);
                      $('#select-a-card').hide();
                      $('.data-selection').fadeIn();
					  
					           $('#content').addClass('show');

                    }
                }
            }
        }
    },
    tooltip: {
        backgroundColor: {
            linearGradient: [0, 0, 0, 60],
            stops: [
                [0, '#FFFFFF'],
                [1, '#E0E0E0']
            ]
        },
        borderWidth: 1,
        borderColor: '#AAA',
        borderRadius: 2,
        formatter: function () {
          if(this.point.name == "United States of America" || this.point.name == "Egypt" || this.point.name == "Ghana" || this.point.name == "Australia" || this.point.name == "Maldives"){return '<b>' + this.point.name + '</b> <br/><b style="color:green;font-weight:bold;">Yes</b> Anti-Money Laundering Laws<br/> <b style="color:green;font-weight:bold;">Yes</b> Terrorism Financing Laws';}
          if(this.point.value <= 50){return '<b>' + this.point.name + '</b> <br/><b style="color:green;font-weight:bold;">Yes</b> Anti-Money Laundering Laws<br/> <b style="color:green;font-weight:bold;">Yes</b> Terrorism Financing Laws';}
          else{return '<b>' + this.point.name + ':</b> <br/><b style="color:red;font-weight:bold;">No</b> Anti-Money Laundering Laws<br/> <bstyle="color:red;font-weight:bold;">No</b> Terrorism Financing Laws';}
        }
    },
    colorAxis: {
        dataClassColor: 'category',
        dataClasses: [{
            name: 'Asia/Pacific Group on Money Laundering (APG)',
            //color: '#555',
            from: 0,
            to: 8
        }, {
            name: 'Committee of Experts on the Evaluation of Anti-Money Laundering Measures (MONEYVAL)',
            //color: '#eee',
            from: 9,
            to: 16
        },{
            name: 'Middle East and North Africa Financial Action Task Force (MENAFATF)',
            //color: '#555',
            from: 17,
            to: 24
        }, {
            name: 'Eastern and Southern Africa Anti-Money Laundering Group (ESAAMLG)',
            //color: '#eee',
            from: 25,
            to: 32
        }, {
            name: 'Caribbean Financial Action Task Force (CFATF)',
            //color: '#eee',
            from: 33,
            to: 40
        },{
            name: 'Financial Action Task Force',
            //color: '#555',
            from: 41,
            to: 48
        }, {
            name: 'Financial Action Task Force of Latin America (GAFILAT)',
            //color: '#eee',
            from: 49,
            to: 56
        },{
            name: 'Eurasian Group (EAG)',
            //color: '#555',
            from: 73,
            to: 80
        }, {
            name: 'Inter Governmental Action Group against Money Laundering in West Africa (GIABA)',
            //color: '#eee',
            from: 63,
            to: 72
        }, {
            name: 'Groupe d\'Action contre le blanchiment d\'Argent en Afrique Centrale (GABAC)',
            //color: '#eee',
            from: 55,
            to: 64
        }, {
            name: 'Multiple Bodies',
            //color: '#eee',
            from: 81,
            to: 100
        }]
    },
    series: [{
        data: data,
        name: 'FATF',
        allowPointSelect: true,
        states: {
            hover: {
                color: '#d65527'
            },
            select: {
                color: '#d65527'
            }
        },
        dataLabels: {
            enabled: true,
            format: '{point.name}'
        }
    }]
});


}


if(window.location.href.split('/').pop() == "product-103.php"){
  function getItemById(id){
    for (var i = 0; i < report_data.length; i++) {
      if(report_data[i].id == id){
        return report_data[i];
      }
    };
  }
  var p1010data = [{"id":"","orig":"","text":""},{"id":"AF","orig":"Afghanistan","text":"Afghanistan"},{"id":"AL","orig":"Albania","text":"Albania"},{"id":"DZ","orig":"Algeria","text":"Algeria"},{"id":"AS","orig":"American Samoa","text":"American Samoa"},{"id":"AD","orig":"Andorra","text":"Andorra"},{"id":"AO","orig":"Angola","text":"Angola"},{"id":"AG","orig":"Antigua and Barbuda","text":"Antigua and Barbuda"},{"id":"AR","orig":"Argentina","text":"Argentina"},{"id":"AM","orig":"Armenia","text":"Armenia"},{"id":"AU","orig":"Australia","text":"Australia"},{"id":"AT","orig":"Austria","text":"Austria"},{"id":"AZ","orig":"Azerbaijan","text":"Azerbaijan"},{"id":"BH","orig":"Bahrain","text":"Bahrain"},{"id":"BU","orig":"Bajo Nuevo Bank (Petrel Is.)","text":"Bajo Nuevo Bank (Petrel Is.)"},{"id":"BD","orig":"Bangladesh","text":"Bangladesh"},{"id":"BB","orig":"Barbados","text":"Barbados"},{"id":"BY","orig":"Belarus","text":"Belarus"},{"id":"BE","orig":"Belgium","text":"Belgium"},{"id":"BZ","orig":"Belize","text":"Belize"},{"id":"BJ","orig":"Benin","text":"Benin"},{"id":"BT","orig":"Bhutan","text":"Bhutan"},{"id":"BO","orig":"Bolivia","text":"Bolivia"},{"id":"BA","orig":"Bosnia and Herzegovina","text":"Bosnia and Herzegovina"},{"id":"BW","orig":"Botswana","text":"Botswana"},{"id":"BR","orig":"Brazil","text":"Brazil"},{"id":"BN","orig":"Brunei","text":"Brunei"},{"id":"BG","orig":"Bulgaria","text":"Bulgaria"},{"id":"BF","orig":"Burkina Faso","text":"Burkina Faso"},{"id":"BI","orig":"Burundi","text":"Burundi"},{"id":"KH","orig":"Cambodia","text":"Cambodia"},{"id":"CM","orig":"Cameroon","text":"Cameroon"},{"id":"CA","orig":"Canada","text":"Canada"},{"id":"CV","orig":"Cape Verde","text":"Cape Verde"},{"id":"CF","orig":"Central African Republic","text":"Central African Republic"},{"id":"TD","orig":"Chad","text":"Chad"},{"id":"CL","orig":"Chile","text":"Chile"},{"id":"CN","orig":"China","text":"China"},{"id":"CO","orig":"Colombia","text":"Colombia"},{"id":"KM","orig":"Comoros","text":"Comoros"},{"id":"CR","orig":"Costa Rica","text":"Costa Rica"},{"id":"HR","orig":"Croatia","text":"Croatia"},{"id":"CU","orig":"Cuba","text":"Cuba"},{"id":"CY","orig":"Cyprus","text":"Cyprus"},{"id":null,"orig":"Cyprus No Mans Area","text":"Cyprus No Mans Area"},{"id":"CZ","orig":"Czech Republic","text":"Czech Republic"},{"id":"CD","orig":"Democratic Republic of the Congo","text":"Democratic Republic of the Congo"},{"id":"DK","orig":"Denmark","text":"Denmark"},{"id":"DJ","orig":"Djibouti","text":"Djibouti"},{"id":"DM","orig":"Dominica","text":"Dominica"},{"id":"DO","orig":"Dominican Republic","text":"Dominican Republic"},{"id":"TL","orig":"East Timor","text":"East Timor"},{"id":"EC","orig":"Ecuador","text":"Ecuador"},{"id":"EG","orig":"Egypt","text":"Egypt"},{"id":"SV","orig":"El Salvador","text":"El Salvador"},{"id":"GQ","orig":"Equatorial Guinea","text":"Equatorial Guinea"},{"id":"ER","orig":"Eritrea","text":"Eritrea"},{"id":"EE","orig":"Estonia","text":"Estonia"},{"id":"ET","orig":"Ethiopia","text":"Ethiopia"},{"id":"FO","orig":"Faroe Islands","text":"Faroe Islands"},{"id":"FM","orig":"Federated States of Micronesia","text":"Federated States of Micronesia"},{"id":"FJ","orig":"Fiji","text":"Fiji"},{"id":"FI","orig":"Finland","text":"Finland"},{"id":"FR","orig":"France","text":"France"},{"id":"GA","orig":"Gabon","text":"Gabon"},{"id":"GM","orig":"Gambia","text":"Gambia"},{"id":"GZ","orig":"Gaza Strip","text":"Gaza Strip"},{"id":"GE","orig":"Georgia","text":"Georgia"},{"id":"DE","orig":"Germany","text":"Germany"},{"id":"GH","orig":"Ghana","text":"Ghana"},{"id":"GR","orig":"Greece","text":"Greece"},{"id":"GL","orig":"Greenland","text":"Greenland"},{"id":"GD","orig":"Grenada","text":"Grenada"},{"id":"GU","orig":"Guam","text":"Guam"},{"id":"GT","orig":"Guatemala","text":"Guatemala"},{"id":"GN","orig":"Guinea","text":"Guinea"},{"id":"GW","orig":"Guinea Bissau","text":"Guinea Bissau"},{"id":"GY","orig":"Guyana","text":"Guyana"},{"id":"HT","orig":"Haiti","text":"Haiti"},{"id":"HN","orig":"Honduras","text":"Honduras"},{"id":"HU","orig":"Hungary","text":"Hungary"},{"id":"IS","orig":"Iceland","text":"Iceland"},{"id":"IN","orig":"India","text":"India"},{"id":"ID","orig":"Indonesia","text":"Indonesia"},{"id":"IR","orig":"Iran","text":"Iran"},{"id":"IQ","orig":"Iraq","text":"Iraq"},{"id":"IE","orig":"Ireland","text":"Ireland"},{"id":"IL","orig":"Israel","text":"Israel"},{"id":"IT","orig":"Italy","text":"Italy"},{"id":"CI","orig":"Ivory Coast","text":"Ivory Coast"},{"id":"JM","orig":"Jamaica","text":"Jamaica"},{"id":"JP","orig":"Japan","text":"Japan"},{"id":"JO","orig":"Jordan","text":"Jordan"},{"id":"KZ","orig":"Kazakhstan","text":"Kazakhstan"},{"id":"KE","orig":"Kenya","text":"Kenya"},{"id":"KI","orig":"Kiribati","text":"Kiribati"},{"id":"KV","orig":"Kosovo","text":"Kosovo"},{"id":"KW","orig":"Kuwait","text":"Kuwait"},{"id":"KG","orig":"Kyrgyzstan","text":"Kyrgyzstan"},{"id":"LA","orig":"Laos","text":"Laos"},{"id":"LV","orig":"Latvia","text":"Latvia"},{"id":"LB","orig":"Lebanon","text":"Lebanon"},{"id":"LS","orig":"Lesotho","text":"Lesotho"},{"id":"LR","orig":"Liberia","text":"Liberia"},{"id":"LY","orig":"Libya","text":"Libya"},{"id":"LI","orig":"Liechtenstein","text":"Liechtenstein"},{"id":"LT","orig":"Lithuania","text":"Lithuania"},{"id":"LU","orig":"Luxembourg","text":"Luxembourg"},{"id":"MK","orig":"Macedonia","text":"Macedonia"},{"id":"MG","orig":"Madagascar","text":"Madagascar"},{"id":"MW","orig":"Malawi","text":"Malawi"},{"id":"MY","orig":"Malaysia","text":"Malaysia"},{"id":"MV","orig":"Maldives","text":"Maldives"},{"id":"ML","orig":"Mali","text":"Mali"},{"id":"MT","orig":"Malta","text":"Malta"},{"id":"MH","orig":"Marshall Islands","text":"Marshall Islands"},{"id":"MR","orig":"Mauritania","text":"Mauritania"},{"id":"MU","orig":"Mauritius","text":"Mauritius"},{"id":"MX","orig":"Mexico","text":"Mexico"},{"id":"MD","orig":"Moldova","text":"Moldova"},{"id":"MC","orig":"Monaco","text":"Monaco"},{"id":"MN","orig":"Mongolia","text":"Mongolia"},{"id":"ME","orig":"Montenegro","text":"Montenegro"},{"id":"MA","orig":"Morocco","text":"Morocco"},{"id":"MZ","orig":"Mozambique","text":"Mozambique"},{"id":"MM","orig":"Myanmar","text":"Myanmar"},{"id":"NA","orig":"Namibia","text":"Namibia"},{"id":"NR","orig":"Nauru","text":"Nauru"},{"id":"NP","orig":"Nepal","text":"Nepal"},{"id":"NL","orig":"Netherlands","text":"Netherlands"},{"id":"NZ","orig":"New Zealand","text":"New Zealand"},{"id":"NI","orig":"Nicaragua","text":"Nicaragua"},{"id":"NE","orig":"Niger","text":"Niger"},{"id":"NG","orig":"Nigeria","text":"Nigeria"},{"id":"KP","orig":"North Korea","text":"North Korea"},{"id":"NC","orig":"Northern Cyprus","text":"Northern Cyprus"},{"id":"MP","orig":"Northern Mariana Islands","text":"Northern Mariana Islands"},{"id":"NO","orig":"Norway","text":"Norway"},{"id":"OM","orig":"Oman","text":"Oman"},{"id":"PK","orig":"Pakistan","text":"Pakistan"},{"id":"PW","orig":"Palau","text":"Palau"},{"id":"PA","orig":"Panama","text":"Panama"},{"id":"PG","orig":"Papua New Guinea","text":"Papua New Guinea"},{"id":"PY","orig":"Paraguay","text":"Paraguay"},{"id":"PE","orig":"Peru","text":"Peru"},{"id":"PH","orig":"Philippines","text":"Philippines"},{"id":"PL","orig":"Poland","text":"Poland"},{"id":"PT","orig":"Portugal","text":"Portugal"},{"id":"PR","orig":"Puerto Rico","text":"Puerto Rico"},{"id":"QA","orig":"Qatar","text":"Qatar"},{"id":"CG","orig":"Republic of Congo","text":"Republic of Congo"},{"id":"RS","orig":"Republic of Serbia","text":"Republic of Serbia"},{"id":"RO","orig":"Romania","text":"Romania"},{"id":"RU","orig":"Russia","text":"Russia"},{"id":"RW","orig":"Rwanda","text":"Rwanda"},{"id":"KN","orig":"Saint Kitts and Nevis","text":"Saint Kitts and Nevis"},{"id":"LC","orig":"Saint Lucia","text":"Saint Lucia"},{"id":"VC","orig":"Saint Vincent and the Grenadines","text":"Saint Vincent and the Grenadines"},{"id":"WS","orig":"Samoa","text":"Samoa"},{"id":"SM","orig":"San Marino","text":"San Marino"},{"id":"ST","orig":"Sao Tome and Principe","text":"Sao Tome and Principe"},{"id":"SA","orig":"Saudi Arabia","text":"Saudi Arabia"},{"id":"SH","orig":"Scarborough Reef","text":"Scarborough Reef"},{"id":"SN","orig":"Senegal","text":"Senegal"},{"id":"SW","orig":"Serranilla Bank","text":"Serranilla Bank"},{"id":"SC","orig":"Seychelles","text":"Seychelles"},{"id":"JK","orig":"Siachen Glacier","text":"Siachen Glacier"},{"id":"SL","orig":"Sierra Leone","text":"Sierra Leone"},{"id":"SG","orig":"Singapore","text":"Singapore"},{"id":"SK","orig":"Slovakia","text":"Slovakia"},{"id":"SI","orig":"Slovenia","text":"Slovenia"},{"id":"SB","orig":"Solomon Islands","text":"Solomon Islands"},{"id":"SO","orig":"Somalia","text":"Somalia"},{"id":"SX","orig":"Somaliland","text":"Somaliland"},{"id":"ZA","orig":"South Africa","text":"South Africa"},{"id":"KR","orig":"South Korea","text":"South Korea"},{"id":"SS","orig":"South Sudan","text":"South Sudan"},{"id":"ES","orig":"Spain","text":"Spain"},{"id":"SP","orig":"Spratly Islands","text":"Spratly Islands"},{"id":"LK","orig":"Sri Lanka","text":"Sri Lanka"},{"id":"SD","orig":"Sudan","text":"Sudan"},{"id":"SR","orig":"Suriname","text":"Suriname"},{"id":"SZ","orig":"Swaziland","text":"Swaziland"},{"id":"SE","orig":"Sweden","text":"Sweden"},{"id":"CH","orig":"Switzerland","text":"Switzerland"},{"id":"SY","orig":"Syria","text":"Syria"},{"id":"TW","orig":"Taiwan","text":"Taiwan"},{"id":"TJ","orig":"Tajikistan","text":"Tajikistan"},{"id":"TH","orig":"Thailand","text":"Thailand"},{"id":"BS","orig":"The Bahamas","text":"The Bahamas"},{"id":"TG","orig":"Togo","text":"Togo"},{"id":"TO","orig":"Tonga","text":"Tonga"},{"id":"TT","orig":"Trinidad and Tobago","text":"Trinidad and Tobago"},{"id":"TN","orig":"Tunisia","text":"Tunisia"},{"id":"TR","orig":"Turkey","text":"Turkey"},{"id":"TM","orig":"Turkmenistan","text":"Turkmenistan"},{"id":"TV","orig":"Tuvalu","text":"Tuvalu"},{"id":"UG","orig":"Uganda","text":"Uganda"},{"id":"UA","orig":"Ukraine","text":"Ukraine"},{"id":"AE","orig":"United Arab Emirates","text":"United Arab Emirates"},{"id":"GB","orig":"United Kingdom","text":"United Kingdom"},{"id":"TZ","orig":"United Republic of Tanzania","text":"United Republic of Tanzania"},{"id":"UM","orig":"United States Minor Outlying Islands","text":"United States Minor Outlying Islands"},{"id":"VI","orig":"United States Virgin Islands","text":"United States Virgin Islands"},{"id":"US","orig":"United States of America","text":"United States of America"},{"id":"UY","orig":"Uruguay","text":"Uruguay"},{"id":"UZ","orig":"Uzbekistan","text":"Uzbekistan"},{"id":"VU","orig":"Vanuatu","text":"Vanuatu"},{"id":"VA","orig":"Vatican","text":"Vatican"},{"id":"VE","orig":"Venezuela","text":"Venezuela"},{"id":"VN","orig":"Vietnam","text":"Vietnam"},{"id":"WE","orig":"West Bank","text":"West Bank"},{"id":"EH","orig":"Western Sahara","text":"Western Sahara"},{"id":"YE","orig":"Yemen","text":"Yemen"},{"id":"ZM","orig":"Zambia","text":"Zambia"},{"id":"ZW","orig":"Zimbabwe","text":"Zimbabwe"}];
  
  $("#country-search101").select2({
      placeholder: "Select a country",
      data: p1010data,
  });

  var report_data = null;
  $.getJSON('data/product103.json',  function(data){

    var dummyCount = 154-data.length;
    
    for (var i = 0; i < dummyCount; i++) {
      var maxYear = Math.floor(Math.random() * (2009 - 1970 + 1)) + 1970;

      data.push({
        "id":i+6,
        "title":"Report Title",
        "issuance_abbr":"Ad",
        "issuance_type":"Advisory",
        "issuance_type_abbr":"AD",
        "year":maxYear,
        "source":"FinCEN",
        "countries_implicated":"Venezuela",
        "bsa_programs":"WLM; CTR; KYC; SAR",
        "executive_summary":"This advisory discusses the widespread public corruption in Venezuela and the methods Venezuelan senior political figures (and their associates and front persons) may use to move and hide corruption proceeds. This advisory also provides financial red flags to assist in identifying and reporting to FinCEN suspicious activity that may be indicative of Venezuelan corruption, including the abuse of Venezuelan government contracts, wire transfers from shell corporations, and real estate purchases in the South Florida and Houston, Texas regions. Specifically, the Venezuelan government appears to use its control over large parts of the economy to generate significant wealth for government officials and SOE executives, their families, and associates."
      });
    };
    report_data = data;

    var tileHtml = card103isotmpl.render(data);
    $('.grid').html(tileHtml);

    var $container = $('.grid');
    var $grid = $container.isotope({
      itemSelector : '.grid-item',
      getSortData: {
        year: '.year',
        name: '.name'
      },
      sortBy : 'year',
      sortAscending: {
        year: false
      },
    });

    var currentYear = new Date().getFullYear(), years = [];
    var startYear = 1970;
    var yearOpts = "";
    while ( startYear <= currentYear ) {
        var year = startYear++;
        yearOpts += "<option value="+year+">"+year+"</option>";
    } 
    $('#gtDate').html(yearOpts);
    $('#ltDate').html(yearOpts);
    $('#ltDate').val('2018');


    $(document).on('click', '.grid-item', function(e){ 
      var actionid = $(this).attr('data-id');

      $('.grid-item').removeClass('active');
        console.log(getItemById(actionid));

        var cardHtml = card103tmpl.render(getItemById(actionid));
        $("#report-card").html(cardHtml);


        $('#content > .col-2').addClass('show');
        $('.prod-104').addClass('show');
        $('.ea-1').show();
        $(this).addClass('active');
      e.stopPropagation(); 

      
    })
    $(document).on('click', '.close-inspector', function() {
      $('#content > .col-2').removeClass('show');
      $('.prod-104').removeClass('show');
      $('.grid-item').removeClass('active');

        $('.ea-1').hide();
    });



  });
Highcharts.chart('product103-guide', {
	
	
	   chart: {
		  backgroundColor: '#222222',
		  borderColor:'#335cad',
		  plotBorderColor:'#335cad',
          height:350,
          margin:75,
		  marginBottom:70,
		marginLeft:150,
		  marginTop:100,
          plotBorderWidth: null,
          plotShadow: false,        
          type: 'bar',
          style: {
              fontFamily: 'monospace',
              color: "#fff"
	  },
   },

	
      title: {
        text: 'Type of Guidance',
		  y:40,
		  style: {color: '#ffffff'
			 }
      },
      credits: false,
 
	
	
	
	

    xAxis: {
		
		
		labels: {
			style: {
				width: '200px'
			},
		},
		useHTML: true,

        categories: ['Advisories','Guidance','311 Special Measures','Administrative Rulings'],
        title: {
            text: null
        },
				  gridLineColor:'#555',
		  lineColor:'#555',
		  minorGridLineColor:'#555',
		  tickColor:'#555',

          title: {
              text: null,
			  color:'#fff'
          }
    },
    yAxis:{
      title: '',
				  gridLineColor:'#555',
		  lineColor:'#555',
		  minorGridLineColor:'#555',
		  tickColor:'#555',

    },
    tooltip: {
        enabled: false,
    },
    plotOptions: {
        bar: {
			colorByPoint: true,
            dataLabels: {
                enabled: true
            },
			colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],

        },
				  line: {	  
			  color:'#555',
			  borderColor: '#555'
		  },
		  area : {
			  color:'#ffffff',
			  fillColor: '#ffffff',
		  },


		
		
		
		
    },


	
    credits: false,
    legend: {
        enabled: false
    },
    series: [{
		borderWidth:0,
      name:'Score',
      data: [154,125,55,86]
    }]
});
Highcharts.chart('product103-bsa', {

    chart: {
        backgroundColor: '#222222',
        plotBackgroundColor: '#222222',
        height:350,
        margin:20,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        style: {
            fontFamily: 'monospace',
            color: "#fff"
        }
    },
    title: {
      text: 'BSA Program Area',
      y:40,
      style: {
         color: '#ffffff',

        },
      useHTML:true,
    },
    credits: false,
    tooltip: {
        enabled: true,
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {

    margin:20,
    itemStyle: {
      color: '#ffffff'
    },
    itemHoverStyle: {
      color: '#ffffff'
    },
    useHTML:true,
  },
  plotOptions: {
        pie: {
      borderWidth:0,
      colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
      size: 180,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
        color: '#ffffff',
                enabled: false,
            },
          showInLegend: true,

        },

    },
  series: [{
        name: 'Areas',
        colorByPoint: true,
        data: [
          {
            name:'WLM',
            y:4
          },{
            name:'CTR',
            y:3
          },{
            name:'KYC',
            y:4
          },{
            name:'SAR',
            y:4
          },
        ]
    }]
});
Highcharts.chart('product103-sub', {

    chart: {
        backgroundColor: '#222222',
        plotBackgroundColor: '#222222',
        height:450,
        margin:20,
		marginBottom:80,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        style: {
            fontFamily: 'monospace',
            color: "#fff"
        }
    },
    title: {
      text: 'BSA Sub-Program Area',
      y:40,
      style: {
         color: '#ffffff',

        },
      useHTML:true,
    },
    credits: false,
    tooltip: {
        enabled: true,
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {
		itemWidth:200,

    margin:20,
    itemStyle: {
      color: '#ffffff'
    },
    itemHoverStyle: {
      color: '#ffffff'
    },
    useHTML:true,
  },
  plotOptions: {
        pie: {
      borderWidth:0,
      colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
      size: 180,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
        color: '#ffffff',
                enabled: false,
            },
          showInLegend: true,

        },

    },
  series: [{
        name: 'Areas',
        colorByPoint: true,
        data: [
          { name: "Foreign Currency Controls", y:1 },
          { name: "Bulk Cash Shipments", y:1 },
          { name: "EDD on Private Bank Accounts", y:1 },
          { name: "EDD on Foreign Correspondent Accounts", y:3 },
          { name: "EDD", y:1 },
          { name: "CRR", y:1 },
          { name: "ERA", y:1 },
          { name: "SAR Filings", y:2 },
          { name: "SAR Filing Instructions", y:2 },
          { name: "314(b) Voluntary Information Sharing", y:1 },
          { name: "Transaction Monitoring", y:2 },
          { name: "OFAC Sanctions Screening", y:3 },
          { name: "PEP Screening", y:2 }
        ]
    }]
});

Highcharts.chart('product103-issuance', {
    chart: {
		  backgroundColor: '#222222',
		  borderColor:'#335cad',
		  plotBorderColor:'#335cad',
          height:350,
          margin:75,
		  marginBottom:70,
		  marginTop:100,
          plotBorderWidth: null,
          plotShadow: false,        
        type: 'column',
          style: {
              fontFamily: 'monospace',
              color: "#fff"
	  },
   },

	
      title: {
        text: 'Issuance',
		  y:40,
		  style: {color: '#ffffff'
			 }
      },
      credits: false,
 
		
	
	
  
    credits: false,
    legend: {
      enabled: false
    },
    xAxis: {
        categories: [
            'Advisory',
            'Update',
            'Withdrawal',
        ],
        crosshair: true
    },
    yAxis: {
		
        min: 0,
        title: {
            text: ''
        }
    },
    tooltip: {
        enabled: false,
    },
    plotOptions: {
        column: {
			colorByPoint :true,
			colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],

            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
      name: '',
		
      data: [3,1,1]
    }]
});

Highcharts.chart('product103-country', {
    chart: {
		  backgroundColor: '#222222',
		  borderColor:'#335cad',
		  plotBorderColor:'#335cad',
          height:400,
          margin:75,
		  marginBottom:150,
		  marginTop:100,
          plotBorderWidth: null,
          plotShadow: false,        
        type: 'column',
          style: {
              fontFamily: 'monospace',
              color: "#fff"
	  },
   },

	
      title: {
        text: 'Country',
		  y:40,
		  style: {color: '#ffffff'
			 }
      },
      credits: false,
 
		
	
	
  
    credits: false,
    legend: {
      enabled: false
    },
    xAxis: {
        categories: ["Venezuela","Afghanistan","Albania","Algeria","Angola","Antigua and Barbuda","Argentina","Bangladesh","Bolivia","Brunei Darussalam","Cambodia","Cuba","Kuwait","Kyrgyzstan","Mongolia","Morocco","Namibia","Nepal","Nicaragua","Philippines","Sri Lanka","Sudan","Thailand","Tajikistan","Zimbabwe","Ghana","Democratic People's Republic of Korea","Cyprus"],
        crosshair: true
    },
    yAxis: {
						  gridLineColor:'#555',
		  lineColor:'#555',
		  minorGridLineColor:'#555',
		  tickColor:'#555',

        min: 0,
        title: {
            text: ''
        }
    },
    tooltip: {
        enabled: false,
    },
    plotOptions: {
        column: {
			colorByPoint :true,
			colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],

            pointPadding: 0.2,
            borderWidth: 0
        }
    },
	
	
	

    series: [{
      name: '',
      data: [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    }]
});

}

if(window.location.href.split('/').pop() == "product-104.php"){
 function entity_switch(entity){
    switch(entity) {
      case "Depository Institutions":
        return 'Di';
        break;
      case "Casinos":
        return 'Ca';
        break;
      case "Money Services Businesses":
        return 'Msb';
        break;
      case "Precious Metals/Jewelry Industry":
        return 'Pmj';
        break;
      case "Securities and Futures":
        return 'Sf';
        break;
      default:
        return 'Na';
    }
 }
 function pillar_switch(pillar){
    switch(pillar) {
      case "Independent Testing of BSA Compliance":
        return 'independent_testing';
        break;
      case "BSA Training Program":
        return 'training';
        break;
      case "Designation of a Qualified BSA Officer":
        return 'qualified_bsa_officer';
        break;
      case "System of Internal Controls":
        return 'internal_controls';
        break;
      default:
        return 'internal_controls';
    }
 }
      function subprogram_switch(sp){
       switch(sp) {
         case "CIP":
           return 'cip';
           break;
         case "CDD":
         case "CDD/EDD on Foreign Correspondent Accounts":
           return 'cdd';
           break;
         case "CRR":
         case "CRR Model":
         case "country Risk Rating Model":
           return 'crr';
           break;
         case "EDD":
         case "EDD on High-Risk Customers and Foreign Correspondent Accounts":
         case "EDD on Foreign Correspondent Accounts":
         case "EDD on Foreign Correspondent Banks":
         case "ERA":
         case "Detection of Suspicious Activity":
           return 'edd';
           break;
         case "Transaction Monitoring":
           return 'tranmon';
           break;
         case "SAR Filings":
         case "SAR Disclosure":
         case "SAR Monitoring":
           return 'sarfile';
           break;
         case "CTR Filings":
         case "CTR Exemptions":
           return 'ctrfile';
           break;
         case "Red Flags Identification":
           return 'redflag';
           break;
         case "Recordkeeping":
         case "Monetary Instrument Recordkeeping":
           return 'recordkeeping';
           break;
         case "314(a) Screening":
           return '314a';
           break;
         case "314(b) Screening":
           return '314b';
           break;
         case "Sanctions Screening":
           return 'sanctions';
           break;
         case "Referral of Suspicious Activity":
         case "PEP Screening":
         case "Aggregation of Currency Transactions":
           return 'riskassesment';
           break;
         case "Establishment of Correspondent Accounts for Foreign Shell Banks":
           return 'coraccs';
           break;
       }
    }
function getActionById(id){
  for (var i = 0; i < action_data.length; i++) {
    if(action_data[i].matter_number === id){
      return action_data[i];
    }
  };
}


var action_data = null;
var filtered_data = null;
$.getJSON('data/product104.json',  function(data){

  for (var i = 0; i < data.length; i++) {
    //get year
    data[i].issuance_date = data[i].issuance_date.split('/').pop();
    //remove $
    data[i].fine_penalty = data[i].fine_penalty.split('$').pop();

    //entities
    if(data[i].financial_institution.includes(',')){
      var fis = data[i].financial_institution.split(',');
      fis = fis.map(function(e){ return e.trim().toLowerCase().replace(/ /g,'_').replace('/', ''); })
      data[i].entity_iso_class = fis.join(' ');

      var acros = [];
      var fis = data[i].financial_institution.split(',');
      for (var j = 0; j < fis.length; j++) {
        acros.push(entity_switch(fis[j].trim()));
      };
      data[i].entity_acro = acros.join(' ');
    }else{
      data[i].entity_iso_class = data[i].financial_institution.toLowerCase().replace(/ /g,'_').replace('/', '');
      data[i].entity_acro = entity_switch(data[i].financial_institution.trim());
    }

    //pillars violated
    if(data[i].aml_pillars_violated.includes(';')){
      var pils = [];
      var pvs = data[i].aml_pillars_violated.split(';');

      for (var j = 0; j < pvs.length; j++) {
        pils.push(pillar_switch(pvs[j].trim()));
      };
      data[i].pvs_iso_class = pils.join(' ');
    }else{
      data[i].pvs_iso_class = pillar_switch(data[i].aml_pillars_violated.trim());
    }

    //bsa_program_area
    if(data[i].bsa_program_area.includes(';')){
      var pas = data[i].bsa_program_area.split(';');
      pas = pas.map(function(e){ return e.trim().toLowerCase(); })
      data[i].bsa_iso_class = pas.join(' ');
    }else{
      data[i].bsa_iso_class = data[i].bsa_program_area.trim().toLowerCase();
    }

    //sub_areas
    if(data[i].sub_areas.includes(';')){
      var pils = [];
      var pvs = data[i].sub_areas.split(';');

      for (var j = 0; j < pvs.length; j++) {
        pils.push(subprogram_switch(pvs[j].trim()));
      };
      data[i].sub_iso_class = pils.join(' ');
    }else{
      data[i].sub_iso_class = subprogram_switch(data[i].sub_areas.trim());
    }


    var pen_amount = parseInt(data[i].fine_penalty.replace(/,/g,''));
    if(pen_amount < 25000000){ data[i].penalty_iso_class = 'pen1'; }
    else if(pen_amount >= 25000000 && pen_amount <= 100000000){ data[i].penalty_iso_class = 'pen2'; }
    else if(pen_amount > 100000000){ data[i].penalty_iso_class = 'pen3'; }

    data[i].assetsize_iso_class = data[i].asset_size.replace(/ /g,'_');
    
  };
  action_data = data;

  var tileHtml = card104isotmpl.render(data);
  $('.grid').html(tileHtml);

  var $container = $('.grid');
  var filters = {};
  var filterFns = {
    greaterThanDate: function(){
      var year_sel = $('#gtDate').val();
      var year = $(this).find('.year').text();
      return parseInt(year) >= parseInt(year_sel);
    },
    lessThanDate: function(){
      var year_sel = $('#ltDate').val();
      var year = $(this).find('.year').text();
      return parseInt(year) <= parseInt(year_sel);
    }
  }

  var $grid = $container.isotope({
    itemSelector : '.grid-item',
    getSortData: {
      year: '.year',
      state: '.state-tag'
    },
    sortBy : 'year',
    sortAscending: {
      year: false
    },
    filter: function(){
      var isMatched = true;
      var $this = $(this);

      for ( var prop in filters ) {
        var filter = filters[ prop ];
        // use function if it matches
        filter = filterFns[ filter ] || filter;
        // test each filter
        if ( filter ) {
          isMatched = isMatched && $(this).is( filter );
        }
        // break if not matched
        if ( !isMatched ) {
          break;
        }
      }
      return isMatched;
    }
  });
  updateHighlightGraphs();


  $('.sort-by-button-group').change(function() {
    var sortByValue = $("option:selected", this).attr('data-sort-by');
    $container.isotope({ sortBy: sortByValue });
  }); 
  function count_arrays(types, arr){
    var counts = {};
     for (var i = 0; i < types.length; i++) {
        counts[types[i]] = 0;
    }   
    for (var i = 0; i < arr.length; i++) {
        counts[arr[i]] = 1 + (counts[arr[i]] || 0);
    }
    return counts;
  }
  function convertToHCFormat(arr){
    var newarr = [];
    var high_key = null;
    var highVal = 0;
    for (var key in arr) {
        if (arr.hasOwnProperty(key)) {
            if(arr[key] >= highVal){ highVal = arr[key]; high_key = key; }
            newarr.push({
              name: key,
              y: arr[key]
            })
        }
    }
    for (var i = 0; i < newarr.length; i++) {
      console.log(newarr[i].name);
      console.log(high_key);
      if(newarr[i].name == high_key){newarr[i].sliced = true; }
    };
    return newarr;
  }
  function updateFilterCount() {
    $('.actionCount').text( $grid.data('isotope').filteredItems.length );

  }
  function updateHighlightGraphs() {
    var recalc = $grid.data('isotope').filteredItems;

    filtered_data = recalc.map(function(a){ 
      var matter_id = $(a.element).attr('data-matter-id');
      return getActionById(matter_id); 
    });

    //recalc data

    //build entity types chart
    var entity_types = [];
    var split_entities = [];

    action_data.map(function(d){ var itms = d.financial_institution.split(','); itms.forEach(function(i){ entity_types.push(i.trim()) }) });
    entity_types = entity_types.unique();
    filtered_data.map(function(d){ var itms = d.financial_institution.split(','); itms.forEach(function(i){ split_entities.push(i.trim()) }) });
    var fichartdata = count_arrays(entity_types, split_entities);
    fichartdata = convertToHCFormat(fichartdata);
    buildFiChart(fichartdata);

    //build pillar chart
    var pillar_types = [];
    var split_pillars = [];
    action_data.map(function(d){ var itms = d.aml_pillars_violated.split(';'); itms.forEach(function(i){ pillar_types.push(i.trim()) }) });
    pillar_types = pillar_types.unique();
    filtered_data.map(function(d){ var itms = d.aml_pillars_violated.split(';'); itms.forEach(function(i){ split_pillars.push(i.trim()) }) });
    var pillarchartdata = count_arrays(pillar_types, split_pillars);
    pillarchartdata = convertToHCFormat(pillarchartdata);
    buildPillarChart(pillarchartdata);

    //build bsa program chart
    var bsa_types = [];
    var split_bsas = [];
    action_data.map(function(d){ var itms = d.bsa_program_area.split(';'); itms.forEach(function(i){ bsa_types.push(i.trim()) }) });
    bsa_types = bsa_types.unique();
    filtered_data.map(function(d){ var itms = d.bsa_program_area.split(';'); itms.forEach(function(i){ split_bsas.push(i.trim()) }) });
    var bsachartdata = count_arrays(bsa_types, split_bsas);
    bsachartdata = convertToHCFormat(bsachartdata);
    buildBsaChart(bsachartdata);

    //build subprogram chart
    var sub_types = [];
    var split_subs = [];
    action_data.map(function(d){ var itms = d.sub_areas.split(';'); itms.forEach(function(i){ sub_types.push(i.trim()) }) });
    sub_types = sub_types.unique();
    filtered_data.map(function(d){ var itms = d.sub_areas.split(';'); itms.forEach(function(i){ split_subs.push(i.trim()) }) });
    var subchartdata = count_arrays(sub_types, split_subs);
    subchartdata = convertToHCFormat(subchartdata);
    buildSubChart(subchartdata);
    
    var assetsize_types = [];
    var split_assetsize = [];
    action_data.map(function(d){ var itms = d.asset_size.split(';'); itms.forEach(function(i){ assetsize_types.push(i.trim()) }) });
    assetsize_types = assetsize_types.unique();
    filtered_data.map(function(d){ var itms = d.asset_size.split(';'); itms.forEach(function(i){ split_assetsize.push(i.trim()) }) });
    var assetsizechartdata = count_arrays(assetsize_types, split_assetsize);
    assetsizechartdata = convertToHCFormat(assetsizechartdata);
    buildAssetsizeChart(assetsizechartdata);

    var year_types = [];
    var split_years = [];
    year_types = action_data.map(function(d){ return d.issuance_date.trim(); });
    split_years = filtered_data.map(function(d){ return d.issuance_date.trim(); });
    var actionsperyeardata = count_arrays(split_years, split_years);
    actionsperyeardata = convertToHCFormat(actionsperyeardata);
    buildActionsperyearChart(actionsperyeardata);
  }

  // filter buttons
  $('#filters select').change(function(){
    var $this = $(this);
    
    // store filter value in object
    // i.e. filters.color = 'red'
    var group = $this.attr('data-filter-group');
    
    filters[ group ] = $this.find(':selected').attr('data-filter-value');
    // console.log( $this.find(':selected') )
    // convert object into array
    var isoFilters = [];
    for ( var prop in filters ) {
      isoFilters.push( filters[ prop ] )
    }
    console.log(filters);
    var selector = isoFilters.join('');
    //$container.isotope({ filter: selector });
    $container.isotope();
    updateFilterCount();
    updateHighlightGraphs();
    return false;
  });
      /*
      $('#filters ul>li').click(function() {
          var $this = $(this);
          var group = $this.parent().data('filter-group');
          filters[ group ] = $this.data('filter-value'); 
          var isoFilters = [];
            for ( var prop in filters ) {
              isoFilters.push( filters[ prop ] )
            }
            console.log(filters);
            var selector = isoFilters.join('');
            $container.isotope({ filter: selector });
            return false;
      });
      */


});


$('.data-selection').hide();
	
$(document).on('click', '.grid-item', function(e){ 
  var actionid = $(this).attr('data-matter-id');

	$('.grid-item').removeClass('active');



    var cardHtml = card104tmpl.render(getActionById(actionid));
    $("#action-card").html(cardHtml);


    $('#content > .col-2').addClass('show');
    $('.prod-104').addClass('show');
    $('.ea-1').show();
    $(this).addClass('active');
	e.stopPropagation(); 

	
})
    $(document).on('click', '.close-inspector', function() {
      $('#content > .col-2').removeClass('show');
      $('.prod-104').removeClass('show');
      $('.grid-item').removeClass('active');

        $('.ea-1').hide();
    });



function buildFiChart(data){
  Highcharts.chart('product104-fi', {
    chart: {
        backgroundColor: '#222222',
        plotBackgroundColor: '#222222',
    height:400,
    margin:20,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
    style: {
            fontFamily: 'monospace',
            color: "#fff"
        },

    
    },

    title: {
        text: 'Entity Type',
    y:40,
    style: {
         color: '#ffffff',

        },
     useHTML:true,

  },
    credits: false,
    tooltip: {
        enabled: true,
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {
    itemWidth:200,  
    margin:20,
    itemStyle: {
      color: '#ffffff'
    },
    itemHoverStyle: {
      color: '#ffffff'
    },
    useHTML:true,

    
  },
  plotOptions: {
        pie: {
      borderWidth:0,
      colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
      size: 180,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
        color: '#ffffff',
                enabled: false,
            },
          showInLegend: true,

        },

    },
    series: [{
        name: 'Institutions',
        colorByPoint: true,
        data: data
    }]
});

}
function buildActionsperyearChart(data){
Highcharts.chart('product104-number', {

    chart: {
        backgroundColor: '#222222',
        plotBackgroundColor: '#222222',
    plotBorderWidth:0,
    height:250,
    margin:20,
        plotBorderWidth: null,
        plotShadow: false,
    style: {
            fontFamily: 'monospace',
            color: "#fff"
        }
    },
  xAxis: {
     lineWidth: 0,
        title: false,

     minorGridLineWidth: 0,
     lineColor: 'transparent',
     labels: {
       enabled: false
     },
     minorTickLength: 0,
     tickLength: 0
  },
  yAxis:{ 
    gridLineWidth: 0,
        title: false,

    minorGridLineWidth: 0,
     labels: {
       enabled: false
     },
  },  
    title: {
        text: '',
    y:40,
    style: {
         color: '#ffffff',

        },
     useHTML:true,

  },
    credits: false,
    tooltip: {
        enabled: true,
        pointFormat: '<b>{point.value}</b>{point.y} Action(s)'
    },
    legend: {
    enabled: false

    
  },
  plotOptions: {
    line: {
          color: '#ffdd59',
    },
        pie: {
      borderWidth:0,
      colors: ['#D65527', '#17BEBB',  '#FED766', '#66c64f', '#c43949', '#464646' ],
      size: 180,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
        color: '#ffffff',
                enabled: false,
            },
          showInLegend: true,

        },

    },
    series: [{
        data: data
    }]
});
}
/*
Highcharts.chart('product104-pe', {

    chart: {
        backgroundColor: '#222222',
        plotBackgroundColor: '#222222',
		plotBorderWidth:0,
		height:250,
		margin:20,
        plotBorderWidth: null,
        plotShadow: false,
		style: {
            fontFamily: 'monospace',
            color: "#fff"
        }
    },
	xAxis: {
	   lineWidth: 0,
		    title: false,

	   minorGridLineWidth: 0,
	   lineColor: 'transparent',
	   labels: {
		   enabled: false
	   },
	   minorTickLength: 0,
	   tickLength: 0
	},
	yAxis:{ 
	  gridLineWidth: 0,
		    title: false,

	  minorGridLineWidth: 0,
	   labels: {
		   enabled: false
	   },
	},	
    title: {
        text: '',
		y:40,
		style: {
         color: '#ffffff',

    	  },
		 useHTML:true,

	},
    credits: false,
    tooltip: {
        enabled: true,
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {
    enabled: false

		
	},
	plotOptions: {
		line: {
					color: '#17BEBB',
		},
        pie: {
			borderWidth:0,
			colors: ['#D65527', '#17BEBB',  '#FED766', '#66c64f', '#c43949', '#464646' ],
			size: 180,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
				color: '#ffffff',
                enabled: false,
            },
				  showInLegend: true,

        },

    },
    series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    }]
});
*/
function buildPillarChart(data){
  Highcharts.chart('product104-pillars', {

    chart: {
        backgroundColor: '#222222',
        plotBackgroundColor: '#222222',
        plotBorderWidth: null,
        plotShadow: false,
    spacingTop:0,
    marginTop:0,
    marginBottom:80,
        type: 'pie',
    style: {
            fontFamily: 'monospace',
            color: "#fff"
        }
    },

    title: {margin:0,
        text: 'Pillar Violated',
    y:40,
    style: {
         color: '#ffffff',

        },
     useHTML:true,

  },
    credits: false,
    tooltip: {
        enabled: true,
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {
    itemWidth:200,  
    margin:40,
    itemStyle: {
      color: '#ffffff'
    },
    itemHoverStyle: {
      color: '#ffffff'
    },
    useHTML:true,

    
  },
  plotOptions: {
        pie: {
      borderWidth:0,
      colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
      size: 180,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
        color: '#ffffff',
                enabled: false,
            },
          showInLegend: true,

        },

    },
  series: [{
        name: 'AML Pillar(s) Violated',
        colorByPoint: true,
        data: data
    }]
});
}
function buildBsaChart(data){
Highcharts.chart('product104-bsa', {

    chart: {
        backgroundColor: '#222222',
        plotBackgroundColor: '#222222',
    height:380,
    margin:20,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
    style: {
            fontFamily: 'monospace',
            color: "#fff"
        }
    },

    title: {
        text: 'BSA Program Area',
    y:40,
    style: {
         color: '#ffffff',

        },
     useHTML:true,

  },
    credits: false,
    tooltip: {
        enabled: true,
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {

    margin:20,
    itemStyle: {
      color: '#ffffff'
    },
    itemHoverStyle: {
      color: '#ffffff'
    },
    useHTML:true,

    
  },
  plotOptions: {
	pie: {
		borderWidth:0,
		colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
		size: 180,
		allowPointSelect: true,
		cursor: 'pointer',
		dataLabels: {
			color: '#ffffff',
			enabled: false,
		},
		showInLegend: true,
	},

    },
  series: [{
        name: 'Areas',
        colorByPoint: true,
        data: data
    }]
});


}
function buildSubChart(data){
  Highcharts.chart('product104-subprograms', {

    chart: {
        backgroundColor: '#222222',
        plotBackgroundColor: '#222222',
        plotBorderWidth: null,
        plotShadow: false,
		spacingTop:0,
		marginTop:0,
		marginBottom:200,
        type: 'pie',
		height:580,
		style: {
			fontFamily: 'monospace',
			color: "#fff"
		}
	},
    title: {
		margin:0,
        text: 'BSA Sub-Program Area',
		y:40,
		style: {
			color: '#ffffff',
        },
		useHTML:true,
	},	
	credits: false,
    tooltip: {
        enabled: true,
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {
    itemWidth:200,  
    margin:40,
    itemStyle: {
      color: '#ffffff'
    },
    itemHoverStyle: {
      color: '#ffffff'
    },
    useHTML:false,

    
  },
  plotOptions: {
        pie: {
      borderWidth:0,
      colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6','#8e44ad', '#c0392b', '#ecf0f1', '#7f8c8d', '#bdc3c7','#27ae60', '#34495e'  ],
      size: 180,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
        color: '#ffffff',
                enabled: false,
            },
          showInLegend: true,

        },

    },
  series: [{
        name: 'Areas',
        colorByPoint: true,
        data: data
    }]
});
}

function buildAssetsizeChart(data){
  Highcharts.chart('product104-assetsize', {

    chart: {
        backgroundColor: '#222222',
        plotBackgroundColor: '#222222',
    height:380,
    margin:20,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
    style: {
            fontFamily: 'monospace',
            color: "#fff"
        }
    },

    title: {
        text: 'Entity Asset Size',
    y:40,
    style: {
         color: '#ffffff',

        },
     useHTML:true,

  },
    credits: false,
    tooltip: {
        enabled: true,
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
	  


	  
    legend: {

    margin:20,
    itemStyle: {
      color: '#ffffff'
    },
    itemHoverStyle: {
      color: '#ffffff'
    },
    useHTML:true,

    
  },
  plotOptions: {
        pie: {
      borderWidth:0,
      colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
      size: 180,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
        color: '#ffffff',
                enabled: false,
            },
          showInLegend: true,

        },

    },
  series: [{
        name: 'Areas',
        colorByPoint: true,
        data: data
    }]
});
}
/*
Highcharts.chart('product104-penaltyamount', {
    chart: {
      backgroundColor: '#222222',
      plotBackgroundColor: '#222222',
      height:400,
      margin:20,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'column',
      style: {
        fontFamily: 'monospace',
        color: "#fff"
      }
    },
    title: {
      text: 'Entity Asset Size',
      y:40,
      style: {
        color: '#ffffff',
      },
      useHTML:true,
    },
    credits: false,
    tooltip: {
        enabled: false,
    },
    legend: {
      enabled: false,
    },
    xAxis: {
        categories: ['1999','2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018'],
    },
  plotOptions: {
        pie: {
      borderWidth:0,
      colors: ['#D65527', '#0fbcf9',  '#ffdd59', '#f53b57', '#0be881', '#3c40c6' ],
      size: 180,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
        color: '#ffffff',
                enabled: false,
            },
          showInLegend: true,

        },

    },
  series: [{
        name: 'Areas',
        colorByPoint: true,
        data: [{
            name: 'Tier I',
            y: 55,
            sliced: true,
            selected: true,
      
        }, {
            name: 'Tier II',
            y: 15
        }, {
            name: 'Tier II',
            y: 10
        }, {
            name: 'Tier IV',
            y: 10
        }, {
            name: 'Tier V',
            y: 10
        }]
    }]
});
*/

}

if(window.location.href.split('/').pop() == "product-105.php"){
    /*
    var p102Data = [];
    var geop102Data = Highcharts.geojson(Highcharts.maps['countries/us/us-all-all-highres']);
    for (var i = 0; i < geop102Data.length; i++) {
      p102Data.push({
        id: geop102Data[i].properties["hc-key"],
        orig: geop102Data[i].name+ ', ' + geop102Data[i].properties['hc-key'].substr(3, 2).toUpperCase(),
        text: geop102Data[i].name+ ', ' + geop102Data[i].properties['hc-key'].substr(3, 2).toUpperCase(),
      });
    };
    p102Data = sortByKey(p102Data, "orig");
    console_data = p102Data;
    */
var p105Data = [{"id":"","orig":"","text":""},{"id":"us-sc-001","orig":"Abbeville, SC","text":"Abbeville, SC"},{"id":"us-la-001","orig":"Acadia, LA","text":"Acadia, LA"},{"id":"us-va-001","orig":"Accomack, VA","text":"Accomack, VA"},{"id":"us-id-001","orig":"Ada, ID","text":"Ada, ID"},{"id":"us-ia-001","orig":"Adair, IA","text":"Adair, IA"},{"id":"us-ky-001","orig":"Adair, KY","text":"Adair, KY"},{"id":"us-mo-001","orig":"Adair, MO","text":"Adair, MO"},{"id":"us-ok-001","orig":"Adair, OK","text":"Adair, OK"},{"id":"us-co-001","orig":"Adams, CO","text":"Adams, CO"},{"id":"us-ia-003","orig":"Adams, IA","text":"Adams, IA"},{"id":"us-id-003","orig":"Adams, ID","text":"Adams, ID"},{"id":"us-il-001","orig":"Adams, IL","text":"Adams, IL"},{"id":"us-in-001","orig":"Adams, IN","text":"Adams, IN"},{"id":"us-ms-001","orig":"Adams, MS","text":"Adams, MS"},{"id":"us-nd-001","orig":"Adams, ND","text":"Adams, ND"},{"id":"us-ne-001","orig":"Adams, NE","text":"Adams, NE"},{"id":"us-oh-001","orig":"Adams, OH","text":"Adams, OH"},{"id":"us-pa-001","orig":"Adams, PA","text":"Adams, PA"},{"id":"us-wa-001","orig":"Adams, WA","text":"Adams, WA"},{"id":"us-wi-001","orig":"Adams, WI","text":"Adams, WI"},{"id":"us-vt-001","orig":"Addison, VT","text":"Addison, VT"},{"id":"us-sc-003","orig":"Aiken, SC","text":"Aiken, SC"},{"id":"us-mn-001","orig":"Aitkin, MN","text":"Aitkin, MN"},{"id":"us-fl-001","orig":"Alachua, FL","text":"Alachua, FL"},{"id":"us-nc-001","orig":"Alamance, NC","text":"Alamance, NC"},{"id":"us-ca-001","orig":"Alameda, CA","text":"Alameda, CA"},{"id":"us-co-003","orig":"Alamosa, CO","text":"Alamosa, CO"},{"id":"us-ny-001","orig":"Albany, NY","text":"Albany, NY"},{"id":"us-wy-001","orig":"Albany, WY","text":"Albany, WY"},{"id":"us-va-003","orig":"Albemarle, VA","text":"Albemarle, VA"},{"id":"us-mi-001","orig":"Alcona, MI","text":"Alcona, MI"},{"id":"us-ms-003","orig":"Alcorn, MS","text":"Alcorn, MS"},{"id":"us-ak-013","orig":"Aleutians East, AK","text":"Aleutians East, AK"},{"id":"us-ak-016","orig":"Aleutians West, AK","text":"Aleutians West, AK"},{"id":"us-il-003","orig":"Alexander, IL","text":"Alexander, IL"},{"id":"us-nc-003","orig":"Alexander, NC","text":"Alexander, NC"},{"id":"us-va-510","orig":"Alexandria, VA","text":"Alexandria, VA"},{"id":"us-ok-003","orig":"Alfalfa, OK","text":"Alfalfa, OK"},{"id":"us-mi-003","orig":"Alger, MI","text":"Alger, MI"},{"id":"us-ia-005","orig":"Allamakee, IA","text":"Allamakee, IA"},{"id":"us-mi-005","orig":"Allegan, MI","text":"Allegan, MI"},{"id":"us-md-001","orig":"Allegany, MD","text":"Allegany, MD"},{"id":"us-ny-003","orig":"Allegany, NY","text":"Allegany, NY"},{"id":"us-nc-005","orig":"Alleghany, NC","text":"Alleghany, NC"},{"id":"us-va-005","orig":"Alleghany, VA","text":"Alleghany, VA"},{"id":"us-pa-003","orig":"Allegheny, PA","text":"Allegheny, PA"},{"id":"us-in-003","orig":"Allen, IN","text":"Allen, IN"},{"id":"us-ks-001","orig":"Allen, KS","text":"Allen, KS"},{"id":"us-ky-003","orig":"Allen, KY","text":"Allen, KY"},{"id":"us-la-003","orig":"Allen, LA","text":"Allen, LA"},{"id":"us-oh-003","orig":"Allen, OH","text":"Allen, OH"},{"id":"us-sc-005","orig":"Allendale, SC","text":"Allendale, SC"},{"id":"us-mi-007","orig":"Alpena, MI","text":"Alpena, MI"},{"id":"us-ca-003","orig":"Alpine, CA","text":"Alpine, CA"},{"id":"us-ca-005","orig":"Amador, CA","text":"Amador, CA"},{"id":"us-va-007","orig":"Amelia, VA","text":"Amelia, VA"},{"id":"us-va-009","orig":"Amherst, VA","text":"Amherst, VA"},{"id":"us-ms-005","orig":"Amite, MS","text":"Amite, MS"},{"id":"us-ak-020","orig":"Anchorage, AK","text":"Anchorage, AK"},{"id":"us-ks-003","orig":"Anderson, KS","text":"Anderson, KS"},{"id":"us-ky-005","orig":"Anderson, KY","text":"Anderson, KY"},{"id":"us-sc-007","orig":"Anderson, SC","text":"Anderson, SC"},{"id":"us-tn-001","orig":"Anderson, TN","text":"Anderson, TN"},{"id":"us-tx-001","orig":"Anderson, TX","text":"Anderson, TX"},{"id":"us-mo-003","orig":"Andrew, MO","text":"Andrew, MO"},{"id":"us-tx-003","orig":"Andrews, TX","text":"Andrews, TX"},{"id":"us-me-001","orig":"Androscoggin, ME","text":"Androscoggin, ME"},{"id":"us-tx-005","orig":"Angelina, TX","text":"Angelina, TX"},{"id":"us-md-003","orig":"Anne Arundel, MD","text":"Anne Arundel, MD"},{"id":"us-mn-003","orig":"Anoka, MN","text":"Anoka, MN"},{"id":"us-nc-007","orig":"Anson, NC","text":"Anson, NC"},{"id":"us-ne-003","orig":"Antelope, NE","text":"Antelope, NE"},{"id":"us-mi-009","orig":"Antrim, MI","text":"Antrim, MI"},{"id":"us-az-001","orig":"Apache, AZ","text":"Apache, AZ"},{"id":"us-ia-007","orig":"Appanoose, IA","text":"Appanoose, IA"},{"id":"us-ga-001","orig":"Appling, GA","text":"Appling, GA"},{"id":"us-va-011","orig":"Appomattox, VA","text":"Appomattox, VA"},{"id":"us-tx-007","orig":"Aransas, TX","text":"Aransas, TX"},{"id":"us-co-005","orig":"Arapahoe, CO","text":"Arapahoe, CO"},{"id":"us-tx-009","orig":"Archer, TX","text":"Archer, TX"},{"id":"us-co-007","orig":"Archuleta, CO","text":"Archuleta, CO"},{"id":"us-mi-011","orig":"Arenac, MI","text":"Arenac, MI"},{"id":"us-ar-001","orig":"Arkansas, AR","text":"Arkansas, AR"},{"id":"us-va-013","orig":"Arlington, VA","text":"Arlington, VA"},{"id":"us-pa-005","orig":"Armstrong, PA","text":"Armstrong, PA"},{"id":"us-tx-011","orig":"Armstrong, TX","text":"Armstrong, TX"},{"id":"us-me-003","orig":"Aroostook, ME","text":"Aroostook, ME"},{"id":"us-ne-005","orig":"Arthur, NE","text":"Arthur, NE"},{"id":"us-la-005","orig":"Ascension, LA","text":"Ascension, LA"},{"id":"us-nc-009","orig":"Ashe, NC","text":"Ashe, NC"},{"id":"us-oh-005","orig":"Ashland, OH","text":"Ashland, OH"},{"id":"us-wi-003","orig":"Ashland, WI","text":"Ashland, WI"},{"id":"us-ar-003","orig":"Ashley, AR","text":"Ashley, AR"},{"id":"us-oh-007","orig":"Ashtabula, OH","text":"Ashtabula, OH"},{"id":"us-wa-003","orig":"Asotin, WA","text":"Asotin, WA"},{"id":"us-la-007","orig":"Assumption, LA","text":"Assumption, LA"},{"id":"us-tx-013","orig":"Atascosa, TX","text":"Atascosa, TX"},{"id":"us-ks-005","orig":"Atchison, KS","text":"Atchison, KS"},{"id":"us-mo-005","orig":"Atchison, MO","text":"Atchison, MO"},{"id":"us-oh-009","orig":"Athens, OH","text":"Athens, OH"},{"id":"us-ga-003","orig":"Atkinson, GA","text":"Atkinson, GA"},{"id":"us-nj-001","orig":"Atlantic, NJ","text":"Atlantic, NJ"},{"id":"us-ok-005","orig":"Atoka, OK","text":"Atoka, OK"},{"id":"us-ms-007","orig":"Attala, MS","text":"Attala, MS"},{"id":"us-mo-007","orig":"Audrain, MO","text":"Audrain, MO"},{"id":"us-ia-009","orig":"Audubon, IA","text":"Audubon, IA"},{"id":"us-oh-011","orig":"Auglaize, OH","text":"Auglaize, OH"},{"id":"us-va-015","orig":"Augusta, VA","text":"Augusta, VA"},{"id":"us-sd-003","orig":"Aurora, SD","text":"Aurora, SD"},{"id":"us-tx-015","orig":"Austin, TX","text":"Austin, TX"},{"id":"us-al-001","orig":"Autauga, AL","text":"Autauga, AL"},{"id":"us-nc-011","orig":"Avery, NC","text":"Avery, NC"},{"id":"us-la-009","orig":"Avoyelles, LA","text":"Avoyelles, LA"},{"id":"us-co-009","orig":"Baca, CO","text":"Baca, CO"},{"id":"us-ga-005","orig":"Bacon, GA","text":"Bacon, GA"},{"id":"us-tx-017","orig":"Bailey, TX","text":"Bailey, TX"},{"id":"us-fl-003","orig":"Baker, FL","text":"Baker, FL"},{"id":"us-ga-007","orig":"Baker, GA","text":"Baker, GA"},{"id":"us-or-001","orig":"Baker, OR","text":"Baker, OR"},{"id":"us-al-003","orig":"Baldwin, AL","text":"Baldwin, AL"},{"id":"us-ga-009","orig":"Baldwin, GA","text":"Baldwin, GA"},{"id":"us-ky-007","orig":"Ballard, KY","text":"Ballard, KY"},{"id":"us-md-005","orig":"Baltimore, MD","text":"Baltimore, MD"},{"id":"us-md-510","orig":"Baltimore, MD","text":"Baltimore, MD"},{"id":"us-sc-009","orig":"Bamberg, SC","text":"Bamberg, SC"},{"id":"us-tx-019","orig":"Bandera, TX","text":"Bandera, TX"},{"id":"us-ga-011","orig":"Banks, GA","text":"Banks, GA"},{"id":"us-ne-007","orig":"Banner, NE","text":"Banner, NE"},{"id":"us-id-005","orig":"Bannock, ID","text":"Bannock, ID"},{"id":"us-mi-013","orig":"Baraga, MI","text":"Baraga, MI"},{"id":"us-ks-007","orig":"Barber, KS","text":"Barber, KS"},{"id":"us-al-005","orig":"Barbour, AL","text":"Barbour, AL"},{"id":"us-wv-001","orig":"Barbour, WV","text":"Barbour, WV"},{"id":"us-nd-003","orig":"Barnes, ND","text":"Barnes, ND"},{"id":"us-ma-001","orig":"Barnstable, MA","text":"Barnstable, MA"},{"id":"us-sc-011","orig":"Barnwell, SC","text":"Barnwell, SC"},{"id":"us-ky-009","orig":"Barren, KY","text":"Barren, KY"},{"id":"us-wi-005","orig":"Barron, WI","text":"Barron, WI"},{"id":"us-ga-013","orig":"Barrow, GA","text":"Barrow, GA"},{"id":"us-mi-015","orig":"Barry, MI","text":"Barry, MI"},{"id":"us-mo-009","orig":"Barry, MO","text":"Barry, MO"},{"id":"us-in-005","orig":"Bartholomew, IN","text":"Bartholomew, IN"},{"id":"us-ks-009","orig":"Barton, KS","text":"Barton, KS"},{"id":"us-mo-011","orig":"Barton, MO","text":"Barton, MO"},{"id":"us-ga-015","orig":"Bartow, GA","text":"Bartow, GA"},{"id":"us-tx-021","orig":"Bastrop, TX","text":"Bastrop, TX"},{"id":"us-mo-013","orig":"Bates, MO","text":"Bates, MO"},{"id":"us-ky-011","orig":"Bath, KY","text":"Bath, KY"},{"id":"us-va-017","orig":"Bath, VA","text":"Bath, VA"},{"id":"us-ar-005","orig":"Baxter, AR","text":"Baxter, AR"},{"id":"us-fl-005","orig":"Bay, FL","text":"Bay, FL"},{"id":"us-mi-017","orig":"Bay, MI","text":"Bay, MI"},{"id":"us-wi-007","orig":"Bayfield, WI","text":"Bayfield, WI"},{"id":"us-tx-023","orig":"Baylor, TX","text":"Baylor, TX"},{"id":"us-sd-005","orig":"Beadle, SD","text":"Beadle, SD"},{"id":"us-id-007","orig":"Bear Lake, ID","text":"Bear Lake, ID"},{"id":"us-nc-013","orig":"Beaufort, NC","text":"Beaufort, NC"},{"id":"us-sc-013","orig":"Beaufort, SC","text":"Beaufort, SC"},{"id":"us-la-011","orig":"Beauregard, LA","text":"Beauregard, LA"},{"id":"us-ok-007","orig":"Beaver, OK","text":"Beaver, OK"},{"id":"us-pa-007","orig":"Beaver, PA","text":"Beaver, PA"},{"id":"us-ut-001","orig":"Beaver, UT","text":"Beaver, UT"},{"id":"us-mt-001","orig":"Beaverhead, MT","text":"Beaverhead, MT"},{"id":"us-mn-005","orig":"Becker, MN","text":"Becker, MN"},{"id":"us-ok-009","orig":"Beckham, OK","text":"Beckham, OK"},{"id":"us-pa-009","orig":"Bedford, PA","text":"Bedford, PA"},{"id":"us-tn-003","orig":"Bedford, TN","text":"Bedford, TN"},{"id":"us-va-019","orig":"Bedford, VA","text":"Bedford, VA"},{"id":"us-va-515","orig":"Bedford, VA","text":"Bedford, VA"},{"id":"us-tx-025","orig":"Bee, TX","text":"Bee, TX"},{"id":"us-nh-001","orig":"Belknap, NH","text":"Belknap, NH"},{"id":"us-ky-013","orig":"Bell, KY","text":"Bell, KY"},{"id":"us-tx-027","orig":"Bell, TX","text":"Bell, TX"},{"id":"us-oh-013","orig":"Belmont, OH","text":"Belmont, OH"},{"id":"us-mn-007","orig":"Beltrami, MN","text":"Beltrami, MN"},{"id":"us-ga-017","orig":"Ben Hill, GA","text":"Ben Hill, GA"},{"id":"us-id-009","orig":"Benewah, ID","text":"Benewah, ID"},{"id":"us-sd-007","orig":"Bennett, SD","text":"Bennett, SD"},{"id":"us-vt-003","orig":"Bennington, VT","text":"Bennington, VT"},{"id":"us-nd-005","orig":"Benson, ND","text":"Benson, ND"},{"id":"us-co-011","orig":"Bent, CO","text":"Bent, CO"},{"id":"us-ar-007","orig":"Benton, AR","text":"Benton, AR"},{"id":"us-ia-011","orig":"Benton, IA","text":"Benton, IA"},{"id":"us-in-007","orig":"Benton, IN","text":"Benton, IN"},{"id":"us-mn-009","orig":"Benton, MN","text":"Benton, MN"},{"id":"us-mo-015","orig":"Benton, MO","text":"Benton, MO"},{"id":"us-ms-009","orig":"Benton, MS","text":"Benton, MS"},{"id":"us-or-003","orig":"Benton, OR","text":"Benton, OR"},{"id":"us-tn-005","orig":"Benton, TN","text":"Benton, TN"},{"id":"us-wa-005","orig":"Benton, WA","text":"Benton, WA"},{"id":"us-mi-019","orig":"Benzie, MI","text":"Benzie, MI"},{"id":"us-nj-003","orig":"Bergen, NJ","text":"Bergen, NJ"},{"id":"us-sc-015","orig":"Berkeley, SC","text":"Berkeley, SC"},{"id":"us-wv-003","orig":"Berkeley, WV","text":"Berkeley, WV"},{"id":"us-pa-011","orig":"Berks, PA","text":"Berks, PA"},{"id":"us-ma-003","orig":"Berkshire, MA","text":"Berkshire, MA"},{"id":"us-nm-001","orig":"Bernalillo, NM","text":"Bernalillo, NM"},{"id":"us-ga-019","orig":"Berrien, GA","text":"Berrien, GA"},{"id":"us-mi-021","orig":"Berrien, MI","text":"Berrien, MI"},{"id":"us-nc-015","orig":"Bertie, NC","text":"Bertie, NC"},{"id":"us-ak-050","orig":"Bethel, AK","text":"Bethel, AK"},{"id":"us-tx-029","orig":"Bexar, TX","text":"Bexar, TX"},{"id":"us-al-007","orig":"Bibb, AL","text":"Bibb, AL"},{"id":"us-ga-021","orig":"Bibb, GA","text":"Bibb, GA"},{"id":"us-la-013","orig":"Bienville, LA","text":"Bienville, LA"},{"id":"us-mt-003","orig":"Big Horn, MT","text":"Big Horn, MT"},{"id":"us-wy-003","orig":"Big Horn, WY","text":"Big Horn, WY"},{"id":"us-mn-011","orig":"Big Stone, MN","text":"Big Stone, MN"},{"id":"us-nd-007","orig":"Billings, ND","text":"Billings, ND"},{"id":"us-id-011","orig":"Bingham, ID","text":"Bingham, ID"},{"id":"us-ia-013","orig":"Black Hawk, IA","text":"Black Hawk, IA"},{"id":"us-in-009","orig":"Blackford, IN","text":"Blackford, IN"},{"id":"us-nc-017","orig":"Bladen, NC","text":"Bladen, NC"},{"id":"us-id-013","orig":"Blaine, ID","text":"Blaine, ID"},{"id":"us-mt-005","orig":"Blaine, MT","text":"Blaine, MT"},{"id":"us-ne-009","orig":"Blaine, NE","text":"Blaine, NE"},{"id":"us-ok-011","orig":"Blaine, OK","text":"Blaine, OK"},{"id":"us-pa-013","orig":"Blair, PA","text":"Blair, PA"},{"id":"us-tx-031","orig":"Blanco, TX","text":"Blanco, TX"},{"id":"us-va-021","orig":"Bland, VA","text":"Bland, VA"},{"id":"us-ga-023","orig":"Bleckley, GA","text":"Bleckley, GA"},{"id":"us-tn-007","orig":"Bledsoe, TN","text":"Bledsoe, TN"},{"id":"us-al-009","orig":"Blount, AL","text":"Blount, AL"},{"id":"us-tn-009","orig":"Blount, TN","text":"Blount, TN"},{"id":"us-mn-013","orig":"Blue Earth, MN","text":"Blue Earth, MN"},{"id":"us-id-015","orig":"Boise, ID","text":"Boise, ID"},{"id":"us-ms-011","orig":"Bolivar, MS","text":"Bolivar, MS"},{"id":"us-mo-017","orig":"Bollinger, MO","text":"Bollinger, MO"},{"id":"us-sd-009","orig":"Bon Homme, SD","text":"Bon Homme, SD"},{"id":"us-il-005","orig":"Bond, IL","text":"Bond, IL"},{"id":"us-id-017","orig":"Bonner, ID","text":"Bonner, ID"},{"id":"us-id-019","orig":"Bonneville, ID","text":"Bonneville, ID"},{"id":"us-ar-009","orig":"Boone, AR","text":"Boone, AR"},{"id":"us-ia-015","orig":"Boone, IA","text":"Boone, IA"},{"id":"us-il-007","orig":"Boone, IL","text":"Boone, IL"},{"id":"us-in-011","orig":"Boone, IN","text":"Boone, IN"},{"id":"us-ky-015","orig":"Boone, KY","text":"Boone, KY"},{"id":"us-mo-019","orig":"Boone, MO","text":"Boone, MO"},{"id":"us-ne-011","orig":"Boone, NE","text":"Boone, NE"},{"id":"us-wv-005","orig":"Boone, WV","text":"Boone, WV"},{"id":"us-tx-033","orig":"Borden, TX","text":"Borden, TX"},{"id":"us-tx-035","orig":"Bosque, TX","text":"Bosque, TX"},{"id":"us-la-015","orig":"Bossier, LA","text":"Bossier, LA"},{"id":"us-va-023","orig":"Botetourt, VA","text":"Botetourt, VA"},{"id":"us-nd-009","orig":"Bottineau, ND","text":"Bottineau, ND"},{"id":"us-co-013","orig":"Boulder, CO","text":"Boulder, CO"},{"id":"us-id-021","orig":"Boundary, ID","text":"Boundary, ID"},{"id":"us-ks-011","orig":"Bourbon, KS","text":"Bourbon, KS"},{"id":"us-ky-017","orig":"Bourbon, KY","text":"Bourbon, KY"},{"id":"us-tx-037","orig":"Bowie, TX","text":"Bowie, TX"},{"id":"us-nd-011","orig":"Bowman, ND","text":"Bowman, ND"},{"id":"us-ne-013","orig":"Box Butte, NE","text":"Box Butte, NE"},{"id":"us-ut-003","orig":"Box Elder, UT","text":"Box Elder, UT"},{"id":"us-ky-019","orig":"Boyd, KY","text":"Boyd, KY"},{"id":"us-ne-015","orig":"Boyd, NE","text":"Boyd, NE"},{"id":"us-ky-021","orig":"Boyle, KY","text":"Boyle, KY"},{"id":"us-ky-023","orig":"Bracken, KY","text":"Bracken, KY"},{"id":"us-fl-007","orig":"Bradford, FL","text":"Bradford, FL"},{"id":"us-pa-015","orig":"Bradford, PA","text":"Bradford, PA"},{"id":"us-ar-011","orig":"Bradley, AR","text":"Bradley, AR"},{"id":"us-tn-011","orig":"Bradley, TN","text":"Bradley, TN"},{"id":"us-mi-023","orig":"Branch, MI","text":"Branch, MI"},{"id":"us-ga-025","orig":"Brantley, GA","text":"Brantley, GA"},{"id":"us-wv-007","orig":"Braxton, WV","text":"Braxton, WV"},{"id":"us-tx-039","orig":"Brazoria, TX","text":"Brazoria, TX"},{"id":"us-tx-041","orig":"Brazos, TX","text":"Brazos, TX"},{"id":"us-ky-025","orig":"Breathitt, KY","text":"Breathitt, KY"},{"id":"us-ky-027","orig":"Breckinridge, KY","text":"Breckinridge, KY"},{"id":"us-ia-017","orig":"Bremer, IA","text":"Bremer, IA"},{"id":"us-fl-009","orig":"Brevard, FL","text":"Brevard, FL"},{"id":"us-tx-043","orig":"Brewster, TX","text":"Brewster, TX"},{"id":"us-tx-045","orig":"Briscoe, TX","text":"Briscoe, TX"},{"id":"us-ak-060","orig":"Bristol Bay, AK","text":"Bristol Bay, AK"},{"id":"us-ma-005","orig":"Bristol, MA","text":"Bristol, MA"},{"id":"us-ri-001","orig":"Bristol, RI","text":"Bristol, RI"},{"id":"us-va-520","orig":"Bristol, VA","text":"Bristol, VA"},{"id":"us-mt-007","orig":"Broadwater, MT","text":"Broadwater, MT"},{"id":"us-ny-005","orig":"Bronx, NY","text":"Bronx, NY"},{"id":"us-wv-009","orig":"Brooke, WV","text":"Brooke, WV"},{"id":"us-sd-011","orig":"Brookings, SD","text":"Brookings, SD"},{"id":"us-ga-027","orig":"Brooks, GA","text":"Brooks, GA"},{"id":"us-tx-047","orig":"Brooks, TX","text":"Brooks, TX"},{"id":"us-ny-007","orig":"Broome, NY","text":"Broome, NY"},{"id":"us-co-014","orig":"Broomfield, CO","text":"Broomfield, CO"},{"id":"us-fl-011","orig":"Broward, FL","text":"Broward, FL"},{"id":"us-il-009","orig":"Brown, IL","text":"Brown, IL"},{"id":"us-in-013","orig":"Brown, IN","text":"Brown, IN"},{"id":"us-ks-013","orig":"Brown, KS","text":"Brown, KS"},{"id":"us-mn-015","orig":"Brown, MN","text":"Brown, MN"},{"id":"us-ne-017","orig":"Brown, NE","text":"Brown, NE"},{"id":"us-oh-015","orig":"Brown, OH","text":"Brown, OH"},{"id":"us-sd-013","orig":"Brown, SD","text":"Brown, SD"},{"id":"us-tx-049","orig":"Brown, TX","text":"Brown, TX"},{"id":"us-wi-009","orig":"Brown, WI","text":"Brown, WI"},{"id":"us-sd-015","orig":"Brule, SD","text":"Brule, SD"},{"id":"us-nc-019","orig":"Brunswick, NC","text":"Brunswick, NC"},{"id":"us-va-025","orig":"Brunswick, VA","text":"Brunswick, VA"},{"id":"us-ga-029","orig":"Bryan, GA","text":"Bryan, GA"},{"id":"us-ok-013","orig":"Bryan, OK","text":"Bryan, OK"},{"id":"us-ia-019","orig":"Buchanan, IA","text":"Buchanan, IA"},{"id":"us-mo-021","orig":"Buchanan, MO","text":"Buchanan, MO"},{"id":"us-va-027","orig":"Buchanan, VA","text":"Buchanan, VA"},{"id":"us-va-029","orig":"Buckingham, VA","text":"Buckingham, VA"},{"id":"us-pa-017","orig":"Bucks, PA","text":"Bucks, PA"},{"id":"us-ia-021","orig":"Buena Vista, IA","text":"Buena Vista, IA"},{"id":"us-va-530","orig":"Buena Vista, VA","text":"Buena Vista, VA"},{"id":"us-ne-019","orig":"Buffalo, NE","text":"Buffalo, NE"},{"id":"us-sd-017","orig":"Buffalo, SD","text":"Buffalo, SD"},{"id":"us-wi-011","orig":"Buffalo, WI","text":"Buffalo, WI"},{"id":"us-ky-029","orig":"Bullitt, KY","text":"Bullitt, KY"},{"id":"us-ga-031","orig":"Bulloch, GA","text":"Bulloch, GA"},{"id":"us-al-011","orig":"Bullock, AL","text":"Bullock, AL"},{"id":"us-nc-021","orig":"Buncombe, NC","text":"Buncombe, NC"},{"id":"us-il-011","orig":"Bureau, IL","text":"Bureau, IL"},{"id":"us-ga-033","orig":"Burke, GA","text":"Burke, GA"},{"id":"us-nc-023","orig":"Burke, NC","text":"Burke, NC"},{"id":"us-nd-013","orig":"Burke, ND","text":"Burke, ND"},{"id":"us-nd-015","orig":"Burleigh, ND","text":"Burleigh, ND"},{"id":"us-tx-051","orig":"Burleson, TX","text":"Burleson, TX"},{"id":"us-nj-005","orig":"Burlington, NJ","text":"Burlington, NJ"},{"id":"us-tx-053","orig":"Burnet, TX","text":"Burnet, TX"},{"id":"us-wi-013","orig":"Burnett, WI","text":"Burnett, WI"},{"id":"us-ne-021","orig":"Burt, NE","text":"Burt, NE"},{"id":"us-al-013","orig":"Butler, AL","text":"Butler, AL"},{"id":"us-ia-023","orig":"Butler, IA","text":"Butler, IA"},{"id":"us-ks-015","orig":"Butler, KS","text":"Butler, KS"},{"id":"us-ky-031","orig":"Butler, KY","text":"Butler, KY"},{"id":"us-mo-023","orig":"Butler, MO","text":"Butler, MO"},{"id":"us-ne-023","orig":"Butler, NE","text":"Butler, NE"},{"id":"us-oh-017","orig":"Butler, OH","text":"Butler, OH"},{"id":"us-pa-019","orig":"Butler, PA","text":"Butler, PA"},{"id":"us-ca-007","orig":"Butte, CA","text":"Butte, CA"},{"id":"us-id-023","orig":"Butte, ID","text":"Butte, ID"},{"id":"us-sd-019","orig":"Butte, SD","text":"Butte, SD"},{"id":"us-ga-035","orig":"Butts, GA","text":"Butts, GA"},{"id":"us-nc-025","orig":"Cabarrus, NC","text":"Cabarrus, NC"},{"id":"us-wv-011","orig":"Cabell, WV","text":"Cabell, WV"},{"id":"us-ut-005","orig":"Cache, UT","text":"Cache, UT"},{"id":"us-la-017","orig":"Caddo, LA","text":"Caddo, LA"},{"id":"us-ok-015","orig":"Caddo, OK","text":"Caddo, OK"},{"id":"us-ca-009","orig":"Calaveras, CA","text":"Calaveras, CA"},{"id":"us-la-019","orig":"Calcasieu, LA","text":"Calcasieu, LA"},{"id":"us-ky-033","orig":"Caldwell, KY","text":"Caldwell, KY"},{"id":"us-la-021","orig":"Caldwell, LA","text":"Caldwell, LA"},{"id":"us-mo-025","orig":"Caldwell, MO","text":"Caldwell, MO"},{"id":"us-nc-027","orig":"Caldwell, NC","text":"Caldwell, NC"},{"id":"us-tx-055","orig":"Caldwell, TX","text":"Caldwell, TX"},{"id":"us-vt-005","orig":"Caledonia, VT","text":"Caledonia, VT"},{"id":"us-al-015","orig":"Calhoun, AL","text":"Calhoun, AL"},{"id":"us-ar-013","orig":"Calhoun, AR","text":"Calhoun, AR"},{"id":"us-fl-013","orig":"Calhoun, FL","text":"Calhoun, FL"},{"id":"us-ga-037","orig":"Calhoun, GA","text":"Calhoun, GA"},{"id":"us-ia-025","orig":"Calhoun, IA","text":"Calhoun, IA"},{"id":"us-il-013","orig":"Calhoun, IL","text":"Calhoun, IL"},{"id":"us-mi-025","orig":"Calhoun, MI","text":"Calhoun, MI"},{"id":"us-ms-013","orig":"Calhoun, MS","text":"Calhoun, MS"},{"id":"us-sc-017","orig":"Calhoun, SC","text":"Calhoun, SC"},{"id":"us-tx-057","orig":"Calhoun, TX","text":"Calhoun, TX"},{"id":"us-wv-013","orig":"Calhoun, WV","text":"Calhoun, WV"},{"id":"us-tx-059","orig":"Callahan, TX","text":"Callahan, TX"},{"id":"us-mo-027","orig":"Callaway, MO","text":"Callaway, MO"},{"id":"us-ky-035","orig":"Calloway, KY","text":"Calloway, KY"},{"id":"us-wi-015","orig":"Calumet, WI","text":"Calumet, WI"},{"id":"us-md-009","orig":"Calvert, MD","text":"Calvert, MD"},{"id":"us-id-025","orig":"Camas, ID","text":"Camas, ID"},{"id":"us-pa-021","orig":"Cambria, PA","text":"Cambria, PA"},{"id":"us-ga-039","orig":"Camden, GA","text":"Camden, GA"},{"id":"us-mo-029","orig":"Camden, MO","text":"Camden, MO"},{"id":"us-nc-029","orig":"Camden, NC","text":"Camden, NC"},{"id":"us-nj-007","orig":"Camden, NJ","text":"Camden, NJ"},{"id":"us-la-023","orig":"Cameron, LA","text":"Cameron, LA"},{"id":"us-pa-023","orig":"Cameron, PA","text":"Cameron, PA"},{"id":"us-tx-061","orig":"Cameron, TX","text":"Cameron, TX"},{"id":"us-tx-063","orig":"Camp, TX","text":"Camp, TX"},{"id":"us-ky-037","orig":"Campbell, KY","text":"Campbell, KY"},{"id":"us-sd-021","orig":"Campbell, SD","text":"Campbell, SD"},{"id":"us-tn-013","orig":"Campbell, TN","text":"Campbell, TN"},{"id":"us-va-031","orig":"Campbell, VA","text":"Campbell, VA"},{"id":"us-wy-005","orig":"Campbell, WY","text":"Campbell, WY"},{"id":"us-ok-017","orig":"Canadian, OK","text":"Canadian, OK"},{"id":"us-ga-043","orig":"Candler, GA","text":"Candler, GA"},{"id":"us-tn-015","orig":"Cannon, TN","text":"Cannon, TN"},{"id":"us-id-027","orig":"Canyon, ID","text":"Canyon, ID"},{"id":"us-mo-031","orig":"Cape Girardeau, MO","text":"Cape Girardeau, MO"},{"id":"us-nj-009","orig":"Cape May, NJ","text":"Cape May, NJ"},{"id":"us-mt-009","orig":"Carbon, MT","text":"Carbon, MT"},{"id":"us-pa-025","orig":"Carbon, PA","text":"Carbon, PA"},{"id":"us-ut-007","orig":"Carbon, UT","text":"Carbon, UT"},{"id":"us-wy-007","orig":"Carbon, WY","text":"Carbon, WY"},{"id":"us-id-029","orig":"Caribou, ID","text":"Caribou, ID"},{"id":"us-ky-039","orig":"Carlisle, KY","text":"Carlisle, KY"},{"id":"us-mn-017","orig":"Carlton, MN","text":"Carlton, MN"},{"id":"us-md-011","orig":"Caroline, MD","text":"Caroline, MD"},{"id":"us-va-033","orig":"Caroline, VA","text":"Caroline, VA"},{"id":"us-ar-015","orig":"Carroll, AR","text":"Carroll, AR"},{"id":"us-ga-045","orig":"Carroll, GA","text":"Carroll, GA"},{"id":"us-ia-027","orig":"Carroll, IA","text":"Carroll, IA"},{"id":"us-il-015","orig":"Carroll, IL","text":"Carroll, IL"},{"id":"us-in-015","orig":"Carroll, IN","text":"Carroll, IN"},{"id":"us-ky-041","orig":"Carroll, KY","text":"Carroll, KY"},{"id":"us-md-013","orig":"Carroll, MD","text":"Carroll, MD"},{"id":"us-mo-033","orig":"Carroll, MO","text":"Carroll, MO"},{"id":"us-ms-015","orig":"Carroll, MS","text":"Carroll, MS"},{"id":"us-nh-003","orig":"Carroll, NH","text":"Carroll, NH"},{"id":"us-oh-019","orig":"Carroll, OH","text":"Carroll, OH"},{"id":"us-tn-017","orig":"Carroll, TN","text":"Carroll, TN"},{"id":"us-va-035","orig":"Carroll, VA","text":"Carroll, VA"},{"id":"us-nv-510","orig":"Carson City, NV","text":"Carson City, NV"},{"id":"us-tx-065","orig":"Carson, TX","text":"Carson, TX"},{"id":"us-ky-043","orig":"Carter, KY","text":"Carter, KY"},{"id":"us-mo-035","orig":"Carter, MO","text":"Carter, MO"},{"id":"us-mt-011","orig":"Carter, MT","text":"Carter, MT"},{"id":"us-ok-019","orig":"Carter, OK","text":"Carter, OK"},{"id":"us-tn-019","orig":"Carter, TN","text":"Carter, TN"},{"id":"us-nc-031","orig":"Carteret, NC","text":"Carteret, NC"},{"id":"us-mn-019","orig":"Carver, MN","text":"Carver, MN"},{"id":"us-mt-013","orig":"Cascade, MT","text":"Cascade, MT"},{"id":"us-ky-045","orig":"Casey, KY","text":"Casey, KY"},{"id":"us-ia-029","orig":"Cass, IA","text":"Cass, IA"},{"id":"us-il-017","orig":"Cass, IL","text":"Cass, IL"},{"id":"us-in-017","orig":"Cass, IN","text":"Cass, IN"},{"id":"us-mi-027","orig":"Cass, MI","text":"Cass, MI"},{"id":"us-mn-021","orig":"Cass, MN","text":"Cass, MN"},{"id":"us-mo-037","orig":"Cass, MO","text":"Cass, MO"},{"id":"us-nd-017","orig":"Cass, ND","text":"Cass, ND"},{"id":"us-ne-025","orig":"Cass, NE","text":"Cass, NE"},{"id":"us-tx-067","orig":"Cass, TX","text":"Cass, TX"},{"id":"us-id-031","orig":"Cassia, ID","text":"Cassia, ID"},{"id":"us-tx-069","orig":"Castro, TX","text":"Castro, TX"},{"id":"us-nc-033","orig":"Caswell, NC","text":"Caswell, NC"},{"id":"us-la-025","orig":"Catahoula, LA","text":"Catahoula, LA"},{"id":"us-nc-035","orig":"Catawba, NC","text":"Catawba, NC"},{"id":"us-ga-047","orig":"Catoosa, GA","text":"Catoosa, GA"},{"id":"us-nm-003","orig":"Catron, NM","text":"Catron, NM"},{"id":"us-ny-009","orig":"Cattaraugus, NY","text":"Cattaraugus, NY"},{"id":"us-nd-019","orig":"Cavalier, ND","text":"Cavalier, ND"},{"id":"us-ny-011","orig":"Cayuga, NY","text":"Cayuga, NY"},{"id":"us-md-015","orig":"Cecil, MD","text":"Cecil, MD"},{"id":"us-ia-031","orig":"Cedar, IA","text":"Cedar, IA"},{"id":"us-mo-039","orig":"Cedar, MO","text":"Cedar, MO"},{"id":"us-ne-027","orig":"Cedar, NE","text":"Cedar, NE"},{"id":"us-pa-027","orig":"Centre, PA","text":"Centre, PA"},{"id":"us-ia-033","orig":"Cerro Gordo, IA","text":"Cerro Gordo, IA"},{"id":"us-co-015","orig":"Chaffee, CO","text":"Chaffee, CO"},{"id":"us-al-017","orig":"Chambers, AL","text":"Chambers, AL"},{"id":"us-tx-071","orig":"Chambers, TX","text":"Chambers, TX"},{"id":"us-il-019","orig":"Champaign, IL","text":"Champaign, IL"},{"id":"us-oh-021","orig":"Champaign, OH","text":"Champaign, OH"},{"id":"us-mo-041","orig":"Chariton, MO","text":"Chariton, MO"},{"id":"us-va-036","orig":"Charles City, VA","text":"Charles City, VA"},{"id":"us-sd-023","orig":"Charles Mix, SD","text":"Charles Mix, SD"},{"id":"us-md-017","orig":"Charles, MD","text":"Charles, MD"},{"id":"us-sc-019","orig":"Charleston, SC","text":"Charleston, SC"},{"id":"us-mi-029","orig":"Charlevoix, MI","text":"Charlevoix, MI"},{"id":"us-fl-015","orig":"Charlotte, FL","text":"Charlotte, FL"},{"id":"us-va-037","orig":"Charlotte, VA","text":"Charlotte, VA"},{"id":"us-va-540","orig":"Charlottesville, VA","text":"Charlottesville, VA"},{"id":"us-ga-049","orig":"Charlton, GA","text":"Charlton, GA"},{"id":"us-ks-017","orig":"Chase, KS","text":"Chase, KS"},{"id":"us-ne-029","orig":"Chase, NE","text":"Chase, NE"},{"id":"us-ga-051","orig":"Chatham, GA","text":"Chatham, GA"},{"id":"us-nc-037","orig":"Chatham, NC","text":"Chatham, NC"},{"id":"us-ga-053","orig":"Chattahoochee, GA","text":"Chattahoochee, GA"},{"id":"us-ga-055","orig":"Chattooga, GA","text":"Chattooga, GA"},{"id":"us-ks-019","orig":"Chautauqua, KS","text":"Chautauqua, KS"},{"id":"us-ny-013","orig":"Chautauqua, NY","text":"Chautauqua, NY"},{"id":"us-nm-005","orig":"Chaves, NM","text":"Chaves, NM"},{"id":"us-tn-021","orig":"Cheatham, TN","text":"Cheatham, TN"},{"id":"us-mi-031","orig":"Cheboygan, MI","text":"Cheboygan, MI"},{"id":"us-wa-007","orig":"Chelan, WA","text":"Chelan, WA"},{"id":"us-ny-015","orig":"Chemung, NY","text":"Chemung, NY"},{"id":"us-ny-017","orig":"Chenango, NY","text":"Chenango, NY"},{"id":"us-al-019","orig":"Cherokee, AL","text":"Cherokee, AL"},{"id":"us-ga-057","orig":"Cherokee, GA","text":"Cherokee, GA"},{"id":"us-ia-035","orig":"Cherokee, IA","text":"Cherokee, IA"},{"id":"us-ks-021","orig":"Cherokee, KS","text":"Cherokee, KS"},{"id":"us-nc-039","orig":"Cherokee, NC","text":"Cherokee, NC"},{"id":"us-ok-021","orig":"Cherokee, OK","text":"Cherokee, OK"},{"id":"us-sc-021","orig":"Cherokee, SC","text":"Cherokee, SC"},{"id":"us-tx-073","orig":"Cherokee, TX","text":"Cherokee, TX"},{"id":"us-ne-031","orig":"Cherry, NE","text":"Cherry, NE"},{"id":"us-va-550","orig":"Chesapeake, VA","text":"Chesapeake, VA"},{"id":"us-nh-005","orig":"Cheshire, NH","text":"Cheshire, NH"},{"id":"us-pa-029","orig":"Chester, PA","text":"Chester, PA"},{"id":"us-sc-023","orig":"Chester, SC","text":"Chester, SC"},{"id":"us-tn-023","orig":"Chester, TN","text":"Chester, TN"},{"id":"us-sc-025","orig":"Chesterfield, SC","text":"Chesterfield, SC"},{"id":"us-va-041","orig":"Chesterfield, VA","text":"Chesterfield, VA"},{"id":"us-co-017","orig":"Cheyenne, CO","text":"Cheyenne, CO"},{"id":"us-ks-023","orig":"Cheyenne, KS","text":"Cheyenne, KS"},{"id":"us-ne-033","orig":"Cheyenne, NE","text":"Cheyenne, NE"},{"id":"us-ia-037","orig":"Chickasaw, IA","text":"Chickasaw, IA"},{"id":"us-ms-017","orig":"Chickasaw, MS","text":"Chickasaw, MS"},{"id":"us-ar-017","orig":"Chicot, AR","text":"Chicot, AR"},{"id":"us-tx-075","orig":"Childress, TX","text":"Childress, TX"},{"id":"us-al-021","orig":"Chilton, AL","text":"Chilton, AL"},{"id":"us-mi-033","orig":"Chippewa, MI","text":"Chippewa, MI"},{"id":"us-mn-023","orig":"Chippewa, MN","text":"Chippewa, MN"},{"id":"us-wi-017","orig":"Chippewa, WI","text":"Chippewa, WI"},{"id":"us-mn-025","orig":"Chisago, MN","text":"Chisago, MN"},{"id":"us-vt-007","orig":"Chittenden, VT","text":"Chittenden, VT"},{"id":"us-al-023","orig":"Choctaw, AL","text":"Choctaw, AL"},{"id":"us-ms-019","orig":"Choctaw, MS","text":"Choctaw, MS"},{"id":"us-ok-023","orig":"Choctaw, OK","text":"Choctaw, OK"},{"id":"us-mt-015","orig":"Chouteau, MT","text":"Chouteau, MT"},{"id":"us-nc-041","orig":"Chowan, NC","text":"Chowan, NC"},{"id":"us-il-021","orig":"Christian, IL","text":"Christian, IL"},{"id":"us-ky-047","orig":"Christian, KY","text":"Christian, KY"},{"id":"us-mo-043","orig":"Christian, MO","text":"Christian, MO"},{"id":"us-nv-001","orig":"Churchill, NV","text":"Churchill, NV"},{"id":"us-nm-006","orig":"Cibola, NM","text":"Cibola, NM"},{"id":"us-ok-025","orig":"Cimarron, OK","text":"Cimarron, OK"},{"id":"us-fl-017","orig":"Citrus, FL","text":"Citrus, FL"},{"id":"us-or-005","orig":"Clackamas, OR","text":"Clackamas, OR"},{"id":"us-la-027","orig":"Claiborne, LA","text":"Claiborne, LA"},{"id":"us-ms-021","orig":"Claiborne, MS","text":"Claiborne, MS"},{"id":"us-tn-025","orig":"Claiborne, TN","text":"Claiborne, TN"},{"id":"us-wa-009","orig":"Clallam, WA","text":"Clallam, WA"},{"id":"us-mi-035","orig":"Clare, MI","text":"Clare, MI"},{"id":"us-sc-027","orig":"Clarendon, SC","text":"Clarendon, SC"},{"id":"us-pa-031","orig":"Clarion, PA","text":"Clarion, PA"},{"id":"us-ar-019","orig":"Clark, AR","text":"Clark, AR"},{"id":"us-id-033","orig":"Clark, ID","text":"Clark, ID"},{"id":"us-il-023","orig":"Clark, IL","text":"Clark, IL"},{"id":"us-in-019","orig":"Clark, IN","text":"Clark, IN"},{"id":"us-ks-025","orig":"Clark, KS","text":"Clark, KS"},{"id":"us-ky-049","orig":"Clark, KY","text":"Clark, KY"},{"id":"us-mo-045","orig":"Clark, MO","text":"Clark, MO"},{"id":"us-nv-003","orig":"Clark, NV","text":"Clark, NV"},{"id":"us-oh-023","orig":"Clark, OH","text":"Clark, OH"},{"id":"us-sd-025","orig":"Clark, SD","text":"Clark, SD"},{"id":"us-wa-011","orig":"Clark, WA","text":"Clark, WA"},{"id":"us-wi-019","orig":"Clark, WI","text":"Clark, WI"},{"id":"us-al-025","orig":"Clarke, AL","text":"Clarke, AL"},{"id":"us-ga-059","orig":"Clarke, GA","text":"Clarke, GA"},{"id":"us-ia-039","orig":"Clarke, IA","text":"Clarke, IA"},{"id":"us-ms-023","orig":"Clarke, MS","text":"Clarke, MS"},{"id":"us-va-043","orig":"Clarke, VA","text":"Clarke, VA"},{"id":"us-or-007","orig":"Clatsop, OR","text":"Clatsop, OR"},{"id":"us-al-027","orig":"Clay, AL","text":"Clay, AL"},{"id":"us-ar-021","orig":"Clay, AR","text":"Clay, AR"},{"id":"us-fl-019","orig":"Clay, FL","text":"Clay, FL"},{"id":"us-ga-061","orig":"Clay, GA","text":"Clay, GA"},{"id":"us-ia-041","orig":"Clay, IA","text":"Clay, IA"},{"id":"us-il-025","orig":"Clay, IL","text":"Clay, IL"},{"id":"us-in-021","orig":"Clay, IN","text":"Clay, IN"},{"id":"us-ks-027","orig":"Clay, KS","text":"Clay, KS"},{"id":"us-ky-051","orig":"Clay, KY","text":"Clay, KY"},{"id":"us-mn-027","orig":"Clay, MN","text":"Clay, MN"},{"id":"us-mo-047","orig":"Clay, MO","text":"Clay, MO"},{"id":"us-ms-025","orig":"Clay, MS","text":"Clay, MS"},{"id":"us-nc-043","orig":"Clay, NC","text":"Clay, NC"},{"id":"us-ne-035","orig":"Clay, NE","text":"Clay, NE"},{"id":"us-sd-027","orig":"Clay, SD","text":"Clay, SD"},{"id":"us-tn-027","orig":"Clay, TN","text":"Clay, TN"},{"id":"us-tx-077","orig":"Clay, TX","text":"Clay, TX"},{"id":"us-wv-015","orig":"Clay, WV","text":"Clay, WV"},{"id":"us-ga-063","orig":"Clayton, GA","text":"Clayton, GA"},{"id":"us-ia-043","orig":"Clayton, IA","text":"Clayton, IA"},{"id":"us-co-019","orig":"Clear Creek, CO","text":"Clear Creek, CO"},{"id":"us-pa-033","orig":"Clearfield, PA","text":"Clearfield, PA"},{"id":"us-id-035","orig":"Clearwater, ID","text":"Clearwater, ID"},{"id":"us-mn-029","orig":"Clearwater, MN","text":"Clearwater, MN"},{"id":"us-al-029","orig":"Cleburne, AL","text":"Cleburne, AL"},{"id":"us-ar-023","orig":"Cleburne, AR","text":"Cleburne, AR"},{"id":"us-oh-025","orig":"Clermont, OH","text":"Clermont, OH"},{"id":"us-ar-025","orig":"Cleveland, AR","text":"Cleveland, AR"},{"id":"us-nc-045","orig":"Cleveland, NC","text":"Cleveland, NC"},{"id":"us-ok-027","orig":"Cleveland, OK","text":"Cleveland, OK"},{"id":"us-ga-065","orig":"Clinch, GA","text":"Clinch, GA"},{"id":"us-ia-045","orig":"Clinton, IA","text":"Clinton, IA"},{"id":"us-il-027","orig":"Clinton, IL","text":"Clinton, IL"},{"id":"us-in-023","orig":"Clinton, IN","text":"Clinton, IN"},{"id":"us-ky-053","orig":"Clinton, KY","text":"Clinton, KY"},{"id":"us-mi-037","orig":"Clinton, MI","text":"Clinton, MI"},{"id":"us-mo-049","orig":"Clinton, MO","text":"Clinton, MO"},{"id":"us-ny-019","orig":"Clinton, NY","text":"Clinton, NY"},{"id":"us-oh-027","orig":"Clinton, OH","text":"Clinton, OH"},{"id":"us-pa-035","orig":"Clinton, PA","text":"Clinton, PA"},{"id":"us-ks-029","orig":"Cloud, KS","text":"Cloud, KS"},{"id":"us-ms-027","orig":"Coahoma, MS","text":"Coahoma, MS"},{"id":"us-ok-029","orig":"Coal, OK","text":"Coal, OK"},{"id":"us-ga-067","orig":"Cobb, GA","text":"Cobb, GA"},{"id":"us-az-003","orig":"Cochise, AZ","text":"Cochise, AZ"},{"id":"us-tx-079","orig":"Cochran, TX","text":"Cochran, TX"},{"id":"us-tn-029","orig":"Cocke, TN","text":"Cocke, TN"},{"id":"us-az-005","orig":"Coconino, AZ","text":"Coconino, AZ"},{"id":"us-sd-029","orig":"Codington, SD","text":"Codington, SD"},{"id":"us-al-031","orig":"Coffee, AL","text":"Coffee, AL"},{"id":"us-ga-069","orig":"Coffee, GA","text":"Coffee, GA"},{"id":"us-tn-031","orig":"Coffee, TN","text":"Coffee, TN"},{"id":"us-ks-031","orig":"Coffey, KS","text":"Coffey, KS"},{"id":"us-tx-081","orig":"Coke, TX","text":"Coke, TX"},{"id":"us-al-033","orig":"Colbert, AL","text":"Colbert, AL"},{"id":"us-mo-051","orig":"Cole, MO","text":"Cole, MO"},{"id":"us-tx-083","orig":"Coleman, TX","text":"Coleman, TX"},{"id":"us-il-029","orig":"Coles, IL","text":"Coles, IL"},{"id":"us-ne-037","orig":"Colfax, NE","text":"Colfax, NE"},{"id":"us-nm-007","orig":"Colfax, NM","text":"Colfax, NM"},{"id":"us-sc-029","orig":"Colleton, SC","text":"Colleton, SC"},{"id":"us-fl-021","orig":"Collier, FL","text":"Collier, FL"},{"id":"us-tx-085","orig":"Collin, TX","text":"Collin, TX"},{"id":"us-tx-087","orig":"Collingsworth, TX","text":"Collingsworth, TX"},{"id":"us-va-570","orig":"Colonial Heights, VA","text":"Colonial Heights, VA"},{"id":"us-tx-089","orig":"Colorado, TX","text":"Colorado, TX"},{"id":"us-ga-071","orig":"Colquitt, GA","text":"Colquitt, GA"},{"id":"us-ar-027","orig":"Columbia, AR","text":"Columbia, AR"},{"id":"us-fl-023","orig":"Columbia, FL","text":"Columbia, FL"},{"id":"us-ga-073","orig":"Columbia, GA","text":"Columbia, GA"},{"id":"us-ny-021","orig":"Columbia, NY","text":"Columbia, NY"},{"id":"us-or-009","orig":"Columbia, OR","text":"Columbia, OR"},{"id":"us-pa-037","orig":"Columbia, PA","text":"Columbia, PA"},{"id":"us-wa-013","orig":"Columbia, WA","text":"Columbia, WA"},{"id":"us-wi-021","orig":"Columbia, WI","text":"Columbia, WI"},{"id":"us-oh-029","orig":"Columbiana, OH","text":"Columbiana, OH"},{"id":"us-nc-047","orig":"Columbus, NC","text":"Columbus, NC"},{"id":"us-ca-011","orig":"Colusa, CA","text":"Colusa, CA"},{"id":"us-tx-091","orig":"Comal, TX","text":"Comal, TX"},{"id":"us-ks-033","orig":"Comanche, KS","text":"Comanche, KS"},{"id":"us-ok-031","orig":"Comanche, OK","text":"Comanche, OK"},{"id":"us-tx-093","orig":"Comanche, TX","text":"Comanche, TX"},{"id":"us-tx-095","orig":"Concho, TX","text":"Concho, TX"},{"id":"us-la-029","orig":"Concordia, LA","text":"Concordia, LA"},{"id":"us-al-035","orig":"Conecuh, AL","text":"Conecuh, AL"},{"id":"us-co-021","orig":"Conejos, CO","text":"Conejos, CO"},{"id":"us-ca-013","orig":"Contra Costa, CA","text":"Contra Costa, CA"},{"id":"us-wy-009","orig":"Converse, WY","text":"Converse, WY"},{"id":"us-ar-029","orig":"Conway, AR","text":"Conway, AR"},{"id":"us-ga-075","orig":"Cook, GA","text":"Cook, GA"},{"id":"us-il-031","orig":"Cook, IL","text":"Cook, IL"},{"id":"us-mn-031","orig":"Cook, MN","text":"Cook, MN"},{"id":"us-tx-097","orig":"Cooke, TX","text":"Cooke, TX"},{"id":"us-mo-053","orig":"Cooper, MO","text":"Cooper, MO"},{"id":"us-nh-007","orig":"Coos, NH","text":"Coos, NH"},{"id":"us-or-011","orig":"Coos, OR","text":"Coos, OR"},{"id":"us-al-037","orig":"Coosa, AL","text":"Coosa, AL"},{"id":"us-ms-029","orig":"Copiah, MS","text":"Copiah, MS"},{"id":"us-sd-031","orig":"Corson, SD","text":"Corson, SD"},{"id":"us-ny-023","orig":"Cortland, NY","text":"Cortland, NY"},{"id":"us-tx-099","orig":"Coryell, TX","text":"Coryell, TX"},{"id":"us-oh-031","orig":"Coshocton, OH","text":"Coshocton, OH"},{"id":"us-co-023","orig":"Costilla, CO","text":"Costilla, CO"},{"id":"us-tx-101","orig":"Cottle, TX","text":"Cottle, TX"},{"id":"us-ok-033","orig":"Cotton, OK","text":"Cotton, OK"},{"id":"us-mn-033","orig":"Cottonwood, MN","text":"Cottonwood, MN"},{"id":"us-al-039","orig":"Covington, AL","text":"Covington, AL"},{"id":"us-ms-031","orig":"Covington, MS","text":"Covington, MS"},{"id":"us-va-580","orig":"Covington, VA","text":"Covington, VA"},{"id":"us-ga-077","orig":"Coweta, GA","text":"Coweta, GA"},{"id":"us-ks-035","orig":"Cowley, KS","text":"Cowley, KS"},{"id":"us-wa-015","orig":"Cowlitz, WA","text":"Cowlitz, WA"},{"id":"us-ok-035","orig":"Craig, OK","text":"Craig, OK"},{"id":"us-va-045","orig":"Craig, VA","text":"Craig, VA"},{"id":"us-ar-031","orig":"Craighead, AR","text":"Craighead, AR"},{"id":"us-tx-103","orig":"Crane, TX","text":"Crane, TX"},{"id":"us-nc-049","orig":"Craven, NC","text":"Craven, NC"},{"id":"us-ar-033","orig":"Crawford, AR","text":"Crawford, AR"},{"id":"us-ga-079","orig":"Crawford, GA","text":"Crawford, GA"},{"id":"us-ia-047","orig":"Crawford, IA","text":"Crawford, IA"},{"id":"us-il-033","orig":"Crawford, IL","text":"Crawford, IL"},{"id":"us-in-025","orig":"Crawford, IN","text":"Crawford, IN"},{"id":"us-ks-037","orig":"Crawford, KS","text":"Crawford, KS"},{"id":"us-mi-039","orig":"Crawford, MI","text":"Crawford, MI"},{"id":"us-mo-055","orig":"Crawford, MO","text":"Crawford, MO"},{"id":"us-oh-033","orig":"Crawford, OH","text":"Crawford, OH"},{"id":"us-pa-039","orig":"Crawford, PA","text":"Crawford, PA"},{"id":"us-wi-023","orig":"Crawford, WI","text":"Crawford, WI"},{"id":"us-ok-037","orig":"Creek, OK","text":"Creek, OK"},{"id":"us-al-041","orig":"Crenshaw, AL","text":"Crenshaw, AL"},{"id":"us-ga-081","orig":"Crisp, GA","text":"Crisp, GA"},{"id":"us-ar-035","orig":"Crittenden, AR","text":"Crittenden, AR"},{"id":"us-ky-055","orig":"Crittenden, KY","text":"Crittenden, KY"},{"id":"us-tn-033","orig":"Crockett, TN","text":"Crockett, TN"},{"id":"us-tx-105","orig":"Crockett, TX","text":"Crockett, TX"},{"id":"us-or-013","orig":"Crook, OR","text":"Crook, OR"},{"id":"us-wy-011","orig":"Crook, WY","text":"Crook, WY"},{"id":"us-tx-107","orig":"Crosby, TX","text":"Crosby, TX"},{"id":"us-ar-037","orig":"Cross, AR","text":"Cross, AR"},{"id":"us-mn-035","orig":"Crow Wing, MN","text":"Crow Wing, MN"},{"id":"us-co-025","orig":"Crowley, CO","text":"Crowley, CO"},{"id":"us-tx-109","orig":"Culberson, TX","text":"Culberson, TX"},{"id":"us-al-043","orig":"Cullman, AL","text":"Cullman, AL"},{"id":"us-va-047","orig":"Culpeper, VA","text":"Culpeper, VA"},{"id":"us-il-035","orig":"Cumberland, IL","text":"Cumberland, IL"},{"id":"us-ky-057","orig":"Cumberland, KY","text":"Cumberland, KY"},{"id":"us-me-005","orig":"Cumberland, ME","text":"Cumberland, ME"},{"id":"us-nc-051","orig":"Cumberland, NC","text":"Cumberland, NC"},{"id":"us-nj-011","orig":"Cumberland, NJ","text":"Cumberland, NJ"},{"id":"us-pa-041","orig":"Cumberland, PA","text":"Cumberland, PA"},{"id":"us-tn-035","orig":"Cumberland, TN","text":"Cumberland, TN"},{"id":"us-va-049","orig":"Cumberland, VA","text":"Cumberland, VA"},{"id":"us-ne-039","orig":"Cuming, NE","text":"Cuming, NE"},{"id":"us-nc-053","orig":"Currituck, NC","text":"Currituck, NC"},{"id":"us-nm-009","orig":"Curry, NM","text":"Curry, NM"},{"id":"us-or-015","orig":"Curry, OR","text":"Curry, OR"},{"id":"us-co-027","orig":"Custer, CO","text":"Custer, CO"},{"id":"us-id-037","orig":"Custer, ID","text":"Custer, ID"},{"id":"us-mt-017","orig":"Custer, MT","text":"Custer, MT"},{"id":"us-ne-041","orig":"Custer, NE","text":"Custer, NE"},{"id":"us-ok-039","orig":"Custer, OK","text":"Custer, OK"},{"id":"us-sd-033","orig":"Custer, SD","text":"Custer, SD"},{"id":"us-oh-035","orig":"Cuyahoga, OH","text":"Cuyahoga, OH"},{"id":"us-ga-083","orig":"Dade, GA","text":"Dade, GA"},{"id":"us-mo-057","orig":"Dade, MO","text":"Dade, MO"},{"id":"us-ut-009","orig":"Daggett, UT","text":"Daggett, UT"},{"id":"us-mn-037","orig":"Dakota, MN","text":"Dakota, MN"},{"id":"us-ne-043","orig":"Dakota, NE","text":"Dakota, NE"},{"id":"us-al-045","orig":"Dale, AL","text":"Dale, AL"},{"id":"us-tx-111","orig":"Dallam, TX","text":"Dallam, TX"},{"id":"us-al-047","orig":"Dallas, AL","text":"Dallas, AL"},{"id":"us-ar-039","orig":"Dallas, AR","text":"Dallas, AR"},{"id":"us-ia-049","orig":"Dallas, IA","text":"Dallas, IA"},{"id":"us-mo-059","orig":"Dallas, MO","text":"Dallas, MO"},{"id":"us-tx-113","orig":"Dallas, TX","text":"Dallas, TX"},{"id":"us-wi-025","orig":"Dane, WI","text":"Dane, WI"},{"id":"us-mt-019","orig":"Daniels, MT","text":"Daniels, MT"},{"id":"us-va-590","orig":"Danville, VA","text":"Danville, VA"},{"id":"us-nc-055","orig":"Dare, NC","text":"Dare, NC"},{"id":"us-oh-037","orig":"Darke, OH","text":"Darke, OH"},{"id":"us-sc-031","orig":"Darlington, SC","text":"Darlington, SC"},{"id":"us-pa-043","orig":"Dauphin, PA","text":"Dauphin, PA"},{"id":"us-nc-057","orig":"Davidson, NC","text":"Davidson, NC"},{"id":"us-tn-037","orig":"Davidson, TN","text":"Davidson, TN"},{"id":"us-nc-059","orig":"Davie, NC","text":"Davie, NC"},{"id":"us-in-027","orig":"Daviess, IN","text":"Daviess, IN"},{"id":"us-ky-059","orig":"Daviess, KY","text":"Daviess, KY"},{"id":"us-mo-061","orig":"Daviess, MO","text":"Daviess, MO"},{"id":"us-ia-051","orig":"Davis, IA","text":"Davis, IA"},{"id":"us-ut-011","orig":"Davis, UT","text":"Davis, UT"},{"id":"us-sd-035","orig":"Davison, SD","text":"Davison, SD"},{"id":"us-ne-045","orig":"Dawes, NE","text":"Dawes, NE"},{"id":"us-ga-085","orig":"Dawson, GA","text":"Dawson, GA"},{"id":"us-mt-021","orig":"Dawson, MT","text":"Dawson, MT"},{"id":"us-ne-047","orig":"Dawson, NE","text":"Dawson, NE"},{"id":"us-tx-115","orig":"Dawson, TX","text":"Dawson, TX"},{"id":"us-sd-037","orig":"Day, SD","text":"Day, SD"},{"id":"us-nm-011","orig":"De Baca, NM","text":"De Baca, NM"},{"id":"us-la-031","orig":"De Soto, LA","text":"De Soto, LA"},{"id":"us-il-039","orig":"De Witt, IL","text":"De Witt, IL"},{"id":"us-al-049","orig":"DeKalb, AL","text":"DeKalb, AL"},{"id":"us-ga-089","orig":"DeKalb, GA","text":"DeKalb, GA"},{"id":"us-il-037","orig":"DeKalb, IL","text":"DeKalb, IL"},{"id":"us-in-033","orig":"DeKalb, IN","text":"DeKalb, IN"},{"id":"us-mo-063","orig":"DeKalb, MO","text":"DeKalb, MO"},{"id":"us-tn-041","orig":"DeKalb, TN","text":"DeKalb, TN"},{"id":"us-fl-027","orig":"DeSoto, FL","text":"DeSoto, FL"},{"id":"us-ms-033","orig":"DeSoto, MS","text":"DeSoto, MS"},{"id":"us-tx-123","orig":"DeWitt, TX","text":"DeWitt, TX"},{"id":"us-tx-117","orig":"Deaf Smith, TX","text":"Deaf Smith, TX"},{"id":"us-in-029","orig":"Dearborn, IN","text":"Dearborn, IN"},{"id":"us-ga-087","orig":"Decatur, GA","text":"Decatur, GA"},{"id":"us-ia-053","orig":"Decatur, IA","text":"Decatur, IA"},{"id":"us-in-031","orig":"Decatur, IN","text":"Decatur, IN"},{"id":"us-ks-039","orig":"Decatur, KS","text":"Decatur, KS"},{"id":"us-tn-039","orig":"Decatur, TN","text":"Decatur, TN"},{"id":"us-mt-023","orig":"Deer Lodge, MT","text":"Deer Lodge, MT"},{"id":"us-oh-039","orig":"Defiance, OH","text":"Defiance, OH"},{"id":"us-ca-015","orig":"Del Norte, CA","text":"Del Norte, CA"},{"id":"us-ia-055","orig":"Delaware, IA","text":"Delaware, IA"},{"id":"us-in-035","orig":"Delaware, IN","text":"Delaware, IN"},{"id":"us-ny-025","orig":"Delaware, NY","text":"Delaware, NY"},{"id":"us-oh-041","orig":"Delaware, OH","text":"Delaware, OH"},{"id":"us-ok-041","orig":"Delaware, OK","text":"Delaware, OK"},{"id":"us-pa-045","orig":"Delaware, PA","text":"Delaware, PA"},{"id":"us-co-029","orig":"Delta, CO","text":"Delta, CO"},{"id":"us-mi-041","orig":"Delta, MI","text":"Delta, MI"},{"id":"us-tx-119","orig":"Delta, TX","text":"Delta, TX"},{"id":"us-ak-068","orig":"Denali, AK","text":"Denali, AK"},{"id":"us-mo-065","orig":"Dent, MO","text":"Dent, MO"},{"id":"us-tx-121","orig":"Denton, TX","text":"Denton, TX"},{"id":"us-co-031","orig":"Denver, CO","text":"Denver, CO"},{"id":"us-ia-057","orig":"Des Moines, IA","text":"Des Moines, IA"},{"id":"us-or-017","orig":"Deschutes, OR","text":"Deschutes, OR"},{"id":"us-ar-041","orig":"Desha, AR","text":"Desha, AR"},{"id":"us-ne-049","orig":"Deuel, NE","text":"Deuel, NE"},{"id":"us-sd-039","orig":"Deuel, SD","text":"Deuel, SD"},{"id":"us-ok-043","orig":"Dewey, OK","text":"Dewey, OK"},{"id":"us-sd-041","orig":"Dewey, SD","text":"Dewey, SD"},{"id":"us-tx-125","orig":"Dickens, TX","text":"Dickens, TX"},{"id":"us-va-051","orig":"Dickenson, VA","text":"Dickenson, VA"},{"id":"us-nd-021","orig":"Dickey, ND","text":"Dickey, ND"},{"id":"us-ia-059","orig":"Dickinson, IA","text":"Dickinson, IA"},{"id":"us-ks-041","orig":"Dickinson, KS","text":"Dickinson, KS"},{"id":"us-mi-043","orig":"Dickinson, MI","text":"Dickinson, MI"},{"id":"us-tn-043","orig":"Dickson, TN","text":"Dickson, TN"},{"id":"us-ak-070","orig":"Dillingham, AK","text":"Dillingham, AK"},{"id":"us-sc-033","orig":"Dillon, SC","text":"Dillon, SC"},{"id":"us-tx-127","orig":"Dimmit, TX","text":"Dimmit, TX"},{"id":"us-va-053","orig":"Dinwiddie, VA","text":"Dinwiddie, VA"},{"id":"us-dc-001","orig":"District of Columbia, DC","text":"District of Columbia, DC"},{"id":"us-nd-023","orig":"Divide, ND","text":"Divide, ND"},{"id":"us-fl-029","orig":"Dixie, FL","text":"Dixie, FL"},{"id":"us-ne-051","orig":"Dixon, NE","text":"Dixon, NE"},{"id":"us-wv-017","orig":"Doddridge, WV","text":"Doddridge, WV"},{"id":"us-ga-091","orig":"Dodge, GA","text":"Dodge, GA"},{"id":"us-mn-039","orig":"Dodge, MN","text":"Dodge, MN"},{"id":"us-ne-053","orig":"Dodge, NE","text":"Dodge, NE"},{"id":"us-wi-027","orig":"Dodge, WI","text":"Dodge, WI"},{"id":"us-co-033","orig":"Dolores, CO","text":"Dolores, CO"},{"id":"us-ks-043","orig":"Doniphan, KS","text":"Doniphan, KS"},{"id":"us-tx-129","orig":"Donley, TX","text":"Donley, TX"},{"id":"us-ga-093","orig":"Dooly, GA","text":"Dooly, GA"},{"id":"us-wi-029","orig":"Door, WI","text":"Door, WI"},{"id":"us-md-019","orig":"Dorchester, MD","text":"Dorchester, MD"},{"id":"us-sc-035","orig":"Dorchester, SC","text":"Dorchester, SC"},{"id":"us-ga-095","orig":"Dougherty, GA","text":"Dougherty, GA"},{"id":"us-co-035","orig":"Douglas, CO","text":"Douglas, CO"},{"id":"us-ga-097","orig":"Douglas, GA","text":"Douglas, GA"},{"id":"us-il-041","orig":"Douglas, IL","text":"Douglas, IL"},{"id":"us-ks-045","orig":"Douglas, KS","text":"Douglas, KS"},{"id":"us-mn-041","orig":"Douglas, MN","text":"Douglas, MN"},{"id":"us-mo-067","orig":"Douglas, MO","text":"Douglas, MO"},{"id":"us-ne-055","orig":"Douglas, NE","text":"Douglas, NE"},{"id":"us-nv-005","orig":"Douglas, NV","text":"Douglas, NV"},{"id":"us-or-019","orig":"Douglas, OR","text":"Douglas, OR"},{"id":"us-sd-043","orig":"Douglas, SD","text":"Douglas, SD"},{"id":"us-wa-017","orig":"Douglas, WA","text":"Douglas, WA"},{"id":"us-wi-031","orig":"Douglas, WI","text":"Douglas, WI"},{"id":"us-nm-013","orig":"Doña Ana, NM","text":"Doña Ana, NM"},{"id":"us-ar-043","orig":"Drew, AR","text":"Drew, AR"},{"id":"us-il-043","orig":"DuPage, IL","text":"DuPage, IL"},{"id":"us-in-037","orig":"Dubois, IN","text":"Dubois, IN"},{"id":"us-ia-061","orig":"Dubuque, IA","text":"Dubuque, IA"},{"id":"us-ut-013","orig":"Duchesne, UT","text":"Duchesne, UT"},{"id":"us-ma-007","orig":"Dukes, MA","text":"Dukes, MA"},{"id":"us-ne-057","orig":"Dundy, NE","text":"Dundy, NE"},{"id":"us-mo-069","orig":"Dunklin, MO","text":"Dunklin, MO"},{"id":"us-nd-025","orig":"Dunn, ND","text":"Dunn, ND"},{"id":"us-wi-033","orig":"Dunn, WI","text":"Dunn, WI"},{"id":"us-nc-061","orig":"Duplin, NC","text":"Duplin, NC"},{"id":"us-nc-063","orig":"Durham, NC","text":"Durham, NC"},{"id":"us-ny-027","orig":"Dutchess, NY","text":"Dutchess, NY"},{"id":"us-fl-031","orig":"Duval, FL","text":"Duval, FL"},{"id":"us-tx-131","orig":"Duval, TX","text":"Duval, TX"},{"id":"us-tn-045","orig":"Dyer, TN","text":"Dyer, TN"},{"id":"us-co-037","orig":"Eagle, CO","text":"Eagle, CO"},{"id":"us-ga-099","orig":"Early, GA","text":"Early, GA"},{"id":"us-la-033","orig":"East Baton Rouge, LA","text":"East Baton Rouge, LA"},{"id":"us-la-035","orig":"East Carroll, LA","text":"East Carroll, LA"},{"id":"us-la-037","orig":"East Feliciana, LA","text":"East Feliciana, LA"},{"id":"us-tx-133","orig":"Eastland, TX","text":"Eastland, TX"},{"id":"us-mi-045","orig":"Eaton, MI","text":"Eaton, MI"},{"id":"us-wi-035","orig":"Eau Claire, WI","text":"Eau Claire, WI"},{"id":"us-ga-101","orig":"Echols, GA","text":"Echols, GA"},{"id":"us-tx-135","orig":"Ector, TX","text":"Ector, TX"},{"id":"us-nd-027","orig":"Eddy, ND","text":"Eddy, ND"},{"id":"us-nm-015","orig":"Eddy, NM","text":"Eddy, NM"},{"id":"us-il-045","orig":"Edgar, IL","text":"Edgar, IL"},{"id":"us-nc-065","orig":"Edgecombe, NC","text":"Edgecombe, NC"},{"id":"us-sc-037","orig":"Edgefield, SC","text":"Edgefield, SC"},{"id":"us-ky-061","orig":"Edmonson, KY","text":"Edmonson, KY"},{"id":"us-sd-045","orig":"Edmunds, SD","text":"Edmunds, SD"},{"id":"us-il-047","orig":"Edwards, IL","text":"Edwards, IL"},{"id":"us-ks-047","orig":"Edwards, KS","text":"Edwards, KS"},{"id":"us-tx-137","orig":"Edwards, TX","text":"Edwards, TX"},{"id":"us-ga-103","orig":"Effingham, GA","text":"Effingham, GA"},{"id":"us-il-049","orig":"Effingham, IL","text":"Effingham, IL"},{"id":"us-ca-017","orig":"El Dorado, CA","text":"El Dorado, CA"},{"id":"us-co-041","orig":"El Paso, CO","text":"El Paso, CO"},{"id":"us-tx-141","orig":"El Paso, TX","text":"El Paso, TX"},{"id":"us-co-039","orig":"Elbert, CO","text":"Elbert, CO"},{"id":"us-ga-105","orig":"Elbert, GA","text":"Elbert, GA"},{"id":"us-ks-049","orig":"Elk, KS","text":"Elk, KS"},{"id":"us-pa-047","orig":"Elk, PA","text":"Elk, PA"},{"id":"us-in-039","orig":"Elkhart, IN","text":"Elkhart, IN"},{"id":"us-nv-007","orig":"Elko, NV","text":"Elko, NV"},{"id":"us-ky-063","orig":"Elliott, KY","text":"Elliott, KY"},{"id":"us-ks-051","orig":"Ellis, KS","text":"Ellis, KS"},{"id":"us-ok-045","orig":"Ellis, OK","text":"Ellis, OK"},{"id":"us-tx-139","orig":"Ellis, TX","text":"Ellis, TX"},{"id":"us-ks-053","orig":"Ellsworth, KS","text":"Ellsworth, KS"},{"id":"us-al-051","orig":"Elmore, AL","text":"Elmore, AL"},{"id":"us-id-039","orig":"Elmore, ID","text":"Elmore, ID"},{"id":"us-ga-107","orig":"Emanuel, GA","text":"Emanuel, GA"},{"id":"us-ut-015","orig":"Emery, UT","text":"Emery, UT"},{"id":"us-ia-063","orig":"Emmet, IA","text":"Emmet, IA"},{"id":"us-mi-047","orig":"Emmet, MI","text":"Emmet, MI"},{"id":"us-nd-029","orig":"Emmons, ND","text":"Emmons, ND"},{"id":"us-va-595","orig":"Emporia, VA","text":"Emporia, VA"},{"id":"us-tx-143","orig":"Erath, TX","text":"Erath, TX"},{"id":"us-ny-029","orig":"Erie, NY","text":"Erie, NY"},{"id":"us-oh-043","orig":"Erie, OH","text":"Erie, OH"},{"id":"us-pa-049","orig":"Erie, PA","text":"Erie, PA"},{"id":"us-al-053","orig":"Escambia, AL","text":"Escambia, AL"},{"id":"us-fl-033","orig":"Escambia, FL","text":"Escambia, FL"},{"id":"us-nv-009","orig":"Esmeralda, NV","text":"Esmeralda, NV"},{"id":"us-ma-009","orig":"Essex, MA","text":"Essex, MA"},{"id":"us-nj-013","orig":"Essex, NJ","text":"Essex, NJ"},{"id":"us-ny-031","orig":"Essex, NY","text":"Essex, NY"},{"id":"us-va-057","orig":"Essex, VA","text":"Essex, VA"},{"id":"us-vt-009","orig":"Essex, VT","text":"Essex, VT"},{"id":"us-ky-065","orig":"Estill, KY","text":"Estill, KY"},{"id":"us-al-055","orig":"Etowah, AL","text":"Etowah, AL"},{"id":"us-nv-011","orig":"Eureka, NV","text":"Eureka, NV"},{"id":"us-la-039","orig":"Evangeline, LA","text":"Evangeline, LA"},{"id":"us-ga-109","orig":"Evans, GA","text":"Evans, GA"},{"id":"us-ak-090","orig":"Fairbanks North Star, AK","text":"Fairbanks North Star, AK"},{"id":"us-va-059","orig":"Fairfax, VA","text":"Fairfax, VA"},{"id":"us-va-600","orig":"Fairfax, VA","text":"Fairfax, VA"},{"id":"us-ct-001","orig":"Fairfield, CT","text":"Fairfield, CT"},{"id":"us-oh-045","orig":"Fairfield, OH","text":"Fairfield, OH"},{"id":"us-sc-039","orig":"Fairfield, SC","text":"Fairfield, SC"},{"id":"us-sd-047","orig":"Fall River, SD","text":"Fall River, SD"},{"id":"us-mt-025","orig":"Fallon, MT","text":"Fallon, MT"},{"id":"us-va-610","orig":"Falls Church, VA","text":"Falls Church, VA"},{"id":"us-tx-145","orig":"Falls, TX","text":"Falls, TX"},{"id":"us-ga-111","orig":"Fannin, GA","text":"Fannin, GA"},{"id":"us-tx-147","orig":"Fannin, TX","text":"Fannin, TX"},{"id":"us-mn-043","orig":"Faribault, MN","text":"Faribault, MN"},{"id":"us-sd-049","orig":"Faulk, SD","text":"Faulk, SD"},{"id":"us-ar-045","orig":"Faulkner, AR","text":"Faulkner, AR"},{"id":"us-va-061","orig":"Fauquier, VA","text":"Fauquier, VA"},{"id":"us-al-057","orig":"Fayette, AL","text":"Fayette, AL"},{"id":"us-ga-113","orig":"Fayette, GA","text":"Fayette, GA"},{"id":"us-ia-065","orig":"Fayette, IA","text":"Fayette, IA"},{"id":"us-il-051","orig":"Fayette, IL","text":"Fayette, IL"},{"id":"us-in-041","orig":"Fayette, IN","text":"Fayette, IN"},{"id":"us-ky-067","orig":"Fayette, KY","text":"Fayette, KY"},{"id":"us-oh-047","orig":"Fayette, OH","text":"Fayette, OH"},{"id":"us-pa-051","orig":"Fayette, PA","text":"Fayette, PA"},{"id":"us-tn-047","orig":"Fayette, TN","text":"Fayette, TN"},{"id":"us-tx-149","orig":"Fayette, TX","text":"Fayette, TX"},{"id":"us-wv-019","orig":"Fayette, WV","text":"Fayette, WV"},{"id":"us-tn-049","orig":"Fentress, TN","text":"Fentress, TN"},{"id":"us-mt-027","orig":"Fergus, MT","text":"Fergus, MT"},{"id":"us-wa-019","orig":"Ferry, WA","text":"Ferry, WA"},{"id":"us-mn-045","orig":"Fillmore, MN","text":"Fillmore, MN"},{"id":"us-ne-059","orig":"Fillmore, NE","text":"Fillmore, NE"},{"id":"us-ks-055","orig":"Finney, KS","text":"Finney, KS"},{"id":"us-tx-151","orig":"Fisher, TX","text":"Fisher, TX"},{"id":"us-fl-035","orig":"Flagler, FL","text":"Flagler, FL"},{"id":"us-mt-029","orig":"Flathead, MT","text":"Flathead, MT"},{"id":"us-ky-069","orig":"Fleming, KY","text":"Fleming, KY"},{"id":"us-sc-041","orig":"Florence, SC","text":"Florence, SC"},{"id":"us-wi-037","orig":"Florence, WI","text":"Florence, WI"},{"id":"us-ga-115","orig":"Floyd, GA","text":"Floyd, GA"},{"id":"us-ia-067","orig":"Floyd, IA","text":"Floyd, IA"},{"id":"us-in-043","orig":"Floyd, IN","text":"Floyd, IN"},{"id":"us-ky-071","orig":"Floyd, KY","text":"Floyd, KY"},{"id":"us-tx-153","orig":"Floyd, TX","text":"Floyd, TX"},{"id":"us-va-063","orig":"Floyd, VA","text":"Floyd, VA"},{"id":"us-va-065","orig":"Fluvanna, VA","text":"Fluvanna, VA"},{"id":"us-tx-155","orig":"Foard, TX","text":"Foard, TX"},{"id":"us-wi-039","orig":"Fond du Lac, WI","text":"Fond du Lac, WI"},{"id":"us-il-053","orig":"Ford, IL","text":"Ford, IL"},{"id":"us-ks-057","orig":"Ford, KS","text":"Ford, KS"},{"id":"us-pa-053","orig":"Forest, PA","text":"Forest, PA"},{"id":"us-wi-041","orig":"Forest, WI","text":"Forest, WI"},{"id":"us-ms-035","orig":"Forrest, MS","text":"Forrest, MS"},{"id":"us-ga-117","orig":"Forsyth, GA","text":"Forsyth, GA"},{"id":"us-nc-067","orig":"Forsyth, NC","text":"Forsyth, NC"},{"id":"us-tx-157","orig":"Fort Bend, TX","text":"Fort Bend, TX"},{"id":"us-nd-031","orig":"Foster, ND","text":"Foster, ND"},{"id":"us-in-045","orig":"Fountain, IN","text":"Fountain, IN"},{"id":"us-al-059","orig":"Franklin, AL","text":"Franklin, AL"},{"id":"us-ar-047","orig":"Franklin, AR","text":"Franklin, AR"},{"id":"us-fl-037","orig":"Franklin, FL","text":"Franklin, FL"},{"id":"us-ga-119","orig":"Franklin, GA","text":"Franklin, GA"},{"id":"us-ia-069","orig":"Franklin, IA","text":"Franklin, IA"},{"id":"us-id-041","orig":"Franklin, ID","text":"Franklin, ID"},{"id":"us-il-055","orig":"Franklin, IL","text":"Franklin, IL"},{"id":"us-in-047","orig":"Franklin, IN","text":"Franklin, IN"},{"id":"us-ks-059","orig":"Franklin, KS","text":"Franklin, KS"},{"id":"us-ky-073","orig":"Franklin, KY","text":"Franklin, KY"},{"id":"us-la-041","orig":"Franklin, LA","text":"Franklin, LA"},{"id":"us-ma-011","orig":"Franklin, MA","text":"Franklin, MA"},{"id":"us-me-007","orig":"Franklin, ME","text":"Franklin, ME"},{"id":"us-mo-071","orig":"Franklin, MO","text":"Franklin, MO"},{"id":"us-ms-037","orig":"Franklin, MS","text":"Franklin, MS"},{"id":"us-nc-069","orig":"Franklin, NC","text":"Franklin, NC"},{"id":"us-ne-061","orig":"Franklin, NE","text":"Franklin, NE"},{"id":"us-ny-033","orig":"Franklin, NY","text":"Franklin, NY"},{"id":"us-oh-049","orig":"Franklin, OH","text":"Franklin, OH"},{"id":"us-pa-055","orig":"Franklin, PA","text":"Franklin, PA"},{"id":"us-tn-051","orig":"Franklin, TN","text":"Franklin, TN"},{"id":"us-tx-159","orig":"Franklin, TX","text":"Franklin, TX"},{"id":"us-va-620","orig":"Franklin, VA","text":"Franklin, VA"},{"id":"us-va-067","orig":"Franklin, VA","text":"Franklin, VA"},{"id":"us-vt-011","orig":"Franklin, VT","text":"Franklin, VT"},{"id":"us-wa-021","orig":"Franklin, WA","text":"Franklin, WA"},{"id":"us-md-021","orig":"Frederick, MD","text":"Frederick, MD"},{"id":"us-va-069","orig":"Frederick, VA","text":"Frederick, VA"},{"id":"us-va-630","orig":"Fredericksburg, VA","text":"Fredericksburg, VA"},{"id":"us-mn-047","orig":"Freeborn, MN","text":"Freeborn, MN"},{"id":"us-tx-161","orig":"Freestone, TX","text":"Freestone, TX"},{"id":"us-co-043","orig":"Fremont, CO","text":"Fremont, CO"},{"id":"us-ia-071","orig":"Fremont, IA","text":"Fremont, IA"},{"id":"us-id-043","orig":"Fremont, ID","text":"Fremont, ID"},{"id":"us-wy-013","orig":"Fremont, WY","text":"Fremont, WY"},{"id":"us-ca-019","orig":"Fresno, CA","text":"Fresno, CA"},{"id":"us-tx-163","orig":"Frio, TX","text":"Frio, TX"},{"id":"us-ne-063","orig":"Frontier, NE","text":"Frontier, NE"},{"id":"us-ar-049","orig":"Fulton, AR","text":"Fulton, AR"},{"id":"us-ga-121","orig":"Fulton, GA","text":"Fulton, GA"},{"id":"us-il-057","orig":"Fulton, IL","text":"Fulton, IL"},{"id":"us-in-049","orig":"Fulton, IN","text":"Fulton, IN"},{"id":"us-ky-075","orig":"Fulton, KY","text":"Fulton, KY"},{"id":"us-ny-035","orig":"Fulton, NY","text":"Fulton, NY"},{"id":"us-oh-051","orig":"Fulton, OH","text":"Fulton, OH"},{"id":"us-pa-057","orig":"Fulton, PA","text":"Fulton, PA"},{"id":"us-ne-065","orig":"Furnas, NE","text":"Furnas, NE"},{"id":"us-fl-039","orig":"Gadsden, FL","text":"Gadsden, FL"},{"id":"us-ne-067","orig":"Gage, NE","text":"Gage, NE"},{"id":"us-tx-165","orig":"Gaines, TX","text":"Gaines, TX"},{"id":"us-va-640","orig":"Galax, VA","text":"Galax, VA"},{"id":"us-il-059","orig":"Gallatin, IL","text":"Gallatin, IL"},{"id":"us-ky-077","orig":"Gallatin, KY","text":"Gallatin, KY"},{"id":"us-mt-031","orig":"Gallatin, MT","text":"Gallatin, MT"},{"id":"us-oh-053","orig":"Gallia, OH","text":"Gallia, OH"},{"id":"us-tx-167","orig":"Galveston, TX","text":"Galveston, TX"},{"id":"us-ne-069","orig":"Garden, NE","text":"Garden, NE"},{"id":"us-co-045","orig":"Garfield, CO","text":"Garfield, CO"},{"id":"us-mt-033","orig":"Garfield, MT","text":"Garfield, MT"},{"id":"us-ne-071","orig":"Garfield, NE","text":"Garfield, NE"},{"id":"us-ok-047","orig":"Garfield, OK","text":"Garfield, OK"},{"id":"us-ut-017","orig":"Garfield, UT","text":"Garfield, UT"},{"id":"us-wa-023","orig":"Garfield, WA","text":"Garfield, WA"},{"id":"us-ar-051","orig":"Garland, AR","text":"Garland, AR"},{"id":"us-ky-079","orig":"Garrard, KY","text":"Garrard, KY"},{"id":"us-md-023","orig":"Garrett, MD","text":"Garrett, MD"},{"id":"us-ok-049","orig":"Garvin, OK","text":"Garvin, OK"},{"id":"us-tx-169","orig":"Garza, TX","text":"Garza, TX"},{"id":"us-mo-073","orig":"Gasconade, MO","text":"Gasconade, MO"},{"id":"us-nc-071","orig":"Gaston, NC","text":"Gaston, NC"},{"id":"us-nc-073","orig":"Gates, NC","text":"Gates, NC"},{"id":"us-ks-061","orig":"Geary, KS","text":"Geary, KS"},{"id":"us-oh-055","orig":"Geauga, OH","text":"Geauga, OH"},{"id":"us-id-045","orig":"Gem, ID","text":"Gem, ID"},{"id":"us-mi-049","orig":"Genesee, MI","text":"Genesee, MI"},{"id":"us-ny-037","orig":"Genesee, NY","text":"Genesee, NY"},{"id":"us-al-061","orig":"Geneva, AL","text":"Geneva, AL"},{"id":"us-mo-075","orig":"Gentry, MO","text":"Gentry, MO"},{"id":"us-ms-039","orig":"George, MS","text":"George, MS"},{"id":"us-sc-043","orig":"Georgetown, SC","text":"Georgetown, SC"},{"id":"us-in-051","orig":"Gibson, IN","text":"Gibson, IN"},{"id":"us-tn-053","orig":"Gibson, TN","text":"Gibson, TN"},{"id":"us-az-007","orig":"Gila, AZ","text":"Gila, AZ"},{"id":"us-fl-041","orig":"Gilchrist, FL","text":"Gilchrist, FL"},{"id":"us-tn-055","orig":"Giles, TN","text":"Giles, TN"},{"id":"us-va-071","orig":"Giles, VA","text":"Giles, VA"},{"id":"us-tx-171","orig":"Gillespie, TX","text":"Gillespie, TX"},{"id":"us-or-021","orig":"Gilliam, OR","text":"Gilliam, OR"},{"id":"us-ga-123","orig":"Gilmer, GA","text":"Gilmer, GA"},{"id":"us-wv-021","orig":"Gilmer, WV","text":"Gilmer, WV"},{"id":"us-co-047","orig":"Gilpin, CO","text":"Gilpin, CO"},{"id":"us-mt-035","orig":"Glacier, MT","text":"Glacier, MT"},{"id":"us-fl-043","orig":"Glades, FL","text":"Glades, FL"},{"id":"us-mi-051","orig":"Gladwin, MI","text":"Gladwin, MI"},{"id":"us-ga-125","orig":"Glascock, GA","text":"Glascock, GA"},{"id":"us-tx-173","orig":"Glasscock, TX","text":"Glasscock, TX"},{"id":"us-ca-021","orig":"Glenn, CA","text":"Glenn, CA"},{"id":"us-nj-015","orig":"Gloucester, NJ","text":"Gloucester, NJ"},{"id":"us-va-073","orig":"Gloucester, VA","text":"Gloucester, VA"},{"id":"us-ga-127","orig":"Glynn, GA","text":"Glynn, GA"},{"id":"us-mi-053","orig":"Gogebic, MI","text":"Gogebic, MI"},{"id":"us-mt-037","orig":"Golden Valley, MT","text":"Golden Valley, MT"},{"id":"us-nd-033","orig":"Golden Valley, ND","text":"Golden Valley, ND"},{"id":"us-tx-175","orig":"Goliad, TX","text":"Goliad, TX"},{"id":"us-tx-177","orig":"Gonzales, TX","text":"Gonzales, TX"},{"id":"us-va-075","orig":"Goochland, VA","text":"Goochland, VA"},{"id":"us-mn-049","orig":"Goodhue, MN","text":"Goodhue, MN"},{"id":"us-id-047","orig":"Gooding, ID","text":"Gooding, ID"},{"id":"us-ga-129","orig":"Gordon, GA","text":"Gordon, GA"},{"id":"us-wy-015","orig":"Goshen, WY","text":"Goshen, WY"},{"id":"us-ne-073","orig":"Gosper, NE","text":"Gosper, NE"},{"id":"us-ks-063","orig":"Gove, KS","text":"Gove, KS"},{"id":"us-ga-131","orig":"Grady, GA","text":"Grady, GA"},{"id":"us-ok-051","orig":"Grady, OK","text":"Grady, OK"},{"id":"us-nh-009","orig":"Grafton, NH","text":"Grafton, NH"},{"id":"us-az-009","orig":"Graham, AZ","text":"Graham, AZ"},{"id":"us-ks-065","orig":"Graham, KS","text":"Graham, KS"},{"id":"us-nc-075","orig":"Graham, NC","text":"Graham, NC"},{"id":"us-tn-057","orig":"Grainger, TN","text":"Grainger, TN"},{"id":"us-nd-035","orig":"Grand Forks, ND","text":"Grand Forks, ND"},{"id":"us-vt-013","orig":"Grand Isle, VT","text":"Grand Isle, VT"},{"id":"us-mi-055","orig":"Grand Traverse, MI","text":"Grand Traverse, MI"},{"id":"us-co-049","orig":"Grand, CO","text":"Grand, CO"},{"id":"us-ut-019","orig":"Grand, UT","text":"Grand, UT"},{"id":"us-mt-039","orig":"Granite, MT","text":"Granite, MT"},{"id":"us-ar-053","orig":"Grant, AR","text":"Grant, AR"},{"id":"us-in-053","orig":"Grant, IN","text":"Grant, IN"},{"id":"us-ks-067","orig":"Grant, KS","text":"Grant, KS"},{"id":"us-ky-081","orig":"Grant, KY","text":"Grant, KY"},{"id":"us-la-043","orig":"Grant, LA","text":"Grant, LA"},{"id":"us-mn-051","orig":"Grant, MN","text":"Grant, MN"},{"id":"us-nd-037","orig":"Grant, ND","text":"Grant, ND"},{"id":"us-ne-075","orig":"Grant, NE","text":"Grant, NE"},{"id":"us-nm-017","orig":"Grant, NM","text":"Grant, NM"},{"id":"us-ok-053","orig":"Grant, OK","text":"Grant, OK"},{"id":"us-or-023","orig":"Grant, OR","text":"Grant, OR"},{"id":"us-sd-051","orig":"Grant, SD","text":"Grant, SD"},{"id":"us-wa-025","orig":"Grant, WA","text":"Grant, WA"},{"id":"us-wi-043","orig":"Grant, WI","text":"Grant, WI"},{"id":"us-wv-023","orig":"Grant, WV","text":"Grant, WV"},{"id":"us-nc-077","orig":"Granville, NC","text":"Granville, NC"},{"id":"us-mi-057","orig":"Gratiot, MI","text":"Gratiot, MI"},{"id":"us-ky-083","orig":"Graves, KY","text":"Graves, KY"},{"id":"us-ks-069","orig":"Gray, KS","text":"Gray, KS"},{"id":"us-tx-179","orig":"Gray, TX","text":"Gray, TX"},{"id":"us-wa-027","orig":"Grays Harbor, WA","text":"Grays Harbor, WA"},{"id":"us-ky-085","orig":"Grayson, KY","text":"Grayson, KY"},{"id":"us-tx-181","orig":"Grayson, TX","text":"Grayson, TX"},{"id":"us-va-077","orig":"Grayson, VA","text":"Grayson, VA"},{"id":"us-ks-071","orig":"Greeley, KS","text":"Greeley, KS"},{"id":"us-ne-077","orig":"Greeley, NE","text":"Greeley, NE"},{"id":"us-wi-047","orig":"Green Lake, WI","text":"Green Lake, WI"},{"id":"us-ky-087","orig":"Green, KY","text":"Green, KY"},{"id":"us-wi-045","orig":"Green, WI","text":"Green, WI"},{"id":"us-wv-025","orig":"Greenbrier, WV","text":"Greenbrier, WV"},{"id":"us-al-063","orig":"Greene, AL","text":"Greene, AL"},{"id":"us-ar-055","orig":"Greene, AR","text":"Greene, AR"},{"id":"us-ga-133","orig":"Greene, GA","text":"Greene, GA"},{"id":"us-ia-073","orig":"Greene, IA","text":"Greene, IA"},{"id":"us-il-061","orig":"Greene, IL","text":"Greene, IL"},{"id":"us-in-055","orig":"Greene, IN","text":"Greene, IN"},{"id":"us-mo-077","orig":"Greene, MO","text":"Greene, MO"},{"id":"us-ms-041","orig":"Greene, MS","text":"Greene, MS"},{"id":"us-nc-079","orig":"Greene, NC","text":"Greene, NC"},{"id":"us-ny-039","orig":"Greene, NY","text":"Greene, NY"},{"id":"us-oh-057","orig":"Greene, OH","text":"Greene, OH"},{"id":"us-pa-059","orig":"Greene, PA","text":"Greene, PA"},{"id":"us-tn-059","orig":"Greene, TN","text":"Greene, TN"},{"id":"us-va-079","orig":"Greene, VA","text":"Greene, VA"},{"id":"us-az-011","orig":"Greenlee, AZ","text":"Greenlee, AZ"},{"id":"us-va-081","orig":"Greensville, VA","text":"Greensville, VA"},{"id":"us-ky-089","orig":"Greenup, KY","text":"Greenup, KY"},{"id":"us-sc-045","orig":"Greenville, SC","text":"Greenville, SC"},{"id":"us-ks-073","orig":"Greenwood, KS","text":"Greenwood, KS"},{"id":"us-sc-047","orig":"Greenwood, SC","text":"Greenwood, SC"},{"id":"us-ok-055","orig":"Greer, OK","text":"Greer, OK"},{"id":"us-tx-183","orig":"Gregg, TX","text":"Gregg, TX"},{"id":"us-sd-053","orig":"Gregory, SD","text":"Gregory, SD"},{"id":"us-ms-043","orig":"Grenada, MS","text":"Grenada, MS"},{"id":"us-nd-039","orig":"Griggs, ND","text":"Griggs, ND"},{"id":"us-tx-185","orig":"Grimes, TX","text":"Grimes, TX"},{"id":"us-ia-075","orig":"Grundy, IA","text":"Grundy, IA"},{"id":"us-il-063","orig":"Grundy, IL","text":"Grundy, IL"},{"id":"us-mo-079","orig":"Grundy, MO","text":"Grundy, MO"},{"id":"us-tn-061","orig":"Grundy, TN","text":"Grundy, TN"},{"id":"us-nm-019","orig":"Guadalupe, NM","text":"Guadalupe, NM"},{"id":"us-tx-187","orig":"Guadalupe, TX","text":"Guadalupe, TX"},{"id":"us-oh-059","orig":"Guernsey, OH","text":"Guernsey, OH"},{"id":"us-nc-081","orig":"Guilford, NC","text":"Guilford, NC"},{"id":"us-fl-045","orig":"Gulf, FL","text":"Gulf, FL"},{"id":"us-co-051","orig":"Gunnison, CO","text":"Gunnison, CO"},{"id":"us-ia-077","orig":"Guthrie, IA","text":"Guthrie, IA"},{"id":"us-ga-135","orig":"Gwinnett, GA","text":"Gwinnett, GA"},{"id":"us-sd-055","orig":"Haakon, SD","text":"Haakon, SD"},{"id":"us-ga-137","orig":"Habersham, GA","text":"Habersham, GA"},{"id":"us-ak-100","orig":"Haines, AK","text":"Haines, AK"},{"id":"us-al-065","orig":"Hale, AL","text":"Hale, AL"},{"id":"us-tx-189","orig":"Hale, TX","text":"Hale, TX"},{"id":"us-nc-083","orig":"Halifax, NC","text":"Halifax, NC"},{"id":"us-va-083","orig":"Halifax, VA","text":"Halifax, VA"},{"id":"us-ga-139","orig":"Hall, GA","text":"Hall, GA"},{"id":"us-ne-079","orig":"Hall, NE","text":"Hall, NE"},{"id":"us-tx-191","orig":"Hall, TX","text":"Hall, TX"},{"id":"us-tn-063","orig":"Hamblen, TN","text":"Hamblen, TN"},{"id":"us-fl-047","orig":"Hamilton, FL","text":"Hamilton, FL"},{"id":"us-ia-079","orig":"Hamilton, IA","text":"Hamilton, IA"},{"id":"us-il-065","orig":"Hamilton, IL","text":"Hamilton, IL"},{"id":"us-in-057","orig":"Hamilton, IN","text":"Hamilton, IN"},{"id":"us-ks-075","orig":"Hamilton, KS","text":"Hamilton, KS"},{"id":"us-ne-081","orig":"Hamilton, NE","text":"Hamilton, NE"},{"id":"us-ny-041","orig":"Hamilton, NY","text":"Hamilton, NY"},{"id":"us-oh-061","orig":"Hamilton, OH","text":"Hamilton, OH"},{"id":"us-tn-065","orig":"Hamilton, TN","text":"Hamilton, TN"},{"id":"us-tx-193","orig":"Hamilton, TX","text":"Hamilton, TX"},{"id":"us-sd-057","orig":"Hamlin, SD","text":"Hamlin, SD"},{"id":"us-ma-013","orig":"Hampden, MA","text":"Hampden, MA"},{"id":"us-ma-015","orig":"Hampshire, MA","text":"Hampshire, MA"},{"id":"us-wv-027","orig":"Hampshire, WV","text":"Hampshire, WV"},{"id":"us-sc-049","orig":"Hampton, SC","text":"Hampton, SC"},{"id":"us-va-650","orig":"Hampton, VA","text":"Hampton, VA"},{"id":"us-ga-141","orig":"Hancock, GA","text":"Hancock, GA"},{"id":"us-ia-081","orig":"Hancock, IA","text":"Hancock, IA"},{"id":"us-il-067","orig":"Hancock, IL","text":"Hancock, IL"},{"id":"us-in-059","orig":"Hancock, IN","text":"Hancock, IN"},{"id":"us-ky-091","orig":"Hancock, KY","text":"Hancock, KY"},{"id":"us-me-009","orig":"Hancock, ME","text":"Hancock, ME"},{"id":"us-ms-045","orig":"Hancock, MS","text":"Hancock, MS"},{"id":"us-oh-063","orig":"Hancock, OH","text":"Hancock, OH"},{"id":"us-tn-067","orig":"Hancock, TN","text":"Hancock, TN"},{"id":"us-wv-029","orig":"Hancock, WV","text":"Hancock, WV"},{"id":"us-sd-059","orig":"Hand, SD","text":"Hand, SD"},{"id":"us-va-085","orig":"Hanover, VA","text":"Hanover, VA"},{"id":"us-tx-195","orig":"Hansford, TX","text":"Hansford, TX"},{"id":"us-sd-061","orig":"Hanson, SD","text":"Hanson, SD"},{"id":"us-ga-143","orig":"Haralson, GA","text":"Haralson, GA"},{"id":"us-fl-049","orig":"Hardee, FL","text":"Hardee, FL"},{"id":"us-tn-069","orig":"Hardeman, TN","text":"Hardeman, TN"},{"id":"us-tx-197","orig":"Hardeman, TX","text":"Hardeman, TX"},{"id":"us-ia-083","orig":"Hardin, IA","text":"Hardin, IA"},{"id":"us-il-069","orig":"Hardin, IL","text":"Hardin, IL"},{"id":"us-ky-093","orig":"Hardin, KY","text":"Hardin, KY"},{"id":"us-oh-065","orig":"Hardin, OH","text":"Hardin, OH"},{"id":"us-tn-071","orig":"Hardin, TN","text":"Hardin, TN"},{"id":"us-tx-199","orig":"Hardin, TX","text":"Hardin, TX"},{"id":"us-nm-021","orig":"Harding, NM","text":"Harding, NM"},{"id":"us-sd-063","orig":"Harding, SD","text":"Harding, SD"},{"id":"us-wv-031","orig":"Hardy, WV","text":"Hardy, WV"},{"id":"us-md-025","orig":"Harford, MD","text":"Harford, MD"},{"id":"us-ky-095","orig":"Harlan, KY","text":"Harlan, KY"},{"id":"us-ne-083","orig":"Harlan, NE","text":"Harlan, NE"},{"id":"us-ok-057","orig":"Harmon, OK","text":"Harmon, OK"},{"id":"us-nc-085","orig":"Harnett, NC","text":"Harnett, NC"},{"id":"us-or-025","orig":"Harney, OR","text":"Harney, OR"},{"id":"us-ks-077","orig":"Harper, KS","text":"Harper, KS"},{"id":"us-ok-059","orig":"Harper, OK","text":"Harper, OK"},{"id":"us-ga-145","orig":"Harris, GA","text":"Harris, GA"},{"id":"us-tx-201","orig":"Harris, TX","text":"Harris, TX"},{"id":"us-ia-085","orig":"Harrison, IA","text":"Harrison, IA"},{"id":"us-in-061","orig":"Harrison, IN","text":"Harrison, IN"},{"id":"us-ky-097","orig":"Harrison, KY","text":"Harrison, KY"},{"id":"us-mo-081","orig":"Harrison, MO","text":"Harrison, MO"},{"id":"us-ms-047","orig":"Harrison, MS","text":"Harrison, MS"},{"id":"us-oh-067","orig":"Harrison, OH","text":"Harrison, OH"},{"id":"us-tx-203","orig":"Harrison, TX","text":"Harrison, TX"},{"id":"us-wv-033","orig":"Harrison, WV","text":"Harrison, WV"},{"id":"us-va-660","orig":"Harrisonburg, VA","text":"Harrisonburg, VA"},{"id":"us-ga-147","orig":"Hart, GA","text":"Hart, GA"},{"id":"us-ky-099","orig":"Hart, KY","text":"Hart, KY"},{"id":"us-ct-003","orig":"Hartford, CT","text":"Hartford, CT"},{"id":"us-tx-205","orig":"Hartley, TX","text":"Hartley, TX"},{"id":"us-ks-079","orig":"Harvey, KS","text":"Harvey, KS"},{"id":"us-ks-081","orig":"Haskell, KS","text":"Haskell, KS"},{"id":"us-ok-061","orig":"Haskell, OK","text":"Haskell, OK"},{"id":"us-tx-207","orig":"Haskell, TX","text":"Haskell, TX"},{"id":"us-hi-001","orig":"Hawaii, HI","text":"Hawaii, HI"},{"id":"us-tn-073","orig":"Hawkins, TN","text":"Hawkins, TN"},{"id":"us-ne-085","orig":"Hayes, NE","text":"Hayes, NE"},{"id":"us-tx-209","orig":"Hays, TX","text":"Hays, TX"},{"id":"us-nc-087","orig":"Haywood, NC","text":"Haywood, NC"},{"id":"us-tn-075","orig":"Haywood, TN","text":"Haywood, TN"},{"id":"us-ga-149","orig":"Heard, GA","text":"Heard, GA"},{"id":"us-tx-211","orig":"Hemphill, TX","text":"Hemphill, TX"},{"id":"us-ar-057","orig":"Hempstead, AR","text":"Hempstead, AR"},{"id":"us-il-071","orig":"Henderson, IL","text":"Henderson, IL"},{"id":"us-ky-101","orig":"Henderson, KY","text":"Henderson, KY"},{"id":"us-nc-089","orig":"Henderson, NC","text":"Henderson, NC"},{"id":"us-tn-077","orig":"Henderson, TN","text":"Henderson, TN"},{"id":"us-tx-213","orig":"Henderson, TX","text":"Henderson, TX"},{"id":"us-in-063","orig":"Hendricks, IN","text":"Hendricks, IN"},{"id":"us-fl-051","orig":"Hendry, FL","text":"Hendry, FL"},{"id":"us-mn-053","orig":"Hennepin, MN","text":"Hennepin, MN"},{"id":"us-va-087","orig":"Henrico, VA","text":"Henrico, VA"},{"id":"us-al-067","orig":"Henry, AL","text":"Henry, AL"},{"id":"us-ga-151","orig":"Henry, GA","text":"Henry, GA"},{"id":"us-ia-087","orig":"Henry, IA","text":"Henry, IA"},{"id":"us-il-073","orig":"Henry, IL","text":"Henry, IL"},{"id":"us-in-065","orig":"Henry, IN","text":"Henry, IN"},{"id":"us-ky-103","orig":"Henry, KY","text":"Henry, KY"},{"id":"us-mo-083","orig":"Henry, MO","text":"Henry, MO"},{"id":"us-oh-069","orig":"Henry, OH","text":"Henry, OH"},{"id":"us-tn-079","orig":"Henry, TN","text":"Henry, TN"},{"id":"us-va-089","orig":"Henry, VA","text":"Henry, VA"},{"id":"us-ny-043","orig":"Herkimer, NY","text":"Herkimer, NY"},{"id":"us-fl-053","orig":"Hernando, FL","text":"Hernando, FL"},{"id":"us-nc-091","orig":"Hertford, NC","text":"Hertford, NC"},{"id":"us-nd-041","orig":"Hettinger, ND","text":"Hettinger, ND"},{"id":"us-ky-105","orig":"Hickman, KY","text":"Hickman, KY"},{"id":"us-tn-081","orig":"Hickman, TN","text":"Hickman, TN"},{"id":"us-mo-085","orig":"Hickory, MO","text":"Hickory, MO"},{"id":"us-nm-023","orig":"Hidalgo, NM","text":"Hidalgo, NM"},{"id":"us-tx-215","orig":"Hidalgo, TX","text":"Hidalgo, TX"},{"id":"us-oh-071","orig":"Highland, OH","text":"Highland, OH"},{"id":"us-va-091","orig":"Highland, VA","text":"Highland, VA"},{"id":"us-fl-055","orig":"Highlands, FL","text":"Highlands, FL"},{"id":"us-mt-041","orig":"Hill, MT","text":"Hill, MT"},{"id":"us-tx-217","orig":"Hill, TX","text":"Hill, TX"},{"id":"us-fl-057","orig":"Hillsborough, FL","text":"Hillsborough, FL"},{"id":"us-nh-011","orig":"Hillsborough, NH","text":"Hillsborough, NH"},{"id":"us-mi-059","orig":"Hillsdale, MI","text":"Hillsdale, MI"},{"id":"us-ms-049","orig":"Hinds, MS","text":"Hinds, MS"},{"id":"us-co-053","orig":"Hinsdale, CO","text":"Hinsdale, CO"},{"id":"us-ne-087","orig":"Hitchcock, NE","text":"Hitchcock, NE"},{"id":"us-oh-073","orig":"Hocking, OH","text":"Hocking, OH"},{"id":"us-tx-219","orig":"Hockley, TX","text":"Hockley, TX"},{"id":"us-ks-083","orig":"Hodgeman, KS","text":"Hodgeman, KS"},{"id":"us-nc-093","orig":"Hoke, NC","text":"Hoke, NC"},{"id":"us-fl-059","orig":"Holmes, FL","text":"Holmes, FL"},{"id":"us-ms-051","orig":"Holmes, MS","text":"Holmes, MS"},{"id":"us-oh-075","orig":"Holmes, OH","text":"Holmes, OH"},{"id":"us-mo-087","orig":"Holt, MO","text":"Holt, MO"},{"id":"us-ne-089","orig":"Holt, NE","text":"Holt, NE"},{"id":"us-hi-003","orig":"Honolulu, HI","text":"Honolulu, HI"},{"id":"us-or-027","orig":"Hood River, OR","text":"Hood River, OR"},{"id":"us-tx-221","orig":"Hood, TX","text":"Hood, TX"},{"id":"us-ne-091","orig":"Hooker, NE","text":"Hooker, NE"},{"id":"us-ak-105","orig":"Hoonah-Angoon, AK","text":"Hoonah-Angoon, AK"},{"id":"us-va-670","orig":"Hopewell, VA","text":"Hopewell, VA"},{"id":"us-ky-107","orig":"Hopkins, KY","text":"Hopkins, KY"},{"id":"us-tx-223","orig":"Hopkins, TX","text":"Hopkins, TX"},{"id":"us-sc-051","orig":"Horry, SC","text":"Horry, SC"},{"id":"us-ar-059","orig":"Hot Spring, AR","text":"Hot Spring, AR"},{"id":"us-wy-017","orig":"Hot Springs, WY","text":"Hot Springs, WY"},{"id":"us-mi-061","orig":"Houghton, MI","text":"Houghton, MI"},{"id":"us-al-069","orig":"Houston, AL","text":"Houston, AL"},{"id":"us-ga-153","orig":"Houston, GA","text":"Houston, GA"},{"id":"us-mn-055","orig":"Houston, MN","text":"Houston, MN"},{"id":"us-tn-083","orig":"Houston, TN","text":"Houston, TN"},{"id":"us-tx-225","orig":"Houston, TX","text":"Houston, TX"},{"id":"us-ar-061","orig":"Howard, AR","text":"Howard, AR"},{"id":"us-ia-089","orig":"Howard, IA","text":"Howard, IA"},{"id":"us-in-067","orig":"Howard, IN","text":"Howard, IN"},{"id":"us-md-027","orig":"Howard, MD","text":"Howard, MD"},{"id":"us-mo-089","orig":"Howard, MO","text":"Howard, MO"},{"id":"us-ne-093","orig":"Howard, NE","text":"Howard, NE"},{"id":"us-tx-227","orig":"Howard, TX","text":"Howard, TX"},{"id":"us-mo-091","orig":"Howell, MO","text":"Howell, MO"},{"id":"us-mn-057","orig":"Hubbard, MN","text":"Hubbard, MN"},{"id":"us-nj-017","orig":"Hudson, NJ","text":"Hudson, NJ"},{"id":"us-tx-229","orig":"Hudspeth, TX","text":"Hudspeth, TX"},{"id":"us-co-055","orig":"Huerfano, CO","text":"Huerfano, CO"},{"id":"us-ok-063","orig":"Hughes, OK","text":"Hughes, OK"},{"id":"us-sd-065","orig":"Hughes, SD","text":"Hughes, SD"},{"id":"us-ca-023","orig":"Humboldt, CA","text":"Humboldt, CA"},{"id":"us-ia-091","orig":"Humboldt, IA","text":"Humboldt, IA"},{"id":"us-nv-013","orig":"Humboldt, NV","text":"Humboldt, NV"},{"id":"us-ms-053","orig":"Humphreys, MS","text":"Humphreys, MS"},{"id":"us-tn-085","orig":"Humphreys, TN","text":"Humphreys, TN"},{"id":"us-tx-231","orig":"Hunt, TX","text":"Hunt, TX"},{"id":"us-nj-019","orig":"Hunterdon, NJ","text":"Hunterdon, NJ"},{"id":"us-pa-061","orig":"Huntingdon, PA","text":"Huntingdon, PA"},{"id":"us-in-069","orig":"Huntington, IN","text":"Huntington, IN"},{"id":"us-mi-063","orig":"Huron, MI","text":"Huron, MI"},{"id":"us-oh-077","orig":"Huron, OH","text":"Huron, OH"},{"id":"us-sd-067","orig":"Hutchinson, SD","text":"Hutchinson, SD"},{"id":"us-tx-233","orig":"Hutchinson, TX","text":"Hutchinson, TX"},{"id":"us-nc-095","orig":"Hyde, NC","text":"Hyde, NC"},{"id":"us-sd-069","orig":"Hyde, SD","text":"Hyde, SD"},{"id":"us-la-045","orig":"Iberia, LA","text":"Iberia, LA"},{"id":"us-la-047","orig":"Iberville, LA","text":"Iberville, LA"},{"id":"us-ia-093","orig":"Ida, IA","text":"Ida, IA"},{"id":"us-id-049","orig":"Idaho, ID","text":"Idaho, ID"},{"id":"us-ca-025","orig":"Imperial, CA","text":"Imperial, CA"},{"id":"us-ar-063","orig":"Independence, AR","text":"Independence, AR"},{"id":"us-fl-061","orig":"Indian River, FL","text":"Indian River, FL"},{"id":"us-pa-063","orig":"Indiana, PA","text":"Indiana, PA"},{"id":"us-mi-065","orig":"Ingham, MI","text":"Ingham, MI"},{"id":"us-ca-027","orig":"Inyo, CA","text":"Inyo, CA"},{"id":"us-mi-067","orig":"Ionia, MI","text":"Ionia, MI"},{"id":"us-mi-069","orig":"Iosco, MI","text":"Iosco, MI"},{"id":"us-ia-095","orig":"Iowa, IA","text":"Iowa, IA"},{"id":"us-wi-049","orig":"Iowa, WI","text":"Iowa, WI"},{"id":"us-nc-097","orig":"Iredell, NC","text":"Iredell, NC"},{"id":"us-tx-235","orig":"Irion, TX","text":"Irion, TX"},{"id":"us-mi-071","orig":"Iron, MI","text":"Iron, MI"},{"id":"us-mo-093","orig":"Iron, MO","text":"Iron, MO"},{"id":"us-ut-021","orig":"Iron, UT","text":"Iron, UT"},{"id":"us-wi-051","orig":"Iron, WI","text":"Iron, WI"},{"id":"us-il-075","orig":"Iroquois, IL","text":"Iroquois, IL"},{"id":"us-ga-155","orig":"Irwin, GA","text":"Irwin, GA"},{"id":"us-mi-073","orig":"Isabella, MI","text":"Isabella, MI"},{"id":"us-mn-059","orig":"Isanti, MN","text":"Isanti, MN"},{"id":"us-wa-029","orig":"Island, WA","text":"Island, WA"},{"id":"us-va-093","orig":"Isle of Wight, VA","text":"Isle of Wight, VA"},{"id":"us-ms-055","orig":"Issaquena, MS","text":"Issaquena, MS"},{"id":"us-mn-061","orig":"Itasca, MN","text":"Itasca, MN"},{"id":"us-ms-057","orig":"Itawamba, MS","text":"Itawamba, MS"},{"id":"us-ar-065","orig":"Izard, AR","text":"Izard, AR"},{"id":"us-tx-237","orig":"Jack, TX","text":"Jack, TX"},{"id":"us-al-071","orig":"Jackson, AL","text":"Jackson, AL"},{"id":"us-ar-067","orig":"Jackson, AR","text":"Jackson, AR"},{"id":"us-co-057","orig":"Jackson, CO","text":"Jackson, CO"},{"id":"us-fl-063","orig":"Jackson, FL","text":"Jackson, FL"},{"id":"us-ga-157","orig":"Jackson, GA","text":"Jackson, GA"},{"id":"us-ia-097","orig":"Jackson, IA","text":"Jackson, IA"},{"id":"us-il-077","orig":"Jackson, IL","text":"Jackson, IL"},{"id":"us-in-071","orig":"Jackson, IN","text":"Jackson, IN"},{"id":"us-ks-085","orig":"Jackson, KS","text":"Jackson, KS"},{"id":"us-ky-109","orig":"Jackson, KY","text":"Jackson, KY"},{"id":"us-la-049","orig":"Jackson, LA","text":"Jackson, LA"},{"id":"us-mi-075","orig":"Jackson, MI","text":"Jackson, MI"},{"id":"us-mn-063","orig":"Jackson, MN","text":"Jackson, MN"},{"id":"us-mo-095","orig":"Jackson, MO","text":"Jackson, MO"},{"id":"us-ms-059","orig":"Jackson, MS","text":"Jackson, MS"},{"id":"us-nc-099","orig":"Jackson, NC","text":"Jackson, NC"},{"id":"us-oh-079","orig":"Jackson, OH","text":"Jackson, OH"},{"id":"us-ok-065","orig":"Jackson, OK","text":"Jackson, OK"},{"id":"us-or-029","orig":"Jackson, OR","text":"Jackson, OR"},{"id":"us-sd-071","orig":"Jackson, SD","text":"Jackson, SD"},{"id":"us-tn-087","orig":"Jackson, TN","text":"Jackson, TN"},{"id":"us-tx-239","orig":"Jackson, TX","text":"Jackson, TX"},{"id":"us-wi-053","orig":"Jackson, WI","text":"Jackson, WI"},{"id":"us-wv-035","orig":"Jackson, WV","text":"Jackson, WV"},{"id":"us-va-095","orig":"James City, VA","text":"James City, VA"},{"id":"us-ga-159","orig":"Jasper, GA","text":"Jasper, GA"},{"id":"us-ia-099","orig":"Jasper, IA","text":"Jasper, IA"},{"id":"us-il-079","orig":"Jasper, IL","text":"Jasper, IL"},{"id":"us-in-073","orig":"Jasper, IN","text":"Jasper, IN"},{"id":"us-mo-097","orig":"Jasper, MO","text":"Jasper, MO"},{"id":"us-ms-061","orig":"Jasper, MS","text":"Jasper, MS"},{"id":"us-sc-053","orig":"Jasper, SC","text":"Jasper, SC"},{"id":"us-tx-241","orig":"Jasper, TX","text":"Jasper, TX"},{"id":"us-in-075","orig":"Jay, IN","text":"Jay, IN"},{"id":"us-ga-161","orig":"Jeff Davis, GA","text":"Jeff Davis, GA"},{"id":"us-tx-243","orig":"Jeff Davis, TX","text":"Jeff Davis, TX"},{"id":"us-la-053","orig":"Jefferson Davis, LA","text":"Jefferson Davis, LA"},{"id":"us-ms-065","orig":"Jefferson Davis, MS","text":"Jefferson Davis, MS"},{"id":"us-al-073","orig":"Jefferson, AL","text":"Jefferson, AL"},{"id":"us-ar-069","orig":"Jefferson, AR","text":"Jefferson, AR"},{"id":"us-co-059","orig":"Jefferson, CO","text":"Jefferson, CO"},{"id":"us-fl-065","orig":"Jefferson, FL","text":"Jefferson, FL"},{"id":"us-ga-163","orig":"Jefferson, GA","text":"Jefferson, GA"},{"id":"us-ia-101","orig":"Jefferson, IA","text":"Jefferson, IA"},{"id":"us-id-051","orig":"Jefferson, ID","text":"Jefferson, ID"},{"id":"us-il-081","orig":"Jefferson, IL","text":"Jefferson, IL"},{"id":"us-in-077","orig":"Jefferson, IN","text":"Jefferson, IN"},{"id":"us-ks-087","orig":"Jefferson, KS","text":"Jefferson, KS"},{"id":"us-ky-111","orig":"Jefferson, KY","text":"Jefferson, KY"},{"id":"us-la-051","orig":"Jefferson, LA","text":"Jefferson, LA"},{"id":"us-mo-099","orig":"Jefferson, MO","text":"Jefferson, MO"},{"id":"us-ms-063","orig":"Jefferson, MS","text":"Jefferson, MS"},{"id":"us-mt-043","orig":"Jefferson, MT","text":"Jefferson, MT"},{"id":"us-ne-095","orig":"Jefferson, NE","text":"Jefferson, NE"},{"id":"us-ny-045","orig":"Jefferson, NY","text":"Jefferson, NY"},{"id":"us-oh-081","orig":"Jefferson, OH","text":"Jefferson, OH"},{"id":"us-ok-067","orig":"Jefferson, OK","text":"Jefferson, OK"},{"id":"us-or-031","orig":"Jefferson, OR","text":"Jefferson, OR"},{"id":"us-pa-065","orig":"Jefferson, PA","text":"Jefferson, PA"},{"id":"us-tn-089","orig":"Jefferson, TN","text":"Jefferson, TN"},{"id":"us-tx-245","orig":"Jefferson, TX","text":"Jefferson, TX"},{"id":"us-wa-031","orig":"Jefferson, WA","text":"Jefferson, WA"},{"id":"us-wi-055","orig":"Jefferson, WI","text":"Jefferson, WI"},{"id":"us-wv-037","orig":"Jefferson, WV","text":"Jefferson, WV"},{"id":"us-ga-165","orig":"Jenkins, GA","text":"Jenkins, GA"},{"id":"us-in-079","orig":"Jennings, IN","text":"Jennings, IN"},{"id":"us-sd-073","orig":"Jerauld, SD","text":"Jerauld, SD"},{"id":"us-id-053","orig":"Jerome, ID","text":"Jerome, ID"},{"id":"us-il-083","orig":"Jersey, IL","text":"Jersey, IL"},{"id":"us-ky-113","orig":"Jessamine, KY","text":"Jessamine, KY"},{"id":"us-ks-089","orig":"Jewell, KS","text":"Jewell, KS"},{"id":"us-tx-247","orig":"Jim Hogg, TX","text":"Jim Hogg, TX"},{"id":"us-tx-249","orig":"Jim Wells, TX","text":"Jim Wells, TX"},{"id":"us-il-085","orig":"Jo Daviess, IL","text":"Jo Daviess, IL"},{"id":"us-ar-071","orig":"Johnson, AR","text":"Johnson, AR"},{"id":"us-ga-167","orig":"Johnson, GA","text":"Johnson, GA"},{"id":"us-ia-103","orig":"Johnson, IA","text":"Johnson, IA"},{"id":"us-il-087","orig":"Johnson, IL","text":"Johnson, IL"},{"id":"us-in-081","orig":"Johnson, IN","text":"Johnson, IN"},{"id":"us-ks-091","orig":"Johnson, KS","text":"Johnson, KS"},{"id":"us-ky-115","orig":"Johnson, KY","text":"Johnson, KY"},{"id":"us-mo-101","orig":"Johnson, MO","text":"Johnson, MO"},{"id":"us-ne-097","orig":"Johnson, NE","text":"Johnson, NE"},{"id":"us-tn-091","orig":"Johnson, TN","text":"Johnson, TN"},{"id":"us-tx-251","orig":"Johnson, TX","text":"Johnson, TX"},{"id":"us-wy-019","orig":"Johnson, WY","text":"Johnson, WY"},{"id":"us-nc-101","orig":"Johnston, NC","text":"Johnston, NC"},{"id":"us-ok-069","orig":"Johnston, OK","text":"Johnston, OK"},{"id":"us-ga-169","orig":"Jones, GA","text":"Jones, GA"},{"id":"us-ia-105","orig":"Jones, IA","text":"Jones, IA"},{"id":"us-ms-067","orig":"Jones, MS","text":"Jones, MS"},{"id":"us-nc-103","orig":"Jones, NC","text":"Jones, NC"},{"id":"us-sd-075","orig":"Jones, SD","text":"Jones, SD"},{"id":"us-tx-253","orig":"Jones, TX","text":"Jones, TX"},{"id":"us-or-033","orig":"Josephine, OR","text":"Josephine, OR"},{"id":"us-ut-023","orig":"Juab, UT","text":"Juab, UT"},{"id":"us-mt-045","orig":"Judith Basin, MT","text":"Judith Basin, MT"},{"id":"us-ak-110","orig":"Juneau, AK","text":"Juneau, AK"},{"id":"us-wi-057","orig":"Juneau, WI","text":"Juneau, WI"},{"id":"us-pa-067","orig":"Juniata, PA","text":"Juniata, PA"},{"id":"us-mi-077","orig":"Kalamazoo, MI","text":"Kalamazoo, MI"},{"id":"us-hi-005","orig":"Kalawao, HI","text":"Kalawao, HI"},{"id":"us-mi-079","orig":"Kalkaska, MI","text":"Kalkaska, MI"},{"id":"us-mn-065","orig":"Kanabec, MN","text":"Kanabec, MN"},{"id":"us-wv-039","orig":"Kanawha, WV","text":"Kanawha, WV"},{"id":"us-mn-067","orig":"Kandiyohi, MN","text":"Kandiyohi, MN"},{"id":"us-il-089","orig":"Kane, IL","text":"Kane, IL"},{"id":"us-ut-025","orig":"Kane, UT","text":"Kane, UT"},{"id":"us-il-091","orig":"Kankakee, IL","text":"Kankakee, IL"},{"id":"us-tx-255","orig":"Karnes, TX","text":"Karnes, TX"},{"id":"us-hi-007","orig":"Kauai, HI","text":"Kauai, HI"},{"id":"us-tx-257","orig":"Kaufman, TX","text":"Kaufman, TX"},{"id":"us-ok-071","orig":"Kay, OK","text":"Kay, OK"},{"id":"us-ne-099","orig":"Kearney, NE","text":"Kearney, NE"},{"id":"us-ks-093","orig":"Kearny, KS","text":"Kearny, KS"},{"id":"us-ne-101","orig":"Keith, NE","text":"Keith, NE"},{"id":"us-ms-069","orig":"Kemper, MS","text":"Kemper, MS"},{"id":"us-ak-122","orig":"Kenai Peninsula, AK","text":"Kenai Peninsula, AK"},{"id":"us-il-093","orig":"Kendall, IL","text":"Kendall, IL"},{"id":"us-tx-259","orig":"Kendall, TX","text":"Kendall, TX"},{"id":"us-tx-261","orig":"Kenedy, TX","text":"Kenedy, TX"},{"id":"us-me-011","orig":"Kennebec, ME","text":"Kennebec, ME"},{"id":"us-wi-059","orig":"Kenosha, WI","text":"Kenosha, WI"},{"id":"us-de-001","orig":"Kent, DE","text":"Kent, DE"},{"id":"us-md-029","orig":"Kent, MD","text":"Kent, MD"},{"id":"us-mi-081","orig":"Kent, MI","text":"Kent, MI"},{"id":"us-ri-003","orig":"Kent, RI","text":"Kent, RI"},{"id":"us-tx-263","orig":"Kent, TX","text":"Kent, TX"},{"id":"us-ky-117","orig":"Kenton, KY","text":"Kenton, KY"},{"id":"us-ia-107","orig":"Keokuk, IA","text":"Keokuk, IA"},{"id":"us-ca-029","orig":"Kern, CA","text":"Kern, CA"},{"id":"us-tx-265","orig":"Kerr, TX","text":"Kerr, TX"},{"id":"us-sc-055","orig":"Kershaw, SC","text":"Kershaw, SC"},{"id":"us-ak-130","orig":"Ketchikan Gateway, AK","text":"Ketchikan Gateway, AK"},{"id":"us-wi-061","orig":"Kewaunee, WI","text":"Kewaunee, WI"},{"id":"us-mi-083","orig":"Keweenaw, MI","text":"Keweenaw, MI"},{"id":"us-ne-103","orig":"Keya Paha, NE","text":"Keya Paha, NE"},{"id":"us-nd-043","orig":"Kidder, ND","text":"Kidder, ND"},{"id":"us-ne-105","orig":"Kimball, NE","text":"Kimball, NE"},{"id":"us-tx-267","orig":"Kimble, TX","text":"Kimble, TX"},{"id":"us-va-099","orig":"King George, VA","text":"King George, VA"},{"id":"us-va-101","orig":"King William, VA","text":"King William, VA"},{"id":"us-va-097","orig":"King and Queen, VA","text":"King and Queen, VA"},{"id":"us-tx-269","orig":"King, TX","text":"King, TX"},{"id":"us-wa-033","orig":"King, WA","text":"King, WA"},{"id":"us-ok-073","orig":"Kingfisher, OK","text":"Kingfisher, OK"},{"id":"us-ks-095","orig":"Kingman, KS","text":"Kingman, KS"},{"id":"us-ca-031","orig":"Kings, CA","text":"Kings, CA"},{"id":"us-ny-047","orig":"Kings, NY","text":"Kings, NY"},{"id":"us-sd-077","orig":"Kingsbury, SD","text":"Kingsbury, SD"},{"id":"us-tx-271","orig":"Kinney, TX","text":"Kinney, TX"},{"id":"us-co-061","orig":"Kiowa, CO","text":"Kiowa, CO"},{"id":"us-ks-097","orig":"Kiowa, KS","text":"Kiowa, KS"},{"id":"us-ok-075","orig":"Kiowa, OK","text":"Kiowa, OK"},{"id":"us-co-063","orig":"Kit Carson, CO","text":"Kit Carson, CO"},{"id":"us-wa-035","orig":"Kitsap, WA","text":"Kitsap, WA"},{"id":"us-wa-037","orig":"Kittitas, WA","text":"Kittitas, WA"},{"id":"us-mn-069","orig":"Kittson, MN","text":"Kittson, MN"},{"id":"us-or-035","orig":"Klamath, OR","text":"Klamath, OR"},{"id":"us-tx-273","orig":"Kleberg, TX","text":"Kleberg, TX"},{"id":"us-wa-039","orig":"Klickitat, WA","text":"Klickitat, WA"},{"id":"us-ky-119","orig":"Knott, KY","text":"Knott, KY"},{"id":"us-il-095","orig":"Knox, IL","text":"Knox, IL"},{"id":"us-in-083","orig":"Knox, IN","text":"Knox, IN"},{"id":"us-ky-121","orig":"Knox, KY","text":"Knox, KY"},{"id":"us-me-013","orig":"Knox, ME","text":"Knox, ME"},{"id":"us-mo-103","orig":"Knox, MO","text":"Knox, MO"},{"id":"us-ne-107","orig":"Knox, NE","text":"Knox, NE"},{"id":"us-oh-083","orig":"Knox, OH","text":"Knox, OH"},{"id":"us-tn-093","orig":"Knox, TN","text":"Knox, TN"},{"id":"us-tx-275","orig":"Knox, TX","text":"Knox, TX"},{"id":"us-ak-150","orig":"Kodiak Island, AK","text":"Kodiak Island, AK"},{"id":"us-mn-071","orig":"Koochiching, MN","text":"Koochiching, MN"},{"id":"us-id-055","orig":"Kootenai, ID","text":"Kootenai, ID"},{"id":"us-in-085","orig":"Kosciusko, IN","text":"Kosciusko, IN"},{"id":"us-ia-109","orig":"Kossuth, IA","text":"Kossuth, IA"},{"id":"us-wi-063","orig":"La Crosse, WI","text":"La Crosse, WI"},{"id":"us-az-012","orig":"La Paz, AZ","text":"La Paz, AZ"},{"id":"us-co-067","orig":"La Plata, CO","text":"La Plata, CO"},{"id":"us-tx-283","orig":"La Salle, TX","text":"La Salle, TX"},{"id":"us-in-087","orig":"LaGrange, IN","text":"LaGrange, IN"},{"id":"us-nd-045","orig":"LaMoure, ND","text":"LaMoure, ND"},{"id":"us-in-091","orig":"LaPorte, IN","text":"LaPorte, IN"},{"id":"us-il-099","orig":"LaSalle, IL","text":"LaSalle, IL"},{"id":"us-la-059","orig":"LaSalle, LA","text":"LaSalle, LA"},{"id":"us-ks-099","orig":"Labette, KS","text":"Labette, KS"},{"id":"us-mn-073","orig":"Lac qui Parle, MN","text":"Lac qui Parle, MN"},{"id":"us-pa-069","orig":"Lackawanna, PA","text":"Lackawanna, PA"},{"id":"us-mo-105","orig":"Laclede, MO","text":"Laclede, MO"},{"id":"us-ar-073","orig":"Lafayette, AR","text":"Lafayette, AR"},{"id":"us-fl-067","orig":"Lafayette, FL","text":"Lafayette, FL"},{"id":"us-la-055","orig":"Lafayette, LA","text":"Lafayette, LA"},{"id":"us-mo-107","orig":"Lafayette, MO","text":"Lafayette, MO"},{"id":"us-ms-071","orig":"Lafayette, MS","text":"Lafayette, MS"},{"id":"us-wi-065","orig":"Lafayette, WI","text":"Lafayette, WI"},{"id":"us-la-057","orig":"Lafourche, LA","text":"Lafourche, LA"},{"id":"us-ak-164","orig":"Lake and Peninsula, AK","text":"Lake and Peninsula, AK"},{"id":"us-mn-077","orig":"Lake of the Woods, MN","text":"Lake of the Woods, MN"},{"id":"us-ca-033","orig":"Lake, CA","text":"Lake, CA"},{"id":"us-co-065","orig":"Lake, CO","text":"Lake, CO"},{"id":"us-fl-069","orig":"Lake, FL","text":"Lake, FL"},{"id":"us-il-097","orig":"Lake, IL","text":"Lake, IL"},{"id":"us-in-089","orig":"Lake, IN","text":"Lake, IN"},{"id":"us-mi-085","orig":"Lake, MI","text":"Lake, MI"},{"id":"us-mn-075","orig":"Lake, MN","text":"Lake, MN"},{"id":"us-mt-047","orig":"Lake, MT","text":"Lake, MT"},{"id":"us-oh-085","orig":"Lake, OH","text":"Lake, OH"},{"id":"us-or-037","orig":"Lake, OR","text":"Lake, OR"},{"id":"us-sd-079","orig":"Lake, SD","text":"Lake, SD"},{"id":"us-tn-095","orig":"Lake, TN","text":"Lake, TN"},{"id":"us-al-075","orig":"Lamar, AL","text":"Lamar, AL"},{"id":"us-ga-171","orig":"Lamar, GA","text":"Lamar, GA"},{"id":"us-ms-073","orig":"Lamar, MS","text":"Lamar, MS"},{"id":"us-tx-277","orig":"Lamar, TX","text":"Lamar, TX"},{"id":"us-tx-279","orig":"Lamb, TX","text":"Lamb, TX"},{"id":"us-vt-015","orig":"Lamoille, VT","text":"Lamoille, VT"},{"id":"us-tx-281","orig":"Lampasas, TX","text":"Lampasas, TX"},{"id":"us-ne-109","orig":"Lancaster, NE","text":"Lancaster, NE"},{"id":"us-pa-071","orig":"Lancaster, PA","text":"Lancaster, PA"},{"id":"us-sc-057","orig":"Lancaster, SC","text":"Lancaster, SC"},{"id":"us-va-103","orig":"Lancaster, VA","text":"Lancaster, VA"},{"id":"us-nv-015","orig":"Lander, NV","text":"Lander, NV"},{"id":"us-ks-101","orig":"Lane, KS","text":"Lane, KS"},{"id":"us-or-039","orig":"Lane, OR","text":"Lane, OR"},{"id":"us-wi-067","orig":"Langlade, WI","text":"Langlade, WI"},{"id":"us-ga-173","orig":"Lanier, GA","text":"Lanier, GA"},{"id":"us-mi-087","orig":"Lapeer, MI","text":"Lapeer, MI"},{"id":"us-wy-021","orig":"Laramie, WY","text":"Laramie, WY"},{"id":"us-co-069","orig":"Larimer, CO","text":"Larimer, CO"},{"id":"us-ky-123","orig":"Larue, KY","text":"Larue, KY"},{"id":"us-co-071","orig":"Las Animas, CO","text":"Las Animas, CO"},{"id":"us-ca-035","orig":"Lassen, CA","text":"Lassen, CA"},{"id":"us-id-057","orig":"Latah, ID","text":"Latah, ID"},{"id":"us-ok-077","orig":"Latimer, OK","text":"Latimer, OK"},{"id":"us-al-077","orig":"Lauderdale, AL","text":"Lauderdale, AL"},{"id":"us-ms-075","orig":"Lauderdale, MS","text":"Lauderdale, MS"},{"id":"us-tn-097","orig":"Lauderdale, TN","text":"Lauderdale, TN"},{"id":"us-ky-125","orig":"Laurel, KY","text":"Laurel, KY"},{"id":"us-ga-175","orig":"Laurens, GA","text":"Laurens, GA"},{"id":"us-sc-059","orig":"Laurens, SC","text":"Laurens, SC"},{"id":"us-tx-285","orig":"Lavaca, TX","text":"Lavaca, TX"},{"id":"us-al-079","orig":"Lawrence, AL","text":"Lawrence, AL"},{"id":"us-ar-075","orig":"Lawrence, AR","text":"Lawrence, AR"},{"id":"us-il-101","orig":"Lawrence, IL","text":"Lawrence, IL"},{"id":"us-in-093","orig":"Lawrence, IN","text":"Lawrence, IN"},{"id":"us-ky-127","orig":"Lawrence, KY","text":"Lawrence, KY"},{"id":"us-mo-109","orig":"Lawrence, MO","text":"Lawrence, MO"},{"id":"us-ms-077","orig":"Lawrence, MS","text":"Lawrence, MS"},{"id":"us-oh-087","orig":"Lawrence, OH","text":"Lawrence, OH"},{"id":"us-pa-073","orig":"Lawrence, PA","text":"Lawrence, PA"},{"id":"us-sd-081","orig":"Lawrence, SD","text":"Lawrence, SD"},{"id":"us-tn-099","orig":"Lawrence, TN","text":"Lawrence, TN"},{"id":"us-ok-079","orig":"Le Flore, OK","text":"Le Flore, OK"},{"id":"us-mn-079","orig":"Le Sueur, MN","text":"Le Sueur, MN"},{"id":"us-nm-025","orig":"Lea, NM","text":"Lea, NM"},{"id":"us-ms-079","orig":"Leake, MS","text":"Leake, MS"},{"id":"us-ks-103","orig":"Leavenworth, KS","text":"Leavenworth, KS"},{"id":"us-pa-075","orig":"Lebanon, PA","text":"Lebanon, PA"},{"id":"us-al-081","orig":"Lee, AL","text":"Lee, AL"},{"id":"us-ar-077","orig":"Lee, AR","text":"Lee, AR"},{"id":"us-fl-071","orig":"Lee, FL","text":"Lee, FL"},{"id":"us-ga-177","orig":"Lee, GA","text":"Lee, GA"},{"id":"us-ia-111","orig":"Lee, IA","text":"Lee, IA"},{"id":"us-il-103","orig":"Lee, IL","text":"Lee, IL"},{"id":"us-ky-129","orig":"Lee, KY","text":"Lee, KY"},{"id":"us-ms-081","orig":"Lee, MS","text":"Lee, MS"},{"id":"us-nc-105","orig":"Lee, NC","text":"Lee, NC"},{"id":"us-sc-061","orig":"Lee, SC","text":"Lee, SC"},{"id":"us-tx-287","orig":"Lee, TX","text":"Lee, TX"},{"id":"us-va-105","orig":"Lee, VA","text":"Lee, VA"},{"id":"us-mi-089","orig":"Leelanau, MI","text":"Leelanau, MI"},{"id":"us-ms-083","orig":"Leflore, MS","text":"Leflore, MS"},{"id":"us-pa-077","orig":"Lehigh, PA","text":"Lehigh, PA"},{"id":"us-id-059","orig":"Lemhi, ID","text":"Lemhi, ID"},{"id":"us-mi-091","orig":"Lenawee, MI","text":"Lenawee, MI"},{"id":"us-nc-107","orig":"Lenoir, NC","text":"Lenoir, NC"},{"id":"us-fl-073","orig":"Leon, FL","text":"Leon, FL"},{"id":"us-tx-289","orig":"Leon, TX","text":"Leon, TX"},{"id":"us-ky-131","orig":"Leslie, KY","text":"Leslie, KY"},{"id":"us-ky-133","orig":"Letcher, KY","text":"Letcher, KY"},{"id":"us-fl-075","orig":"Levy, FL","text":"Levy, FL"},{"id":"us-mt-049","orig":"Lewis and Clark, MT","text":"Lewis and Clark, MT"},{"id":"us-id-061","orig":"Lewis, ID","text":"Lewis, ID"},{"id":"us-ky-135","orig":"Lewis, KY","text":"Lewis, KY"},{"id":"us-mo-111","orig":"Lewis, MO","text":"Lewis, MO"},{"id":"us-ny-049","orig":"Lewis, NY","text":"Lewis, NY"},{"id":"us-tn-101","orig":"Lewis, TN","text":"Lewis, TN"},{"id":"us-wa-041","orig":"Lewis, WA","text":"Lewis, WA"},{"id":"us-wv-041","orig":"Lewis, WV","text":"Lewis, WV"},{"id":"us-sc-063","orig":"Lexington, SC","text":"Lexington, SC"},{"id":"us-va-678","orig":"Lexington, VA","text":"Lexington, VA"},{"id":"us-fl-077","orig":"Liberty, FL","text":"Liberty, FL"},{"id":"us-ga-179","orig":"Liberty, GA","text":"Liberty, GA"},{"id":"us-mt-051","orig":"Liberty, MT","text":"Liberty, MT"},{"id":"us-tx-291","orig":"Liberty, TX","text":"Liberty, TX"},{"id":"us-oh-089","orig":"Licking, OH","text":"Licking, OH"},{"id":"us-al-083","orig":"Limestone, AL","text":"Limestone, AL"},{"id":"us-tx-293","orig":"Limestone, TX","text":"Limestone, TX"},{"id":"us-ar-079","orig":"Lincoln, AR","text":"Lincoln, AR"},{"id":"us-co-073","orig":"Lincoln, CO","text":"Lincoln, CO"},{"id":"us-ga-181","orig":"Lincoln, GA","text":"Lincoln, GA"},{"id":"us-id-063","orig":"Lincoln, ID","text":"Lincoln, ID"},{"id":"us-ks-105","orig":"Lincoln, KS","text":"Lincoln, KS"},{"id":"us-ky-137","orig":"Lincoln, KY","text":"Lincoln, KY"},{"id":"us-la-061","orig":"Lincoln, LA","text":"Lincoln, LA"},{"id":"us-me-015","orig":"Lincoln, ME","text":"Lincoln, ME"},{"id":"us-mn-081","orig":"Lincoln, MN","text":"Lincoln, MN"},{"id":"us-mo-113","orig":"Lincoln, MO","text":"Lincoln, MO"},{"id":"us-ms-085","orig":"Lincoln, MS","text":"Lincoln, MS"},{"id":"us-mt-053","orig":"Lincoln, MT","text":"Lincoln, MT"},{"id":"us-nc-109","orig":"Lincoln, NC","text":"Lincoln, NC"},{"id":"us-ne-111","orig":"Lincoln, NE","text":"Lincoln, NE"},{"id":"us-nm-027","orig":"Lincoln, NM","text":"Lincoln, NM"},{"id":"us-nv-017","orig":"Lincoln, NV","text":"Lincoln, NV"},{"id":"us-ok-081","orig":"Lincoln, OK","text":"Lincoln, OK"},{"id":"us-or-041","orig":"Lincoln, OR","text":"Lincoln, OR"},{"id":"us-sd-083","orig":"Lincoln, SD","text":"Lincoln, SD"},{"id":"us-tn-103","orig":"Lincoln, TN","text":"Lincoln, TN"},{"id":"us-wa-043","orig":"Lincoln, WA","text":"Lincoln, WA"},{"id":"us-wi-069","orig":"Lincoln, WI","text":"Lincoln, WI"},{"id":"us-wv-043","orig":"Lincoln, WV","text":"Lincoln, WV"},{"id":"us-wy-023","orig":"Lincoln, WY","text":"Lincoln, WY"},{"id":"us-ia-113","orig":"Linn, IA","text":"Linn, IA"},{"id":"us-ks-107","orig":"Linn, KS","text":"Linn, KS"},{"id":"us-mo-115","orig":"Linn, MO","text":"Linn, MO"},{"id":"us-or-043","orig":"Linn, OR","text":"Linn, OR"},{"id":"us-tx-295","orig":"Lipscomb, TX","text":"Lipscomb, TX"},{"id":"us-ct-005","orig":"Litchfield, CT","text":"Litchfield, CT"},{"id":"us-ar-081","orig":"Little River, AR","text":"Little River, AR"},{"id":"us-tx-297","orig":"Live Oak, TX","text":"Live Oak, TX"},{"id":"us-il-105","orig":"Livingston, IL","text":"Livingston, IL"},{"id":"us-ky-139","orig":"Livingston, KY","text":"Livingston, KY"},{"id":"us-la-063","orig":"Livingston, LA","text":"Livingston, LA"},{"id":"us-mi-093","orig":"Livingston, MI","text":"Livingston, MI"},{"id":"us-mo-117","orig":"Livingston, MO","text":"Livingston, MO"},{"id":"us-ny-051","orig":"Livingston, NY","text":"Livingston, NY"},{"id":"us-tx-299","orig":"Llano, TX","text":"Llano, TX"},{"id":"us-ar-083","orig":"Logan, AR","text":"Logan, AR"},{"id":"us-co-075","orig":"Logan, CO","text":"Logan, CO"},{"id":"us-il-107","orig":"Logan, IL","text":"Logan, IL"},{"id":"us-ks-109","orig":"Logan, KS","text":"Logan, KS"},{"id":"us-ky-141","orig":"Logan, KY","text":"Logan, KY"},{"id":"us-nd-047","orig":"Logan, ND","text":"Logan, ND"},{"id":"us-ne-113","orig":"Logan, NE","text":"Logan, NE"},{"id":"us-oh-091","orig":"Logan, OH","text":"Logan, OH"},{"id":"us-ok-083","orig":"Logan, OK","text":"Logan, OK"},{"id":"us-wv-045","orig":"Logan, WV","text":"Logan, WV"},{"id":"us-ga-183","orig":"Long, GA","text":"Long, GA"},{"id":"us-ar-085","orig":"Lonoke, AR","text":"Lonoke, AR"},{"id":"us-oh-093","orig":"Lorain, OH","text":"Lorain, OH"},{"id":"us-nm-028","orig":"Los Alamos, NM","text":"Los Alamos, NM"},{"id":"us-ca-037","orig":"Los Angeles, CA","text":"Los Angeles, CA"},{"id":"us-tn-105","orig":"Loudon, TN","text":"Loudon, TN"},{"id":"us-va-107","orig":"Loudoun, VA","text":"Loudoun, VA"},{"id":"us-ia-115","orig":"Louisa, IA","text":"Louisa, IA"},{"id":"us-va-109","orig":"Louisa, VA","text":"Louisa, VA"},{"id":"us-ne-115","orig":"Loup, NE","text":"Loup, NE"},{"id":"us-ok-085","orig":"Love, OK","text":"Love, OK"},{"id":"us-tx-301","orig":"Loving, TX","text":"Loving, TX"},{"id":"us-al-085","orig":"Lowndes, AL","text":"Lowndes, AL"},{"id":"us-ga-185","orig":"Lowndes, GA","text":"Lowndes, GA"},{"id":"us-ms-087","orig":"Lowndes, MS","text":"Lowndes, MS"},{"id":"us-tx-303","orig":"Lubbock, TX","text":"Lubbock, TX"},{"id":"us-ia-117","orig":"Lucas, IA","text":"Lucas, IA"},{"id":"us-oh-095","orig":"Lucas, OH","text":"Lucas, OH"},{"id":"us-mi-095","orig":"Luce, MI","text":"Luce, MI"},{"id":"us-ga-187","orig":"Lumpkin, GA","text":"Lumpkin, GA"},{"id":"us-nm-029","orig":"Luna, NM","text":"Luna, NM"},{"id":"us-va-111","orig":"Lunenburg, VA","text":"Lunenburg, VA"},{"id":"us-pa-079","orig":"Luzerne, PA","text":"Luzerne, PA"},{"id":"us-pa-081","orig":"Lycoming, PA","text":"Lycoming, PA"},{"id":"us-sd-085","orig":"Lyman, SD","text":"Lyman, SD"},{"id":"us-va-680","orig":"Lynchburg, VA","text":"Lynchburg, VA"},{"id":"us-tx-305","orig":"Lynn, TX","text":"Lynn, TX"},{"id":"us-ia-119","orig":"Lyon, IA","text":"Lyon, IA"},{"id":"us-ks-111","orig":"Lyon, KS","text":"Lyon, KS"},{"id":"us-ky-143","orig":"Lyon, KY","text":"Lyon, KY"},{"id":"us-mn-083","orig":"Lyon, MN","text":"Lyon, MN"},{"id":"us-nv-019","orig":"Lyon, NV","text":"Lyon, NV"},{"id":"us-mi-097","orig":"Mackinac, MI","text":"Mackinac, MI"},{"id":"us-mi-099","orig":"Macomb, MI","text":"Macomb, MI"},{"id":"us-al-087","orig":"Macon, AL","text":"Macon, AL"},{"id":"us-ga-193","orig":"Macon, GA","text":"Macon, GA"},{"id":"us-il-115","orig":"Macon, IL","text":"Macon, IL"},{"id":"us-mo-121","orig":"Macon, MO","text":"Macon, MO"},{"id":"us-nc-113","orig":"Macon, NC","text":"Macon, NC"},{"id":"us-tn-111","orig":"Macon, TN","text":"Macon, TN"},{"id":"us-il-117","orig":"Macoupin, IL","text":"Macoupin, IL"},{"id":"us-ca-039","orig":"Madera, CA","text":"Madera, CA"},{"id":"us-al-089","orig":"Madison, AL","text":"Madison, AL"},{"id":"us-ar-087","orig":"Madison, AR","text":"Madison, AR"},{"id":"us-fl-079","orig":"Madison, FL","text":"Madison, FL"},{"id":"us-ga-195","orig":"Madison, GA","text":"Madison, GA"},{"id":"us-ia-121","orig":"Madison, IA","text":"Madison, IA"},{"id":"us-id-065","orig":"Madison, ID","text":"Madison, ID"},{"id":"us-il-119","orig":"Madison, IL","text":"Madison, IL"},{"id":"us-in-095","orig":"Madison, IN","text":"Madison, IN"},{"id":"us-ky-151","orig":"Madison, KY","text":"Madison, KY"},{"id":"us-la-065","orig":"Madison, LA","text":"Madison, LA"},{"id":"us-mo-123","orig":"Madison, MO","text":"Madison, MO"},{"id":"us-ms-089","orig":"Madison, MS","text":"Madison, MS"},{"id":"us-mt-057","orig":"Madison, MT","text":"Madison, MT"},{"id":"us-nc-115","orig":"Madison, NC","text":"Madison, NC"},{"id":"us-ne-119","orig":"Madison, NE","text":"Madison, NE"},{"id":"us-ny-053","orig":"Madison, NY","text":"Madison, NY"},{"id":"us-oh-097","orig":"Madison, OH","text":"Madison, OH"},{"id":"us-tn-113","orig":"Madison, TN","text":"Madison, TN"},{"id":"us-tx-313","orig":"Madison, TX","text":"Madison, TX"},{"id":"us-va-113","orig":"Madison, VA","text":"Madison, VA"},{"id":"us-ky-153","orig":"Magoffin, KY","text":"Magoffin, KY"},{"id":"us-ia-123","orig":"Mahaska, IA","text":"Mahaska, IA"},{"id":"us-mn-087","orig":"Mahnomen, MN","text":"Mahnomen, MN"},{"id":"us-oh-099","orig":"Mahoning, OH","text":"Mahoning, OH"},{"id":"us-ok-093","orig":"Major, OK","text":"Major, OK"},{"id":"us-or-045","orig":"Malheur, OR","text":"Malheur, OR"},{"id":"us-va-685","orig":"Manassas Park, VA","text":"Manassas Park, VA"},{"id":"us-va-683","orig":"Manassas, VA","text":"Manassas, VA"},{"id":"us-fl-081","orig":"Manatee, FL","text":"Manatee, FL"},{"id":"us-mi-101","orig":"Manistee, MI","text":"Manistee, MI"},{"id":"us-wi-071","orig":"Manitowoc, WI","text":"Manitowoc, WI"},{"id":"us-wi-073","orig":"Marathon, WI","text":"Marathon, WI"},{"id":"us-al-091","orig":"Marengo, AL","text":"Marengo, AL"},{"id":"us-az-013","orig":"Maricopa, AZ","text":"Maricopa, AZ"},{"id":"us-mo-125","orig":"Maries, MO","text":"Maries, MO"},{"id":"us-ca-041","orig":"Marin, CA","text":"Marin, CA"},{"id":"us-wi-075","orig":"Marinette, WI","text":"Marinette, WI"},{"id":"us-al-093","orig":"Marion, AL","text":"Marion, AL"},{"id":"us-ar-089","orig":"Marion, AR","text":"Marion, AR"},{"id":"us-fl-083","orig":"Marion, FL","text":"Marion, FL"},{"id":"us-ga-197","orig":"Marion, GA","text":"Marion, GA"},{"id":"us-ia-125","orig":"Marion, IA","text":"Marion, IA"},{"id":"us-il-121","orig":"Marion, IL","text":"Marion, IL"},{"id":"us-in-097","orig":"Marion, IN","text":"Marion, IN"},{"id":"us-ks-115","orig":"Marion, KS","text":"Marion, KS"},{"id":"us-ky-155","orig":"Marion, KY","text":"Marion, KY"},{"id":"us-mo-127","orig":"Marion, MO","text":"Marion, MO"},{"id":"us-ms-091","orig":"Marion, MS","text":"Marion, MS"},{"id":"us-oh-101","orig":"Marion, OH","text":"Marion, OH"},{"id":"us-or-047","orig":"Marion, OR","text":"Marion, OR"},{"id":"us-sc-067","orig":"Marion, SC","text":"Marion, SC"},{"id":"us-tn-115","orig":"Marion, TN","text":"Marion, TN"},{"id":"us-tx-315","orig":"Marion, TX","text":"Marion, TX"},{"id":"us-wv-049","orig":"Marion, WV","text":"Marion, WV"},{"id":"us-ca-043","orig":"Mariposa, CA","text":"Mariposa, CA"},{"id":"us-sc-069","orig":"Marlboro, SC","text":"Marlboro, SC"},{"id":"us-mi-103","orig":"Marquette, MI","text":"Marquette, MI"},{"id":"us-wi-077","orig":"Marquette, WI","text":"Marquette, WI"},{"id":"us-al-095","orig":"Marshall, AL","text":"Marshall, AL"},{"id":"us-ia-127","orig":"Marshall, IA","text":"Marshall, IA"},{"id":"us-il-123","orig":"Marshall, IL","text":"Marshall, IL"},{"id":"us-in-099","orig":"Marshall, IN","text":"Marshall, IN"},{"id":"us-ks-117","orig":"Marshall, KS","text":"Marshall, KS"},{"id":"us-ky-157","orig":"Marshall, KY","text":"Marshall, KY"},{"id":"us-mn-089","orig":"Marshall, MN","text":"Marshall, MN"},{"id":"us-ms-093","orig":"Marshall, MS","text":"Marshall, MS"},{"id":"us-ok-095","orig":"Marshall, OK","text":"Marshall, OK"},{"id":"us-sd-091","orig":"Marshall, SD","text":"Marshall, SD"},{"id":"us-tn-117","orig":"Marshall, TN","text":"Marshall, TN"},{"id":"us-wv-051","orig":"Marshall, WV","text":"Marshall, WV"},{"id":"us-fl-085","orig":"Martin, FL","text":"Martin, FL"},{"id":"us-in-101","orig":"Martin, IN","text":"Martin, IN"},{"id":"us-ky-159","orig":"Martin, KY","text":"Martin, KY"},{"id":"us-mn-091","orig":"Martin, MN","text":"Martin, MN"},{"id":"us-nc-117","orig":"Martin, NC","text":"Martin, NC"},{"id":"us-tx-317","orig":"Martin, TX","text":"Martin, TX"},{"id":"us-va-690","orig":"Martinsville, VA","text":"Martinsville, VA"},{"id":"us-il-125","orig":"Mason, IL","text":"Mason, IL"},{"id":"us-ky-161","orig":"Mason, KY","text":"Mason, KY"},{"id":"us-mi-105","orig":"Mason, MI","text":"Mason, MI"},{"id":"us-tx-319","orig":"Mason, TX","text":"Mason, TX"},{"id":"us-wa-045","orig":"Mason, WA","text":"Mason, WA"},{"id":"us-wv-053","orig":"Mason, WV","text":"Mason, WV"},{"id":"us-il-127","orig":"Massac, IL","text":"Massac, IL"},{"id":"us-tx-321","orig":"Matagorda, TX","text":"Matagorda, TX"},{"id":"us-ak-170","orig":"Matanuska-Susitna, AK","text":"Matanuska-Susitna, AK"},{"id":"us-va-115","orig":"Mathews, VA","text":"Mathews, VA"},{"id":"us-hi-009","orig":"Maui, HI","text":"Maui, HI"},{"id":"us-tn-119","orig":"Maury, TN","text":"Maury, TN"},{"id":"us-tx-323","orig":"Maverick, TX","text":"Maverick, TX"},{"id":"us-ok-097","orig":"Mayes, OK","text":"Mayes, OK"},{"id":"us-ok-087","orig":"McClain, OK","text":"McClain, OK"},{"id":"us-mt-055","orig":"McCone, MT","text":"McCone, MT"},{"id":"us-sd-087","orig":"McCook, SD","text":"McCook, SD"},{"id":"us-sc-065","orig":"McCormick, SC","text":"McCormick, SC"},{"id":"us-ky-145","orig":"McCracken, KY","text":"McCracken, KY"},{"id":"us-ky-147","orig":"McCreary, KY","text":"McCreary, KY"},{"id":"us-tx-307","orig":"McCulloch, TX","text":"McCulloch, TX"},{"id":"us-ok-089","orig":"McCurtain, OK","text":"McCurtain, OK"},{"id":"us-mo-119","orig":"McDonald, MO","text":"McDonald, MO"},{"id":"us-il-109","orig":"McDonough, IL","text":"McDonough, IL"},{"id":"us-nc-111","orig":"McDowell, NC","text":"McDowell, NC"},{"id":"us-wv-047","orig":"McDowell, WV","text":"McDowell, WV"},{"id":"us-ga-189","orig":"McDuffie, GA","text":"McDuffie, GA"},{"id":"us-il-111","orig":"McHenry, IL","text":"McHenry, IL"},{"id":"us-nd-049","orig":"McHenry, ND","text":"McHenry, ND"},{"id":"us-ga-191","orig":"McIntosh, GA","text":"McIntosh, GA"},{"id":"us-nd-051","orig":"McIntosh, ND","text":"McIntosh, ND"},{"id":"us-ok-091","orig":"McIntosh, OK","text":"McIntosh, OK"},{"id":"us-pa-083","orig":"McKean, PA","text":"McKean, PA"},{"id":"us-nd-053","orig":"McKenzie, ND","text":"McKenzie, ND"},{"id":"us-nm-031","orig":"McKinley, NM","text":"McKinley, NM"},{"id":"us-il-113","orig":"McLean, IL","text":"McLean, IL"},{"id":"us-ky-149","orig":"McLean, KY","text":"McLean, KY"},{"id":"us-nd-055","orig":"McLean, ND","text":"McLean, ND"},{"id":"us-tx-309","orig":"McLennan, TX","text":"McLennan, TX"},{"id":"us-mn-085","orig":"McLeod, MN","text":"McLeod, MN"},{"id":"us-tn-107","orig":"McMinn, TN","text":"McMinn, TN"},{"id":"us-tx-311","orig":"McMullen, TX","text":"McMullen, TX"},{"id":"us-tn-109","orig":"McNairy, TN","text":"McNairy, TN"},{"id":"us-ks-113","orig":"McPherson, KS","text":"McPherson, KS"},{"id":"us-ne-117","orig":"McPherson, NE","text":"McPherson, NE"},{"id":"us-sd-089","orig":"McPherson, SD","text":"McPherson, SD"},{"id":"us-ks-119","orig":"Meade, KS","text":"Meade, KS"},{"id":"us-ky-163","orig":"Meade, KY","text":"Meade, KY"},{"id":"us-sd-093","orig":"Meade, SD","text":"Meade, SD"},{"id":"us-mt-059","orig":"Meagher, MT","text":"Meagher, MT"},{"id":"us-nc-119","orig":"Mecklenburg, NC","text":"Mecklenburg, NC"},{"id":"us-va-117","orig":"Mecklenburg, VA","text":"Mecklenburg, VA"},{"id":"us-mi-107","orig":"Mecosta, MI","text":"Mecosta, MI"},{"id":"us-oh-103","orig":"Medina, OH","text":"Medina, OH"},{"id":"us-tx-325","orig":"Medina, TX","text":"Medina, TX"},{"id":"us-mn-093","orig":"Meeker, MN","text":"Meeker, MN"},{"id":"us-oh-105","orig":"Meigs, OH","text":"Meigs, OH"},{"id":"us-tn-121","orig":"Meigs, TN","text":"Meigs, TN"},{"id":"us-sd-095","orig":"Mellette, SD","text":"Mellette, SD"},{"id":"us-il-129","orig":"Menard, IL","text":"Menard, IL"},{"id":"us-tx-327","orig":"Menard, TX","text":"Menard, TX"},{"id":"us-ca-045","orig":"Mendocino, CA","text":"Mendocino, CA"},{"id":"us-ky-165","orig":"Menifee, KY","text":"Menifee, KY"},{"id":"us-mi-109","orig":"Menominee, MI","text":"Menominee, MI"},{"id":"us-wi-078","orig":"Menominee, WI","text":"Menominee, WI"},{"id":"us-ca-047","orig":"Merced, CA","text":"Merced, CA"},{"id":"us-il-131","orig":"Mercer, IL","text":"Mercer, IL"},{"id":"us-ky-167","orig":"Mercer, KY","text":"Mercer, KY"},{"id":"us-mo-129","orig":"Mercer, MO","text":"Mercer, MO"},{"id":"us-nd-057","orig":"Mercer, ND","text":"Mercer, ND"},{"id":"us-nj-021","orig":"Mercer, NJ","text":"Mercer, NJ"},{"id":"us-oh-107","orig":"Mercer, OH","text":"Mercer, OH"},{"id":"us-pa-085","orig":"Mercer, PA","text":"Mercer, PA"},{"id":"us-wv-055","orig":"Mercer, WV","text":"Mercer, WV"},{"id":"us-ga-199","orig":"Meriwether, GA","text":"Meriwether, GA"},{"id":"us-ne-121","orig":"Merrick, NE","text":"Merrick, NE"},{"id":"us-nh-013","orig":"Merrimack, NH","text":"Merrimack, NH"},{"id":"us-co-077","orig":"Mesa, CO","text":"Mesa, CO"},{"id":"us-ky-169","orig":"Metcalfe, KY","text":"Metcalfe, KY"},{"id":"us-in-103","orig":"Miami, IN","text":"Miami, IN"},{"id":"us-ks-121","orig":"Miami, KS","text":"Miami, KS"},{"id":"us-oh-109","orig":"Miami, OH","text":"Miami, OH"},{"id":"us-fl-086","orig":"Miami-Dade, FL","text":"Miami-Dade, FL"},{"id":"us-ct-007","orig":"Middlesex, CT","text":"Middlesex, CT"},{"id":"us-ma-017","orig":"Middlesex, MA","text":"Middlesex, MA"},{"id":"us-nj-023","orig":"Middlesex, NJ","text":"Middlesex, NJ"},{"id":"us-va-119","orig":"Middlesex, VA","text":"Middlesex, VA"},{"id":"us-mi-111","orig":"Midland, MI","text":"Midland, MI"},{"id":"us-tx-329","orig":"Midland, TX","text":"Midland, TX"},{"id":"us-pa-087","orig":"Mifflin, PA","text":"Mifflin, PA"},{"id":"us-tx-331","orig":"Milam, TX","text":"Milam, TX"},{"id":"us-ut-027","orig":"Millard, UT","text":"Millard, UT"},{"id":"us-mn-095","orig":"Mille Lacs, MN","text":"Mille Lacs, MN"},{"id":"us-ar-091","orig":"Miller, AR","text":"Miller, AR"},{"id":"us-ga-201","orig":"Miller, GA","text":"Miller, GA"},{"id":"us-mo-131","orig":"Miller, MO","text":"Miller, MO"},{"id":"us-ia-129","orig":"Mills, IA","text":"Mills, IA"},{"id":"us-tx-333","orig":"Mills, TX","text":"Mills, TX"},{"id":"us-wi-079","orig":"Milwaukee, WI","text":"Milwaukee, WI"},{"id":"us-sd-097","orig":"Miner, SD","text":"Miner, SD"},{"id":"us-co-079","orig":"Mineral, CO","text":"Mineral, CO"},{"id":"us-mt-061","orig":"Mineral, MT","text":"Mineral, MT"},{"id":"us-nv-021","orig":"Mineral, NV","text":"Mineral, NV"},{"id":"us-wv-057","orig":"Mineral, WV","text":"Mineral, WV"},{"id":"us-wv-059","orig":"Mingo, WV","text":"Mingo, WV"},{"id":"us-id-067","orig":"Minidoka, ID","text":"Minidoka, ID"},{"id":"us-sd-099","orig":"Minnehaha, SD","text":"Minnehaha, SD"},{"id":"us-mi-113","orig":"Missaukee, MI","text":"Missaukee, MI"},{"id":"us-ar-093","orig":"Mississippi, AR","text":"Mississippi, AR"},{"id":"us-mo-133","orig":"Mississippi, MO","text":"Mississippi, MO"},{"id":"us-mt-063","orig":"Missoula, MT","text":"Missoula, MT"},{"id":"us-ga-205","orig":"Mitchell, GA","text":"Mitchell, GA"},{"id":"us-ia-131","orig":"Mitchell, IA","text":"Mitchell, IA"},{"id":"us-ks-123","orig":"Mitchell, KS","text":"Mitchell, KS"},{"id":"us-nc-121","orig":"Mitchell, NC","text":"Mitchell, NC"},{"id":"us-tx-335","orig":"Mitchell, TX","text":"Mitchell, TX"},{"id":"us-al-097","orig":"Mobile, AL","text":"Mobile, AL"},{"id":"us-ca-049","orig":"Modoc, CA","text":"Modoc, CA"},{"id":"us-co-081","orig":"Moffat, CO","text":"Moffat, CO"},{"id":"us-az-015","orig":"Mohave, AZ","text":"Mohave, AZ"},{"id":"us-mo-135","orig":"Moniteau, MO","text":"Moniteau, MO"},{"id":"us-nj-025","orig":"Monmouth, NJ","text":"Monmouth, NJ"},{"id":"us-ca-051","orig":"Mono, CA","text":"Mono, CA"},{"id":"us-ia-133","orig":"Monona, IA","text":"Monona, IA"},{"id":"us-wv-061","orig":"Monongalia, WV","text":"Monongalia, WV"},{"id":"us-al-099","orig":"Monroe, AL","text":"Monroe, AL"},{"id":"us-ar-095","orig":"Monroe, AR","text":"Monroe, AR"},{"id":"us-fl-087","orig":"Monroe, FL","text":"Monroe, FL"},{"id":"us-ga-207","orig":"Monroe, GA","text":"Monroe, GA"},{"id":"us-ia-135","orig":"Monroe, IA","text":"Monroe, IA"},{"id":"us-il-133","orig":"Monroe, IL","text":"Monroe, IL"},{"id":"us-in-105","orig":"Monroe, IN","text":"Monroe, IN"},{"id":"us-ky-171","orig":"Monroe, KY","text":"Monroe, KY"},{"id":"us-mi-115","orig":"Monroe, MI","text":"Monroe, MI"},{"id":"us-mo-137","orig":"Monroe, MO","text":"Monroe, MO"},{"id":"us-ms-095","orig":"Monroe, MS","text":"Monroe, MS"},{"id":"us-ny-055","orig":"Monroe, NY","text":"Monroe, NY"},{"id":"us-oh-111","orig":"Monroe, OH","text":"Monroe, OH"},{"id":"us-pa-089","orig":"Monroe, PA","text":"Monroe, PA"},{"id":"us-tn-123","orig":"Monroe, TN","text":"Monroe, TN"},{"id":"us-wi-081","orig":"Monroe, WI","text":"Monroe, WI"},{"id":"us-wv-063","orig":"Monroe, WV","text":"Monroe, WV"},{"id":"us-tx-337","orig":"Montague, TX","text":"Montague, TX"},{"id":"us-mi-117","orig":"Montcalm, MI","text":"Montcalm, MI"},{"id":"us-ca-053","orig":"Monterey, CA","text":"Monterey, CA"},{"id":"us-co-083","orig":"Montezuma, CO","text":"Montezuma, CO"},{"id":"us-al-101","orig":"Montgomery, AL","text":"Montgomery, AL"},{"id":"us-ar-097","orig":"Montgomery, AR","text":"Montgomery, AR"},{"id":"us-ga-209","orig":"Montgomery, GA","text":"Montgomery, GA"},{"id":"us-ia-137","orig":"Montgomery, IA","text":"Montgomery, IA"},{"id":"us-il-135","orig":"Montgomery, IL","text":"Montgomery, IL"},{"id":"us-in-107","orig":"Montgomery, IN","text":"Montgomery, IN"},{"id":"us-ks-125","orig":"Montgomery, KS","text":"Montgomery, KS"},{"id":"us-ky-173","orig":"Montgomery, KY","text":"Montgomery, KY"},{"id":"us-md-031","orig":"Montgomery, MD","text":"Montgomery, MD"},{"id":"us-mo-139","orig":"Montgomery, MO","text":"Montgomery, MO"},{"id":"us-ms-097","orig":"Montgomery, MS","text":"Montgomery, MS"},{"id":"us-nc-123","orig":"Montgomery, NC","text":"Montgomery, NC"},{"id":"us-ny-057","orig":"Montgomery, NY","text":"Montgomery, NY"},{"id":"us-oh-113","orig":"Montgomery, OH","text":"Montgomery, OH"},{"id":"us-pa-091","orig":"Montgomery, PA","text":"Montgomery, PA"},{"id":"us-tn-125","orig":"Montgomery, TN","text":"Montgomery, TN"},{"id":"us-tx-339","orig":"Montgomery, TX","text":"Montgomery, TX"},{"id":"us-va-121","orig":"Montgomery, VA","text":"Montgomery, VA"},{"id":"us-mi-119","orig":"Montmorency, MI","text":"Montmorency, MI"},{"id":"us-pa-093","orig":"Montour, PA","text":"Montour, PA"},{"id":"us-co-085","orig":"Montrose, CO","text":"Montrose, CO"},{"id":"us-sd-101","orig":"Moody, SD","text":"Moody, SD"},{"id":"us-nc-125","orig":"Moore, NC","text":"Moore, NC"},{"id":"us-tn-127","orig":"Moore, TN","text":"Moore, TN"},{"id":"us-tx-341","orig":"Moore, TX","text":"Moore, TX"},{"id":"us-nm-033","orig":"Mora, NM","text":"Mora, NM"},{"id":"us-la-067","orig":"Morehouse, LA","text":"Morehouse, LA"},{"id":"us-al-103","orig":"Morgan, AL","text":"Morgan, AL"},{"id":"us-co-087","orig":"Morgan, CO","text":"Morgan, CO"},{"id":"us-ga-211","orig":"Morgan, GA","text":"Morgan, GA"},{"id":"us-il-137","orig":"Morgan, IL","text":"Morgan, IL"},{"id":"us-in-109","orig":"Morgan, IN","text":"Morgan, IN"},{"id":"us-ky-175","orig":"Morgan, KY","text":"Morgan, KY"},{"id":"us-mo-141","orig":"Morgan, MO","text":"Morgan, MO"},{"id":"us-oh-115","orig":"Morgan, OH","text":"Morgan, OH"},{"id":"us-tn-129","orig":"Morgan, TN","text":"Morgan, TN"},{"id":"us-ut-029","orig":"Morgan, UT","text":"Morgan, UT"},{"id":"us-wv-065","orig":"Morgan, WV","text":"Morgan, WV"},{"id":"us-ne-123","orig":"Morrill, NE","text":"Morrill, NE"},{"id":"us-ks-127","orig":"Morris, KS","text":"Morris, KS"},{"id":"us-nj-027","orig":"Morris, NJ","text":"Morris, NJ"},{"id":"us-tx-343","orig":"Morris, TX","text":"Morris, TX"},{"id":"us-mn-097","orig":"Morrison, MN","text":"Morrison, MN"},{"id":"us-oh-117","orig":"Morrow, OH","text":"Morrow, OH"},{"id":"us-or-049","orig":"Morrow, OR","text":"Morrow, OR"},{"id":"us-ks-129","orig":"Morton, KS","text":"Morton, KS"},{"id":"us-nd-059","orig":"Morton, ND","text":"Morton, ND"},{"id":"us-tx-345","orig":"Motley, TX","text":"Motley, TX"},{"id":"us-il-139","orig":"Moultrie, IL","text":"Moultrie, IL"},{"id":"us-nd-061","orig":"Mountrail, ND","text":"Mountrail, ND"},{"id":"us-mn-099","orig":"Mower, MN","text":"Mower, MN"},{"id":"us-ky-177","orig":"Muhlenberg, KY","text":"Muhlenberg, KY"},{"id":"us-or-051","orig":"Multnomah, OR","text":"Multnomah, OR"},{"id":"us-ga-213","orig":"Murray, GA","text":"Murray, GA"},{"id":"us-mn-101","orig":"Murray, MN","text":"Murray, MN"},{"id":"us-ok-099","orig":"Murray, OK","text":"Murray, OK"},{"id":"us-ia-139","orig":"Muscatine, IA","text":"Muscatine, IA"},{"id":"us-ga-215","orig":"Muscogee, GA","text":"Muscogee, GA"},{"id":"us-mi-121","orig":"Muskegon, MI","text":"Muskegon, MI"},{"id":"us-oh-119","orig":"Muskingum, OH","text":"Muskingum, OH"},{"id":"us-ok-101","orig":"Muskogee, OK","text":"Muskogee, OK"},{"id":"us-mt-065","orig":"Musselshell, MT","text":"Musselshell, MT"},{"id":"us-tx-347","orig":"Nacogdoches, TX","text":"Nacogdoches, TX"},{"id":"us-ne-125","orig":"Nance, NE","text":"Nance, NE"},{"id":"us-ma-019","orig":"Nantucket, MA","text":"Nantucket, MA"},{"id":"us-ca-055","orig":"Napa, CA","text":"Napa, CA"},{"id":"us-nc-127","orig":"Nash, NC","text":"Nash, NC"},{"id":"us-fl-089","orig":"Nassau, FL","text":"Nassau, FL"},{"id":"us-ny-059","orig":"Nassau, NY","text":"Nassau, NY"},{"id":"us-la-069","orig":"Natchitoches, LA","text":"Natchitoches, LA"},{"id":"us-wy-025","orig":"Natrona, WY","text":"Natrona, WY"},{"id":"us-az-017","orig":"Navajo, AZ","text":"Navajo, AZ"},{"id":"us-tx-349","orig":"Navarro, TX","text":"Navarro, TX"},{"id":"us-ky-179","orig":"Nelson, KY","text":"Nelson, KY"},{"id":"us-nd-063","orig":"Nelson, ND","text":"Nelson, ND"},{"id":"us-va-125","orig":"Nelson, VA","text":"Nelson, VA"},{"id":"us-ks-131","orig":"Nemaha, KS","text":"Nemaha, KS"},{"id":"us-ne-127","orig":"Nemaha, NE","text":"Nemaha, NE"},{"id":"us-ks-133","orig":"Neosho, KS","text":"Neosho, KS"},{"id":"us-ms-099","orig":"Neshoba, MS","text":"Neshoba, MS"},{"id":"us-ks-135","orig":"Ness, KS","text":"Ness, KS"},{"id":"us-ar-099","orig":"Nevada, AR","text":"Nevada, AR"},{"id":"us-ca-057","orig":"Nevada, CA","text":"Nevada, CA"},{"id":"us-de-003","orig":"New Castle, DE","text":"New Castle, DE"},{"id":"us-nc-129","orig":"New Hanover, NC","text":"New Hanover, NC"},{"id":"us-ct-009","orig":"New Haven, CT","text":"New Haven, CT"},{"id":"us-va-127","orig":"New Kent, VA","text":"New Kent, VA"},{"id":"us-ct-011","orig":"New London, CT","text":"New London, CT"},{"id":"us-mo-143","orig":"New Madrid, MO","text":"New Madrid, MO"},{"id":"us-ny-061","orig":"New York, NY","text":"New York, NY"},{"id":"us-mi-123","orig":"Newaygo, MI","text":"Newaygo, MI"},{"id":"us-sc-071","orig":"Newberry, SC","text":"Newberry, SC"},{"id":"us-va-700","orig":"Newport News, VA","text":"Newport News, VA"},{"id":"us-ri-005","orig":"Newport, RI","text":"Newport, RI"},{"id":"us-ar-101","orig":"Newton, AR","text":"Newton, AR"},{"id":"us-ga-217","orig":"Newton, GA","text":"Newton, GA"},{"id":"us-in-111","orig":"Newton, IN","text":"Newton, IN"},{"id":"us-mo-145","orig":"Newton, MO","text":"Newton, MO"},{"id":"us-ms-101","orig":"Newton, MS","text":"Newton, MS"},{"id":"us-tx-351","orig":"Newton, TX","text":"Newton, TX"},{"id":"us-id-069","orig":"Nez Perce, ID","text":"Nez Perce, ID"},{"id":"us-ny-063","orig":"Niagara, NY","text":"Niagara, NY"},{"id":"us-ky-181","orig":"Nicholas, KY","text":"Nicholas, KY"},{"id":"us-wv-067","orig":"Nicholas, WV","text":"Nicholas, WV"},{"id":"us-mn-103","orig":"Nicollet, MN","text":"Nicollet, MN"},{"id":"us-wy-027","orig":"Niobrara, WY","text":"Niobrara, WY"},{"id":"us-in-113","orig":"Noble, IN","text":"Noble, IN"},{"id":"us-oh-121","orig":"Noble, OH","text":"Noble, OH"},{"id":"us-ok-103","orig":"Noble, OK","text":"Noble, OK"},{"id":"us-mn-105","orig":"Nobles, MN","text":"Nobles, MN"},{"id":"us-mo-147","orig":"Nodaway, MO","text":"Nodaway, MO"},{"id":"us-tx-353","orig":"Nolan, TX","text":"Nolan, TX"},{"id":"us-ak-180","orig":"Nome, AK","text":"Nome, AK"},{"id":"us-ma-021","orig":"Norfolk, MA","text":"Norfolk, MA"},{"id":"us-va-710","orig":"Norfolk, VA","text":"Norfolk, VA"},{"id":"us-mn-107","orig":"Norman, MN","text":"Norman, MN"},{"id":"us-ak-185","orig":"North Slope, AK","text":"North Slope, AK"},{"id":"us-nc-131","orig":"Northampton, NC","text":"Northampton, NC"},{"id":"us-pa-095","orig":"Northampton, PA","text":"Northampton, PA"},{"id":"us-va-131","orig":"Northampton, VA","text":"Northampton, VA"},{"id":"us-pa-097","orig":"Northumberland, PA","text":"Northumberland, PA"},{"id":"us-va-133","orig":"Northumberland, VA","text":"Northumberland, VA"},{"id":"us-ak-188","orig":"Northwest Arctic, AK","text":"Northwest Arctic, AK"},{"id":"us-ks-137","orig":"Norton, KS","text":"Norton, KS"},{"id":"us-va-720","orig":"Norton, VA","text":"Norton, VA"},{"id":"us-va-135","orig":"Nottoway, VA","text":"Nottoway, VA"},{"id":"us-ok-105","orig":"Nowata, OK","text":"Nowata, OK"},{"id":"us-ms-103","orig":"Noxubee, MS","text":"Noxubee, MS"},{"id":"us-ne-129","orig":"Nuckolls, NE","text":"Nuckolls, NE"},{"id":"us-tx-355","orig":"Nueces, TX","text":"Nueces, TX"},{"id":"us-nv-023","orig":"Nye, NV","text":"Nye, NV"},{"id":"us-ia-141","orig":"O'Brien, IA","text":"O'Brien, IA"},{"id":"us-mi-125","orig":"Oakland, MI","text":"Oakland, MI"},{"id":"us-tn-131","orig":"Obion, TN","text":"Obion, TN"},{"id":"us-nj-029","orig":"Ocean, NJ","text":"Ocean, NJ"},{"id":"us-mi-127","orig":"Oceana, MI","text":"Oceana, MI"},{"id":"us-tx-357","orig":"Ochiltree, TX","text":"Ochiltree, TX"},{"id":"us-ga-219","orig":"Oconee, GA","text":"Oconee, GA"},{"id":"us-sc-073","orig":"Oconee, SC","text":"Oconee, SC"},{"id":"us-wi-083","orig":"Oconto, WI","text":"Oconto, WI"},{"id":"us-mi-129","orig":"Ogemaw, MI","text":"Ogemaw, MI"},{"id":"us-il-141","orig":"Ogle, IL","text":"Ogle, IL"},{"id":"us-ga-221","orig":"Oglethorpe, GA","text":"Oglethorpe, GA"},{"id":"us-in-115","orig":"Ohio, IN","text":"Ohio, IN"},{"id":"us-ky-183","orig":"Ohio, KY","text":"Ohio, KY"},{"id":"us-wv-069","orig":"Ohio, WV","text":"Ohio, WV"},{"id":"us-fl-091","orig":"Okaloosa, FL","text":"Okaloosa, FL"},{"id":"us-wa-047","orig":"Okanogan, WA","text":"Okanogan, WA"},{"id":"us-fl-093","orig":"Okeechobee, FL","text":"Okeechobee, FL"},{"id":"us-ok-107","orig":"Okfuskee, OK","text":"Okfuskee, OK"},{"id":"us-ok-109","orig":"Oklahoma, OK","text":"Oklahoma, OK"},{"id":"us-ok-111","orig":"Okmulgee, OK","text":"Okmulgee, OK"},{"id":"us-ms-105","orig":"Oktibbeha, MS","text":"Oktibbeha, MS"},{"id":"us-ky-185","orig":"Oldham, KY","text":"Oldham, KY"},{"id":"us-tx-359","orig":"Oldham, TX","text":"Oldham, TX"},{"id":"us-nd-065","orig":"Oliver, ND","text":"Oliver, ND"},{"id":"us-mn-109","orig":"Olmsted, MN","text":"Olmsted, MN"},{"id":"us-id-071","orig":"Oneida, ID","text":"Oneida, ID"},{"id":"us-ny-065","orig":"Oneida, NY","text":"Oneida, NY"},{"id":"us-wi-085","orig":"Oneida, WI","text":"Oneida, WI"},{"id":"us-ny-067","orig":"Onondaga, NY","text":"Onondaga, NY"},{"id":"us-nc-133","orig":"Onslow, NC","text":"Onslow, NC"},{"id":"us-ny-069","orig":"Ontario, NY","text":"Ontario, NY"},{"id":"us-mi-131","orig":"Ontonagon, MI","text":"Ontonagon, MI"},{"id":"us-ca-059","orig":"Orange, CA","text":"Orange, CA"},{"id":"us-fl-095","orig":"Orange, FL","text":"Orange, FL"},{"id":"us-in-117","orig":"Orange, IN","text":"Orange, IN"},{"id":"us-nc-135","orig":"Orange, NC","text":"Orange, NC"},{"id":"us-ny-071","orig":"Orange, NY","text":"Orange, NY"},{"id":"us-tx-361","orig":"Orange, TX","text":"Orange, TX"},{"id":"us-va-137","orig":"Orange, VA","text":"Orange, VA"},{"id":"us-vt-017","orig":"Orange, VT","text":"Orange, VT"},{"id":"us-sc-075","orig":"Orangeburg, SC","text":"Orangeburg, SC"},{"id":"us-mo-149","orig":"Oregon, MO","text":"Oregon, MO"},{"id":"us-la-071","orig":"Orleans, LA","text":"Orleans, LA"},{"id":"us-ny-073","orig":"Orleans, NY","text":"Orleans, NY"},{"id":"us-vt-019","orig":"Orleans, VT","text":"Orleans, VT"},{"id":"us-ks-139","orig":"Osage, KS","text":"Osage, KS"},{"id":"us-mo-151","orig":"Osage, MO","text":"Osage, MO"},{"id":"us-ok-113","orig":"Osage, OK","text":"Osage, OK"},{"id":"us-ks-141","orig":"Osborne, KS","text":"Osborne, KS"},{"id":"us-fl-097","orig":"Osceola, FL","text":"Osceola, FL"},{"id":"us-ia-143","orig":"Osceola, IA","text":"Osceola, IA"},{"id":"us-mi-133","orig":"Osceola, MI","text":"Osceola, MI"},{"id":"us-mi-135","orig":"Oscoda, MI","text":"Oscoda, MI"},{"id":"us-ny-075","orig":"Oswego, NY","text":"Oswego, NY"},{"id":"us-co-089","orig":"Otero, CO","text":"Otero, CO"},{"id":"us-nm-035","orig":"Otero, NM","text":"Otero, NM"},{"id":"us-ne-131","orig":"Otoe, NE","text":"Otoe, NE"},{"id":"us-mi-137","orig":"Otsego, MI","text":"Otsego, MI"},{"id":"us-ny-077","orig":"Otsego, NY","text":"Otsego, NY"},{"id":"us-ks-143","orig":"Ottawa, KS","text":"Ottawa, KS"},{"id":"us-mi-139","orig":"Ottawa, MI","text":"Ottawa, MI"},{"id":"us-oh-123","orig":"Ottawa, OH","text":"Ottawa, OH"},{"id":"us-ok-115","orig":"Ottawa, OK","text":"Ottawa, OK"},{"id":"us-mn-111","orig":"Otter Tail, MN","text":"Otter Tail, MN"},{"id":"us-ar-103","orig":"Ouachita, AR","text":"Ouachita, AR"},{"id":"us-la-073","orig":"Ouachita, LA","text":"Ouachita, LA"},{"id":"us-co-091","orig":"Ouray, CO","text":"Ouray, CO"},{"id":"us-wi-087","orig":"Outagamie, WI","text":"Outagamie, WI"},{"id":"us-tn-133","orig":"Overton, TN","text":"Overton, TN"},{"id":"us-in-119","orig":"Owen, IN","text":"Owen, IN"},{"id":"us-ky-187","orig":"Owen, KY","text":"Owen, KY"},{"id":"us-ky-189","orig":"Owsley, KY","text":"Owsley, KY"},{"id":"us-id-073","orig":"Owyhee, ID","text":"Owyhee, ID"},{"id":"us-me-017","orig":"Oxford, ME","text":"Oxford, ME"},{"id":"us-mo-153","orig":"Ozark, MO","text":"Ozark, MO"},{"id":"us-wi-089","orig":"Ozaukee, WI","text":"Ozaukee, WI"},{"id":"us-wa-049","orig":"Pacific, WA","text":"Pacific, WA"},{"id":"us-ia-145","orig":"Page, IA","text":"Page, IA"},{"id":"us-va-139","orig":"Page, VA","text":"Page, VA"},{"id":"us-fl-099","orig":"Palm Beach, FL","text":"Palm Beach, FL"},{"id":"us-ia-147","orig":"Palo Alto, IA","text":"Palo Alto, IA"},{"id":"us-tx-363","orig":"Palo Pinto, TX","text":"Palo Pinto, TX"},{"id":"us-nc-137","orig":"Pamlico, NC","text":"Pamlico, NC"},{"id":"us-ms-107","orig":"Panola, MS","text":"Panola, MS"},{"id":"us-tx-365","orig":"Panola, TX","text":"Panola, TX"},{"id":"us-co-093","orig":"Park, CO","text":"Park, CO"},{"id":"us-mt-067","orig":"Park, MT","text":"Park, MT"},{"id":"us-wy-029","orig":"Park, WY","text":"Park, WY"},{"id":"us-in-121","orig":"Parke, IN","text":"Parke, IN"},{"id":"us-tx-367","orig":"Parker, TX","text":"Parker, TX"},{"id":"us-tx-369","orig":"Parmer, TX","text":"Parmer, TX"},{"id":"us-fl-101","orig":"Pasco, FL","text":"Pasco, FL"},{"id":"us-nc-139","orig":"Pasquotank, NC","text":"Pasquotank, NC"},{"id":"us-nj-031","orig":"Passaic, NJ","text":"Passaic, NJ"},{"id":"us-va-141","orig":"Patrick, VA","text":"Patrick, VA"},{"id":"us-ga-223","orig":"Paulding, GA","text":"Paulding, GA"},{"id":"us-oh-125","orig":"Paulding, OH","text":"Paulding, OH"},{"id":"us-ks-145","orig":"Pawnee, KS","text":"Pawnee, KS"},{"id":"us-ne-133","orig":"Pawnee, NE","text":"Pawnee, NE"},{"id":"us-ok-117","orig":"Pawnee, OK","text":"Pawnee, OK"},{"id":"us-id-075","orig":"Payette, ID","text":"Payette, ID"},{"id":"us-ok-119","orig":"Payne, OK","text":"Payne, OK"},{"id":"us-ga-225","orig":"Peach, GA","text":"Peach, GA"},{"id":"us-ms-109","orig":"Pearl River, MS","text":"Pearl River, MS"},{"id":"us-tx-371","orig":"Pecos, TX","text":"Pecos, TX"},{"id":"us-nd-067","orig":"Pembina, ND","text":"Pembina, ND"},{"id":"us-mo-155","orig":"Pemiscot, MO","text":"Pemiscot, MO"},{"id":"us-wa-051","orig":"Pend Oreille, WA","text":"Pend Oreille, WA"},{"id":"us-nc-141","orig":"Pender, NC","text":"Pender, NC"},{"id":"us-ky-191","orig":"Pendleton, KY","text":"Pendleton, KY"},{"id":"us-wv-071","orig":"Pendleton, WV","text":"Pendleton, WV"},{"id":"us-mn-113","orig":"Pennington, MN","text":"Pennington, MN"},{"id":"us-sd-103","orig":"Pennington, SD","text":"Pennington, SD"},{"id":"us-me-019","orig":"Penobscot, ME","text":"Penobscot, ME"},{"id":"us-il-143","orig":"Peoria, IL","text":"Peoria, IL"},{"id":"us-wi-091","orig":"Pepin, WI","text":"Pepin, WI"},{"id":"us-ne-135","orig":"Perkins, NE","text":"Perkins, NE"},{"id":"us-sd-105","orig":"Perkins, SD","text":"Perkins, SD"},{"id":"us-nc-143","orig":"Perquimans, NC","text":"Perquimans, NC"},{"id":"us-al-105","orig":"Perry, AL","text":"Perry, AL"},{"id":"us-ar-105","orig":"Perry, AR","text":"Perry, AR"},{"id":"us-il-145","orig":"Perry, IL","text":"Perry, IL"},{"id":"us-in-123","orig":"Perry, IN","text":"Perry, IN"},{"id":"us-ky-193","orig":"Perry, KY","text":"Perry, KY"},{"id":"us-mo-157","orig":"Perry, MO","text":"Perry, MO"},{"id":"us-ms-111","orig":"Perry, MS","text":"Perry, MS"},{"id":"us-oh-127","orig":"Perry, OH","text":"Perry, OH"},{"id":"us-pa-099","orig":"Perry, PA","text":"Perry, PA"},{"id":"us-tn-135","orig":"Perry, TN","text":"Perry, TN"},{"id":"us-nv-027","orig":"Pershing, NV","text":"Pershing, NV"},{"id":"us-nc-145","orig":"Person, NC","text":"Person, NC"},{"id":"us-ak-195","orig":"Petersburg, AK","text":"Petersburg, AK"},{"id":"us-va-730","orig":"Petersburg, VA","text":"Petersburg, VA"},{"id":"us-mt-069","orig":"Petroleum, MT","text":"Petroleum, MT"},{"id":"us-mo-159","orig":"Pettis, MO","text":"Pettis, MO"},{"id":"us-mo-161","orig":"Phelps, MO","text":"Phelps, MO"},{"id":"us-ne-137","orig":"Phelps, NE","text":"Phelps, NE"},{"id":"us-pa-101","orig":"Philadelphia, PA","text":"Philadelphia, PA"},{"id":"us-ar-107","orig":"Phillips, AR","text":"Phillips, AR"},{"id":"us-co-095","orig":"Phillips, CO","text":"Phillips, CO"},{"id":"us-ks-147","orig":"Phillips, KS","text":"Phillips, KS"},{"id":"us-mt-071","orig":"Phillips, MT","text":"Phillips, MT"},{"id":"us-il-147","orig":"Piatt, IL","text":"Piatt, IL"},{"id":"us-oh-129","orig":"Pickaway, OH","text":"Pickaway, OH"},{"id":"us-al-107","orig":"Pickens, AL","text":"Pickens, AL"},{"id":"us-ga-227","orig":"Pickens, GA","text":"Pickens, GA"},{"id":"us-sc-077","orig":"Pickens, SC","text":"Pickens, SC"},{"id":"us-tn-137","orig":"Pickett, TN","text":"Pickett, TN"},{"id":"us-ga-229","orig":"Pierce, GA","text":"Pierce, GA"},{"id":"us-nd-069","orig":"Pierce, ND","text":"Pierce, ND"},{"id":"us-ne-139","orig":"Pierce, NE","text":"Pierce, NE"},{"id":"us-wa-053","orig":"Pierce, WA","text":"Pierce, WA"},{"id":"us-wi-093","orig":"Pierce, WI","text":"Pierce, WI"},{"id":"us-al-109","orig":"Pike, AL","text":"Pike, AL"},{"id":"us-ar-109","orig":"Pike, AR","text":"Pike, AR"},{"id":"us-ga-231","orig":"Pike, GA","text":"Pike, GA"},{"id":"us-il-149","orig":"Pike, IL","text":"Pike, IL"},{"id":"us-in-125","orig":"Pike, IN","text":"Pike, IN"},{"id":"us-ky-195","orig":"Pike, KY","text":"Pike, KY"},{"id":"us-mo-163","orig":"Pike, MO","text":"Pike, MO"},{"id":"us-ms-113","orig":"Pike, MS","text":"Pike, MS"},{"id":"us-oh-131","orig":"Pike, OH","text":"Pike, OH"},{"id":"us-pa-103","orig":"Pike, PA","text":"Pike, PA"},{"id":"us-az-019","orig":"Pima, AZ","text":"Pima, AZ"},{"id":"us-az-021","orig":"Pinal, AZ","text":"Pinal, AZ"},{"id":"us-mn-115","orig":"Pine, MN","text":"Pine, MN"},{"id":"us-fl-103","orig":"Pinellas, FL","text":"Pinellas, FL"},{"id":"us-mn-117","orig":"Pipestone, MN","text":"Pipestone, MN"},{"id":"us-me-021","orig":"Piscataquis, ME","text":"Piscataquis, ME"},{"id":"us-co-097","orig":"Pitkin, CO","text":"Pitkin, CO"},{"id":"us-nc-147","orig":"Pitt, NC","text":"Pitt, NC"},{"id":"us-ok-121","orig":"Pittsburg, OK","text":"Pittsburg, OK"},{"id":"us-va-143","orig":"Pittsylvania, VA","text":"Pittsylvania, VA"},{"id":"us-ut-031","orig":"Piute, UT","text":"Piute, UT"},{"id":"us-ca-061","orig":"Placer, CA","text":"Placer, CA"},{"id":"us-la-075","orig":"Plaquemines, LA","text":"Plaquemines, LA"},{"id":"us-mo-165","orig":"Platte, MO","text":"Platte, MO"},{"id":"us-ne-141","orig":"Platte, NE","text":"Platte, NE"},{"id":"us-wy-031","orig":"Platte, WY","text":"Platte, WY"},{"id":"us-wv-073","orig":"Pleasants, WV","text":"Pleasants, WV"},{"id":"us-ca-063","orig":"Plumas, CA","text":"Plumas, CA"},{"id":"us-ia-149","orig":"Plymouth, IA","text":"Plymouth, IA"},{"id":"us-ma-023","orig":"Plymouth, MA","text":"Plymouth, MA"},{"id":"us-ia-151","orig":"Pocahontas, IA","text":"Pocahontas, IA"},{"id":"us-wv-075","orig":"Pocahontas, WV","text":"Pocahontas, WV"},{"id":"us-ar-111","orig":"Poinsett, AR","text":"Poinsett, AR"},{"id":"us-la-077","orig":"Pointe Coupee, LA","text":"Pointe Coupee, LA"},{"id":"us-ar-113","orig":"Polk, AR","text":"Polk, AR"},{"id":"us-fl-105","orig":"Polk, FL","text":"Polk, FL"},{"id":"us-ga-233","orig":"Polk, GA","text":"Polk, GA"},{"id":"us-ia-153","orig":"Polk, IA","text":"Polk, IA"},{"id":"us-mn-119","orig":"Polk, MN","text":"Polk, MN"},{"id":"us-mo-167","orig":"Polk, MO","text":"Polk, MO"},{"id":"us-nc-149","orig":"Polk, NC","text":"Polk, NC"},{"id":"us-ne-143","orig":"Polk, NE","text":"Polk, NE"},{"id":"us-or-053","orig":"Polk, OR","text":"Polk, OR"},{"id":"us-tn-139","orig":"Polk, TN","text":"Polk, TN"},{"id":"us-tx-373","orig":"Polk, TX","text":"Polk, TX"},{"id":"us-wi-095","orig":"Polk, WI","text":"Polk, WI"},{"id":"us-mt-073","orig":"Pondera, MT","text":"Pondera, MT"},{"id":"us-ms-115","orig":"Pontotoc, MS","text":"Pontotoc, MS"},{"id":"us-ok-123","orig":"Pontotoc, OK","text":"Pontotoc, OK"},{"id":"us-ar-115","orig":"Pope, AR","text":"Pope, AR"},{"id":"us-il-151","orig":"Pope, IL","text":"Pope, IL"},{"id":"us-mn-121","orig":"Pope, MN","text":"Pope, MN"},{"id":"us-va-735","orig":"Poquoson, VA","text":"Poquoson, VA"},{"id":"us-oh-133","orig":"Portage, OH","text":"Portage, OH"},{"id":"us-wi-097","orig":"Portage, WI","text":"Portage, WI"},{"id":"us-in-127","orig":"Porter, IN","text":"Porter, IN"},{"id":"us-va-740","orig":"Portsmouth, VA","text":"Portsmouth, VA"},{"id":"us-in-129","orig":"Posey, IN","text":"Posey, IN"},{"id":"us-ks-149","orig":"Pottawatomie, KS","text":"Pottawatomie, KS"},{"id":"us-ok-125","orig":"Pottawatomie, OK","text":"Pottawatomie, OK"},{"id":"us-ia-155","orig":"Pottawattamie, IA","text":"Pottawattamie, IA"},{"id":"us-pa-105","orig":"Potter, PA","text":"Potter, PA"},{"id":"us-sd-107","orig":"Potter, SD","text":"Potter, SD"},{"id":"us-tx-375","orig":"Potter, TX","text":"Potter, TX"},{"id":"us-mt-075","orig":"Powder River, MT","text":"Powder River, MT"},{"id":"us-ky-197","orig":"Powell, KY","text":"Powell, KY"},{"id":"us-mt-077","orig":"Powell, MT","text":"Powell, MT"},{"id":"us-id-077","orig":"Power, ID","text":"Power, ID"},{"id":"us-ia-157","orig":"Poweshiek, IA","text":"Poweshiek, IA"},{"id":"us-va-145","orig":"Powhatan, VA","text":"Powhatan, VA"},{"id":"us-ar-117","orig":"Prairie, AR","text":"Prairie, AR"},{"id":"us-mt-079","orig":"Prairie, MT","text":"Prairie, MT"},{"id":"us-ks-151","orig":"Pratt, KS","text":"Pratt, KS"},{"id":"us-oh-135","orig":"Preble, OH","text":"Preble, OH"},{"id":"us-ms-117","orig":"Prentiss, MS","text":"Prentiss, MS"},{"id":"us-tx-377","orig":"Presidio, TX","text":"Presidio, TX"},{"id":"us-mi-141","orig":"Presque Isle, MI","text":"Presque Isle, MI"},{"id":"us-wv-077","orig":"Preston, WV","text":"Preston, WV"},{"id":"us-wi-099","orig":"Price, WI","text":"Price, WI"},{"id":"us-va-147","orig":"Prince Edward, VA","text":"Prince Edward, VA"},{"id":"us-md-033","orig":"Prince George's, MD","text":"Prince George's, MD"},{"id":"us-va-149","orig":"Prince George, VA","text":"Prince George, VA"},{"id":"us-va-153","orig":"Prince William, VA","text":"Prince William, VA"},{"id":"us-ak-198","orig":"Prince of Wales-Hyder, AK","text":"Prince of Wales-Hyder, AK"},{"id":"us-ri-007","orig":"Providence, RI","text":"Providence, RI"},{"id":"us-co-099","orig":"Prowers, CO","text":"Prowers, CO"},{"id":"us-co-101","orig":"Pueblo, CO","text":"Pueblo, CO"},{"id":"us-ar-119","orig":"Pulaski, AR","text":"Pulaski, AR"},{"id":"us-ga-235","orig":"Pulaski, GA","text":"Pulaski, GA"},{"id":"us-il-153","orig":"Pulaski, IL","text":"Pulaski, IL"},{"id":"us-in-131","orig":"Pulaski, IN","text":"Pulaski, IN"},{"id":"us-ky-199","orig":"Pulaski, KY","text":"Pulaski, KY"},{"id":"us-mo-169","orig":"Pulaski, MO","text":"Pulaski, MO"},{"id":"us-va-155","orig":"Pulaski, VA","text":"Pulaski, VA"},{"id":"us-ok-127","orig":"Pushmataha, OK","text":"Pushmataha, OK"},{"id":"us-fl-107","orig":"Putnam, FL","text":"Putnam, FL"},{"id":"us-ga-237","orig":"Putnam, GA","text":"Putnam, GA"},{"id":"us-il-155","orig":"Putnam, IL","text":"Putnam, IL"},{"id":"us-in-133","orig":"Putnam, IN","text":"Putnam, IN"},{"id":"us-mo-171","orig":"Putnam, MO","text":"Putnam, MO"},{"id":"us-ny-079","orig":"Putnam, NY","text":"Putnam, NY"},{"id":"us-oh-137","orig":"Putnam, OH","text":"Putnam, OH"},{"id":"us-tn-141","orig":"Putnam, TN","text":"Putnam, TN"},{"id":"us-wv-079","orig":"Putnam, WV","text":"Putnam, WV"},{"id":"us-nm-037","orig":"Quay, NM","text":"Quay, NM"},{"id":"us-md-035","orig":"Queen Anne's, MD","text":"Queen Anne's, MD"},{"id":"us-ny-081","orig":"Queens, NY","text":"Queens, NY"},{"id":"us-ga-239","orig":"Quitman, GA","text":"Quitman, GA"},{"id":"us-ms-119","orig":"Quitman, MS","text":"Quitman, MS"},{"id":"us-ga-241","orig":"Rabun, GA","text":"Rabun, GA"},{"id":"us-wi-101","orig":"Racine, WI","text":"Racine, WI"},{"id":"us-va-750","orig":"Radford, VA","text":"Radford, VA"},{"id":"us-tx-379","orig":"Rains, TX","text":"Rains, TX"},{"id":"us-wv-081","orig":"Raleigh, WV","text":"Raleigh, WV"},{"id":"us-mo-173","orig":"Ralls, MO","text":"Ralls, MO"},{"id":"us-mn-123","orig":"Ramsey, MN","text":"Ramsey, MN"},{"id":"us-nd-071","orig":"Ramsey, ND","text":"Ramsey, ND"},{"id":"us-tx-381","orig":"Randall, TX","text":"Randall, TX"},{"id":"us-al-111","orig":"Randolph, AL","text":"Randolph, AL"},{"id":"us-ar-121","orig":"Randolph, AR","text":"Randolph, AR"},{"id":"us-ga-243","orig":"Randolph, GA","text":"Randolph, GA"},{"id":"us-il-157","orig":"Randolph, IL","text":"Randolph, IL"},{"id":"us-in-135","orig":"Randolph, IN","text":"Randolph, IN"},{"id":"us-mo-175","orig":"Randolph, MO","text":"Randolph, MO"},{"id":"us-nc-151","orig":"Randolph, NC","text":"Randolph, NC"},{"id":"us-wv-083","orig":"Randolph, WV","text":"Randolph, WV"},{"id":"us-ms-121","orig":"Rankin, MS","text":"Rankin, MS"},{"id":"us-nd-073","orig":"Ransom, ND","text":"Ransom, ND"},{"id":"us-la-079","orig":"Rapides, LA","text":"Rapides, LA"},{"id":"us-va-157","orig":"Rappahannock, VA","text":"Rappahannock, VA"},{"id":"us-mt-081","orig":"Ravalli, MT","text":"Ravalli, MT"},{"id":"us-ks-153","orig":"Rawlins, KS","text":"Rawlins, KS"},{"id":"us-mo-177","orig":"Ray, MO","text":"Ray, MO"},{"id":"us-tx-383","orig":"Reagan, TX","text":"Reagan, TX"},{"id":"us-tx-385","orig":"Real, TX","text":"Real, TX"},{"id":"us-mn-125","orig":"Red Lake, MN","text":"Red Lake, MN"},{"id":"us-la-081","orig":"Red River, LA","text":"Red River, LA"},{"id":"us-tx-387","orig":"Red River, TX","text":"Red River, TX"},{"id":"us-ne-145","orig":"Red Willow, NE","text":"Red Willow, NE"},{"id":"us-mn-127","orig":"Redwood, MN","text":"Redwood, MN"},{"id":"us-tx-389","orig":"Reeves, TX","text":"Reeves, TX"},{"id":"us-tx-391","orig":"Refugio, TX","text":"Refugio, TX"},{"id":"us-ks-155","orig":"Reno, KS","text":"Reno, KS"},{"id":"us-ny-083","orig":"Rensselaer, NY","text":"Rensselaer, NY"},{"id":"us-mn-129","orig":"Renville, MN","text":"Renville, MN"},{"id":"us-nd-075","orig":"Renville, ND","text":"Renville, ND"},{"id":"us-ks-157","orig":"Republic, KS","text":"Republic, KS"},{"id":"us-mo-179","orig":"Reynolds, MO","text":"Reynolds, MO"},{"id":"us-tn-143","orig":"Rhea, TN","text":"Rhea, TN"},{"id":"us-ks-159","orig":"Rice, KS","text":"Rice, KS"},{"id":"us-mn-131","orig":"Rice, MN","text":"Rice, MN"},{"id":"us-ut-033","orig":"Rich, UT","text":"Rich, UT"},{"id":"us-ne-147","orig":"Richardson, NE","text":"Richardson, NE"},{"id":"us-il-159","orig":"Richland, IL","text":"Richland, IL"},{"id":"us-la-083","orig":"Richland, LA","text":"Richland, LA"},{"id":"us-mt-083","orig":"Richland, MT","text":"Richland, MT"},{"id":"us-nd-077","orig":"Richland, ND","text":"Richland, ND"},{"id":"us-oh-139","orig":"Richland, OH","text":"Richland, OH"},{"id":"us-sc-079","orig":"Richland, SC","text":"Richland, SC"},{"id":"us-wi-103","orig":"Richland, WI","text":"Richland, WI"},{"id":"us-ga-245","orig":"Richmond, GA","text":"Richmond, GA"},{"id":"us-nc-153","orig":"Richmond, NC","text":"Richmond, NC"},{"id":"us-ny-085","orig":"Richmond, NY","text":"Richmond, NY"},{"id":"us-va-760","orig":"Richmond, VA","text":"Richmond, VA"},{"id":"us-va-159","orig":"Richmond, VA","text":"Richmond, VA"},{"id":"us-ks-161","orig":"Riley, KS","text":"Riley, KS"},{"id":"us-ia-159","orig":"Ringgold, IA","text":"Ringgold, IA"},{"id":"us-nm-039","orig":"Rio Arriba, NM","text":"Rio Arriba, NM"},{"id":"us-co-103","orig":"Rio Blanco, CO","text":"Rio Blanco, CO"},{"id":"us-co-105","orig":"Rio Grande, CO","text":"Rio Grande, CO"},{"id":"us-in-137","orig":"Ripley, IN","text":"Ripley, IN"},{"id":"us-mo-181","orig":"Ripley, MO","text":"Ripley, MO"},{"id":"us-wv-085","orig":"Ritchie, WV","text":"Ritchie, WV"},{"id":"us-ca-065","orig":"Riverside, CA","text":"Riverside, CA"},{"id":"us-tn-145","orig":"Roane, TN","text":"Roane, TN"},{"id":"us-wv-087","orig":"Roane, WV","text":"Roane, WV"},{"id":"us-va-770","orig":"Roanoke, VA","text":"Roanoke, VA"},{"id":"us-va-161","orig":"Roanoke, VA","text":"Roanoke, VA"},{"id":"us-sd-109","orig":"Roberts, SD","text":"Roberts, SD"},{"id":"us-tx-393","orig":"Roberts, TX","text":"Roberts, TX"},{"id":"us-ky-201","orig":"Robertson, KY","text":"Robertson, KY"},{"id":"us-tn-147","orig":"Robertson, TN","text":"Robertson, TN"},{"id":"us-tx-395","orig":"Robertson, TX","text":"Robertson, TX"},{"id":"us-nc-155","orig":"Robeson, NC","text":"Robeson, NC"},{"id":"us-il-161","orig":"Rock Island, IL","text":"Rock Island, IL"},{"id":"us-mn-133","orig":"Rock, MN","text":"Rock, MN"},{"id":"us-ne-149","orig":"Rock, NE","text":"Rock, NE"},{"id":"us-wi-105","orig":"Rock, WI","text":"Rock, WI"},{"id":"us-va-163","orig":"Rockbridge, VA","text":"Rockbridge, VA"},{"id":"us-ky-203","orig":"Rockcastle, KY","text":"Rockcastle, KY"},{"id":"us-ga-247","orig":"Rockdale, GA","text":"Rockdale, GA"},{"id":"us-nc-157","orig":"Rockingham, NC","text":"Rockingham, NC"},{"id":"us-nh-015","orig":"Rockingham, NH","text":"Rockingham, NH"},{"id":"us-va-165","orig":"Rockingham, VA","text":"Rockingham, VA"},{"id":"us-ny-087","orig":"Rockland, NY","text":"Rockland, NY"},{"id":"us-tx-397","orig":"Rockwall, TX","text":"Rockwall, TX"},{"id":"us-ok-129","orig":"Roger Mills, OK","text":"Roger Mills, OK"},{"id":"us-ok-131","orig":"Rogers, OK","text":"Rogers, OK"},{"id":"us-nd-079","orig":"Rolette, ND","text":"Rolette, ND"},{"id":"us-ks-163","orig":"Rooks, KS","text":"Rooks, KS"},{"id":"us-mt-085","orig":"Roosevelt, MT","text":"Roosevelt, MT"},{"id":"us-nm-041","orig":"Roosevelt, NM","text":"Roosevelt, NM"},{"id":"us-mi-143","orig":"Roscommon, MI","text":"Roscommon, MI"},{"id":"us-mn-135","orig":"Roseau, MN","text":"Roseau, MN"},{"id":"us-mt-087","orig":"Rosebud, MT","text":"Rosebud, MT"},{"id":"us-oh-141","orig":"Ross, OH","text":"Ross, OH"},{"id":"us-co-107","orig":"Routt, CO","text":"Routt, CO"},{"id":"us-ky-205","orig":"Rowan, KY","text":"Rowan, KY"},{"id":"us-nc-159","orig":"Rowan, NC","text":"Rowan, NC"},{"id":"us-tx-399","orig":"Runnels, TX","text":"Runnels, TX"},{"id":"us-in-139","orig":"Rush, IN","text":"Rush, IN"},{"id":"us-ks-165","orig":"Rush, KS","text":"Rush, KS"},{"id":"us-tx-401","orig":"Rusk, TX","text":"Rusk, TX"},{"id":"us-wi-107","orig":"Rusk, WI","text":"Rusk, WI"},{"id":"us-al-113","orig":"Russell, AL","text":"Russell, AL"},{"id":"us-ks-167","orig":"Russell, KS","text":"Russell, KS"},{"id":"us-ky-207","orig":"Russell, KY","text":"Russell, KY"},{"id":"us-va-167","orig":"Russell, VA","text":"Russell, VA"},{"id":"us-nc-161","orig":"Rutherford, NC","text":"Rutherford, NC"},{"id":"us-tn-149","orig":"Rutherford, TN","text":"Rutherford, TN"},{"id":"us-vt-021","orig":"Rutland, VT","text":"Rutland, VT"},{"id":"us-la-085","orig":"Sabine, LA","text":"Sabine, LA"},{"id":"us-tx-403","orig":"Sabine, TX","text":"Sabine, TX"},{"id":"us-ia-161","orig":"Sac, IA","text":"Sac, IA"},{"id":"us-ca-067","orig":"Sacramento, CA","text":"Sacramento, CA"},{"id":"us-me-023","orig":"Sagadahoc, ME","text":"Sagadahoc, ME"},{"id":"us-mi-145","orig":"Saginaw, MI","text":"Saginaw, MI"},{"id":"us-co-109","orig":"Saguache, CO","text":"Saguache, CO"},{"id":"us-nj-033","orig":"Salem, NJ","text":"Salem, NJ"},{"id":"us-va-775","orig":"Salem, VA","text":"Salem, VA"},{"id":"us-ar-125","orig":"Saline, AR","text":"Saline, AR"},{"id":"us-il-165","orig":"Saline, IL","text":"Saline, IL"},{"id":"us-ks-169","orig":"Saline, KS","text":"Saline, KS"},{"id":"us-mo-195","orig":"Saline, MO","text":"Saline, MO"},{"id":"us-ne-151","orig":"Saline, NE","text":"Saline, NE"},{"id":"us-ut-035","orig":"Salt Lake, UT","text":"Salt Lake, UT"},{"id":"us-sc-081","orig":"Saluda, SC","text":"Saluda, SC"},{"id":"us-nc-163","orig":"Sampson, NC","text":"Sampson, NC"},{"id":"us-tx-405","orig":"San Augustine, TX","text":"San Augustine, TX"},{"id":"us-ca-069","orig":"San Benito, CA","text":"San Benito, CA"},{"id":"us-ca-071","orig":"San Bernardino, CA","text":"San Bernardino, CA"},{"id":"us-ca-073","orig":"San Diego, CA","text":"San Diego, CA"},{"id":"us-ca-075","orig":"San Francisco, CA","text":"San Francisco, CA"},{"id":"us-tx-407","orig":"San Jacinto, TX","text":"San Jacinto, TX"},{"id":"us-ca-077","orig":"San Joaquin, CA","text":"San Joaquin, CA"},{"id":"us-co-111","orig":"San Juan, CO","text":"San Juan, CO"},{"id":"us-nm-045","orig":"San Juan, NM","text":"San Juan, NM"},{"id":"us-ut-037","orig":"San Juan, UT","text":"San Juan, UT"},{"id":"us-wa-055","orig":"San Juan, WA","text":"San Juan, WA"},{"id":"us-ca-079","orig":"San Luis Obispo, CA","text":"San Luis Obispo, CA"},{"id":"us-ca-081","orig":"San Mateo, CA","text":"San Mateo, CA"},{"id":"us-co-113","orig":"San Miguel, CO","text":"San Miguel, CO"},{"id":"us-nm-047","orig":"San Miguel, NM","text":"San Miguel, NM"},{"id":"us-tx-409","orig":"San Patricio, TX","text":"San Patricio, TX"},{"id":"us-tx-411","orig":"San Saba, TX","text":"San Saba, TX"},{"id":"us-sd-111","orig":"Sanborn, SD","text":"Sanborn, SD"},{"id":"us-mt-089","orig":"Sanders, MT","text":"Sanders, MT"},{"id":"us-nm-043","orig":"Sandoval, NM","text":"Sandoval, NM"},{"id":"us-oh-143","orig":"Sandusky, OH","text":"Sandusky, OH"},{"id":"us-il-167","orig":"Sangamon, IL","text":"Sangamon, IL"},{"id":"us-mi-151","orig":"Sanilac, MI","text":"Sanilac, MI"},{"id":"us-ut-039","orig":"Sanpete, UT","text":"Sanpete, UT"},{"id":"us-ca-083","orig":"Santa Barbara, CA","text":"Santa Barbara, CA"},{"id":"us-ca-085","orig":"Santa Clara, CA","text":"Santa Clara, CA"},{"id":"us-az-023","orig":"Santa Cruz, AZ","text":"Santa Cruz, AZ"},{"id":"us-ca-087","orig":"Santa Cruz, CA","text":"Santa Cruz, CA"},{"id":"us-nm-049","orig":"Santa Fe, NM","text":"Santa Fe, NM"},{"id":"us-fl-113","orig":"Santa Rosa, FL","text":"Santa Rosa, FL"},{"id":"us-fl-115","orig":"Sarasota, FL","text":"Sarasota, FL"},{"id":"us-ny-091","orig":"Saratoga, NY","text":"Saratoga, NY"},{"id":"us-nd-081","orig":"Sargent, ND","text":"Sargent, ND"},{"id":"us-ne-153","orig":"Sarpy, NE","text":"Sarpy, NE"},{"id":"us-wi-111","orig":"Sauk, WI","text":"Sauk, WI"},{"id":"us-ne-155","orig":"Saunders, NE","text":"Saunders, NE"},{"id":"us-wi-113","orig":"Sawyer, WI","text":"Sawyer, WI"},{"id":"us-ny-093","orig":"Schenectady, NY","text":"Schenectady, NY"},{"id":"us-tx-413","orig":"Schleicher, TX","text":"Schleicher, TX"},{"id":"us-ga-249","orig":"Schley, GA","text":"Schley, GA"},{"id":"us-ny-095","orig":"Schoharie, NY","text":"Schoharie, NY"},{"id":"us-mi-153","orig":"Schoolcraft, MI","text":"Schoolcraft, MI"},{"id":"us-il-169","orig":"Schuyler, IL","text":"Schuyler, IL"},{"id":"us-mo-197","orig":"Schuyler, MO","text":"Schuyler, MO"},{"id":"us-ny-097","orig":"Schuyler, NY","text":"Schuyler, NY"},{"id":"us-pa-107","orig":"Schuylkill, PA","text":"Schuylkill, PA"},{"id":"us-oh-145","orig":"Scioto, OH","text":"Scioto, OH"},{"id":"us-mo-199","orig":"Scotland, MO","text":"Scotland, MO"},{"id":"us-nc-165","orig":"Scotland, NC","text":"Scotland, NC"},{"id":"us-ar-127","orig":"Scott, AR","text":"Scott, AR"},{"id":"us-ia-163","orig":"Scott, IA","text":"Scott, IA"},{"id":"us-il-171","orig":"Scott, IL","text":"Scott, IL"},{"id":"us-in-143","orig":"Scott, IN","text":"Scott, IN"},{"id":"us-ks-171","orig":"Scott, KS","text":"Scott, KS"},{"id":"us-ky-209","orig":"Scott, KY","text":"Scott, KY"},{"id":"us-mn-139","orig":"Scott, MN","text":"Scott, MN"},{"id":"us-mo-201","orig":"Scott, MO","text":"Scott, MO"},{"id":"us-ms-123","orig":"Scott, MS","text":"Scott, MS"},{"id":"us-tn-151","orig":"Scott, TN","text":"Scott, TN"},{"id":"us-va-169","orig":"Scott, VA","text":"Scott, VA"},{"id":"us-ne-157","orig":"Scotts Bluff, NE","text":"Scotts Bluff, NE"},{"id":"us-ga-251","orig":"Screven, GA","text":"Screven, GA"},{"id":"us-tx-415","orig":"Scurry, TX","text":"Scurry, TX"},{"id":"us-ar-129","orig":"Searcy, AR","text":"Searcy, AR"},{"id":"us-ar-131","orig":"Sebastian, AR","text":"Sebastian, AR"},{"id":"us-co-115","orig":"Sedgwick, CO","text":"Sedgwick, CO"},{"id":"us-ks-173","orig":"Sedgwick, KS","text":"Sedgwick, KS"},{"id":"us-fl-117","orig":"Seminole, FL","text":"Seminole, FL"},{"id":"us-ga-253","orig":"Seminole, GA","text":"Seminole, GA"},{"id":"us-ok-133","orig":"Seminole, OK","text":"Seminole, OK"},{"id":"us-ny-099","orig":"Seneca, NY","text":"Seneca, NY"},{"id":"us-oh-147","orig":"Seneca, OH","text":"Seneca, OH"},{"id":"us-tn-153","orig":"Sequatchie, TN","text":"Sequatchie, TN"},{"id":"us-ok-135","orig":"Sequoyah, OK","text":"Sequoyah, OK"},{"id":"us-ar-133","orig":"Sevier, AR","text":"Sevier, AR"},{"id":"us-tn-155","orig":"Sevier, TN","text":"Sevier, TN"},{"id":"us-ut-041","orig":"Sevier, UT","text":"Sevier, UT"},{"id":"us-ks-175","orig":"Seward, KS","text":"Seward, KS"},{"id":"us-ne-159","orig":"Seward, NE","text":"Seward, NE"},{"id":"us-tx-417","orig":"Shackelford, TX","text":"Shackelford, TX"},{"id":"us-mo-203","orig":"Shannon, MO","text":"Shannon, MO"},{"id":"us-sd-113","orig":"Shannon, SD","text":"Shannon, SD"},{"id":"us-ms-125","orig":"Sharkey, MS","text":"Sharkey, MS"},{"id":"us-ar-135","orig":"Sharp, AR","text":"Sharp, AR"},{"id":"us-ca-089","orig":"Shasta, CA","text":"Shasta, CA"},{"id":"us-wi-115","orig":"Shawano, WI","text":"Shawano, WI"},{"id":"us-ks-177","orig":"Shawnee, KS","text":"Shawnee, KS"},{"id":"us-wi-117","orig":"Sheboygan, WI","text":"Sheboygan, WI"},{"id":"us-al-117","orig":"Shelby, AL","text":"Shelby, AL"},{"id":"us-ia-165","orig":"Shelby, IA","text":"Shelby, IA"},{"id":"us-il-173","orig":"Shelby, IL","text":"Shelby, IL"},{"id":"us-in-145","orig":"Shelby, IN","text":"Shelby, IN"},{"id":"us-ky-211","orig":"Shelby, KY","text":"Shelby, KY"},{"id":"us-mo-205","orig":"Shelby, MO","text":"Shelby, MO"},{"id":"us-oh-149","orig":"Shelby, OH","text":"Shelby, OH"},{"id":"us-tn-157","orig":"Shelby, TN","text":"Shelby, TN"},{"id":"us-tx-419","orig":"Shelby, TX","text":"Shelby, TX"},{"id":"us-va-171","orig":"Shenandoah, VA","text":"Shenandoah, VA"},{"id":"us-mn-141","orig":"Sherburne, MN","text":"Sherburne, MN"},{"id":"us-ks-179","orig":"Sheridan, KS","text":"Sheridan, KS"},{"id":"us-mt-091","orig":"Sheridan, MT","text":"Sheridan, MT"},{"id":"us-nd-083","orig":"Sheridan, ND","text":"Sheridan, ND"},{"id":"us-ne-161","orig":"Sheridan, NE","text":"Sheridan, NE"},{"id":"us-wy-033","orig":"Sheridan, WY","text":"Sheridan, WY"},{"id":"us-ks-181","orig":"Sherman, KS","text":"Sherman, KS"},{"id":"us-ne-163","orig":"Sherman, NE","text":"Sherman, NE"},{"id":"us-or-055","orig":"Sherman, OR","text":"Sherman, OR"},{"id":"us-tx-421","orig":"Sherman, TX","text":"Sherman, TX"},{"id":"us-mi-155","orig":"Shiawassee, MI","text":"Shiawassee, MI"},{"id":"us-id-079","orig":"Shoshone, ID","text":"Shoshone, ID"},{"id":"us-mn-143","orig":"Sibley, MN","text":"Sibley, MN"},{"id":"us-ca-091","orig":"Sierra, CA","text":"Sierra, CA"},{"id":"us-nm-051","orig":"Sierra, NM","text":"Sierra, NM"},{"id":"us-mt-093","orig":"Silver Bow, MT","text":"Silver Bow, MT"},{"id":"us-ky-213","orig":"Simpson, KY","text":"Simpson, KY"},{"id":"us-ms-127","orig":"Simpson, MS","text":"Simpson, MS"},{"id":"us-ia-167","orig":"Sioux, IA","text":"Sioux, IA"},{"id":"us-nd-085","orig":"Sioux, ND","text":"Sioux, ND"},{"id":"us-ne-165","orig":"Sioux, NE","text":"Sioux, NE"},{"id":"us-ca-093","orig":"Siskiyou, CA","text":"Siskiyou, CA"},{"id":"us-ak-220","orig":"Sitka, AK","text":"Sitka, AK"},{"id":"us-wa-057","orig":"Skagit, WA","text":"Skagit, WA"},{"id":"us-ak-230","orig":"Skagway, AK","text":"Skagway, AK"},{"id":"us-wa-059","orig":"Skamania, WA","text":"Skamania, WA"},{"id":"us-nd-087","orig":"Slope, ND","text":"Slope, ND"},{"id":"us-ks-183","orig":"Smith, KS","text":"Smith, KS"},{"id":"us-ms-129","orig":"Smith, MS","text":"Smith, MS"},{"id":"us-tn-159","orig":"Smith, TN","text":"Smith, TN"},{"id":"us-tx-423","orig":"Smith, TX","text":"Smith, TX"},{"id":"us-va-173","orig":"Smyth, VA","text":"Smyth, VA"},{"id":"us-wa-061","orig":"Snohomish, WA","text":"Snohomish, WA"},{"id":"us-pa-109","orig":"Snyder, PA","text":"Snyder, PA"},{"id":"us-nm-053","orig":"Socorro, NM","text":"Socorro, NM"},{"id":"us-ca-095","orig":"Solano, CA","text":"Solano, CA"},{"id":"us-md-039","orig":"Somerset, MD","text":"Somerset, MD"},{"id":"us-me-025","orig":"Somerset, ME","text":"Somerset, ME"},{"id":"us-nj-035","orig":"Somerset, NJ","text":"Somerset, NJ"},{"id":"us-pa-111","orig":"Somerset, PA","text":"Somerset, PA"},{"id":"us-tx-425","orig":"Somervell, TX","text":"Somervell, TX"},{"id":"us-ca-097","orig":"Sonoma, CA","text":"Sonoma, CA"},{"id":"us-va-175","orig":"Southampton, VA","text":"Southampton, VA"},{"id":"us-ak-240","orig":"Southeast Fairbanks, AK","text":"Southeast Fairbanks, AK"},{"id":"us-ga-255","orig":"Spalding, GA","text":"Spalding, GA"},{"id":"us-sc-083","orig":"Spartanburg, SC","text":"Spartanburg, SC"},{"id":"us-in-147","orig":"Spencer, IN","text":"Spencer, IN"},{"id":"us-ky-215","orig":"Spencer, KY","text":"Spencer, KY"},{"id":"us-sd-115","orig":"Spink, SD","text":"Spink, SD"},{"id":"us-wa-063","orig":"Spokane, WA","text":"Spokane, WA"},{"id":"us-va-177","orig":"Spotsylvania, VA","text":"Spotsylvania, VA"},{"id":"us-la-087","orig":"St. Bernard, LA","text":"St. Bernard, LA"},{"id":"us-la-089","orig":"St. Charles, LA","text":"St. Charles, LA"},{"id":"us-mo-183","orig":"St. Charles, MO","text":"St. Charles, MO"},{"id":"us-al-115","orig":"St. Clair, AL","text":"St. Clair, AL"},{"id":"us-il-163","orig":"St. Clair, IL","text":"St. Clair, IL"},{"id":"us-mi-147","orig":"St. Clair, MI","text":"St. Clair, MI"},{"id":"us-mo-185","orig":"St. Clair, MO","text":"St. Clair, MO"},{"id":"us-wi-109","orig":"St. Croix, WI","text":"St. Croix, WI"},{"id":"us-ar-123","orig":"St. Francis, AR","text":"St. Francis, AR"},{"id":"us-mo-187","orig":"St. Francois, MO","text":"St. Francois, MO"},{"id":"us-la-091","orig":"St. Helena, LA","text":"St. Helena, LA"},{"id":"us-la-093","orig":"St. James, LA","text":"St. James, LA"},{"id":"us-la-095","orig":"St. John the Baptist, LA","text":"St. John the Baptist, LA"},{"id":"us-fl-109","orig":"St. Johns, FL","text":"St. Johns, FL"},{"id":"us-in-141","orig":"St. Joseph, IN","text":"St. Joseph, IN"},{"id":"us-mi-149","orig":"St. Joseph, MI","text":"St. Joseph, MI"},{"id":"us-la-097","orig":"St. Landry, LA","text":"St. Landry, LA"},{"id":"us-ny-089","orig":"St. Lawrence, NY","text":"St. Lawrence, NY"},{"id":"us-mn-137","orig":"St. Louis, MN","text":"St. Louis, MN"},{"id":"us-mo-510","orig":"St. Louis, MO","text":"St. Louis, MO"},{"id":"us-mo-189","orig":"St. Louis, MO","text":"St. Louis, MO"},{"id":"us-fl-111","orig":"St. Lucie, FL","text":"St. Lucie, FL"},{"id":"us-la-099","orig":"St. Martin, LA","text":"St. Martin, LA"},{"id":"us-md-037","orig":"St. Mary's, MD","text":"St. Mary's, MD"},{"id":"us-la-101","orig":"St. Mary, LA","text":"St. Mary, LA"},{"id":"us-la-103","orig":"St. Tammany, LA","text":"St. Tammany, LA"},{"id":"us-ks-185","orig":"Stafford, KS","text":"Stafford, KS"},{"id":"us-va-179","orig":"Stafford, VA","text":"Stafford, VA"},{"id":"us-ca-099","orig":"Stanislaus, CA","text":"Stanislaus, CA"},{"id":"us-sd-117","orig":"Stanley, SD","text":"Stanley, SD"},{"id":"us-nc-167","orig":"Stanly, NC","text":"Stanly, NC"},{"id":"us-ks-187","orig":"Stanton, KS","text":"Stanton, KS"},{"id":"us-ne-167","orig":"Stanton, NE","text":"Stanton, NE"},{"id":"us-il-175","orig":"Stark, IL","text":"Stark, IL"},{"id":"us-nd-089","orig":"Stark, ND","text":"Stark, ND"},{"id":"us-oh-151","orig":"Stark, OH","text":"Stark, OH"},{"id":"us-in-149","orig":"Starke, IN","text":"Starke, IN"},{"id":"us-tx-427","orig":"Starr, TX","text":"Starr, TX"},{"id":"us-va-790","orig":"Staunton, VA","text":"Staunton, VA"},{"id":"us-mo-186","orig":"Ste. Genevieve, MO","text":"Ste. Genevieve, MO"},{"id":"us-mn-145","orig":"Stearns, MN","text":"Stearns, MN"},{"id":"us-mn-147","orig":"Steele, MN","text":"Steele, MN"},{"id":"us-nd-091","orig":"Steele, ND","text":"Steele, ND"},{"id":"us-ga-257","orig":"Stephens, GA","text":"Stephens, GA"},{"id":"us-ok-137","orig":"Stephens, OK","text":"Stephens, OK"},{"id":"us-tx-429","orig":"Stephens, TX","text":"Stephens, TX"},{"id":"us-il-177","orig":"Stephenson, IL","text":"Stephenson, IL"},{"id":"us-tx-431","orig":"Sterling, TX","text":"Sterling, TX"},{"id":"us-in-151","orig":"Steuben, IN","text":"Steuben, IN"},{"id":"us-ny-101","orig":"Steuben, NY","text":"Steuben, NY"},{"id":"us-ks-189","orig":"Stevens, KS","text":"Stevens, KS"},{"id":"us-mn-149","orig":"Stevens, MN","text":"Stevens, MN"},{"id":"us-wa-065","orig":"Stevens, WA","text":"Stevens, WA"},{"id":"us-ga-259","orig":"Stewart, GA","text":"Stewart, GA"},{"id":"us-tn-161","orig":"Stewart, TN","text":"Stewart, TN"},{"id":"us-mt-095","orig":"Stillwater, MT","text":"Stillwater, MT"},{"id":"us-mo-207","orig":"Stoddard, MO","text":"Stoddard, MO"},{"id":"us-nc-169","orig":"Stokes, NC","text":"Stokes, NC"},{"id":"us-ar-137","orig":"Stone, AR","text":"Stone, AR"},{"id":"us-mo-209","orig":"Stone, MO","text":"Stone, MO"},{"id":"us-ms-131","orig":"Stone, MS","text":"Stone, MS"},{"id":"us-tx-433","orig":"Stonewall, TX","text":"Stonewall, TX"},{"id":"us-nv-029","orig":"Storey, NV","text":"Storey, NV"},{"id":"us-ia-169","orig":"Story, IA","text":"Story, IA"},{"id":"us-nh-017","orig":"Strafford, NH","text":"Strafford, NH"},{"id":"us-nd-093","orig":"Stutsman, ND","text":"Stutsman, ND"},{"id":"us-wy-035","orig":"Sublette, WY","text":"Sublette, WY"},{"id":"us-ma-025","orig":"Suffolk, MA","text":"Suffolk, MA"},{"id":"us-ny-103","orig":"Suffolk, NY","text":"Suffolk, NY"},{"id":"us-va-800","orig":"Suffolk, VA","text":"Suffolk, VA"},{"id":"us-in-153","orig":"Sullivan, IN","text":"Sullivan, IN"},{"id":"us-mo-211","orig":"Sullivan, MO","text":"Sullivan, MO"},{"id":"us-nh-019","orig":"Sullivan, NH","text":"Sullivan, NH"},{"id":"us-ny-105","orig":"Sullivan, NY","text":"Sullivan, NY"},{"id":"us-pa-113","orig":"Sullivan, PA","text":"Sullivan, PA"},{"id":"us-tn-163","orig":"Sullivan, TN","text":"Sullivan, TN"},{"id":"us-sd-119","orig":"Sully, SD","text":"Sully, SD"},{"id":"us-wv-089","orig":"Summers, WV","text":"Summers, WV"},{"id":"us-co-117","orig":"Summit, CO","text":"Summit, CO"},{"id":"us-oh-153","orig":"Summit, OH","text":"Summit, OH"},{"id":"us-ut-043","orig":"Summit, UT","text":"Summit, UT"},{"id":"us-ks-191","orig":"Sumner, KS","text":"Sumner, KS"},{"id":"us-tn-165","orig":"Sumner, TN","text":"Sumner, TN"},{"id":"us-al-119","orig":"Sumter, AL","text":"Sumter, AL"},{"id":"us-fl-119","orig":"Sumter, FL","text":"Sumter, FL"},{"id":"us-ga-261","orig":"Sumter, GA","text":"Sumter, GA"},{"id":"us-sc-085","orig":"Sumter, SC","text":"Sumter, SC"},{"id":"us-ms-133","orig":"Sunflower, MS","text":"Sunflower, MS"},{"id":"us-nc-171","orig":"Surry, NC","text":"Surry, NC"},{"id":"us-va-181","orig":"Surry, VA","text":"Surry, VA"},{"id":"us-pa-115","orig":"Susquehanna, PA","text":"Susquehanna, PA"},{"id":"us-de-005","orig":"Sussex, DE","text":"Sussex, DE"},{"id":"us-nj-037","orig":"Sussex, NJ","text":"Sussex, NJ"},{"id":"us-va-183","orig":"Sussex, VA","text":"Sussex, VA"},{"id":"us-ca-101","orig":"Sutter, CA","text":"Sutter, CA"},{"id":"us-tx-435","orig":"Sutton, TX","text":"Sutton, TX"},{"id":"us-fl-121","orig":"Suwannee, FL","text":"Suwannee, FL"},{"id":"us-nc-173","orig":"Swain, NC","text":"Swain, NC"},{"id":"us-mt-097","orig":"Sweet Grass, MT","text":"Sweet Grass, MT"},{"id":"us-wy-037","orig":"Sweetwater, WY","text":"Sweetwater, WY"},{"id":"us-mn-151","orig":"Swift, MN","text":"Swift, MN"},{"id":"us-tx-437","orig":"Swisher, TX","text":"Swisher, TX"},{"id":"us-in-155","orig":"Switzerland, IN","text":"Switzerland, IN"},{"id":"us-ga-263","orig":"Talbot, GA","text":"Talbot, GA"},{"id":"us-md-041","orig":"Talbot, MD","text":"Talbot, MD"},{"id":"us-ga-265","orig":"Taliaferro, GA","text":"Taliaferro, GA"},{"id":"us-al-121","orig":"Talladega, AL","text":"Talladega, AL"},{"id":"us-ms-135","orig":"Tallahatchie, MS","text":"Tallahatchie, MS"},{"id":"us-al-123","orig":"Tallapoosa, AL","text":"Tallapoosa, AL"},{"id":"us-ia-171","orig":"Tama, IA","text":"Tama, IA"},{"id":"us-mo-213","orig":"Taney, MO","text":"Taney, MO"},{"id":"us-la-105","orig":"Tangipahoa, LA","text":"Tangipahoa, LA"},{"id":"us-nm-055","orig":"Taos, NM","text":"Taos, NM"},{"id":"us-tx-439","orig":"Tarrant, TX","text":"Tarrant, TX"},{"id":"us-ms-137","orig":"Tate, MS","text":"Tate, MS"},{"id":"us-ga-267","orig":"Tattnall, GA","text":"Tattnall, GA"},{"id":"us-fl-123","orig":"Taylor, FL","text":"Taylor, FL"},{"id":"us-ga-269","orig":"Taylor, GA","text":"Taylor, GA"},{"id":"us-ia-173","orig":"Taylor, IA","text":"Taylor, IA"},{"id":"us-ky-217","orig":"Taylor, KY","text":"Taylor, KY"},{"id":"us-tx-441","orig":"Taylor, TX","text":"Taylor, TX"},{"id":"us-wi-119","orig":"Taylor, WI","text":"Taylor, WI"},{"id":"us-wv-091","orig":"Taylor, WV","text":"Taylor, WV"},{"id":"us-il-179","orig":"Tazewell, IL","text":"Tazewell, IL"},{"id":"us-va-185","orig":"Tazewell, VA","text":"Tazewell, VA"},{"id":"us-ca-103","orig":"Tehama, CA","text":"Tehama, CA"},{"id":"us-ga-271","orig":"Telfair, GA","text":"Telfair, GA"},{"id":"us-co-119","orig":"Teller, CO","text":"Teller, CO"},{"id":"us-la-107","orig":"Tensas, LA","text":"Tensas, LA"},{"id":"us-la-109","orig":"Terrebonne, LA","text":"Terrebonne, LA"},{"id":"us-ga-273","orig":"Terrell, GA","text":"Terrell, GA"},{"id":"us-tx-443","orig":"Terrell, TX","text":"Terrell, TX"},{"id":"us-tx-445","orig":"Terry, TX","text":"Terry, TX"},{"id":"us-id-081","orig":"Teton, ID","text":"Teton, ID"},{"id":"us-mt-099","orig":"Teton, MT","text":"Teton, MT"},{"id":"us-wy-039","orig":"Teton, WY","text":"Teton, WY"},{"id":"us-mo-215","orig":"Texas, MO","text":"Texas, MO"},{"id":"us-ok-139","orig":"Texas, OK","text":"Texas, OK"},{"id":"us-ne-169","orig":"Thayer, NE","text":"Thayer, NE"},{"id":"us-ga-275","orig":"Thomas, GA","text":"Thomas, GA"},{"id":"us-ks-193","orig":"Thomas, KS","text":"Thomas, KS"},{"id":"us-ne-171","orig":"Thomas, NE","text":"Thomas, NE"},{"id":"us-tx-447","orig":"Throckmorton, TX","text":"Throckmorton, TX"},{"id":"us-ne-173","orig":"Thurston, NE","text":"Thurston, NE"},{"id":"us-wa-067","orig":"Thurston, WA","text":"Thurston, WA"},{"id":"us-ga-277","orig":"Tift, GA","text":"Tift, GA"},{"id":"us-or-057","orig":"Tillamook, OR","text":"Tillamook, OR"},{"id":"us-ok-141","orig":"Tillman, OK","text":"Tillman, OK"},{"id":"us-ny-107","orig":"Tioga, NY","text":"Tioga, NY"},{"id":"us-pa-117","orig":"Tioga, PA","text":"Tioga, PA"},{"id":"us-ms-139","orig":"Tippah, MS","text":"Tippah, MS"},{"id":"us-in-157","orig":"Tippecanoe, IN","text":"Tippecanoe, IN"},{"id":"us-in-159","orig":"Tipton, IN","text":"Tipton, IN"},{"id":"us-tn-167","orig":"Tipton, TN","text":"Tipton, TN"},{"id":"us-ms-141","orig":"Tishomingo, MS","text":"Tishomingo, MS"},{"id":"us-tx-449","orig":"Titus, TX","text":"Titus, TX"},{"id":"us-ky-219","orig":"Todd, KY","text":"Todd, KY"},{"id":"us-mn-153","orig":"Todd, MN","text":"Todd, MN"},{"id":"us-sd-121","orig":"Todd, SD","text":"Todd, SD"},{"id":"us-ct-013","orig":"Tolland, CT","text":"Tolland, CT"},{"id":"us-tx-451","orig":"Tom Green, TX","text":"Tom Green, TX"},{"id":"us-ny-109","orig":"Tompkins, NY","text":"Tompkins, NY"},{"id":"us-ut-045","orig":"Tooele, UT","text":"Tooele, UT"},{"id":"us-mt-101","orig":"Toole, MT","text":"Toole, MT"},{"id":"us-ga-279","orig":"Toombs, GA","text":"Toombs, GA"},{"id":"us-nm-057","orig":"Torrance, NM","text":"Torrance, NM"},{"id":"us-nd-095","orig":"Towner, ND","text":"Towner, ND"},{"id":"us-ga-281","orig":"Towns, GA","text":"Towns, GA"},{"id":"us-nd-097","orig":"Traill, ND","text":"Traill, ND"},{"id":"us-nc-175","orig":"Transylvania, NC","text":"Transylvania, NC"},{"id":"us-mn-155","orig":"Traverse, MN","text":"Traverse, MN"},{"id":"us-tx-453","orig":"Travis, TX","text":"Travis, TX"},{"id":"us-mt-103","orig":"Treasure, MT","text":"Treasure, MT"},{"id":"us-ks-195","orig":"Trego, KS","text":"Trego, KS"},{"id":"us-wi-121","orig":"Trempealeau, WI","text":"Trempealeau, WI"},{"id":"us-ga-283","orig":"Treutlen, GA","text":"Treutlen, GA"},{"id":"us-ky-221","orig":"Trigg, KY","text":"Trigg, KY"},{"id":"us-ky-223","orig":"Trimble, KY","text":"Trimble, KY"},{"id":"us-ca-105","orig":"Trinity, CA","text":"Trinity, CA"},{"id":"us-tx-455","orig":"Trinity, TX","text":"Trinity, TX"},{"id":"us-sd-123","orig":"Tripp, SD","text":"Tripp, SD"},{"id":"us-ga-285","orig":"Troup, GA","text":"Troup, GA"},{"id":"us-tn-169","orig":"Trousdale, TN","text":"Trousdale, TN"},{"id":"us-oh-155","orig":"Trumbull, OH","text":"Trumbull, OH"},{"id":"us-wv-093","orig":"Tucker, WV","text":"Tucker, WV"},{"id":"us-ca-107","orig":"Tulare, CA","text":"Tulare, CA"},{"id":"us-ok-143","orig":"Tulsa, OK","text":"Tulsa, OK"},{"id":"us-ms-143","orig":"Tunica, MS","text":"Tunica, MS"},{"id":"us-ca-109","orig":"Tuolumne, CA","text":"Tuolumne, CA"},{"id":"us-ga-287","orig":"Turner, GA","text":"Turner, GA"},{"id":"us-sd-125","orig":"Turner, SD","text":"Turner, SD"},{"id":"us-al-125","orig":"Tuscaloosa, AL","text":"Tuscaloosa, AL"},{"id":"us-oh-157","orig":"Tuscarawas, OH","text":"Tuscarawas, OH"},{"id":"us-mi-157","orig":"Tuscola, MI","text":"Tuscola, MI"},{"id":"us-ga-289","orig":"Twiggs, GA","text":"Twiggs, GA"},{"id":"us-id-083","orig":"Twin Falls, ID","text":"Twin Falls, ID"},{"id":"us-tx-457","orig":"Tyler, TX","text":"Tyler, TX"},{"id":"us-wv-095","orig":"Tyler, WV","text":"Tyler, WV"},{"id":"us-nc-177","orig":"Tyrrell, NC","text":"Tyrrell, NC"},{"id":"us-wy-041","orig":"Uinta, WY","text":"Uinta, WY"},{"id":"us-ut-047","orig":"Uintah, UT","text":"Uintah, UT"},{"id":"us-ny-111","orig":"Ulster, NY","text":"Ulster, NY"},{"id":"us-or-059","orig":"Umatilla, OR","text":"Umatilla, OR"},{"id":"us-tn-171","orig":"Unicoi, TN","text":"Unicoi, TN"},{"id":"us-ar-139","orig":"Union, AR","text":"Union, AR"},{"id":"us-fl-125","orig":"Union, FL","text":"Union, FL"},{"id":"us-ga-291","orig":"Union, GA","text":"Union, GA"},{"id":"us-ia-175","orig":"Union, IA","text":"Union, IA"},{"id":"us-il-181","orig":"Union, IL","text":"Union, IL"},{"id":"us-in-161","orig":"Union, IN","text":"Union, IN"},{"id":"us-ky-225","orig":"Union, KY","text":"Union, KY"},{"id":"us-la-111","orig":"Union, LA","text":"Union, LA"},{"id":"us-ms-145","orig":"Union, MS","text":"Union, MS"},{"id":"us-nc-179","orig":"Union, NC","text":"Union, NC"},{"id":"us-nj-039","orig":"Union, NJ","text":"Union, NJ"},{"id":"us-nm-059","orig":"Union, NM","text":"Union, NM"},{"id":"us-oh-159","orig":"Union, OH","text":"Union, OH"},{"id":"us-or-061","orig":"Union, OR","text":"Union, OR"},{"id":"us-pa-119","orig":"Union, PA","text":"Union, PA"},{"id":"us-sc-087","orig":"Union, SC","text":"Union, SC"},{"id":"us-sd-127","orig":"Union, SD","text":"Union, SD"},{"id":"us-tn-173","orig":"Union, TN","text":"Union, TN"},{"id":"us-tx-459","orig":"Upshur, TX","text":"Upshur, TX"},{"id":"us-wv-097","orig":"Upshur, WV","text":"Upshur, WV"},{"id":"us-ga-293","orig":"Upson, GA","text":"Upson, GA"},{"id":"us-tx-461","orig":"Upton, TX","text":"Upton, TX"},{"id":"us-ut-049","orig":"Utah, UT","text":"Utah, UT"},{"id":"us-tx-463","orig":"Uvalde, TX","text":"Uvalde, TX"},{"id":"us-tx-465","orig":"Val Verde, TX","text":"Val Verde, TX"},{"id":"us-ak-261","orig":"Valdez-Cordova, AK","text":"Valdez-Cordova, AK"},{"id":"us-nm-061","orig":"Valencia, NM","text":"Valencia, NM"},{"id":"us-id-085","orig":"Valley, ID","text":"Valley, ID"},{"id":"us-mt-105","orig":"Valley, MT","text":"Valley, MT"},{"id":"us-ne-175","orig":"Valley, NE","text":"Valley, NE"},{"id":"us-ar-141","orig":"Van Buren, AR","text":"Van Buren, AR"},{"id":"us-ia-177","orig":"Van Buren, IA","text":"Van Buren, IA"},{"id":"us-mi-159","orig":"Van Buren, MI","text":"Van Buren, MI"},{"id":"us-tn-175","orig":"Van Buren, TN","text":"Van Buren, TN"},{"id":"us-oh-161","orig":"Van Wert, OH","text":"Van Wert, OH"},{"id":"us-tx-467","orig":"Van Zandt, TX","text":"Van Zandt, TX"},{"id":"us-nc-181","orig":"Vance, NC","text":"Vance, NC"},{"id":"us-in-163","orig":"Vanderburgh, IN","text":"Vanderburgh, IN"},{"id":"us-pa-121","orig":"Venango, PA","text":"Venango, PA"},{"id":"us-ca-111","orig":"Ventura, CA","text":"Ventura, CA"},{"id":"us-il-183","orig":"Vermilion, IL","text":"Vermilion, IL"},{"id":"us-la-113","orig":"Vermilion, LA","text":"Vermilion, LA"},{"id":"us-in-165","orig":"Vermillion, IN","text":"Vermillion, IN"},{"id":"us-la-115","orig":"Vernon, LA","text":"Vernon, LA"},{"id":"us-mo-217","orig":"Vernon, MO","text":"Vernon, MO"},{"id":"us-wi-123","orig":"Vernon, WI","text":"Vernon, WI"},{"id":"us-tx-469","orig":"Victoria, TX","text":"Victoria, TX"},{"id":"us-in-167","orig":"Vigo, IN","text":"Vigo, IN"},{"id":"us-wi-125","orig":"Vilas, WI","text":"Vilas, WI"},{"id":"us-oh-163","orig":"Vinton, OH","text":"Vinton, OH"},{"id":"us-va-810","orig":"Virginia Beach, VA","text":"Virginia Beach, VA"},{"id":"us-fl-127","orig":"Volusia, FL","text":"Volusia, FL"},{"id":"us-il-185","orig":"Wabash, IL","text":"Wabash, IL"},{"id":"us-in-169","orig":"Wabash, IN","text":"Wabash, IN"},{"id":"us-mn-157","orig":"Wabasha, MN","text":"Wabasha, MN"},{"id":"us-ks-197","orig":"Wabaunsee, KS","text":"Wabaunsee, KS"},{"id":"us-ak-270","orig":"Wade Hampton, AK","text":"Wade Hampton, AK"},{"id":"us-mn-159","orig":"Wadena, MN","text":"Wadena, MN"},{"id":"us-ok-145","orig":"Wagoner, OK","text":"Wagoner, OK"},{"id":"us-wa-069","orig":"Wahkiakum, WA","text":"Wahkiakum, WA"},{"id":"us-nc-183","orig":"Wake, NC","text":"Wake, NC"},{"id":"us-fl-129","orig":"Wakulla, FL","text":"Wakulla, FL"},{"id":"us-me-027","orig":"Waldo, ME","text":"Waldo, ME"},{"id":"us-al-127","orig":"Walker, AL","text":"Walker, AL"},{"id":"us-ga-295","orig":"Walker, GA","text":"Walker, GA"},{"id":"us-tx-471","orig":"Walker, TX","text":"Walker, TX"},{"id":"us-wa-071","orig":"Walla Walla, WA","text":"Walla Walla, WA"},{"id":"us-ks-199","orig":"Wallace, KS","text":"Wallace, KS"},{"id":"us-tx-473","orig":"Waller, TX","text":"Waller, TX"},{"id":"us-or-063","orig":"Wallowa, OR","text":"Wallowa, OR"},{"id":"us-nd-099","orig":"Walsh, ND","text":"Walsh, ND"},{"id":"us-ms-147","orig":"Walthall, MS","text":"Walthall, MS"},{"id":"us-fl-131","orig":"Walton, FL","text":"Walton, FL"},{"id":"us-ga-297","orig":"Walton, GA","text":"Walton, GA"},{"id":"us-sd-129","orig":"Walworth, SD","text":"Walworth, SD"},{"id":"us-wi-127","orig":"Walworth, WI","text":"Walworth, WI"},{"id":"us-ia-179","orig":"Wapello, IA","text":"Wapello, IA"},{"id":"us-nd-101","orig":"Ward, ND","text":"Ward, ND"},{"id":"us-tx-475","orig":"Ward, TX","text":"Ward, TX"},{"id":"us-ga-299","orig":"Ware, GA","text":"Ware, GA"},{"id":"us-ga-301","orig":"Warren, GA","text":"Warren, GA"},{"id":"us-ia-181","orig":"Warren, IA","text":"Warren, IA"},{"id":"us-il-187","orig":"Warren, IL","text":"Warren, IL"},{"id":"us-in-171","orig":"Warren, IN","text":"Warren, IN"},{"id":"us-ky-227","orig":"Warren, KY","text":"Warren, KY"},{"id":"us-mo-219","orig":"Warren, MO","text":"Warren, MO"},{"id":"us-ms-149","orig":"Warren, MS","text":"Warren, MS"},{"id":"us-nc-185","orig":"Warren, NC","text":"Warren, NC"},{"id":"us-nj-041","orig":"Warren, NJ","text":"Warren, NJ"},{"id":"us-ny-113","orig":"Warren, NY","text":"Warren, NY"},{"id":"us-oh-165","orig":"Warren, OH","text":"Warren, OH"},{"id":"us-pa-123","orig":"Warren, PA","text":"Warren, PA"},{"id":"us-tn-177","orig":"Warren, TN","text":"Warren, TN"},{"id":"us-va-187","orig":"Warren, VA","text":"Warren, VA"},{"id":"us-in-173","orig":"Warrick, IN","text":"Warrick, IN"},{"id":"us-ut-051","orig":"Wasatch, UT","text":"Wasatch, UT"},{"id":"us-or-065","orig":"Wasco, OR","text":"Wasco, OR"},{"id":"us-mn-161","orig":"Waseca, MN","text":"Waseca, MN"},{"id":"us-wy-043","orig":"Washakie, WY","text":"Washakie, WY"},{"id":"us-wi-129","orig":"Washburn, WI","text":"Washburn, WI"},{"id":"us-al-129","orig":"Washington, AL","text":"Washington, AL"},{"id":"us-ar-143","orig":"Washington, AR","text":"Washington, AR"},{"id":"us-co-121","orig":"Washington, CO","text":"Washington, CO"},{"id":"us-fl-133","orig":"Washington, FL","text":"Washington, FL"},{"id":"us-ga-303","orig":"Washington, GA","text":"Washington, GA"},{"id":"us-ia-183","orig":"Washington, IA","text":"Washington, IA"},{"id":"us-id-087","orig":"Washington, ID","text":"Washington, ID"},{"id":"us-il-189","orig":"Washington, IL","text":"Washington, IL"},{"id":"us-in-175","orig":"Washington, IN","text":"Washington, IN"},{"id":"us-ks-201","orig":"Washington, KS","text":"Washington, KS"},{"id":"us-ky-229","orig":"Washington, KY","text":"Washington, KY"},{"id":"us-la-117","orig":"Washington, LA","text":"Washington, LA"},{"id":"us-md-043","orig":"Washington, MD","text":"Washington, MD"},{"id":"us-me-029","orig":"Washington, ME","text":"Washington, ME"},{"id":"us-mn-163","orig":"Washington, MN","text":"Washington, MN"},{"id":"us-mo-221","orig":"Washington, MO","text":"Washington, MO"},{"id":"us-ms-151","orig":"Washington, MS","text":"Washington, MS"},{"id":"us-nc-187","orig":"Washington, NC","text":"Washington, NC"},{"id":"us-ne-177","orig":"Washington, NE","text":"Washington, NE"},{"id":"us-ny-115","orig":"Washington, NY","text":"Washington, NY"},{"id":"us-oh-167","orig":"Washington, OH","text":"Washington, OH"},{"id":"us-ok-147","orig":"Washington, OK","text":"Washington, OK"},{"id":"us-or-067","orig":"Washington, OR","text":"Washington, OR"},{"id":"us-pa-125","orig":"Washington, PA","text":"Washington, PA"},{"id":"us-ri-009","orig":"Washington, RI","text":"Washington, RI"},{"id":"us-tn-179","orig":"Washington, TN","text":"Washington, TN"},{"id":"us-tx-477","orig":"Washington, TX","text":"Washington, TX"},{"id":"us-ut-053","orig":"Washington, UT","text":"Washington, UT"},{"id":"us-va-191","orig":"Washington, VA","text":"Washington, VA"},{"id":"us-vt-023","orig":"Washington, VT","text":"Washington, VT"},{"id":"us-wi-131","orig":"Washington, WI","text":"Washington, WI"},{"id":"us-ok-149","orig":"Washita, OK","text":"Washita, OK"},{"id":"us-nv-031","orig":"Washoe, NV","text":"Washoe, NV"},{"id":"us-mi-161","orig":"Washtenaw, MI","text":"Washtenaw, MI"},{"id":"us-nc-189","orig":"Watauga, NC","text":"Watauga, NC"},{"id":"us-mn-165","orig":"Watonwan, MN","text":"Watonwan, MN"},{"id":"us-wi-133","orig":"Waukesha, WI","text":"Waukesha, WI"},{"id":"us-wi-135","orig":"Waupaca, WI","text":"Waupaca, WI"},{"id":"us-wi-137","orig":"Waushara, WI","text":"Waushara, WI"},{"id":"us-ga-305","orig":"Wayne, GA","text":"Wayne, GA"},{"id":"us-ia-185","orig":"Wayne, IA","text":"Wayne, IA"},{"id":"us-il-191","orig":"Wayne, IL","text":"Wayne, IL"},{"id":"us-in-177","orig":"Wayne, IN","text":"Wayne, IN"},{"id":"us-ky-231","orig":"Wayne, KY","text":"Wayne, KY"},{"id":"us-mi-163","orig":"Wayne, MI","text":"Wayne, MI"},{"id":"us-mo-223","orig":"Wayne, MO","text":"Wayne, MO"},{"id":"us-ms-153","orig":"Wayne, MS","text":"Wayne, MS"},{"id":"us-nc-191","orig":"Wayne, NC","text":"Wayne, NC"},{"id":"us-ne-179","orig":"Wayne, NE","text":"Wayne, NE"},{"id":"us-ny-117","orig":"Wayne, NY","text":"Wayne, NY"},{"id":"us-oh-169","orig":"Wayne, OH","text":"Wayne, OH"},{"id":"us-pa-127","orig":"Wayne, PA","text":"Wayne, PA"},{"id":"us-tn-181","orig":"Wayne, TN","text":"Wayne, TN"},{"id":"us-ut-055","orig":"Wayne, UT","text":"Wayne, UT"},{"id":"us-wv-099","orig":"Wayne, WV","text":"Wayne, WV"},{"id":"us-va-820","orig":"Waynesboro, VA","text":"Waynesboro, VA"},{"id":"us-tn-183","orig":"Weakley, TN","text":"Weakley, TN"},{"id":"us-tx-479","orig":"Webb, TX","text":"Webb, TX"},{"id":"us-ut-057","orig":"Weber, UT","text":"Weber, UT"},{"id":"us-ga-307","orig":"Webster, GA","text":"Webster, GA"},{"id":"us-ia-187","orig":"Webster, IA","text":"Webster, IA"},{"id":"us-ky-233","orig":"Webster, KY","text":"Webster, KY"},{"id":"us-la-119","orig":"Webster, LA","text":"Webster, LA"},{"id":"us-mo-225","orig":"Webster, MO","text":"Webster, MO"},{"id":"us-ms-155","orig":"Webster, MS","text":"Webster, MS"},{"id":"us-ne-181","orig":"Webster, NE","text":"Webster, NE"},{"id":"us-wv-101","orig":"Webster, WV","text":"Webster, WV"},{"id":"us-co-123","orig":"Weld, CO","text":"Weld, CO"},{"id":"us-in-179","orig":"Wells, IN","text":"Wells, IN"},{"id":"us-nd-103","orig":"Wells, ND","text":"Wells, ND"},{"id":"us-la-121","orig":"West Baton Rouge, LA","text":"West Baton Rouge, LA"},{"id":"us-la-123","orig":"West Carroll, LA","text":"West Carroll, LA"},{"id":"us-la-125","orig":"West Feliciana, LA","text":"West Feliciana, LA"},{"id":"us-ny-119","orig":"Westchester, NY","text":"Westchester, NY"},{"id":"us-pa-129","orig":"Westmoreland, PA","text":"Westmoreland, PA"},{"id":"us-va-193","orig":"Westmoreland, VA","text":"Westmoreland, VA"},{"id":"us-wy-045","orig":"Weston, WY","text":"Weston, WY"},{"id":"us-wv-103","orig":"Wetzel, WV","text":"Wetzel, WV"},{"id":"us-mi-165","orig":"Wexford, MI","text":"Wexford, MI"},{"id":"us-tx-481","orig":"Wharton, TX","text":"Wharton, TX"},{"id":"us-wa-073","orig":"Whatcom, WA","text":"Whatcom, WA"},{"id":"us-mt-107","orig":"Wheatland, MT","text":"Wheatland, MT"},{"id":"us-ga-309","orig":"Wheeler, GA","text":"Wheeler, GA"},{"id":"us-ne-183","orig":"Wheeler, NE","text":"Wheeler, NE"},{"id":"us-or-069","orig":"Wheeler, OR","text":"Wheeler, OR"},{"id":"us-tx-483","orig":"Wheeler, TX","text":"Wheeler, TX"},{"id":"us-nv-033","orig":"White Pine, NV","text":"White Pine, NV"},{"id":"us-ar-145","orig":"White, AR","text":"White, AR"},{"id":"us-ga-311","orig":"White, GA","text":"White, GA"},{"id":"us-il-193","orig":"White, IL","text":"White, IL"},{"id":"us-in-181","orig":"White, IN","text":"White, IN"},{"id":"us-tn-185","orig":"White, TN","text":"White, TN"},{"id":"us-il-195","orig":"Whiteside, IL","text":"Whiteside, IL"},{"id":"us-ga-313","orig":"Whitfield, GA","text":"Whitfield, GA"},{"id":"us-in-183","orig":"Whitley, IN","text":"Whitley, IN"},{"id":"us-ky-235","orig":"Whitley, KY","text":"Whitley, KY"},{"id":"us-wa-075","orig":"Whitman, WA","text":"Whitman, WA"},{"id":"us-mt-109","orig":"Wibaux, MT","text":"Wibaux, MT"},{"id":"us-ks-203","orig":"Wichita, KS","text":"Wichita, KS"},{"id":"us-tx-485","orig":"Wichita, TX","text":"Wichita, TX"},{"id":"us-md-045","orig":"Wicomico, MD","text":"Wicomico, MD"},{"id":"us-tx-487","orig":"Wilbarger, TX","text":"Wilbarger, TX"},{"id":"us-al-131","orig":"Wilcox, AL","text":"Wilcox, AL"},{"id":"us-ga-315","orig":"Wilcox, GA","text":"Wilcox, GA"},{"id":"us-ga-317","orig":"Wilkes, GA","text":"Wilkes, GA"},{"id":"us-nc-193","orig":"Wilkes, NC","text":"Wilkes, NC"},{"id":"us-mn-167","orig":"Wilkin, MN","text":"Wilkin, MN"},{"id":"us-ga-319","orig":"Wilkinson, GA","text":"Wilkinson, GA"},{"id":"us-ms-157","orig":"Wilkinson, MS","text":"Wilkinson, MS"},{"id":"us-il-197","orig":"Will, IL","text":"Will, IL"},{"id":"us-tx-489","orig":"Willacy, TX","text":"Willacy, TX"},{"id":"us-nd-105","orig":"Williams, ND","text":"Williams, ND"},{"id":"us-oh-171","orig":"Williams, OH","text":"Williams, OH"},{"id":"us-sc-089","orig":"Williamsburg, SC","text":"Williamsburg, SC"},{"id":"us-va-830","orig":"Williamsburg, VA","text":"Williamsburg, VA"},{"id":"us-il-199","orig":"Williamson, IL","text":"Williamson, IL"},{"id":"us-tn-187","orig":"Williamson, TN","text":"Williamson, TN"},{"id":"us-tx-491","orig":"Williamson, TX","text":"Williamson, TX"},{"id":"us-ks-205","orig":"Wilson, KS","text":"Wilson, KS"},{"id":"us-nc-195","orig":"Wilson, NC","text":"Wilson, NC"},{"id":"us-tn-189","orig":"Wilson, TN","text":"Wilson, TN"},{"id":"us-tx-493","orig":"Wilson, TX","text":"Wilson, TX"},{"id":"us-va-840","orig":"Winchester, VA","text":"Winchester, VA"},{"id":"us-ct-015","orig":"Windham, CT","text":"Windham, CT"},{"id":"us-vt-025","orig":"Windham, VT","text":"Windham, VT"},{"id":"us-vt-027","orig":"Windsor, VT","text":"Windsor, VT"},{"id":"us-tx-495","orig":"Winkler, TX","text":"Winkler, TX"},{"id":"us-la-127","orig":"Winn, LA","text":"Winn, LA"},{"id":"us-ia-189","orig":"Winnebago, IA","text":"Winnebago, IA"},{"id":"us-il-201","orig":"Winnebago, IL","text":"Winnebago, IL"},{"id":"us-wi-139","orig":"Winnebago, WI","text":"Winnebago, WI"},{"id":"us-ia-191","orig":"Winneshiek, IA","text":"Winneshiek, IA"},{"id":"us-mn-169","orig":"Winona, MN","text":"Winona, MN"},{"id":"us-al-133","orig":"Winston, AL","text":"Winston, AL"},{"id":"us-ms-159","orig":"Winston, MS","text":"Winston, MS"},{"id":"us-wv-105","orig":"Wirt, WV","text":"Wirt, WV"},{"id":"us-tx-497","orig":"Wise, TX","text":"Wise, TX"},{"id":"us-va-195","orig":"Wise, VA","text":"Wise, VA"},{"id":"us-ky-237","orig":"Wolfe, KY","text":"Wolfe, KY"},{"id":"us-oh-173","orig":"Wood, OH","text":"Wood, OH"},{"id":"us-tx-499","orig":"Wood, TX","text":"Wood, TX"},{"id":"us-wi-141","orig":"Wood, WI","text":"Wood, WI"},{"id":"us-wv-107","orig":"Wood, WV","text":"Wood, WV"},{"id":"us-ia-193","orig":"Woodbury, IA","text":"Woodbury, IA"},{"id":"us-il-203","orig":"Woodford, IL","text":"Woodford, IL"},{"id":"us-ky-239","orig":"Woodford, KY","text":"Woodford, KY"},{"id":"us-ar-147","orig":"Woodruff, AR","text":"Woodruff, AR"},{"id":"us-ok-151","orig":"Woods, OK","text":"Woods, OK"},{"id":"us-ks-207","orig":"Woodson, KS","text":"Woodson, KS"},{"id":"us-ok-153","orig":"Woodward, OK","text":"Woodward, OK"},{"id":"us-ma-027","orig":"Worcester, MA","text":"Worcester, MA"},{"id":"us-md-047","orig":"Worcester, MD","text":"Worcester, MD"},{"id":"us-ga-321","orig":"Worth, GA","text":"Worth, GA"},{"id":"us-ia-195","orig":"Worth, IA","text":"Worth, IA"},{"id":"us-mo-227","orig":"Worth, MO","text":"Worth, MO"},{"id":"us-ak-275","orig":"Wrangell, AK","text":"Wrangell, AK"},{"id":"us-ia-197","orig":"Wright, IA","text":"Wright, IA"},{"id":"us-mn-171","orig":"Wright, MN","text":"Wright, MN"},{"id":"us-mo-229","orig":"Wright, MO","text":"Wright, MO"},{"id":"us-oh-175","orig":"Wyandot, OH","text":"Wyandot, OH"},{"id":"us-ks-209","orig":"Wyandotte, KS","text":"Wyandotte, KS"},{"id":"us-ny-121","orig":"Wyoming, NY","text":"Wyoming, NY"},{"id":"us-pa-131","orig":"Wyoming, PA","text":"Wyoming, PA"},{"id":"us-wv-109","orig":"Wyoming, WV","text":"Wyoming, WV"},{"id":"us-va-197","orig":"Wythe, VA","text":"Wythe, VA"},{"id":"us-nc-197","orig":"Yadkin, NC","text":"Yadkin, NC"},{"id":"us-wa-077","orig":"Yakima, WA","text":"Yakima, WA"},{"id":"us-ak-282","orig":"Yakutat, AK","text":"Yakutat, AK"},{"id":"us-ms-161","orig":"Yalobusha, MS","text":"Yalobusha, MS"},{"id":"us-or-071","orig":"Yamhill, OR","text":"Yamhill, OR"},{"id":"us-nc-199","orig":"Yancey, NC","text":"Yancey, NC"},{"id":"us-sd-135","orig":"Yankton, SD","text":"Yankton, SD"},{"id":"us-ny-123","orig":"Yates, NY","text":"Yates, NY"},{"id":"us-az-025","orig":"Yavapai, AZ","text":"Yavapai, AZ"},{"id":"us-ms-163","orig":"Yazoo, MS","text":"Yazoo, MS"},{"id":"us-ar-149","orig":"Yell, AR","text":"Yell, AR"},{"id":"us-mn-173","orig":"Yellow Medicine, MN","text":"Yellow Medicine, MN"},{"id":"us-mt-111","orig":"Yellowstone, MT","text":"Yellowstone, MT"},{"id":"us-tx-501","orig":"Yoakum, TX","text":"Yoakum, TX"},{"id":"us-ca-113","orig":"Yolo, CA","text":"Yolo, CA"},{"id":"us-me-031","orig":"York, ME","text":"York, ME"},{"id":"us-ne-185","orig":"York, NE","text":"York, NE"},{"id":"us-pa-133","orig":"York, PA","text":"York, PA"},{"id":"us-sc-091","orig":"York, SC","text":"York, SC"},{"id":"us-va-199","orig":"York, VA","text":"York, VA"},{"id":"us-tx-503","orig":"Young, TX","text":"Young, TX"},{"id":"us-ca-115","orig":"Yuba, CA","text":"Yuba, CA"},{"id":"us-ak-290","orig":"Yukon-Koyukuk, AK","text":"Yukon-Koyukuk, AK"},{"id":"us-az-027","orig":"Yuma, AZ","text":"Yuma, AZ"},{"id":"us-co-125","orig":"Yuma, CO","text":"Yuma, CO"},{"id":"us-tx-505","orig":"Zapata, TX","text":"Zapata, TX"},{"id":"us-tx-507","orig":"Zavala, TX","text":"Zavala, TX"},{"id":"us-sd-137","orig":"Ziebach, SD","text":"Ziebach, SD"}];

    $("#country-search105").select2({
        placeholder: "Select a county",
        data: p105Data
    }).on('select2:select', function (e) {
        var data = e.params.data;
        for(var i = 0; i <  Highcharts.charts[0].series[0].points.length; i++){

           if(Highcharts.charts[0].series[0].points[i].properties["hc-key"] == data.id){
              console.log(Highcharts.charts[0].series[0].points[i]);
              Highcharts.charts[0].series[0].points[i].select();
              Highcharts.charts[0].series[0].points[i].zoomTo();
              Highcharts.charts[0].mapZoom(5);

                            var cData = {
                              county: data.text,
                              hidtaRisk: 'X',
                              hifcaRisk: '',
                              bothRisk: '',
                              noRisk: '',
                              zipCode: '89658', //**need to clarify
                              latLong: '35.25, -105.24',
                              region: 'Region IX',
                            };

                            if(data.text == "Maricopa, AZ"){
                              cData.hidtaRisk = 'YES'; cData.hifcaRisk = 'YES'; cData.bothRisk = 'YES'; cData.noRisk = 'NO'; cData.zipCode = '85001, 85002, 85003, 85004, 85005, 85006, 85007, 85008, 85009, 85010, 85011, 85012, 85013, 85014, 85015, 85016, 85017, 85018, 85019, 85020, 85021, 85022, 85023, 85024, 85025, 85026, 85027, 85028, 85029, 85030, 85031, 85032, 85033, 85034, 85035, 85036, 85037, 85038, 85039, 85040, 85041, 85042, 85043, 85044, 85045, 85046, 85048, 85050, 85051, 85053, 85054, 85060, 85061, 85062, 85063, 85064, 85065, 85066, 85067, 85068, 85069, 85070, 85071, 85072, 85073, 85074, 85075, 85076, 85078, 85079, 85080, 85082, 85083, 85085, 85086, 85087, 85097, 85098, 85142, 85201, 85202, 85203, 85204, 85205, 85206, 85207, 85208, 85209, 85210, 85211, 85212, 85213, 85214, 85215, 85216, 85224, 85225, 85226, 85233, 85234, 85236, 85244, 85246, 85248, 85249, 85250, 85251, 85252, 85253, 85254, 85255, 85256, 85257, 85258, 85259, 85260, 85261, 85262, 85263, 85264, 85266, 85267, 85268, 85269, 85271, 85274, 85275, 85277, 85280, 85281, 85282, 85283, 85284, 85285, 85286, 85287, 85295, 85296, 85297, 85298, 85299, 85301, 85302, 85303, 85304, 85305, 85306, 85307, 85308, 85309, 85310, 85311, 85312, 85318, 85320, 85322, 85323, 85326, 85327, 85329, 85331, 85335, 85337, 85338, 85339, 85340, 85342, 85343, 85345, 85351, 85353, 85354, 85355, 85358, 85361, 85363, 85372, 85373, 85374, 85375, 85376, 85377, 85378, 85379, 85380, 85381, 85382, 85383, 85385, 85387, 85388, 85390, 85392, 85395, 85396,'; cData.latLong = '33.468, -112.3947'; cData.region = 'Region IX'; cData.specialNote = ' Maricopa County is a highly populated metro area (the city of Phoenix is located in Maricopa County and is a top 10 US city and Arizona’s capital) with close proximity to the US/Mexico border. As such, Maricopa County is disproportionally affected by illegal immigration, human smuggling, and drug trafficking.'; 
                            }
                            if(data.text == "Palm Beach, FL"){
                              cData.hidtaRisk = 'YES'; cData.hifcaRisk = 'YES'; cData.bothRisk = 'YES'; cData.noRisk = 'NO'; cData.zipCode = '33401, 33402, 33403, 33404, 33405, 33406, 33407, 33408, 33409, 33410, 33411, 33412, 33413, 33414, 33415, 33416, 33417, 33418, 33419, 33420, 33421, 33422, 33424, 33425, 33426, 33427, 33428, 33429, 33430, 33431, 33432, 33433, 33434, 33435, 33436, 33437, 33438, 33444, 33445, 33446, 33448, 33449, 33454, 33458, 33459, 33460, 33461, 33462, 33463, 33464, 33465, 33466, 33467, 33468, 33469, 33470, 33472, 33473, 33474, 33476, 33477, 33478, 33480, 33481, 33482, 33483, 33484, 33486, 33487, 33488, 33493, 33496, 33497, 33498, 33499'; cData.latLong = '26.7165, -80.0679'; cData.region = 'Region IV'; cData.specialNote = 'Palm Beach County stretches along Florida’s Atlantic coast and includes metro areas like West Palm Beach and Boca Raton. Palm Beach is the wealthiest county in Florida and has high profile high net worth individuals as residents. As such certain financial crimes are prevalent in this area.'; 
                            }
                            if(data.text == "New York, NY"){
                              cData.hidtaRisk = 'YES'; cData.hifcaRisk = 'YES'; cData.bothRisk = 'YES'; cData.noRisk = 'NO'; cData.zipCode = '10001, 10002, 10003, 10004, 10005, 10006, 10007, 10008, 10009, 10010, 10011, 10012, 10013, 10014, 10016, 10017, 10018, 10019, 10020, 10021, 10022, 10023, 10024, 10025, 10026, 10027, 10028, 10029, 10030, 10031, 10032, 10033, 10034, 10035, 10036, 10037, 10038, 10039, 10040, 10041, 10043, 10044, 10045, 10055, 10060, 10065, 10069, 10075, 10080, 10081, 10087, 10090, 10101, 10102, 10103, 10104, 10105, 10106, 10107, 10108, 10109, 10110, 10111, 10112, 10113, 10114, 10115, 10116, 10117, 10118, 10119, 10120, 10121, 10122, 10123, 10124, 10125, 10126, 10128, 10129, 10130, 10131, 10132, 10133, 10138, 10150, 10151, 10152, 10153, 10154, 10155, 10156, 10157, 10158, 10159, 10160, 10161, 10162, 10163, 10164, 10165, 10166, 10167, 10168, 10169, 10170, 10171, 10172, 10173, 10174, 10175, 10176, 10177, 10178, 10179, 10185, 10199, 10203, 10211, 10212, 10213, 10242, 10249, 10256, 10258, 10259, 10260, 10261, 10265, 10268, 10269, 10270, 10271, 10272, 10273, 10274, 10275, 10276, 10277, 10278, 10279, 10280, 10281, 10282, 10285, 10286'; cData.latLong = '26.7165, -80.0679'; cData.region = 'Region IV'; cData.specialNote = 'New York County encompasses New York City, NY and is considered a world financial center. The area is heavily populated by foreign banks operating in the US and other types of international systems and conduits (trade/logistics/ports). As such money laundering and other types of financial crimes (fraud and corruption) are prevalent in this area.'; 
                            }

                            $('.data-selection').hide();
                            var cardHtml = card105tmpl.render(cData);
                            $("#county-card").html(cardHtml);
			   				            $('#select-a-card').hide();

                            $('.data-selection').show();
                            $('#content').addClass('show');
           }
        }
    });


    var countiesMap = Highcharts.geojson(Highcharts.maps['countries/us/us-all-all-highres']);
        /*
        // Extract the line paths from the GeoJSON
        lines = Highcharts.geojson(Highcharts.maps['countries/us/us-all-all'], 'mapline'),
        // Filter out the state borders and separator lines, we want these in separate series
        borderLines = Highcharts.grep(lines, function (l) {
            return l.properties['hc-group'] === '__border_lines__';
        }),
        separatorLines = Highcharts.grep(lines, function (l) {
            return l.properties['hc-group'] === '__separator_lines__';
        });
        */
        // Add state acronym for tooltip
        Highcharts.each(countiesMap, function (mapPoint) {
            mapPoint.name = mapPoint.name + ', ' + mapPoint.properties['hc-key'].substr(3, 2).toUpperCase();
        });
        /*
        var cData = [];
        var geoCData = Highcharts.geojson(Highcharts.maps['countries/us/us-all-all-highres']);
        for (var i = 0; i < geoCData.length; i++) {
          cData.push([geoCData[i].properties["hc-key"], Math.floor(Math.random() * Math.floor(4))])
        };
        console_data = cData;
        */
var cData = [{"hc-key":"us-ca-083","value":3},{"hc-key":"us-ca-111","value":0},{"hc-key":"us-ca-037","value":0},{"hc-key":"us-ri-009","value":2},{"hc-key":"us-fl-087","value":3},{"hc-key":"us-wa-069","value":1},{"hc-key":"us-mi-089","value":0},{"hc-key":"us-mi-003","value":3},{"hc-key":"us-ma-001","value":2},{"hc-key":"us-fl-086","value":2},{"hc-key":"us-fl-017","value":3},{"hc-key":"us-ny-103","value":0},{"hc-key":"us-mi-033","value":1},{"hc-key":"us-al-097","value":3},{"hc-key":"us-co-014","value":0},{"hc-key":"us-nc-031","value":3},{"hc-key":"us-tx-007","value":1},{"hc-key":"us-wi-003","value":1},{"hc-key":"us-la-087","value":3},{"hc-key":"us-wa-031","value":2},{"hc-key":"us-md-019","value":0},{"hc-key":"us-ma-007","value":0},{"hc-key":"us-ma-019","value":1},{"hc-key":"us-ny-045","value":2},{"hc-key":"us-fl-037","value":1},{"hc-key":"us-or-007","value":3},{"hc-key":"us-co-031","value":2},{"hc-key":"us-co-005","value":1},{"hc-key":"us-tx-167","value":0},{"hc-key":"us-ga-039","value":2},{"hc-key":"us-wi-007","value":1},{"hc-key":"us-ny-085","value":1},{"hc-key":"us-la-075","value":0},{"hc-key":"us-me-027","value":1},{"hc-key":"us-fl-071","value":3},{"hc-key":"us-wa-073","value":3},{"hc-key":"us-nj-029","value":0},{"hc-key":"us-ga-179","value":2},{"hc-key":"us-fl-075","value":2},{"hc-key":"us-nc-095","value":2},{"hc-key":"us-sc-019","value":1},{"hc-key":"us-md-039","value":1},{"hc-key":"us-la-051","value":1},{"hc-key":"us-va-001","value":1},{"hc-key":"us-nc-129","value":0},{"hc-key":"us-wa-033","value":3},{"hc-key":"us-wa-049","value":2},{"hc-key":"us-ms-047","value":3},{"hc-key":"us-fl-021","value":2},{"hc-key":"us-ms-059","value":0},{"hc-key":"us-wa-055","value":2},{"hc-key":"us-la-109","value":3},{"hc-key":"us-me-015","value":1},{"hc-key":"us-tx-321","value":2},{"hc-key":"us-mi-097","value":1},{"hc-key":"us-la-057","value":1},{"hc-key":"us-fl-009","value":0},{"hc-key":"us-me-009","value":0},{"hc-key":"us-me-029","value":1},{"hc-key":"us-me-013","value":3},{"hc-key":"us-mi-029","value":0},{"hc-key":"us-ri-005","value":2},{"hc-key":"us-wa-045","value":3},{"hc-key":"us-wa-057","value":1},{"hc-key":"us-nc-019","value":3},{"hc-key":"us-va-131","value":2},{"hc-key":"us-ga-191","value":3},{"hc-key":"us-wa-053","value":3},{"hc-key":"us-nc-055","value":3},{"hc-key":"us-nj-001","value":1},{"hc-key":"us-wi-029","value":0},{"hc-key":"us-va-600","value":3},{"hc-key":"us-va-059","value":3},{"hc-key":"us-tx-057","value":1},{"hc-key":"us-nm-003","value":2},{"hc-key":"us-nm-017","value":1},{"hc-key":"us-nd-019","value":0},{"hc-key":"us-ut-003","value":1},{"hc-key":"us-nv-007","value":1},{"hc-key":"us-or-037","value":2},{"hc-key":"us-ca-049","value":3},{"hc-key":"us-sd-121","value":2},{"hc-key":"us-sd-095","value":2},{"hc-key":"us-wa-047","value":1},{"hc-key":"us-tx-371","value":2},{"hc-key":"us-tx-043","value":2},{"hc-key":"us-mi-141","value":3},{"hc-key":"us-mt-005","value":1},{"hc-key":"us-mt-041","value":0},{"hc-key":"us-mn-075","value":3},{"hc-key":"us-nm-031","value":0},{"hc-key":"us-az-001","value":0},{"hc-key":"us-tx-243","value":3},{"hc-key":"us-tx-377","value":3},{"hc-key":"us-tn-043","value":1},{"hc-key":"us-mt-019","value":2},{"hc-key":"us-va-750","value":2},{"hc-key":"us-tn-105","value":2},{"hc-key":"us-mt-013","value":3},{"hc-key":"us-mt-015","value":1},{"hc-key":"us-va-580","value":1},{"hc-key":"us-nd-023","value":0},{"hc-key":"us-co-097","value":0},{"hc-key":"us-co-045","value":2},{"hc-key":"us-va-690","value":2},{"hc-key":"us-la-113","value":3},{"hc-key":"us-mn-135","value":3},{"hc-key":"us-nh-007","value":0},{"hc-key":"us-tx-261","value":1},{"hc-key":"us-wy-001","value":0},{"hc-key":"us-wy-007","value":0},{"hc-key":"us-ca-027","value":2},{"hc-key":"us-ca-071","value":1},{"hc-key":"us-ny-065","value":3},{"hc-key":"us-ny-049","value":2},{"hc-key":"us-va-515","value":3},{"hc-key":"us-in-033","value":3},{"hc-key":"us-in-151","value":1},{"hc-key":"us-va-153","value":3},{"hc-key":"us-va-179","value":1},{"hc-key":"us-ga-097","value":3},{"hc-key":"us-ga-067","value":3},{"hc-key":"us-mt-053","value":0},{"hc-key":"us-id-021","value":3},{"hc-key":"us-ga-145","value":3},{"hc-key":"us-ga-285","value":0},{"hc-key":"us-wi-107","value":0},{"hc-key":"us-wi-119","value":2},{"hc-key":"us-tx-373","value":1},{"hc-key":"us-va-820","value":0},{"hc-key":"us-va-790","value":3},{"hc-key":"us-va-540","value":2},{"hc-key":"us-mt-101","value":1},{"hc-key":"us-va-595","value":3},{"hc-key":"us-tn-037","value":1},{"hc-key":"us-oh-065","value":0},{"hc-key":"us-oh-091","value":1},{"hc-key":"us-va-678","value":0},{"hc-key":"us-va-530","value":0},{"hc-key":"us-va-720","value":2},{"hc-key":"us-id-073","value":2},{"hc-key":"us-or-045","value":1},{"hc-key":"us-ny-057","value":1},{"hc-key":"us-ny-035","value":2},{"hc-key":"us-nd-075","value":3},{"hc-key":"us-nd-009","value":1},{"hc-key":"us-ut-025","value":3},{"hc-key":"us-az-005","value":0},{"hc-key":"us-co-011","value":3},{"hc-key":"us-co-061","value":2},{"hc-key":"us-me-007","value":3},{"hc-key":"us-va-660","value":2},{"hc-key":"us-va-683","value":3},{"hc-key":"us-va-840","value":1},{"hc-key":"us-mt-105","value":0},{"hc-key":"us-az-027","value":3},{"hc-key":"us-az-019","value":3},{"hc-key":"us-az-013","value":2},{"hc-key":"us-nv-033","value":3},{"hc-key":"us-nv-011","value":0},{"hc-key":"us-ca-075","value":1},{"hc-key":"us-ca-041","value":2},{"hc-key":"us-fl-053","value":1},{"hc-key":"us-mi-019","value":0},{"hc-key":"us-mi-163","value":1},{"hc-key":"us-ny-059","value":1},{"hc-key":"us-co-069","value":2},{"hc-key":"us-co-013","value":1},{"hc-key":"us-co-059","value":2},{"hc-key":"us-nc-049","value":2},{"hc-key":"us-wi-113","value":3},{"hc-key":"us-md-011","value":1},{"hc-key":"us-md-041","value":0},{"hc-key":"us-md-045","value":1},{"hc-key":"us-co-001","value":0},{"hc-key":"us-co-035","value":1},{"hc-key":"us-co-093","value":3},{"hc-key":"us-tx-071","value":0},{"hc-key":"us-tx-201","value":2},{"hc-key":"us-me-031","value":1},{"hc-key":"us-co-075","value":2},{"hc-key":"us-co-123","value":2},{"hc-key":"us-tx-061","value":0},{"hc-key":"us-tx-215","value":0},{"hc-key":"us-md-047","value":0},{"hc-key":"us-fl-051","value":0},{"hc-key":"us-la-101","value":2},{"hc-key":"us-ny-061","value":2},{"hc-key":"us-ny-005","value":3},{"hc-key":"us-ny-047","value":0},{"hc-key":"us-wa-035","value":3},{"hc-key":"us-wa-067","value":1},{"hc-key":"us-ga-127","value":2},{"hc-key":"us-nj-009","value":1},{"hc-key":"us-md-033","value":1},{"hc-key":"us-tx-391","value":3},{"hc-key":"us-az-017","value":0},{"hc-key":"us-wy-031","value":0},{"hc-key":"us-wy-021","value":3},{"hc-key":"us-tn-021","value":0},{"hc-key":"us-tn-147","value":2},{"hc-key":"us-tn-149","value":0},{"hc-key":"us-ne-031","value":3},{"hc-key":"us-nv-023","value":3},{"hc-key":"us-nv-021","value":2},{"hc-key":"us-nv-017","value":2},{"hc-key":"us-nv-003","value":3},{"hc-key":"us-ut-027","value":3},{"hc-key":"us-fl-029","value":0},{"hc-key":"us-fl-067","value":2},{"hc-key":"us-fl-121","value":1},{"hc-key":"us-ut-023","value":3},{"hc-key":"us-wa-017","value":0},{"hc-key":"us-oh-003","value":3},{"hc-key":"us-oh-137","value":1},{"hc-key":"us-in-135","value":1},{"hc-key":"us-in-065","value":1},{"hc-key":"us-in-177","value":3},{"hc-key":"us-tx-443","value":2},{"hc-key":"us-tx-105","value":2},{"hc-key":"us-tn-125","value":0},{"hc-key":"us-va-685","value":0},{"hc-key":"us-in-105","value":3},{"hc-key":"us-in-093","value":1},{"hc-key":"us-in-071","value":1},{"hc-key":"us-in-101","value":0},{"hc-key":"us-co-009","value":1},{"hc-key":"us-co-071","value":1},{"hc-key":"us-nc-137","value":1},{"hc-key":"us-nc-013","value":0},{"hc-key":"us-mn-031","value":3},{"hc-key":"us-nm-043","value":0},{"hc-key":"us-ut-037","value":1},{"hc-key":"us-pa-101","value":1},{"hc-key":"us-nj-007","value":0},{"hc-key":"us-nc-103","value":3},{"hc-key":"us-md-035","value":3},{"hc-key":"us-wi-117","value":3},{"hc-key":"us-or-041","value":3},{"hc-key":"us-or-057","value":2},{"hc-key":"us-ne-105","value":1},{"hc-key":"us-co-125","value":3},{"hc-key":"us-tx-389","value":0},{"hc-key":"us-nc-187","value":0},{"hc-key":"us-sc-015","value":3},{"hc-key":"us-la-095","value":2},{"hc-key":"us-ma-005","value":0},{"hc-key":"us-ri-001","value":0},{"hc-key":"us-ma-023","value":1},{"hc-key":"us-wa-041","value":2},{"hc-key":"us-wa-027","value":0},{"hc-key":"us-mt-091","value":3},{"hc-key":"us-md-003","value":1},{"hc-key":"us-al-071","value":3},{"hc-key":"us-al-095","value":1},{"hc-key":"us-tn-051","value":2},{"hc-key":"us-tn-031","value":1},{"hc-key":"us-tn-145","value":3},{"hc-key":"us-ga-293","value":3},{"hc-key":"us-ga-231","value":3},{"hc-key":"us-va-191","value":0},{"hc-key":"us-va-520","value":3},{"hc-key":"us-ga-249","value":2},{"hc-key":"us-ga-269","value":1},{"hc-key":"us-ga-079","value":0},{"hc-key":"us-ga-197","value":0},{"hc-key":"us-va-095","value":2},{"hc-key":"us-va-830","value":1},{"hc-key":"us-ga-273","value":0},{"hc-key":"us-ga-037","value":2},{"hc-key":"us-ga-177","value":1},{"hc-key":"us-oh-063","value":0},{"hc-key":"us-oh-173","value":2},{"hc-key":"us-oh-175","value":0},{"hc-key":"us-ut-033","value":2},{"hc-key":"us-wy-023","value":2},{"hc-key":"us-wy-037","value":3},{"hc-key":"us-id-007","value":0},{"hc-key":"us-wy-039","value":0},{"hc-key":"us-la-023","value":1},{"hc-key":"us-la-019","value":1},{"hc-key":"us-tx-361","value":3},{"hc-key":"us-mi-007","value":2},{"hc-key":"us-tn-137","value":1},{"hc-key":"us-ky-053","value":0},{"hc-key":"us-ky-231","value":0},{"hc-key":"us-fl-129","value":2},{"hc-key":"us-fl-131","value":1},{"hc-key":"us-fl-059","value":3},{"hc-key":"us-fl-133","value":2},{"hc-key":"us-ne-059","value":3},{"hc-key":"us-ne-035","value":2},{"hc-key":"us-ne-181","value":0},{"hc-key":"us-ne-129","value":2},{"hc-key":"us-ks-157","value":1},{"hc-key":"us-ne-001","value":2},{"hc-key":"us-mi-109","value":1},{"hc-key":"us-mi-103","value":3},{"hc-key":"us-mi-043","value":0},{"hc-key":"us-mi-013","value":0},{"hc-key":"us-wy-041","value":2},{"hc-key":"us-sd-067","value":1},{"hc-key":"us-sd-043","value":2},{"hc-key":"us-in-069","value":1},{"hc-key":"us-in-003","value":0},{"hc-key":"us-ga-025","value":2},{"hc-key":"us-ga-049","value":1},{"hc-key":"us-ga-015","value":2},{"hc-key":"us-ga-129","value":3},{"hc-key":"us-ga-115","value":3},{"hc-key":"us-ga-233","value":2},{"hc-key":"us-ne-089","value":1},{"hc-key":"us-ne-015","value":3},{"hc-key":"us-ne-103","value":1},{"hc-key":"us-mo-083","value":3},{"hc-key":"us-mo-101","value":1},{"hc-key":"us-mo-107","value":1},{"hc-key":"us-wi-093","value":0},{"hc-key":"us-wi-033","value":3},{"hc-key":"us-co-109","value":3},{"hc-key":"us-co-003","value":2},{"hc-key":"us-co-027","value":0},{"hc-key":"us-wi-063","value":3},{"hc-key":"us-wi-053","value":1},{"hc-key":"us-ny-101","value":3},{"hc-key":"us-ny-003","value":0},{"hc-key":"us-ny-121","value":1},{"hc-key":"us-ny-051","value":0},{"hc-key":"us-ga-063","value":2},{"hc-key":"us-ga-089","value":2},{"hc-key":"us-ks-111","value":1},{"hc-key":"us-ks-073","value":2},{"hc-key":"us-ks-031","value":1},{"hc-key":"us-ks-207","value":2},{"hc-key":"us-in-111","value":3},{"hc-key":"us-il-091","value":2},{"hc-key":"us-in-073","value":1},{"hc-key":"us-in-149","value":3},{"hc-key":"us-in-131","value":0},{"hc-key":"us-in-181","value":0},{"hc-key":"us-in-007","value":0},{"hc-key":"us-tx-245","value":0},{"hc-key":"us-al-049","value":2},{"hc-key":"us-al-019","value":2},{"hc-key":"us-mt-065","value":2},{"hc-key":"us-mt-087","value":1},{"hc-key":"us-co-017","value":0},{"hc-key":"us-ks-199","value":0},{"hc-key":"us-co-067","value":3},{"hc-key":"us-co-111","value":1},{"hc-key":"us-co-113","value":2},{"hc-key":"us-ut-057","value":1},{"hc-key":"us-ut-005","value":1},{"hc-key":"us-id-041","value":0},{"hc-key":"us-tx-327","value":2},{"hc-key":"us-tx-413","value":0},{"hc-key":"us-me-023","value":3},{"hc-key":"us-me-001","value":0},{"hc-key":"us-me-011","value":3},{"hc-key":"us-mt-059","value":1},{"hc-key":"us-mt-097","value":1},{"hc-key":"us-ok-095","value":3},{"hc-key":"us-tx-181","value":2},{"hc-key":"us-wv-103","value":2},{"hc-key":"us-pa-059","value":0},{"hc-key":"us-pa-051","value":2},{"hc-key":"us-al-091","value":2},{"hc-key":"us-al-065","value":1},{"hc-key":"us-al-105","value":1},{"hc-key":"us-ky-209","value":3},{"hc-key":"us-ky-017","value":0},{"hc-key":"us-il-113","value":1},{"hc-key":"us-il-147","value":1},{"hc-key":"us-il-203","value":2},{"hc-key":"us-il-019","value":2},{"hc-key":"us-il-053","value":3},{"hc-key":"us-il-139","value":2},{"hc-key":"us-al-063","value":0},{"hc-key":"us-id-051","value":2},{"hc-key":"us-id-033","value":1},{"hc-key":"us-nc-037","value":1},{"hc-key":"us-nc-063","value":2},{"hc-key":"us-ia-151","value":3},{"hc-key":"us-ia-187","value":2},{"hc-key":"us-ia-025","value":3},{"hc-key":"us-ia-027","value":0},{"hc-key":"us-tx-147","value":3},{"hc-key":"us-tx-119","value":0},{"hc-key":"us-tn-159","value":0},{"hc-key":"us-tn-169","value":0},{"hc-key":"us-or-029","value":1},{"hc-key":"us-or-035","value":0},{"hc-key":"us-tx-127","value":3},{"hc-key":"us-tx-507","value":1},{"hc-key":"us-fl-011","value":2},{"hc-key":"us-mi-151","value":2},{"hc-key":"us-nc-145","value":1},{"hc-key":"us-ms-151","value":1},{"hc-key":"us-ms-055","value":0},{"hc-key":"us-ms-125","value":3},{"hc-key":"us-ar-085","value":0},{"hc-key":"us-ar-001","value":3},{"hc-key":"us-tx-027","value":0},{"hc-key":"us-tx-309","value":2},{"hc-key":"us-fl-099","value":2},{"hc-key":"us-ga-073","value":0},{"hc-key":"us-ga-181","value":0},{"hc-key":"us-ga-189","value":1},{"hc-key":"us-ky-003","value":1},{"hc-key":"us-tn-165","value":1},{"hc-key":"us-or-005","value":3},{"hc-key":"us-or-051","value":2},{"hc-key":"us-tn-115","value":1},{"hc-key":"us-tn-061","value":0},{"hc-key":"us-in-179","value":3},{"hc-key":"us-in-075","value":2},{"hc-key":"us-in-009","value":3},{"hc-key":"us-va-121","value":3},{"hc-key":"us-va-063","value":3},{"hc-key":"us-ms-145","value":2},{"hc-key":"us-ms-009","value":0},{"hc-key":"us-ia-109","value":0},{"hc-key":"us-ia-147","value":2},{"hc-key":"us-ia-063","value":0},{"hc-key":"us-ia-053","value":2},{"hc-key":"us-ia-159","value":1},{"hc-key":"us-nc-001","value":3},{"hc-key":"us-nc-151","value":3},{"hc-key":"us-in-171","value":1},{"hc-key":"us-ne-109","value":1},{"hc-key":"us-ne-151","value":3},{"hc-key":"us-ia-031","value":1},{"hc-key":"us-ia-045","value":2},{"hc-key":"us-va-073","value":2},{"hc-key":"us-va-097","value":0},{"hc-key":"us-va-119","value":3},{"hc-key":"us-pa-123","value":0},{"hc-key":"us-pa-053","value":2},{"hc-key":"us-pa-065","value":1},{"hc-key":"us-in-089","value":2},{"hc-key":"us-tn-027","value":0},{"hc-key":"us-wa-013","value":3},{"hc-key":"us-or-063","value":0},{"hc-key":"us-or-001","value":1},{"hc-key":"us-ky-127","value":3},{"hc-key":"us-ky-175","value":1},{"hc-key":"us-il-025","value":3},{"hc-key":"us-il-049","value":0},{"hc-key":"us-il-051","value":2},{"hc-key":"us-il-035","value":3},{"hc-key":"us-tn-123","value":3},{"hc-key":"us-in-157","value":0},{"hc-key":"us-in-107","value":3},{"hc-key":"us-in-063","value":3},{"hc-key":"us-tx-277","value":3},{"hc-key":"us-nd-035","value":1},{"hc-key":"us-nd-099","value":1},{"hc-key":"us-nd-067","value":0},{"hc-key":"us-ks-071","value":0},{"hc-key":"us-co-099","value":2},{"hc-key":"us-ks-075","value":0},{"hc-key":"us-mo-057","value":0},{"hc-key":"us-mo-167","value":2},{"hc-key":"us-mo-109","value":0},{"hc-key":"us-il-029","value":1},{"hc-key":"us-il-033","value":2},{"hc-key":"us-il-079","value":3},{"hc-key":"us-ia-073","value":3},{"hc-key":"us-pa-031","value":3},{"hc-key":"us-in-117","value":3},{"hc-key":"us-in-037","value":3},{"hc-key":"us-in-027","value":2},{"hc-key":"us-in-083","value":0},{"hc-key":"us-in-123","value":3},{"hc-key":"us-in-025","value":1},{"hc-key":"us-in-173","value":3},{"hc-key":"us-in-147","value":3},{"hc-key":"us-in-163","value":2},{"hc-key":"us-va-057","value":1},{"hc-key":"us-mn-043","value":2},{"hc-key":"us-mn-147","value":0},{"hc-key":"us-mn-047","value":0},{"hc-key":"us-ia-195","value":3},{"hc-key":"us-va-167","value":2},{"hc-key":"us-va-027","value":2},{"hc-key":"us-co-089","value":1},{"hc-key":"us-ok-135","value":1},{"hc-key":"us-ok-079","value":3},{"hc-key":"us-ok-101","value":1},{"hc-key":"us-mo-043","value":3},{"hc-key":"us-mo-067","value":2},{"hc-key":"us-mo-215","value":1},{"hc-key":"us-in-115","value":0},{"hc-key":"us-in-137","value":3},{"hc-key":"us-in-031","value":0},{"hc-key":"us-in-139","value":2},{"hc-key":"us-la-021","value":3},{"hc-key":"us-la-049","value":3},{"hc-key":"us-mt-073","value":2},{"hc-key":"us-ct-007","value":2},{"hc-key":"us-ct-011","value":2},{"hc-key":"us-in-161","value":0},{"hc-key":"us-oh-017","value":1},{"hc-key":"us-oh-165","value":1},{"hc-key":"us-mo-145","value":2},{"hc-key":"us-mo-119","value":2},{"hc-key":"us-ne-033","value":2},{"hc-key":"us-ne-007","value":3},{"hc-key":"us-ne-157","value":1},{"hc-key":"us-wy-015","value":1},{"hc-key":"us-ne-165","value":3},{"hc-key":"us-ga-263","value":1},{"hc-key":"us-al-081","value":0},{"hc-key":"us-nd-081","value":1},{"hc-key":"us-nd-021","value":1},{"hc-key":"us-mn-085","value":2},{"hc-key":"us-mn-143","value":3},{"hc-key":"us-ga-171","value":3},{"hc-key":"us-ca-097","value":1},{"hc-key":"us-id-065","value":1},{"hc-key":"us-id-019","value":1},{"hc-key":"us-ga-297","value":1},{"hc-key":"us-ga-013","value":0},{"hc-key":"us-ok-115","value":0},{"hc-key":"us-ok-035","value":3},{"hc-key":"us-ks-021","value":1},{"hc-key":"us-pa-033","value":3},{"hc-key":"us-il-167","value":2},{"hc-key":"us-il-135","value":0},{"hc-key":"us-il-005","value":1},{"hc-key":"us-ms-071","value":1},{"hc-key":"us-ms-115","value":3},{"hc-key":"us-mn-055","value":0},{"hc-key":"us-mn-045","value":0},{"hc-key":"us-ia-191","value":0},{"hc-key":"us-mn-099","value":3},{"hc-key":"us-mn-109","value":3},{"hc-key":"us-mn-039","value":2},{"hc-key":"us-sd-097","value":2},{"hc-key":"us-sd-087","value":0},{"hc-key":"us-sd-099","value":0},{"hc-key":"us-sd-101","value":3},{"hc-key":"us-al-017","value":1},{"hc-key":"us-in-047","value":3},{"hc-key":"us-mn-121","value":0},{"hc-key":"us-mn-067","value":3},{"hc-key":"us-tx-177","value":3},{"hc-key":"us-tx-149","value":0},{"hc-key":"us-tx-011","value":1},{"hc-key":"us-tx-045","value":2},{"hc-key":"us-mi-071","value":2},{"hc-key":"us-mi-131","value":0},{"hc-key":"us-mn-133","value":1},{"hc-key":"us-mn-105","value":1},{"hc-key":"us-ia-119","value":2},{"hc-key":"us-la-111","value":0},{"hc-key":"us-la-061","value":2},{"hc-key":"us-la-027","value":0},{"hc-key":"us-pa-043","value":3},{"hc-key":"us-pa-097","value":1},{"hc-key":"us-pa-067","value":2},{"hc-key":"us-pa-099","value":3},{"hc-key":"us-pa-071","value":1},{"hc-key":"us-ok-027","value":1},{"hc-key":"us-ok-017","value":0},{"hc-key":"us-ok-015","value":2},{"hc-key":"us-wv-079","value":1},{"hc-key":"us-wv-011","value":3},{"hc-key":"us-ar-137","value":1},{"hc-key":"us-ar-141","value":0},{"hc-key":"us-co-119","value":3},{"hc-key":"us-co-065","value":2},{"hc-key":"us-co-117","value":0},{"hc-key":"us-ny-089","value":3},{"hc-key":"us-ny-041","value":0},{"hc-key":"us-ny-091","value":0},{"hc-key":"us-va-005","value":3},{"hc-key":"us-va-045","value":3},{"hc-key":"us-va-023","value":0},{"hc-key":"us-va-161","value":1},{"hc-key":"us-mo-081","value":0},{"hc-key":"us-ga-045","value":1},{"hc-key":"us-ga-223","value":0},{"hc-key":"us-tn-041","value":0},{"hc-key":"us-tn-141","value":3},{"hc-key":"us-nd-013","value":0},{"hc-key":"us-nd-101","value":0},{"hc-key":"us-ma-027","value":3},{"hc-key":"us-nh-011","value":1},{"hc-key":"us-nh-005","value":0},{"hc-key":"us-ny-111","value":3},{"hc-key":"us-ny-027","value":1},{"hc-key":"us-tx-225","value":2},{"hc-key":"us-tx-313","value":2},{"hc-key":"us-tx-041","value":1},{"hc-key":"us-tx-477","value":0},{"hc-key":"us-co-101","value":0},{"hc-key":"us-co-041","value":0},{"hc-key":"us-mo-105","value":2},{"hc-key":"us-sc-033","value":0},{"hc-key":"us-nc-155","value":1},{"hc-key":"us-nc-051","value":3},{"hc-key":"us-mi-001","value":2},{"hc-key":"us-ca-067","value":3},{"hc-key":"us-ca-017","value":3},{"hc-key":"us-ca-061","value":2},{"hc-key":"us-ca-115","value":3},{"hc-key":"us-ar-129","value":3},{"hc-key":"us-nc-093","value":0},{"hc-key":"us-nc-165","value":0},{"hc-key":"us-mo-005","value":0},{"hc-key":"us-ia-071","value":2},{"hc-key":"us-mo-087","value":1},{"hc-key":"us-ne-131","value":0},{"hc-key":"us-pa-023","value":3},{"hc-key":"us-sd-073","value":2},{"hc-key":"us-sd-015","value":2},{"hc-key":"us-sd-023","value":2},{"hc-key":"us-tn-177","value":3},{"hc-key":"us-tn-185","value":3},{"hc-key":"us-il-023","value":3},{"hc-key":"us-sc-035","value":0},{"hc-key":"us-ks-139","value":1},{"hc-key":"us-oh-133","value":0},{"hc-key":"us-oh-155","value":3},{"hc-key":"us-oh-007","value":2},{"hc-key":"us-pa-019","value":3},{"hc-key":"us-pa-005","value":2},{"hc-key":"us-pa-073","value":2},{"hc-key":"us-pa-007","value":0},{"hc-key":"us-ks-113","value":1},{"hc-key":"us-ks-115","value":1},{"hc-key":"us-mo-225","value":0},{"hc-key":"us-mn-033","value":3},{"hc-key":"us-al-057","value":0},{"hc-key":"us-al-125","value":0},{"hc-key":"us-al-099","value":2},{"hc-key":"us-al-013","value":0},{"hc-key":"us-al-035","value":1},{"hc-key":"us-al-131","value":2},{"hc-key":"us-ms-015","value":3},{"hc-key":"us-ms-097","value":3},{"hc-key":"us-ne-099","value":2},{"hc-key":"us-id-081","value":2},{"hc-key":"us-il-161","value":1},{"hc-key":"us-ar-113","value":2},{"hc-key":"us-ar-133","value":2},{"hc-key":"us-wy-043","value":3},{"hc-key":"us-wy-013","value":3},{"hc-key":"us-ny-097","value":0},{"hc-key":"us-ny-015","value":0},{"hc-key":"us-ar-069","value":3},{"hc-key":"us-wi-123","value":2},{"hc-key":"us-wi-111","value":1},{"hc-key":"us-wi-049","value":2},{"hc-key":"us-wi-025","value":3},{"hc-key":"us-wi-043","value":3},{"hc-key":"us-mo-123","value":0},{"hc-key":"us-mo-093","value":2},{"hc-key":"us-tx-289","value":0},{"hc-key":"us-tx-005","value":2},{"hc-key":"us-ks-195","value":0},{"hc-key":"us-ks-051","value":0},{"hc-key":"us-ks-165","value":3},{"hc-key":"us-ks-187","value":0},{"hc-key":"us-tx-463","value":0},{"hc-key":"us-tx-325","value":3},{"hc-key":"us-tx-019","value":1},{"hc-key":"us-tx-029","value":2},{"hc-key":"us-tx-013","value":3},{"hc-key":"us-mn-063","value":0},{"hc-key":"us-ia-059","value":3},{"hc-key":"us-mn-165","value":2},{"hc-key":"us-ok-063","value":0},{"hc-key":"us-ok-029","value":3},{"hc-key":"us-tx-103","value":3},{"hc-key":"us-tx-461","value":1},{"hc-key":"us-in-051","value":3},{"hc-key":"us-in-129","value":0},{"hc-key":"us-va-510","value":2},{"hc-key":"us-dc-001","value":1},{"hc-key":"us-va-013","value":3},{"hc-key":"us-mn-171","value":0},{"hc-key":"us-co-037","value":1},{"hc-key":"us-co-085","value":2},{"hc-key":"us-fl-109","value":0},{"hc-key":"us-ms-019","value":3},{"hc-key":"us-mo-079","value":2},{"hc-key":"us-mo-117","value":0},{"hc-key":"us-mi-123","value":2},{"hc-key":"us-mi-085","value":3},{"hc-key":"us-mi-133","value":2},{"hc-key":"us-il-031","value":2},{"hc-key":"us-il-059","value":0},{"hc-key":"us-al-033","value":0},{"hc-key":"us-ms-141","value":3},{"hc-key":"us-ms-003","value":3},{"hc-key":"us-tn-071","value":2},{"hc-key":"us-ms-139","value":0},{"hc-key":"us-wv-051","value":1},{"hc-key":"us-wv-035","value":2},{"hc-key":"us-id-037","value":3},{"hc-key":"us-id-039","value":0},{"hc-key":"us-id-013","value":0},{"hc-key":"us-oh-131","value":3},{"hc-key":"us-oh-145","value":1},{"hc-key":"us-oh-087","value":0},{"hc-key":"us-oh-001","value":0},{"hc-key":"us-oh-015","value":1},{"hc-key":"us-fl-055","value":0},{"hc-key":"us-fl-027","value":0},{"hc-key":"us-mo-209","value":3},{"hc-key":"us-ar-015","value":0},{"hc-key":"us-va-089","value":0},{"hc-key":"us-nc-169","value":2},{"hc-key":"us-nc-157","value":3},{"hc-key":"us-ia-121","value":2},{"hc-key":"us-ia-039","value":0},{"hc-key":"us-tx-203","value":3},{"hc-key":"us-tx-401","value":1},{"hc-key":"us-ia-181","value":0},{"hc-key":"us-oh-161","value":3},{"hc-key":"us-oh-125","value":1},{"hc-key":"us-fl-043","value":2},{"hc-key":"us-ga-199","value":2},{"hc-key":"us-ga-077","value":0},{"hc-key":"us-co-007","value":1},{"hc-key":"us-co-105","value":2},{"hc-key":"us-co-023","value":0},{"hc-key":"us-tx-099","value":0},{"hc-key":"us-mo-019","value":1},{"hc-key":"us-mo-089","value":2},{"hc-key":"us-mi-139","value":0},{"hc-key":"us-mi-009","value":1},{"hc-key":"us-nc-153","value":1},{"hc-key":"us-sc-069","value":2},{"hc-key":"us-nc-007","value":3},{"hc-key":"us-id-055","value":0},{"hc-key":"us-id-009","value":3},{"hc-key":"us-il-011","value":1},{"hc-key":"us-il-073","value":1},{"hc-key":"us-il-037","value":1},{"hc-key":"us-il-103","value":3},{"hc-key":"us-ne-127","value":3},{"hc-key":"us-mi-035","value":2},{"hc-key":"us-mi-143","value":3},{"hc-key":"us-ca-091","value":0},{"hc-key":"us-ca-035","value":0},{"hc-key":"us-fl-081","value":1},{"hc-key":"us-co-015","value":3},{"hc-key":"us-co-043","value":3},{"hc-key":"us-ia-089","value":3},{"hc-key":"us-ia-037","value":2},{"hc-key":"us-ia-017","value":3},{"hc-key":"us-ia-065","value":2},{"hc-key":"us-ia-043","value":0},{"hc-key":"us-il-093","value":0},{"hc-key":"us-nd-089","value":2},{"hc-key":"us-nd-057","value":1},{"hc-key":"us-mi-095","value":0},{"hc-key":"us-mi-153","value":0},{"hc-key":"us-ok-019","value":2},{"hc-key":"us-ok-099","value":1},{"hc-key":"us-nm-055","value":2},{"hc-key":"us-ms-011","value":1},{"hc-key":"us-ar-017","value":2},{"hc-key":"us-wv-027","value":3},{"hc-key":"us-md-001","value":3},{"hc-key":"us-wv-065","value":3},{"hc-key":"us-pa-057","value":3},{"hc-key":"us-tx-077","value":3},{"hc-key":"us-tx-237","value":1},{"hc-key":"us-tx-367","value":0},{"hc-key":"us-id-049","value":1},{"hc-key":"us-id-069","value":2},{"hc-key":"us-mo-221","value":2},{"hc-key":"us-ny-109","value":3},{"hc-key":"us-ny-011","value":1},{"hc-key":"us-ny-075","value":1},{"hc-key":"us-ny-053","value":3},{"hc-key":"us-tx-363","value":1},{"hc-key":"us-tx-143","value":3},{"hc-key":"us-tx-133","value":2},{"hc-key":"us-tx-059","value":3},{"hc-key":"us-tx-417","value":0},{"hc-key":"us-tx-207","value":0},{"hc-key":"us-va-085","value":2},{"hc-key":"us-va-075","value":3},{"hc-key":"us-oh-033","value":0},{"hc-key":"us-oh-139","value":1},{"hc-key":"us-il-067","value":3},{"hc-key":"us-il-001","value":0},{"hc-key":"us-il-009","value":3},{"hc-key":"us-il-075","value":2},{"hc-key":"us-ga-113","value":3},{"hc-key":"us-ga-265","value":1},{"hc-key":"us-ga-141","value":2},{"hc-key":"us-tx-091","value":3},{"hc-key":"us-tx-259","value":3},{"hc-key":"us-ny-001","value":1},{"hc-key":"us-ny-039","value":2},{"hc-key":"us-ok-089","value":3},{"hc-key":"us-ga-019","value":1},{"hc-key":"us-ga-277","value":1},{"hc-key":"us-ga-155","value":2},{"hc-key":"us-ga-321","value":1},{"hc-key":"us-ga-287","value":3},{"hc-key":"us-ga-081","value":0},{"hc-key":"us-ga-075","value":3},{"hc-key":"us-sd-025","value":0},{"hc-key":"us-sd-077","value":1},{"hc-key":"us-sd-005","value":0},{"hc-key":"us-ny-043","value":0},{"hc-key":"us-mi-053","value":1},{"hc-key":"us-mn-027","value":2},{"hc-key":"us-mn-005","value":2},{"hc-key":"us-mn-107","value":2},{"hc-key":"us-mn-057","value":3},{"hc-key":"us-tn-013","value":1},{"hc-key":"us-tn-173","value":1},{"hc-key":"us-tn-057","value":3},{"hc-key":"us-nd-015","value":0},{"hc-key":"us-nd-029","value":0},{"hc-key":"us-nd-047","value":0},{"hc-key":"us-tx-039","value":3},{"hc-key":"us-tx-157","value":3},{"hc-key":"us-tx-481","value":1},{"hc-key":"us-tn-009","value":3},{"hc-key":"us-mt-031","value":2},{"hc-key":"us-ga-143","value":0},{"hc-key":"us-il-041","value":3},{"hc-key":"us-va-177","value":2},{"hc-key":"us-va-137","value":2},{"hc-key":"us-va-079","value":1},{"hc-key":"us-wi-027","value":1},{"hc-key":"us-wi-055","value":1},{"hc-key":"us-wi-047","value":2},{"hc-key":"us-wi-137","value":2},{"hc-key":"us-ar-049","value":3},{"hc-key":"us-ar-065","value":0},{"hc-key":"us-ar-063","value":1},{"hc-key":"us-ca-031","value":1},{"hc-key":"us-ca-053","value":0},{"hc-key":"us-ca-087","value":3},{"hc-key":"us-ca-069","value":3},{"hc-key":"us-mo-125","value":0},{"hc-key":"us-mo-131","value":1},{"hc-key":"us-nc-005","value":0},{"hc-key":"us-nc-193","value":3},{"hc-key":"us-nc-189","value":1},{"hc-key":"us-nc-027","value":1},{"hc-key":"us-ks-043","value":1},{"hc-key":"us-mo-021","value":1},{"hc-key":"us-mo-003","value":0},{"hc-key":"us-mo-165","value":2},{"hc-key":"us-mo-047","value":2},{"hc-key":"us-ks-005","value":3},{"hc-key":"us-ne-177","value":3},{"hc-key":"us-ia-085","value":2},{"hc-key":"us-ne-021","value":3},{"hc-key":"us-ia-165","value":0},{"hc-key":"us-ia-009","value":0},{"hc-key":"us-ia-023","value":2},{"hc-key":"us-ia-013","value":3},{"hc-key":"us-ia-011","value":3},{"hc-key":"us-ky-129","value":2},{"hc-key":"us-ky-189","value":1},{"hc-key":"us-ky-051","value":1},{"hc-key":"us-pa-015","value":3},{"hc-key":"us-pa-117","value":2},{"hc-key":"us-oh-029","value":3},{"hc-key":"us-wv-029","value":0},{"hc-key":"us-mo-051","value":1},{"hc-key":"us-mi-159","value":0},{"hc-key":"us-mi-027","value":0},{"hc-key":"us-mi-021","value":1},{"hc-key":"us-in-091","value":3},{"hc-key":"us-in-127","value":2},{"hc-key":"us-pa-061","value":0},{"hc-key":"us-va-141","value":2},{"hc-key":"us-mi-119","value":3},{"hc-key":"us-mi-135","value":3},{"hc-key":"us-nm-007","value":2},{"hc-key":"us-il-133","value":3},{"hc-key":"us-il-163","value":3},{"hc-key":"us-il-189","value":1},{"hc-key":"us-pa-009","value":3},{"hc-key":"us-pa-013","value":0},{"hc-key":"us-ar-083","value":1},{"hc-key":"us-ar-047","value":3},{"hc-key":"us-ar-127","value":1},{"hc-key":"us-nc-003","value":0},{"hc-key":"us-vt-025","value":3},{"hc-key":"us-nh-019","value":1},{"hc-key":"us-vt-027","value":1},{"hc-key":"us-nh-013","value":2},{"hc-key":"us-mn-089","value":1},{"hc-key":"us-mn-113","value":1},{"hc-key":"us-va-101","value":2},{"hc-key":"us-co-055","value":3},{"hc-key":"us-tx-255","value":0},{"hc-key":"us-tx-123","value":1},{"hc-key":"us-ut-029","value":0},{"hc-key":"us-ut-011","value":0},{"hc-key":"us-tx-397","value":1},{"hc-key":"us-tx-113","value":3},{"hc-key":"us-tx-257","value":1},{"hc-key":"us-tn-087","value":3},{"hc-key":"us-il-159","value":2},{"hc-key":"us-il-047","value":1},{"hc-key":"us-il-191","value":3},{"hc-key":"us-il-185","value":1},{"hc-key":"us-al-009","value":3},{"hc-key":"us-mt-051","value":2},{"hc-key":"us-tx-311","value":0},{"hc-key":"us-tx-131","value":1},{"hc-key":"us-tx-247","value":3},{"hc-key":"us-tx-047","value":2},{"hc-key":"us-mo-163","value":0},{"hc-key":"us-mo-173","value":3},{"hc-key":"us-tx-251","value":1},{"hc-key":"us-or-049","value":3},{"hc-key":"us-or-023","value":2},{"hc-key":"us-tx-159","value":0},{"hc-key":"us-ny-113","value":3},{"hc-key":"us-va-157","value":2},{"hc-key":"us-va-113","value":3},{"hc-key":"us-ga-057","value":3},{"hc-key":"us-fl-095","value":1},{"hc-key":"us-fl-069","value":2},{"hc-key":"us-ok-111","value":1},{"hc-key":"us-in-099","value":3},{"hc-key":"us-in-049","value":0},{"hc-key":"us-in-169","value":0},{"hc-key":"us-mn-169","value":1},{"hc-key":"us-la-035","value":2},{"hc-key":"us-ms-149","value":1},{"hc-key":"us-ms-021","value":1},{"hc-key":"us-la-107","value":0},{"hc-key":"us-mo-147","value":2},{"hc-key":"us-mo-139","value":0},{"hc-key":"us-ks-099","value":1},{"hc-key":"us-nc-131","value":1},{"hc-key":"us-nc-083","value":3},{"hc-key":"us-ky-193","value":0},{"hc-key":"us-pa-035","value":0},{"hc-key":"us-ok-131","value":1},{"hc-key":"us-ok-147","value":0},{"hc-key":"us-ok-113","value":2},{"hc-key":"us-wv-061","value":0},{"hc-key":"us-wv-077","value":2},{"hc-key":"us-wv-023","value":0},{"hc-key":"us-wv-071","value":2},{"hc-key":"us-wi-135","value":3},{"hc-key":"us-wi-115","value":0},{"hc-key":"us-wi-087","value":2},{"hc-key":"us-tx-435","value":2},{"hc-key":"us-tx-137","value":3},{"hc-key":"us-tx-265","value":3},{"hc-key":"us-tx-271","value":3},{"hc-key":"us-mo-153","value":1},{"hc-key":"us-tx-473","value":1},{"hc-key":"us-al-053","value":0},{"hc-key":"us-vt-009","value":3},{"hc-key":"us-nh-009","value":1},{"hc-key":"us-vt-017","value":3},{"hc-key":"us-ga-137","value":0},{"hc-key":"us-ga-139","value":0},{"hc-key":"us-ga-157","value":1},{"hc-key":"us-ga-135","value":0},{"hc-key":"us-ga-279","value":0},{"hc-key":"us-ga-283","value":3},{"hc-key":"us-ga-061","value":1},{"hc-key":"us-wi-125","value":3},{"hc-key":"us-wi-041","value":2},{"hc-key":"us-mn-007","value":1},{"hc-key":"us-mn-077","value":0},{"hc-key":"us-md-009","value":2},{"hc-key":"us-tx-323","value":1},{"hc-key":"us-nm-001","value":1},{"hc-key":"us-nm-006","value":1},{"hc-key":"us-nm-045","value":3},{"hc-key":"us-nm-049","value":2},{"hc-key":"us-nm-028","value":2},{"hc-key":"us-nm-057","value":1},{"hc-key":"us-nj-005","value":2},{"hc-key":"us-ga-099","value":3},{"hc-key":"us-nd-083","value":0},{"hc-key":"us-nd-049","value":2},{"hc-key":"us-nd-045","value":1},{"hc-key":"us-id-023","value":0},{"hc-key":"us-nd-043","value":0},{"hc-key":"us-nd-103","value":0},{"hc-key":"us-tx-273","value":3},{"hc-key":"us-tx-489","value":3},{"hc-key":"us-il-055","value":2},{"hc-key":"us-il-065","value":1},{"hc-key":"us-il-081","value":1},{"hc-key":"us-ar-051","value":1},{"hc-key":"us-ar-105","value":2},{"hc-key":"us-ar-029","value":3},{"hc-key":"us-ar-149","value":1},{"hc-key":"us-ks-181","value":2},{"hc-key":"us-ks-023","value":2},{"hc-key":"us-ks-153","value":0},{"hc-key":"us-ia-185","value":1},{"hc-key":"us-va-775","value":3},{"hc-key":"us-va-770","value":2},{"hc-key":"us-tx-165","value":1},{"hc-key":"us-tx-317","value":1},{"hc-key":"us-tx-037","value":3},{"hc-key":"us-tx-067","value":1},{"hc-key":"us-ar-091","value":2},{"hc-key":"us-ar-073","value":3},{"hc-key":"us-mo-127","value":0},{"hc-key":"us-mo-111","value":1},{"hc-key":"us-ia-061","value":2},{"hc-key":"us-ia-097","value":1},{"hc-key":"us-nd-005","value":3},{"hc-key":"us-nd-063","value":0},{"hc-key":"us-ny-019","value":1},{"hc-key":"us-ny-031","value":1},{"hc-key":"us-oh-079","value":2},{"hc-key":"us-oh-163","value":0},{"hc-key":"us-oh-073","value":3},{"hc-key":"us-oh-129","value":2},{"hc-key":"us-oh-045","value":1},{"hc-key":"us-al-085","value":1},{"hc-key":"us-ia-113","value":2},{"hc-key":"us-ga-215","value":0},{"hc-key":"us-tx-003","value":0},{"hc-key":"us-tx-495","value":1},{"hc-key":"us-nm-025","value":0},{"hc-key":"us-nm-015","value":3},{"hc-key":"us-oh-149","value":3},{"hc-key":"us-oh-021","value":0},{"hc-key":"us-oh-109","value":2},{"hc-key":"us-oh-023","value":0},{"hc-key":"us-tx-117","value":3},{"hc-key":"us-nm-037","value":3},{"hc-key":"us-tx-359","value":3},{"hc-key":"us-ok-133","value":2},{"hc-key":"us-sc-081","value":1},{"hc-key":"us-sc-003","value":1},{"hc-key":"us-ga-251","value":1},{"hc-key":"us-ga-033","value":0},{"hc-key":"us-ga-165","value":1},{"hc-key":"us-ga-245","value":3},{"hc-key":"us-nc-029","value":0},{"hc-key":"us-nc-053","value":1},{"hc-key":"us-tx-305","value":3},{"hc-key":"us-tx-169","value":1},{"hc-key":"us-wi-079","value":1},{"hc-key":"us-wi-089","value":1},{"hc-key":"us-sc-047","value":1},{"hc-key":"us-sc-059","value":3},{"hc-key":"us-sc-045","value":0},{"hc-key":"us-sc-001","value":2},{"hc-key":"us-sd-019","value":2},{"hc-key":"us-wy-011","value":0},{"hc-key":"us-wy-005","value":3},{"hc-key":"us-mt-011","value":0},{"hc-key":"us-mt-025","value":1},{"hc-key":"us-nd-011","value":2},{"hc-key":"us-ma-013","value":3},{"hc-key":"us-ma-015","value":3},{"hc-key":"us-tx-055","value":3},{"hc-key":"us-tx-209","value":3},{"hc-key":"us-al-111","value":0},{"hc-key":"us-al-027","value":2},{"hc-key":"us-al-037","value":0},{"hc-key":"us-ks-161","value":0},{"hc-key":"us-ks-061","value":1},{"hc-key":"us-tn-163","value":2},{"hc-key":"us-tn-019","value":0},{"hc-key":"us-va-111","value":3},{"hc-key":"us-va-037","value":2},{"hc-key":"us-tn-179","value":1},{"hc-key":"us-tn-073","value":2},{"hc-key":"us-sc-071","value":3},{"hc-key":"us-va-181","value":0},{"hc-key":"us-va-093","value":1},{"hc-key":"us-va-175","value":2},{"hc-key":"us-va-800","value":3},{"hc-key":"us-va-550","value":3},{"hc-key":"us-nc-073","value":3},{"hc-key":"us-or-069","value":0},{"hc-key":"us-va-061","value":2},{"hc-key":"us-nv-005","value":0},{"hc-key":"us-in-035","value":3},{"hc-key":"us-wi-131","value":1},{"hc-key":"us-pa-085","value":0},{"hc-key":"us-pa-039","value":2},{"hc-key":"us-il-193","value":3},{"hc-key":"us-il-039","value":0},{"hc-key":"us-il-115","value":3},{"hc-key":"us-il-173","value":3},{"hc-key":"us-il-137","value":3},{"hc-key":"us-ut-039","value":3},{"hc-key":"us-mn-049","value":2},{"hc-key":"us-nc-173","value":2},{"hc-key":"us-fl-103","value":1},{"hc-key":"us-fl-101","value":3},{"hc-key":"us-fl-119","value":3},{"hc-key":"us-nd-091","value":0},{"hc-key":"us-nd-003","value":3},{"hc-key":"us-nd-073","value":1},{"hc-key":"us-nd-097","value":2},{"hc-key":"us-nc-011","value":2},{"hc-key":"us-wv-107","value":0},{"hc-key":"us-wv-073","value":0},{"hc-key":"us-wi-097","value":1},{"hc-key":"us-sc-063","value":3},{"hc-key":"us-md-005","value":0},{"hc-key":"us-md-510","value":3},{"hc-key":"us-wi-039","value":1},{"hc-key":"us-ok-059","value":1},{"hc-key":"us-ok-151","value":0},{"hc-key":"us-ok-003","value":2},{"hc-key":"us-ks-007","value":3},{"hc-key":"us-ky-063","value":3},{"hc-key":"us-ma-003","value":0},{"hc-key":"us-mo-041","value":3},{"hc-key":"us-mo-195","value":0},{"hc-key":"us-ks-167","value":1},{"hc-key":"us-tn-111","value":3},{"hc-key":"us-ar-023","value":0},{"hc-key":"us-id-003","value":2},{"hc-key":"us-id-045","value":2},{"hc-key":"us-ma-021","value":2},{"hc-key":"us-pa-037","value":1},{"hc-key":"us-pa-107","value":0},{"hc-key":"us-nc-109","value":0},{"hc-key":"us-nc-045","value":0},{"hc-key":"us-ar-019","value":2},{"hc-key":"us-ar-059","value":0},{"hc-key":"us-il-169","value":1},{"hc-key":"us-il-057","value":3},{"hc-key":"us-ca-051","value":2},{"hc-key":"us-nm-013","value":0},{"hc-key":"us-nd-039","value":1},{"hc-key":"us-ca-055","value":1},{"hc-key":"us-ca-095","value":3},{"hc-key":"us-tx-449","value":3},{"hc-key":"us-wv-057","value":2},{"hc-key":"us-nc-125","value":1},{"hc-key":"us-ks-009","value":1},{"hc-key":"us-nd-087","value":2},{"hc-key":"us-la-083","value":0},{"hc-key":"us-ne-041","value":3},{"hc-key":"us-ne-071","value":2},{"hc-key":"us-al-119","value":1},{"hc-key":"us-ms-069","value":2},{"hc-key":"us-al-093","value":2},{"hc-key":"us-al-133","value":2},{"hc-key":"us-fl-117","value":1},{"hc-key":"us-ms-131","value":0},{"hc-key":"us-ms-109","value":0},{"hc-key":"us-ms-035","value":0},{"hc-key":"us-ms-073","value":1},{"hc-key":"us-mi-037","value":1},{"hc-key":"us-mi-065","value":0},{"hc-key":"us-ks-149","value":3},{"hc-key":"us-ks-177","value":3},{"hc-key":"us-sd-061","value":3},{"hc-key":"us-sd-035","value":0},{"hc-key":"us-mn-061","value":0},{"hc-key":"us-co-073","value":2},{"hc-key":"us-co-063","value":3},{"hc-key":"us-ny-077","value":1},{"hc-key":"us-ny-115","value":0},{"hc-key":"us-in-005","value":2},{"hc-key":"us-in-081","value":1},{"hc-key":"us-tx-445","value":1},{"hc-key":"us-al-121","value":1},{"hc-key":"us-ia-021","value":1},{"hc-key":"us-ia-035","value":3},{"hc-key":"us-nc-197","value":1},{"hc-key":"us-nc-067","value":2},{"hc-key":"us-nc-081","value":2},{"hc-key":"us-nc-057","value":2},{"hc-key":"us-nv-510","value":1},{"hc-key":"us-ga-193","value":1},{"hc-key":"us-ga-261","value":3},{"hc-key":"us-mo-023","value":1},{"hc-key":"us-mo-223","value":2},{"hc-key":"us-mo-179","value":3},{"hc-key":"us-co-081","value":2},{"hc-key":"us-wy-025","value":3},{"hc-key":"us-wy-009","value":2},{"hc-key":"us-wi-065","value":2},{"hc-key":"us-il-177","value":0},{"hc-key":"us-wi-045","value":2},{"hc-key":"us-wi-105","value":0},{"hc-key":"us-mi-087","value":1},{"hc-key":"us-la-015","value":0},{"hc-key":"us-la-119","value":3},{"hc-key":"us-ar-027","value":0},{"hc-key":"us-ms-143","value":1},{"hc-key":"us-ms-119","value":0},{"hc-key":"us-ms-135","value":1},{"hc-key":"us-ms-161","value":0},{"hc-key":"us-la-053","value":0},{"hc-key":"us-la-039","value":3},{"hc-key":"us-la-079","value":1},{"hc-key":"us-tn-023","value":0},{"hc-key":"us-tn-077","value":1},{"hc-key":"us-tn-069","value":3},{"hc-key":"us-az-011","value":2},{"hc-key":"us-az-003","value":3},{"hc-key":"us-nm-023","value":0},{"hc-key":"us-ok-009","value":0},{"hc-key":"us-ok-055","value":2},{"hc-key":"us-ok-057","value":2},{"hc-key":"us-tx-075","value":2},{"hc-key":"us-mo-011","value":0},{"hc-key":"us-mo-097","value":0},{"hc-key":"us-mo-039","value":0},{"hc-key":"us-oh-037","value":2},{"hc-key":"us-sd-045","value":0},{"hc-key":"us-sd-129","value":1},{"hc-key":"us-sd-137","value":1},{"hc-key":"us-sd-041","value":3},{"hc-key":"us-sd-055","value":3},{"hc-key":"us-ms-111","value":1},{"hc-key":"us-ms-041","value":3},{"hc-key":"us-ms-039","value":3},{"hc-key":"us-ca-013","value":3},{"hc-key":"us-va-087","value":1},{"hc-key":"us-ny-119","value":1},{"hc-key":"us-ny-087","value":3},{"hc-key":"us-ny-071","value":3},{"hc-key":"us-nj-003","value":2},{"hc-key":"us-mo-017","value":2},{"hc-key":"us-wv-047","value":1},{"hc-key":"us-wv-059","value":0},{"hc-key":"us-wv-033","value":0},{"hc-key":"us-wv-049","value":2},{"hc-key":"us-nc-077","value":1},{"hc-key":"us-ne-179","value":1},{"hc-key":"us-ne-139","value":0},{"hc-key":"us-sc-005","value":0},{"hc-key":"us-nd-051","value":1},{"hc-key":"us-ne-023","value":0},{"hc-key":"us-ne-143","value":1},{"hc-key":"us-ne-141","value":3},{"hc-key":"us-nc-185","value":1},{"hc-key":"us-nc-069","value":2},{"hc-key":"us-de-005","value":2},{"hc-key":"us-mo-175","value":0},{"hc-key":"us-mo-137","value":0},{"hc-key":"us-ks-035","value":0},{"hc-key":"us-ks-015","value":1},{"hc-key":"us-mi-105","value":0},{"hc-key":"us-il-007","value":1},{"hc-key":"us-il-201","value":2},{"hc-key":"us-wi-127","value":1},{"hc-key":"us-sc-007","value":0},{"hc-key":"us-sc-077","value":3},{"hc-key":"us-in-011","value":2},{"hc-key":"us-in-097","value":3},{"hc-key":"us-in-023","value":2},{"hc-key":"us-in-015","value":3},{"hc-key":"us-in-057","value":2},{"hc-key":"us-in-095","value":3},{"hc-key":"us-il-027","value":1},{"hc-key":"us-id-063","value":0},{"hc-key":"us-id-067","value":3},{"hc-key":"us-id-031","value":3},{"hc-key":"us-id-053","value":3},{"hc-key":"us-ms-127","value":3},{"hc-key":"us-ms-121","value":2},{"hc-key":"us-tx-345","value":1},{"hc-key":"us-tx-101","value":0},{"hc-key":"us-tx-125","value":1},{"hc-key":"us-tx-153","value":2},{"hc-key":"us-or-039","value":2},{"hc-key":"us-or-003","value":2},{"hc-key":"us-ok-033","value":3},{"hc-key":"us-ok-137","value":2},{"hc-key":"us-tx-485","value":2},{"hc-key":"us-mi-155","value":1},{"hc-key":"us-ks-089","value":2},{"hc-key":"us-ks-123","value":0},{"hc-key":"us-ks-029","value":2},{"hc-key":"us-ms-091","value":3},{"hc-key":"us-il-085","value":1},{"hc-key":"us-in-059","value":3},{"hc-key":"us-va-620","value":0},{"hc-key":"us-tx-329","value":3},{"hc-key":"us-ks-101","value":2},{"hc-key":"us-ks-171","value":2},{"hc-key":"us-il-199","value":0},{"hc-key":"us-mt-029","value":0},{"hc-key":"us-ks-189","value":2},{"hc-key":"us-ky-067","value":1},{"hc-key":"us-ky-151","value":3},{"hc-key":"us-ky-203","value":3},{"hc-key":"us-sd-013","value":1},{"hc-key":"us-wv-001","value":3},{"hc-key":"us-tn-055","value":3},{"hc-key":"us-tn-099","value":1},{"hc-key":"us-ne-045","value":0},{"hc-key":"us-ne-013","value":0},{"hc-key":"us-pa-109","value":1},{"hc-key":"us-tx-469","value":3},{"hc-key":"us-tx-239","value":1},{"hc-key":"us-mi-147","value":2},{"hc-key":"us-ne-173","value":3},{"hc-key":"us-ne-043","value":0},{"hc-key":"us-vt-011","value":2},{"hc-key":"us-va-067","value":1},{"hc-key":"us-ok-011","value":2},{"hc-key":"us-ok-039","value":3},{"hc-key":"us-wv-093","value":1},{"hc-key":"us-wi-023","value":2},{"hc-key":"us-ia-157","value":3},{"hc-key":"us-ia-123","value":2},{"hc-key":"us-ia-125","value":0},{"hc-key":"us-ia-051","value":2},{"hc-key":"us-ia-179","value":0},{"hc-key":"us-ia-135","value":0},{"hc-key":"us-ks-091","value":0},{"hc-key":"us-ks-045","value":1},{"hc-key":"us-ks-059","value":2},{"hc-key":"us-ky-059","value":3},{"hc-key":"us-ky-237","value":3},{"hc-key":"us-ky-197","value":0},{"hc-key":"us-or-061","value":0},{"hc-key":"us-ga-151","value":1},{"hc-key":"us-nd-001","value":0},{"hc-key":"us-nd-041","value":0},{"hc-key":"us-sd-105","value":3},{"hc-key":"us-nd-085","value":1},{"hc-key":"us-pa-063","value":0},{"hc-key":"us-pa-021","value":3},{"hc-key":"us-tn-107","value":1},{"hc-key":"us-mi-093","value":2},{"hc-key":"us-mi-075","value":1},{"hc-key":"us-co-087","value":2},{"hc-key":"us-pa-127","value":0},{"hc-key":"us-pa-069","value":3},{"hc-key":"us-ok-123","value":2},{"hc-key":"us-ok-037","value":3},{"hc-key":"us-ok-081","value":2},{"hc-key":"us-tn-175","value":0},{"hc-key":"us-ok-125","value":1},{"hc-key":"us-ct-013","value":0},{"hc-key":"us-ga-003","value":2},{"hc-key":"us-tx-145","value":0},{"hc-key":"us-ks-033","value":2},{"hc-key":"us-ky-113","value":3},{"hc-key":"us-ky-171","value":0},{"hc-key":"us-ok-075","value":2},{"hc-key":"us-sd-079","value":3},{"hc-key":"us-nc-033","value":0},{"hc-key":"us-al-003","value":0},{"hc-key":"us-va-127","value":1},{"hc-key":"us-ga-053","value":3},{"hc-key":"us-in-077","value":0},{"hc-key":"us-va-019","value":2},{"hc-key":"us-va-009","value":3},{"hc-key":"us-va-011","value":1},{"hc-key":"us-nm-033","value":3},{"hc-key":"us-ne-077","value":1},{"hc-key":"us-ne-011","value":2},{"hc-key":"us-mo-149","value":0},{"hc-key":"us-tn-161","value":3},{"hc-key":"us-tn-083","value":2},{"hc-key":"us-pa-113","value":3},{"hc-key":"us-pa-131","value":3},{"hc-key":"us-in-167","value":0},{"hc-key":"us-il-045","value":1},{"hc-key":"us-in-165","value":0},{"hc-key":"us-mi-149","value":1},{"hc-key":"us-in-039","value":3},{"hc-key":"us-in-087","value":2},{"hc-key":"us-tx-189","value":0},{"hc-key":"us-tx-303","value":3},{"hc-key":"us-tx-107","value":0},{"hc-key":"us-mn-093","value":3},{"hc-key":"us-oh-005","value":3},{"hc-key":"us-ne-119","value":3},{"hc-key":"us-ne-167","value":2},{"hc-key":"us-fl-085","value":0},{"hc-key":"us-ga-227","value":0},{"hc-key":"us-ga-085","value":0},{"hc-key":"us-ga-319","value":3},{"hc-key":"us-ga-167","value":3},{"hc-key":"us-ar-095","value":3},{"hc-key":"us-ar-117","value":3},{"hc-key":"us-ar-147","value":3},{"hc-key":"us-mo-059","value":1},{"hc-key":"us-mo-169","value":3},{"hc-key":"us-va-143","value":2},{"hc-key":"us-ks-145","value":1},{"hc-key":"us-ks-137","value":2},{"hc-key":"us-ks-147","value":2},{"hc-key":"us-ks-065","value":3},{"hc-key":"us-ks-063","value":1},{"hc-key":"us-ks-109","value":0},{"hc-key":"us-la-099","value":3},{"hc-key":"us-la-097","value":0},{"hc-key":"us-la-055","value":1},{"hc-key":"us-oh-095","value":2},{"hc-key":"us-oh-123","value":2},{"hc-key":"us-oh-069","value":0},{"hc-key":"us-ar-031","value":3},{"hc-key":"us-ar-093","value":2},{"hc-key":"us-ar-035","value":0},{"hc-key":"us-ky-117","value":2},{"hc-key":"us-ky-081","value":2},{"hc-key":"us-ky-077","value":2},{"hc-key":"us-ky-097","value":0},{"hc-key":"us-ky-001","value":1},{"hc-key":"us-ky-217","value":2},{"hc-key":"us-nc-075","value":3},{"hc-key":"us-va-071","value":0},{"hc-key":"us-wv-055","value":0},{"hc-key":"us-nm-035","value":0},{"hc-key":"us-ne-049","value":1},{"hc-key":"us-ne-101","value":1},{"hc-key":"us-ne-135","value":0},{"hc-key":"us-ne-117","value":3},{"hc-key":"us-ks-039","value":0},{"hc-key":"us-ne-145","value":2},{"hc-key":"us-ne-149","value":0},{"hc-key":"us-tn-081","value":1},{"hc-key":"us-tn-085","value":3},{"hc-key":"us-il-125","value":2},{"hc-key":"us-il-107","value":3},{"hc-key":"us-va-077","value":0},{"hc-key":"us-va-197","value":1},{"hc-key":"us-ga-303","value":1},{"hc-key":"us-wi-035","value":2},{"hc-key":"us-tn-153","value":0},{"hc-key":"us-tn-007","value":2},{"hc-key":"us-tn-065","value":1},{"hc-key":"us-tx-471","value":2},{"hc-key":"us-tx-407","value":0},{"hc-key":"us-tx-291","value":1},{"hc-key":"us-ks-191","value":1},{"hc-key":"us-ok-071","value":2},{"hc-key":"us-mi-127","value":0},{"hc-key":"us-mn-001","value":3},{"hc-key":"us-mn-065","value":0},{"hc-key":"us-oh-085","value":2},{"hc-key":"us-ca-003","value":3},{"hc-key":"us-il-105","value":1},{"hc-key":"us-il-179","value":0},{"hc-key":"us-oh-135","value":1},{"hc-key":"us-ma-025","value":2},{"hc-key":"us-ma-017","value":3},{"hc-key":"us-tx-115","value":3},{"hc-key":"us-ny-069","value":0},{"hc-key":"us-ks-121","value":2},{"hc-key":"us-mo-037","value":1},{"hc-key":"us-mo-095","value":0},{"hc-key":"us-ga-153","value":0},{"hc-key":"us-ga-023","value":3},{"hc-key":"us-ga-235","value":0},{"hc-key":"us-ga-091","value":3},{"hc-key":"us-ny-023","value":3},{"hc-key":"us-mi-161","value":3},{"hc-key":"us-in-113","value":0},{"hc-key":"us-ga-225","value":2},{"hc-key":"us-ga-311","value":1},{"hc-key":"us-ga-163","value":3},{"hc-key":"us-wi-099","value":0},{"hc-key":"us-wi-085","value":1},{"hc-key":"us-wi-067","value":1},{"hc-key":"us-wi-078","value":2},{"hc-key":"us-wi-083","value":1},{"hc-key":"us-wa-063","value":1},{"hc-key":"us-wa-043","value":0},{"hc-key":"us-nj-039","value":1},{"hc-key":"us-nj-017","value":1},{"hc-key":"us-oh-075","value":2},{"hc-key":"us-oh-169","value":3},{"hc-key":"us-oh-151","value":2},{"hc-key":"us-oh-019","value":0},{"hc-key":"us-tx-187","value":3},{"hc-key":"us-wa-029","value":1},{"hc-key":"us-md-017","value":3},{"hc-key":"us-va-033","value":3},{"hc-key":"us-va-630","value":2},{"hc-key":"us-md-037","value":3},{"hc-key":"us-va-099","value":1},{"hc-key":"us-va-193","value":3},{"hc-key":"us-de-003","value":0},{"hc-key":"us-pa-029","value":3},{"hc-key":"us-nj-015","value":2},{"hc-key":"us-nj-033","value":0},{"hc-key":"us-nj-011","value":0},{"hc-key":"us-pa-011","value":3},{"hc-key":"us-pa-077","value":1},{"hc-key":"us-pa-095","value":3},{"hc-key":"us-md-015","value":3},{"hc-key":"us-md-029","value":2},{"hc-key":"us-wa-065","value":0},{"hc-key":"us-ia-101","value":3},{"hc-key":"us-ia-177","value":3},{"hc-key":"us-ne-137","value":0},{"hc-key":"us-ne-073","value":3},{"hc-key":"us-ms-087","value":2},{"hc-key":"us-ms-095","value":1},{"hc-key":"us-fl-033","value":1},{"hc-key":"us-fl-113","value":1},{"hc-key":"us-tx-439","value":1},{"hc-key":"us-va-169","value":1},{"hc-key":"us-ok-085","value":1},{"hc-key":"us-ga-289","value":2},{"hc-key":"us-oh-099","value":3},{"hc-key":"us-tx-381","value":3},{"hc-key":"us-tx-375","value":2},{"hc-key":"us-tx-437","value":1},{"hc-key":"us-mo-151","value":2},{"hc-key":"us-il-089","value":2},{"hc-key":"us-il-043","value":1},{"hc-key":"us-sc-031","value":3},{"hc-key":"us-sc-055","value":1},{"hc-key":"us-sc-079","value":3},{"hc-key":"us-sc-057","value":2},{"hc-key":"us-nc-119","value":2},{"hc-key":"us-nc-139","value":0},{"hc-key":"us-or-019","value":1},{"hc-key":"us-or-033","value":1},{"hc-key":"us-pa-089","value":3},{"hc-key":"us-ar-101","value":3},{"hc-key":"us-ar-071","value":0},{"hc-key":"us-ks-135","value":0},{"hc-key":"us-la-047","value":1},{"hc-key":"us-la-077","value":2},{"hc-key":"us-la-121","value":3},{"hc-key":"us-ne-067","value":2},{"hc-key":"us-ne-095","value":0},{"hc-key":"us-al-115","value":3},{"hc-key":"us-ms-101","value":0},{"hc-key":"us-ms-099","value":3},{"hc-key":"us-ms-123","value":0},{"hc-key":"us-ga-095","value":2},{"hc-key":"us-id-017","value":2},{"hc-key":"us-ga-047","value":1},{"hc-key":"us-md-027","value":0},{"hc-key":"us-md-013","value":0},{"hc-key":"us-md-021","value":3},{"hc-key":"us-ut-035","value":1},{"hc-key":"us-ct-003","value":1},{"hc-key":"us-ct-009","value":0},{"hc-key":"us-ct-015","value":0},{"hc-key":"us-oh-157","value":3},{"hc-key":"us-oh-067","value":2},{"hc-key":"us-oh-081","value":0},{"hc-key":"us-oh-013","value":0},{"hc-key":"us-oh-111","value":0},{"hc-key":"us-oh-059","value":1},{"hc-key":"us-oh-047","value":0},{"hc-key":"us-oh-027","value":0},{"hc-key":"us-oh-071","value":1},{"hc-key":"us-mn-071","value":3},{"hc-key":"us-id-077","value":3},{"hc-key":"us-id-011","value":0},{"hc-key":"us-id-029","value":2},{"hc-key":"us-wi-121","value":3},{"hc-key":"us-sd-071","value":1},{"hc-key":"us-va-036","value":0},{"hc-key":"us-va-149","value":0},{"hc-key":"us-va-041","value":0},{"hc-key":"us-al-023","value":3},{"hc-key":"us-al-129","value":3},{"hc-key":"us-in-055","value":3},{"hc-key":"us-ne-047","value":3},{"hc-key":"us-ne-111","value":3},{"hc-key":"us-nd-105","value":1},{"hc-key":"us-nd-017","value":3},{"hc-key":"us-il-151","value":3},{"hc-key":"us-il-069","value":1},{"hc-key":"us-ne-005","value":0},{"hc-key":"us-tx-163","value":1},{"hc-key":"us-ia-103","value":3},{"hc-key":"us-ks-001","value":0},{"hc-key":"us-ks-133","value":0},{"hc-key":"us-ky-089","value":1},{"hc-key":"us-ne-087","value":2},{"hc-key":"us-ne-085","value":1},{"hc-key":"us-ne-063","value":3},{"hc-key":"us-ne-057","value":0},{"hc-key":"us-co-115","value":1},{"hc-key":"us-co-095","value":3},{"hc-key":"us-ky-101","value":1},{"hc-key":"us-ms-105","value":3},{"hc-key":"us-md-023","value":3},{"hc-key":"us-mi-059","value":2},{"hc-key":"us-oh-171","value":2},{"hc-key":"us-oh-051","value":0},{"hc-key":"us-ky-177","value":1},{"hc-key":"us-ky-149","value":0},{"hc-key":"us-ky-141","value":1},{"hc-key":"us-nh-015","value":3},{"hc-key":"us-pa-133","value":1},{"hc-key":"us-ne-097","value":3},{"hc-key":"us-sd-075","value":1},{"hc-key":"us-nd-007","value":3},{"hc-key":"us-ne-003","value":1},{"hc-key":"us-mo-073","value":2},{"hc-key":"us-ky-061","value":1},{"hc-key":"us-ky-031","value":3},{"hc-key":"us-ga-211","value":3},{"hc-key":"us-ms-033","value":2},{"hc-key":"us-ms-137","value":3},{"hc-key":"us-ar-107","value":2},{"hc-key":"us-al-117","value":3},{"hc-key":"us-ok-149","value":2},{"hc-key":"us-nc-135","value":3},{"hc-key":"us-in-103","value":3},{"hc-key":"us-in-017","value":3},{"hc-key":"us-wi-061","value":3},{"hc-key":"us-ny-063","value":0},{"hc-key":"us-ny-029","value":0},{"hc-key":"us-ny-009","value":0},{"hc-key":"us-md-031","value":2},{"hc-key":"us-va-610","value":0},{"hc-key":"us-va-035","value":1},{"hc-key":"us-il-083","value":3},{"hc-key":"us-mo-183","value":0},{"hc-key":"us-mo-071","value":2},{"hc-key":"us-ks-013","value":2},{"hc-key":"us-ga-029","value":2},{"hc-key":"us-ga-109","value":3},{"hc-key":"us-ga-031","value":3},{"hc-key":"us-ky-119","value":2},{"hc-key":"us-ky-071","value":3},{"hc-key":"us-ky-159","value":1},{"hc-key":"us-ga-309","value":2},{"hc-key":"us-nc-107","value":2},{"hc-key":"us-tx-483","value":3},{"hc-key":"us-tx-211","value":0},{"hc-key":"us-tx-393","value":2},{"hc-key":"us-tx-295","value":3},{"hc-key":"us-ok-007","value":3},{"hc-key":"us-ks-175","value":2},{"hc-key":"us-or-009","value":2},{"hc-key":"us-sc-041","value":2},{"hc-key":"us-ut-053","value":1},{"hc-key":"us-ut-021","value":2},{"hc-key":"us-wy-033","value":2},{"hc-key":"us-wy-003","value":0},{"hc-key":"us-mt-003","value":0},{"hc-key":"us-pa-047","value":3},{"hc-key":"us-pa-083","value":1},{"hc-key":"us-ms-031","value":2},{"hc-key":"us-in-013","value":2},{"hc-key":"us-ky-065","value":0},{"hc-key":"us-tx-087","value":3},{"hc-key":"us-tx-089","value":1},{"hc-key":"us-il-063","value":2},{"hc-key":"us-mn-073","value":3},{"hc-key":"us-sd-051","value":2},{"hc-key":"us-sd-039","value":0},{"hc-key":"us-mn-081","value":2},{"hc-key":"us-mn-083","value":1},{"hc-key":"us-ga-043","value":3},{"hc-key":"us-tx-497","value":3},{"hc-key":"us-ga-307","value":3},{"hc-key":"us-ga-159","value":2},{"hc-key":"us-ga-237","value":3},{"hc-key":"us-ga-169","value":1},{"hc-key":"us-ga-009","value":1},{"hc-key":"us-ga-021","value":1},{"hc-key":"us-ny-079","value":1},{"hc-key":"us-va-199","value":3},{"hc-key":"us-va-735","value":3},{"hc-key":"us-va-700","value":0},{"hc-key":"us-ok-065","value":0},{"hc-key":"us-tx-173","value":0},{"hc-key":"us-tx-431","value":1},{"hc-key":"us-tx-335","value":2},{"hc-key":"us-tx-415","value":2},{"hc-key":"us-tx-383","value":2},{"hc-key":"us-ok-093","value":3},{"hc-key":"us-wa-051","value":0},{"hc-key":"us-nd-055","value":1},{"hc-key":"us-or-027","value":3},{"hc-key":"us-ia-081","value":1},{"hc-key":"us-ia-197","value":3},{"hc-key":"us-tx-419","value":2},{"hc-key":"us-tx-403","value":2},{"hc-key":"us-mo-055","value":1},{"hc-key":"us-wv-075","value":1},{"hc-key":"us-va-017","value":3},{"hc-key":"us-mn-111","value":2},{"hc-key":"us-ms-017","value":2},{"hc-key":"us-ms-081","value":1},{"hc-key":"us-la-067","value":1},{"hc-key":"us-al-047","value":1},{"hc-key":"us-al-001","value":3},{"hc-key":"us-tx-093","value":0},{"hc-key":"us-tx-425","value":0},{"hc-key":"us-mn-153","value":2},{"hc-key":"us-mn-021","value":1},{"hc-key":"us-co-079","value":2},{"hc-key":"us-nm-039","value":2},{"hc-key":"us-ms-163","value":1},{"hc-key":"us-mo-177","value":1},{"hc-key":"us-oh-041","value":1},{"hc-key":"us-oh-117","value":3},{"hc-key":"us-nd-031","value":1},{"hc-key":"us-ar-033","value":0},{"hc-key":"us-ok-001","value":3},{"hc-key":"us-ok-041","value":3},{"hc-key":"us-tx-053","value":1},{"hc-key":"us-tx-411","value":0},{"hc-key":"us-tx-299","value":0},{"hc-key":"us-il-109","value":1},{"hc-key":"us-il-187","value":1},{"hc-key":"us-il-071","value":2},{"hc-key":"us-ar-067","value":3},{"hc-key":"us-ar-037","value":0},{"hc-key":"us-tx-315","value":2},{"hc-key":"us-ky-083","value":0},{"hc-key":"us-tn-079","value":1},{"hc-key":"us-nc-087","value":0},{"hc-key":"us-nc-115","value":0},{"hc-key":"us-mn-037","value":3},{"hc-key":"us-or-025","value":0},{"hc-key":"us-or-013","value":1},{"hc-key":"us-nv-013","value":3},{"hc-key":"us-wi-001","value":1},{"hc-key":"us-wi-077","value":0},{"hc-key":"us-mo-045","value":0},{"hc-key":"us-mo-103","value":0},{"hc-key":"us-mo-001","value":2},{"hc-key":"us-mo-199","value":0},{"hc-key":"us-ky-015","value":2},{"hc-key":"us-ky-079","value":2},{"hc-key":"us-ky-021","value":0},{"hc-key":"us-ky-229","value":1},{"hc-key":"us-ky-155","value":3},{"hc-key":"us-la-115","value":3},{"hc-key":"us-la-009","value":0},{"hc-key":"us-la-059","value":3},{"hc-key":"us-mo-227","value":1},{"hc-key":"us-ne-163","value":1},{"hc-key":"us-sd-021","value":0},{"hc-key":"us-sd-063","value":1},{"hc-key":"us-mi-057","value":0},{"hc-key":"us-ia-001","value":1},{"hc-key":"us-ia-175","value":1},{"hc-key":"us-il-127","value":1},{"hc-key":"us-sd-007","value":0},{"hc-key":"us-ne-017","value":2},{"hc-key":"us-ne-171","value":0},{"hc-key":"us-ne-113","value":0},{"hc-key":"us-ne-121","value":2},{"hc-key":"us-ky-039","value":0},{"hc-key":"us-in-119","value":1},{"hc-key":"us-in-021","value":2},{"hc-key":"us-in-109","value":1},{"hc-key":"us-tx-139","value":1},{"hc-key":"us-tx-213","value":0},{"hc-key":"us-in-053","value":3},{"hc-key":"us-fl-035","value":3},{"hc-key":"us-ms-077","value":2},{"hc-key":"us-la-127","value":0},{"hc-key":"us-il-131","value":1},{"hc-key":"us-il-175","value":1},{"hc-key":"us-sd-029","value":3},{"hc-key":"us-ga-259","value":2},{"hc-key":"us-nj-025","value":2},{"hc-key":"us-ar-121","value":0},{"hc-key":"us-ar-021","value":1},{"hc-key":"us-ar-045","value":1},{"hc-key":"us-ia-107","value":2},{"hc-key":"us-il-061","value":1},{"hc-key":"us-il-171","value":1},{"hc-key":"us-ca-105","value":3},{"hc-key":"us-ca-045","value":2},{"hc-key":"us-ca-023","value":3},{"hc-key":"us-mn-025","value":2},{"hc-key":"us-mn-003","value":0},{"hc-key":"us-sd-009","value":3},{"hc-key":"us-sd-135","value":0},{"hc-key":"us-tn-059","value":3},{"hc-key":"us-va-183","value":3},{"hc-key":"us-in-141","value":2},{"hc-key":"us-ga-119","value":0},{"hc-key":"us-ga-257","value":0},{"hc-key":"us-ar-005","value":1},{"hc-key":"us-ms-007","value":3},{"hc-key":"us-ks-011","value":2},{"hc-key":"us-va-049","value":3},{"hc-key":"us-va-147","value":2},{"hc-key":"us-va-145","value":3},{"hc-key":"us-sc-085","value":2},{"hc-key":"us-wv-045","value":3},{"hc-key":"us-wv-109","value":1},{"hc-key":"us-wv-005","value":2},{"hc-key":"us-ca-029","value":0},{"hc-key":"us-tx-097","value":1},{"hc-key":"us-sc-075","value":3},{"hc-key":"us-ia-131","value":2},{"hc-key":"us-mn-101","value":3},{"hc-key":"us-mn-117","value":0},{"hc-key":"us-ri-007","value":3},{"hc-key":"us-mo-161","value":1},{"hc-key":"us-ms-117","value":2},{"hc-key":"us-ky-013","value":3},{"hc-key":"us-ky-235","value":1},{"hc-key":"us-tx-491","value":1},{"hc-key":"us-ga-107","value":3},{"hc-key":"us-la-025","value":3},{"hc-key":"us-pa-025","value":1},{"hc-key":"us-ne-133","value":2},{"hc-key":"us-tn-171","value":2},{"hc-key":"us-sc-083","value":3},{"hc-key":"us-nc-161","value":3},{"hc-key":"us-wi-005","value":2},{"hc-key":"us-wi-073","value":3},{"hc-key":"us-wv-101","value":3},{"hc-key":"us-wv-007","value":0},{"hc-key":"us-wv-015","value":1},{"hc-key":"us-wv-067","value":0},{"hc-key":"us-mo-085","value":1},{"hc-key":"us-mo-009","value":3},{"hc-key":"us-mn-123","value":2},{"hc-key":"us-ar-081","value":2},{"hc-key":"us-nv-019","value":0},{"hc-key":"us-ms-085","value":1},{"hc-key":"us-ms-005","value":3},{"hc-key":"us-ms-113","value":1},{"hc-key":"us-ms-147","value":2},{"hc-key":"us-la-105","value":0},{"hc-key":"us-tn-003","value":3},{"hc-key":"us-id-085","value":0},{"hc-key":"us-id-059","value":3},{"hc-key":"us-ky-091","value":2},{"hc-key":"us-ky-183","value":3},{"hc-key":"us-ky-107","value":0},{"hc-key":"us-ia-137","value":0},{"hc-key":"us-ia-129","value":2},{"hc-key":"us-ne-153","value":0},{"hc-key":"us-ne-025","value":2},{"hc-key":"us-ne-055","value":1},{"hc-key":"us-ga-071","value":0},{"hc-key":"us-il-157","value":2},{"hc-key":"us-mo-157","value":0},{"hc-key":"us-va-173","value":2},{"hc-key":"us-nm-011","value":3},{"hc-key":"us-nm-041","value":1},{"hc-key":"us-pa-111","value":1},{"hc-key":"us-oh-101","value":3},{"hc-key":"us-il-149","value":2},{"hc-key":"us-al-107","value":3},{"hc-key":"us-ok-045","value":3},{"hc-key":"us-il-145","value":2},{"hc-key":"us-ia-075","value":2},{"hc-key":"us-co-107","value":2},{"hc-key":"us-co-049","value":2},{"hc-key":"us-co-019","value":0},{"hc-key":"us-co-103","value":0},{"hc-key":"us-nj-035","value":3},{"hc-key":"us-nj-023","value":3},{"hc-key":"us-il-087","value":2},{"hc-key":"us-co-053","value":0},{"hc-key":"us-co-091","value":3},{"hc-key":"us-mn-059","value":1},{"hc-key":"us-pa-049","value":0},{"hc-key":"us-ms-029","value":2},{"hc-key":"us-ky-161","value":0},{"hc-key":"us-mn-095","value":3},{"hc-key":"us-tx-179","value":3},{"hc-key":"us-tx-129","value":3},{"hc-key":"us-fl-013","value":2},{"hc-key":"us-fl-005","value":2},{"hc-key":"us-tn-181","value":0},{"hc-key":"us-al-077","value":0},{"hc-key":"us-al-083","value":2},{"hc-key":"us-in-045","value":0},{"hc-key":"us-oh-115","value":1},{"hc-key":"us-oh-127","value":2},{"hc-key":"us-va-590","value":3},{"hc-key":"us-ga-011","value":3},{"hc-key":"us-ga-229","value":3},{"hc-key":"us-ga-305","value":2},{"hc-key":"us-ok-109","value":2},{"hc-key":"us-ok-069","value":1},{"hc-key":"us-ok-005","value":0},{"hc-key":"us-nc-099","value":2},{"hc-key":"us-ut-043","value":2},{"hc-key":"us-nv-015","value":3},{"hc-key":"us-nv-027","value":3},{"hc-key":"us-ky-023","value":3},{"hc-key":"us-ky-201","value":2},{"hc-key":"us-wi-037","value":1},{"hc-key":"us-mn-159","value":1},{"hc-key":"us-ky-043","value":1},{"hc-key":"us-nc-097","value":3},{"hc-key":"us-nc-159","value":2},{"hc-key":"us-nc-025","value":0},{"hc-key":"us-nc-167","value":3},{"hc-key":"us-va-083","value":1},{"hc-key":"us-nc-091","value":1},{"hc-key":"us-or-021","value":2},{"hc-key":"us-ar-009","value":0},{"hc-key":"us-tx-433","value":1},{"hc-key":"us-tx-253","value":2},{"hc-key":"us-tx-083","value":1},{"hc-key":"us-tx-185","value":0},{"hc-key":"us-mi-157","value":1},{"hc-key":"us-mi-049","value":1},{"hc-key":"us-la-007","value":2},{"hc-key":"us-la-093","value":2},{"hc-key":"us-la-005","value":1},{"hc-key":"us-ms-013","value":0},{"hc-key":"us-mt-033","value":1},{"hc-key":"us-mt-017","value":0},{"hc-key":"us-il-141","value":0},{"hc-key":"us-nd-077","value":2},{"hc-key":"us-sd-109","value":0},{"hc-key":"us-wi-095","value":0},{"hc-key":"us-ca-015","value":1},{"hc-key":"us-ok-127","value":2},{"hc-key":"us-wa-061","value":0},{"hc-key":"us-wa-007","value":3},{"hc-key":"us-ny-105","value":1},{"hc-key":"us-nc-149","value":1},{"hc-key":"us-nc-163","value":3},{"hc-key":"us-nc-085","value":1},{"hc-key":"us-nc-105","value":1},{"hc-key":"us-in-159","value":2},{"hc-key":"us-in-067","value":0},{"hc-key":"us-ks-027","value":1},{"hc-key":"us-ok-145","value":3},{"hc-key":"us-ok-097","value":3},{"hc-key":"us-ar-003","value":2},{"hc-key":"us-ar-043","value":0},{"hc-key":"us-ar-041","value":2},{"hc-key":"us-ar-079","value":3},{"hc-key":"us-tn-135","value":1},{"hc-key":"us-mn-163","value":2},{"hc-key":"us-va-740","value":0},{"hc-key":"us-tx-235","value":0},{"hc-key":"us-mo-025","value":1},{"hc-key":"us-mo-049","value":1},{"hc-key":"us-fl-007","value":0},{"hc-key":"us-fl-107","value":3},{"hc-key":"us-ky-163","value":0},{"hc-key":"us-ne-161","value":2},{"hc-key":"us-ne-075","value":1},{"hc-key":"us-id-071","value":2},{"hc-key":"us-id-035","value":0},{"hc-key":"us-ky-095","value":0},{"hc-key":"us-mn-035","value":0},{"hc-key":"us-az-021","value":1},{"hc-key":"us-az-009","value":1},{"hc-key":"us-ar-123","value":0},{"hc-key":"us-ks-183","value":2},{"hc-key":"us-fl-065","value":2},{"hc-key":"us-ms-037","value":1},{"hc-key":"us-la-037","value":2},{"hc-key":"us-la-091","value":2},{"hc-key":"us-ar-097","value":1},{"hc-key":"us-mi-025","value":2},{"hc-key":"us-mi-077","value":3},{"hc-key":"us-mi-015","value":1},{"hc-key":"us-al-029","value":1},{"hc-key":"us-ca-089","value":3},{"hc-key":"us-ia-153","value":1},{"hc-key":"us-ia-049","value":0},{"hc-key":"us-ga-183","value":0},{"hc-key":"us-tn-187","value":0},{"hc-key":"us-wa-003","value":0},{"hc-key":"us-mn-155","value":3},{"hc-key":"us-mn-167","value":2},{"hc-key":"us-mn-051","value":1},{"hc-key":"us-il-017","value":0},{"hc-key":"us-ga-247","value":0},{"hc-key":"us-ok-049","value":0},{"hc-key":"us-la-031","value":2},{"hc-key":"us-ca-019","value":3},{"hc-key":"us-ga-147","value":2},{"hc-key":"us-ga-105","value":3},{"hc-key":"us-al-005","value":3},{"hc-key":"us-al-109","value":0},{"hc-key":"us-al-011","value":2},{"hc-key":"us-nc-017","value":2},{"hc-key":"us-tn-035","value":1},{"hc-key":"us-tn-129","value":0},{"hc-key":"us-mi-137","value":2},{"hc-key":"us-ia-161","value":3},{"hc-key":"us-ut-041","value":2},{"hc-key":"us-ut-015","value":0},{"hc-key":"us-wa-019","value":3},{"hc-key":"us-mt-083","value":1},{"hc-key":"us-mt-109","value":2},{"hc-key":"us-nm-027","value":3},{"hc-key":"us-nm-053","value":1},{"hc-key":"us-ok-021","value":2},{"hc-key":"us-mn-173","value":0},{"hc-key":"us-tx-231","value":0},{"hc-key":"us-tx-223","value":2},{"hc-key":"us-tx-467","value":3},{"hc-key":"us-tx-353","value":0},{"hc-key":"us-tx-499","value":0},{"hc-key":"us-ks-019","value":0},{"hc-key":"us-nc-039","value":0},{"hc-key":"us-ga-291","value":0},{"hc-key":"us-ga-111","value":0},{"hc-key":"us-ky-195","value":3},{"hc-key":"us-fl-041","value":3},{"hc-key":"us-nc-035","value":0},{"hc-key":"us-ky-025","value":1},{"hc-key":"us-mo-181","value":1},{"hc-key":"us-tx-109","value":3},{"hc-key":"us-ar-135","value":2},{"hc-key":"us-nc-177","value":2},{"hc-key":"us-mn-131","value":0},{"hc-key":"us-mn-161","value":3},{"hc-key":"us-mn-139","value":2},{"hc-key":"us-mo-219","value":2},{"hc-key":"us-ky-227","value":3},{"hc-key":"us-la-017","value":0},{"hc-key":"us-ia-127","value":2},{"hc-key":"us-ia-169","value":0},{"hc-key":"us-ia-083","value":3},{"hc-key":"us-ok-141","value":0},{"hc-key":"us-ok-031","value":0},{"hc-key":"us-nc-113","value":2},{"hc-key":"us-wv-063","value":1},{"hc-key":"us-in-155","value":1},{"hc-key":"us-al-061","value":3},{"hc-key":"us-il-155","value":0},{"hc-key":"us-il-099","value":1},{"hc-key":"us-ia-055","value":3},{"hc-key":"us-ia-105","value":3},{"hc-key":"us-ks-127","value":2},{"hc-key":"us-id-015","value":2},{"hc-key":"us-nc-143","value":3},{"hc-key":"us-ga-101","value":3},{"hc-key":"us-fl-047","value":3},{"hc-key":"us-ms-045","value":2},{"hc-key":"us-ga-315","value":1},{"hc-key":"us-la-081","value":2},{"hc-key":"us-tn-093","value":0},{"hc-key":"us-tn-089","value":1},{"hc-key":"us-ks-209","value":1},{"hc-key":"us-ga-281","value":2},{"hc-key":"us-ms-129","value":2},{"hc-key":"us-ms-061","value":2},{"hc-key":"us-id-075","value":2},{"hc-key":"us-id-079","value":0},{"hc-key":"us-ia-067","value":3},{"hc-key":"us-ky-145","value":0},{"hc-key":"us-ky-225","value":0},{"hc-key":"us-ky-055","value":3},{"hc-key":"us-sd-125","value":3},{"hc-key":"us-tn-029","value":3},{"hc-key":"us-tn-067","value":2},{"hc-key":"us-sc-089","value":2},{"hc-key":"us-sc-067","value":2},{"hc-key":"us-la-117","value":3},{"hc-key":"us-mo-207","value":1},{"hc-key":"us-ga-093","value":0},{"hc-key":"us-mn-145","value":0},{"hc-key":"us-mn-009","value":1},{"hc-key":"us-ne-155","value":2},{"hc-key":"us-mn-141","value":0},{"hc-key":"us-tn-025","value":0},{"hc-key":"us-tx-337","value":3},{"hc-key":"us-tx-049","value":0},{"hc-key":"us-tx-333","value":1},{"hc-key":"us-wi-101","value":3},{"hc-key":"us-wi-059","value":1},{"hc-key":"us-wi-009","value":1},{"hc-key":"us-ok-103","value":2},{"hc-key":"us-ok-083","value":2},{"hc-key":"us-ok-121","value":0},{"hc-key":"us-wv-017","value":3},{"hc-key":"us-wv-087","value":1},{"hc-key":"us-wv-105","value":0},{"hc-key":"us-me-017","value":0},{"hc-key":"us-nh-003","value":1},{"hc-key":"us-wv-095","value":3},{"hc-key":"us-mn-079","value":3},{"hc-key":"us-wa-037","value":0},{"hc-key":"us-wi-133","value":1},{"hc-key":"us-ks-151","value":1},{"hc-key":"us-al-045","value":2},{"hc-key":"us-al-069","value":1},{"hc-key":"us-al-067","value":0},{"hc-key":"us-ca-109","value":0},{"hc-key":"us-ca-039","value":3},{"hc-key":"us-mo-075","value":0},{"hc-key":"us-ne-009","value":3},{"hc-key":"us-ut-009","value":0},{"hc-key":"us-nc-071","value":2},{"hc-key":"us-nm-019","value":1},{"hc-key":"us-mt-027","value":2},{"hc-key":"us-mt-037","value":2},{"hc-key":"us-nc-195","value":0},{"hc-key":"us-nc-101","value":2},{"hc-key":"us-ca-085","value":1},{"hc-key":"us-il-183","value":3},{"hc-key":"us-mo-189","value":3},{"hc-key":"us-ca-103","value":3},{"hc-key":"us-tx-151","value":3},{"hc-key":"us-la-085","value":0},{"hc-key":"us-nc-015","value":2},{"hc-key":"us-tn-143","value":0},{"hc-key":"us-nc-079","value":0},{"hc-key":"us-ky-131","value":0},{"hc-key":"us-il-123","value":3},{"hc-key":"us-tx-081","value":0},{"hc-key":"us-tn-133","value":2},{"hc-key":"us-mi-063","value":3},{"hc-key":"us-mi-129","value":1},{"hc-key":"us-mi-069","value":1},{"hc-key":"us-ga-069","value":0},{"hc-key":"us-ga-299","value":1},{"hc-key":"us-nd-037","value":0},{"hc-key":"us-la-103","value":2},{"hc-key":"us-oh-057","value":0},{"hc-key":"us-mn-137","value":3},{"hc-key":"us-oh-043","value":2},{"hc-key":"us-oh-093","value":1},{"hc-key":"us-ia-015","value":0},{"hc-key":"us-sd-027","value":2},{"hc-key":"us-ne-051","value":0},{"hc-key":"us-sd-127","value":1},{"hc-key":"us-md-025","value":1},{"hc-key":"us-sc-037","value":0},{"hc-key":"us-va-133","value":2},{"hc-key":"us-wi-015","value":1},{"hc-key":"us-ky-121","value":3},{"hc-key":"us-mi-067","value":3},{"hc-key":"us-tn-095","value":1},{"hc-key":"us-tn-131","value":0},{"hc-key":"us-ky-105","value":1},{"hc-key":"us-ky-035","value":3},{"hc-key":"us-va-015","value":1},{"hc-key":"us-va-003","value":2},{"hc-key":"us-va-029","value":2},{"hc-key":"us-va-043","value":2},{"hc-key":"us-wa-023","value":3},{"hc-key":"us-ky-109","value":3},{"hc-key":"us-sc-073","value":2},{"hc-key":"us-la-033","value":3},{"hc-key":"us-mi-081","value":3},{"hc-key":"us-nc-147","value":2},{"hc-key":"us-nc-065","value":0},{"hc-key":"us-mt-009","value":1},{"hc-key":"us-mt-111","value":1},{"hc-key":"us-ne-039","value":2},{"hc-key":"us-ga-001","value":2},{"hc-key":"us-ia-099","value":0},{"hc-key":"us-ms-153","value":2},{"hc-key":"us-ms-023","value":3},{"hc-key":"us-tx-155","value":0},{"hc-key":"us-tx-275","value":3},{"hc-key":"us-fl-105","value":0},{"hc-key":"us-al-087","value":2},{"hc-key":"us-al-123","value":0},{"hc-key":"us-or-043","value":2},{"hc-key":"us-or-017","value":0},{"hc-key":"us-ia-155","value":0},{"hc-key":"us-ia-145","value":1},{"hc-key":"us-mo-229","value":0},{"hc-key":"us-ny-107","value":1},{"hc-key":"us-tx-287","value":1},{"hc-key":"us-ga-121","value":1},{"hc-key":"us-tx-073","value":3},{"hc-key":"us-mo-007","value":1},{"hc-key":"us-sc-029","value":1},{"hc-key":"us-ar-055","value":0},{"hc-key":"us-tx-191","value":3},{"hc-key":"us-wi-013","value":3},{"hc-key":"us-wi-129","value":2},{"hc-key":"us-nc-141","value":3},{"hc-key":"us-nc-061","value":0},{"hc-key":"us-tx-221","value":3},{"hc-key":"us-sd-117","value":0},{"hc-key":"us-sd-119","value":1},{"hc-key":"us-nv-001","value":1},{"hc-key":"us-nv-031","value":3},{"hc-key":"us-ky-007","value":0},{"hc-key":"us-tx-009","value":0},{"hc-key":"us-tx-503","value":0},{"hc-key":"us-tx-023","value":3},{"hc-key":"us-tx-405","value":1},{"hc-key":"us-nv-029","value":3},{"hc-key":"us-ct-001","value":0},{"hc-key":"us-ca-009","value":3},{"hc-key":"us-ca-077","value":2},{"hc-key":"us-ms-159","value":2},{"hc-key":"us-ky-167","value":1},{"hc-key":"us-ne-183","value":3},{"hc-key":"us-ky-135","value":1},{"hc-key":"us-mn-149","value":1},{"hc-key":"us-ms-053","value":3},{"hc-key":"us-ms-051","value":3},{"hc-key":"us-al-025","value":1},{"hc-key":"us-ks-017","value":0},{"hc-key":"us-ky-143","value":1},{"hc-key":"us-ky-033","value":2},{"hc-key":"us-ky-221","value":3},{"hc-key":"us-sd-003","value":2},{"hc-key":"us-ne-061","value":0},{"hc-key":"us-ks-037","value":1},{"hc-key":"us-mn-023","value":0},{"hc-key":"us-sc-049","value":3},{"hc-key":"us-sc-013","value":0},{"hc-key":"us-id-043","value":0},{"hc-key":"us-va-053","value":2},{"hc-key":"us-va-081","value":1},{"hc-key":"us-fl-111","value":1},{"hc-key":"us-ny-073","value":1},{"hc-key":"us-ny-055","value":2},{"hc-key":"us-sd-083","value":0},{"hc-key":"us-wi-017","value":3},{"hc-key":"us-ky-157","value":2},{"hc-key":"us-ga-065","value":0},{"hc-key":"us-ga-185","value":2},{"hc-key":"us-mi-099","value":0},{"hc-key":"us-tx-331","value":3},{"hc-key":"us-ky-087","value":1},{"hc-key":"us-ky-099","value":2},{"hc-key":"us-ky-137","value":3},{"hc-key":"us-va-710","value":2},{"hc-key":"us-va-155","value":3},{"hc-key":"us-nc-179","value":3},{"hc-key":"us-nd-061","value":0},{"hc-key":"us-nd-053","value":3},{"hc-key":"us-fl-091","value":1},{"hc-key":"us-ca-001","value":2},{"hc-key":"us-il-013","value":1},{"hc-key":"us-in-085","value":1},{"hc-key":"us-ms-103","value":0},{"hc-key":"us-mt-045","value":3},{"hc-key":"us-nc-121","value":2},{"hc-key":"us-or-053","value":0},{"hc-key":"us-or-065","value":0},{"hc-key":"us-or-047","value":2},{"hc-key":"us-ia-173","value":1},{"hc-key":"us-oh-039","value":0},{"hc-key":"us-wv-039","value":0},{"hc-key":"us-wv-019","value":2},{"hc-key":"us-wv-025","value":0},{"hc-key":"us-il-121","value":1},{"hc-key":"us-va-051","value":2},{"hc-key":"us-va-007","value":1},{"hc-key":"us-ga-243","value":1},{"hc-key":"us-nm-029","value":1},{"hc-key":"us-tx-355","value":0},{"hc-key":"us-tx-409","value":1},{"hc-key":"us-wy-045","value":1},{"hc-key":"us-sd-081","value":2},{"hc-key":"us-sd-093","value":1},{"hc-key":"us-sd-033","value":1},{"hc-key":"us-al-089","value":1},{"hc-key":"us-tn-189","value":3},{"hc-key":"us-tx-063","value":3},{"hc-key":"us-tx-459","value":1},{"hc-key":"us-ok-067","value":1},{"hc-key":"us-wv-085","value":0},{"hc-key":"us-wv-021","value":0},{"hc-key":"us-la-045","value":1},{"hc-key":"us-nc-133","value":0},{"hc-key":"us-tx-349","value":2},{"hc-key":"us-tx-161","value":0},{"hc-key":"us-pa-087","value":1},{"hc-key":"us-tx-399","value":1},{"hc-key":"us-tx-441","value":3},{"hc-key":"us-tx-465","value":3},{"hc-key":"us-ks-193","value":2},{"hc-key":"us-ga-161","value":2},{"hc-key":"us-ar-145","value":2},{"hc-key":"us-ms-083","value":0},{"hc-key":"us-mo-053","value":1},{"hc-key":"us-il-197","value":2},{"hc-key":"us-nm-009","value":3},{"hc-key":"us-tx-369","value":3},{"hc-key":"us-sd-011","value":0},{"hc-key":"us-ms-027","value":0},{"hc-key":"us-ia-057","value":2},{"hc-key":"us-ia-087","value":3},{"hc-key":"us-ia-163","value":1},{"hc-key":"us-ks-143","value":1},{"hc-key":"us-fl-023","value":1},{"hc-key":"us-ny-095","value":3},{"hc-key":"us-ny-025","value":0},{"hc-key":"us-ok-117","value":0},{"hc-key":"us-ny-007","value":2},{"hc-key":"us-oh-009","value":1},{"hc-key":"us-wv-083","value":0},{"hc-key":"us-ca-057","value":1},{"hc-key":"us-wi-031","value":1},{"hc-key":"us-mn-017","value":0},{"hc-key":"us-ky-199","value":3},{"hc-key":"us-tx-241","value":2},{"hc-key":"us-ut-031","value":0},{"hc-key":"us-ut-017","value":0},{"hc-key":"us-ut-001","value":0},{"hc-key":"us-tn-011","value":3},{"hc-key":"us-tn-139","value":0},{"hc-key":"us-ga-213","value":2},{"hc-key":"us-mi-107","value":3},{"hc-key":"us-mi-117","value":0},{"hc-key":"us-ny-083","value":1},{"hc-key":"us-ga-017","value":2},{"hc-key":"us-ga-271","value":0},{"hc-key":"us-ar-077","value":1},{"hc-key":"us-fl-003","value":0},{"hc-key":"us-fl-031","value":0},{"hc-key":"us-ga-173","value":1},{"hc-key":"us-al-015","value":2},{"hc-key":"us-ky-057","value":0},{"hc-key":"us-ms-107","value":0},{"hc-key":"us-ga-239","value":0},{"hc-key":"us-tx-199","value":0},{"hc-key":"us-ct-005","value":0},{"hc-key":"us-oh-143","value":0},{"hc-key":"us-oh-147","value":0},{"hc-key":"us-il-153","value":2},{"hc-key":"us-ok-107","value":1},{"hc-key":"us-mi-145","value":3},{"hc-key":"us-vt-015","value":3},{"hc-key":"us-vt-005","value":1},{"hc-key":"us-il-119","value":3},{"hc-key":"us-tx-085","value":0},{"hc-key":"us-ne-053","value":3},{"hc-key":"us-fl-115","value":3},{"hc-key":"us-fl-015","value":3},{"hc-key":"us-pa-091","value":1},{"hc-key":"us-id-061","value":1},{"hc-key":"us-nj-037","value":3},{"hc-key":"us-nj-027","value":0},{"hc-key":"us-mo-211","value":1},{"hc-key":"us-ne-029","value":3},{"hc-key":"us-oh-113","value":2},{"hc-key":"us-tx-447","value":3},{"hc-key":"us-ga-059","value":2},{"hc-key":"us-ga-221","value":2},{"hc-key":"us-mi-017","value":2},{"hc-key":"us-tn-157","value":3},{"hc-key":"us-ca-011","value":0},{"hc-key":"us-ca-007","value":3},{"hc-key":"us-mn-115","value":2},{"hc-key":"us-tx-021","value":3},{"hc-key":"us-wa-039","value":3},{"hc-key":"us-wa-005","value":2},{"hc-key":"us-wa-071","value":3},{"hc-key":"us-ks-205","value":1},{"hc-key":"us-ks-049","value":3},{"hc-key":"us-ks-125","value":1},{"hc-key":"us-la-043","value":3},{"hc-key":"us-la-069","value":0},{"hc-key":"us-mi-011","value":0},{"hc-key":"us-ia-111","value":2},{"hc-key":"us-ks-141","value":1},{"hc-key":"us-ks-105","value":1},{"hc-key":"us-ks-169","value":3},{"hc-key":"us-ks-053","value":3},{"hc-key":"us-ny-099","value":3},{"hc-key":"us-ny-123","value":3},{"hc-key":"us-ky-125","value":3},{"hc-key":"us-tn-121","value":3},{"hc-key":"us-mo-213","value":1},{"hc-key":"us-co-047","value":0},{"hc-key":"us-ok-087","value":2},{"hc-key":"us-wv-031","value":3},{"hc-key":"us-va-171","value":3},{"hc-key":"us-va-139","value":0},{"hc-key":"us-ok-061","value":3},{"hc-key":"us-wv-089","value":1},{"hc-key":"us-nj-013","value":0},{"hc-key":"us-mo-115","value":0},{"hc-key":"us-ga-209","value":2},{"hc-key":"us-or-055","value":1},{"hc-key":"us-ga-133","value":2},{"hc-key":"us-sd-017","value":1},{"hc-key":"us-sd-085","value":2},{"hc-key":"us-sd-053","value":3},{"hc-key":"us-tx-135","value":2},{"hc-key":"us-tx-475","value":1},{"hc-key":"us-tx-293","value":2},{"hc-key":"us-wv-037","value":2},{"hc-key":"us-wv-003","value":2},{"hc-key":"us-mt-067","value":3},{"hc-key":"us-tx-121","value":1},{"hc-key":"us-sc-021","value":2},{"hc-key":"us-nc-199","value":3},{"hc-key":"us-tx-387","value":0},{"hc-key":"us-ga-301","value":1},{"hc-key":"us-tx-197","value":3},{"hc-key":"us-mo-099","value":0},{"hc-key":"us-mo-186","value":1},{"hc-key":"us-ky-085","value":1},{"hc-key":"us-ga-149","value":0},{"hc-key":"us-al-079","value":0},{"hc-key":"us-al-043","value":2},{"hc-key":"us-ms-093","value":1},{"hc-key":"us-ks-025","value":0},{"hc-key":"us-ks-081","value":1},{"hc-key":"us-la-013","value":2},{"hc-key":"us-ga-295","value":1},{"hc-key":"us-ia-095","value":2},{"hc-key":"us-ne-019","value":1},{"hc-key":"us-ne-093","value":3},{"hc-key":"us-tx-357","value":1},{"hc-key":"us-wa-015","value":1},{"hc-key":"us-tx-297","value":1},{"hc-key":"us-mo-135","value":2},{"hc-key":"us-ok-043","value":2},{"hc-key":"us-nc-023","value":0},{"hc-key":"us-nc-191","value":1},{"hc-key":"us-ar-061","value":2},{"hc-key":"us-nc-059","value":0},{"hc-key":"us-nc-111","value":3},{"hc-key":"us-ar-103","value":3},{"hc-key":"us-ar-099","value":3},{"hc-key":"us-co-121","value":2},{"hc-key":"us-tx-385","value":2},{"hc-key":"us-oh-011","value":0},{"hc-key":"us-oh-107","value":0},{"hc-key":"us-in-001","value":3},{"hc-key":"us-ky-153","value":3},{"hc-key":"us-tx-017","value":1},{"hc-key":"us-mo-027","value":0},{"hc-key":"us-wy-029","value":0},{"hc-key":"us-pa-093","value":1},{"hc-key":"us-ny-081","value":2},{"hc-key":"us-mi-031","value":2},{"hc-key":"us-mi-051","value":3},{"hc-key":"us-mi-111","value":1},{"hc-key":"us-ky-173","value":0},{"hc-key":"us-sc-023","value":2},{"hc-key":"us-ky-123","value":2},{"hc-key":"us-ia-193","value":3},{"hc-key":"us-ia-093","value":0},{"hc-key":"us-fl-123","value":3},{"hc-key":"us-mo-201","value":2},{"hc-key":"us-il-003","value":0},{"hc-key":"us-il-181","value":0},{"hc-key":"us-mo-113","value":1},{"hc-key":"us-ga-051","value":1},{"hc-key":"us-fl-125","value":0},{"hc-key":"us-ny-037","value":1},{"hc-key":"us-ia-141","value":1},{"hc-key":"us-ia-041","value":1},{"hc-key":"us-sd-107","value":3},{"hc-key":"us-me-005","value":2},{"hc-key":"us-ks-095","value":0},{"hc-key":"us-ga-241","value":0},{"hc-key":"us-wv-097","value":2},{"hc-key":"us-ar-119","value":2},{"hc-key":"us-ia-047","value":3},{"hc-key":"us-oh-055","value":2},{"hc-key":"us-ny-117","value":2},{"hc-key":"us-va-163","value":2},{"hc-key":"us-nd-095","value":1},{"hc-key":"us-pa-045","value":1},{"hc-key":"us-vt-021","value":0},{"hc-key":"us-vt-003","value":2},{"hc-key":"us-fl-019","value":1},{"hc-key":"us-nc-183","value":2},{"hc-key":"us-oh-061","value":2},{"hc-key":"us-az-015","value":0},{"hc-key":"us-mt-055","value":3},{"hc-key":"us-ia-019","value":1},{"hc-key":"us-sd-047","value":0},{"hc-key":"us-mn-069","value":0},{"hc-key":"us-ga-219","value":1},{"hc-key":"us-tx-493","value":3},{"hc-key":"us-fl-079","value":3},{"hc-key":"us-in-153","value":3},{"hc-key":"us-ga-027","value":0},{"hc-key":"us-ks-185","value":0},{"hc-key":"us-ks-159","value":1},{"hc-key":"us-mi-115","value":2},{"hc-key":"us-ky-139","value":1},{"hc-key":"us-tn-103","value":2},{"hc-key":"us-ky-103","value":1},{"hc-key":"us-ky-211","value":0},{"hc-key":"us-pa-001","value":3},{"hc-key":"us-sc-009","value":0},{"hc-key":"us-ut-013","value":3},{"hc-key":"us-ut-047","value":3},{"hc-key":"us-ut-049","value":1},{"hc-key":"us-mo-141","value":1},{"hc-key":"us-mo-015","value":0},{"hc-key":"us-ia-143","value":0},{"hc-key":"us-in-079","value":0},{"hc-key":"us-in-143","value":1},{"hc-key":"us-id-083","value":0},{"hc-key":"us-il-165","value":1},{"hc-key":"us-sc-061","value":0},{"hc-key":"us-wy-019","value":1},{"hc-key":"us-mi-165","value":2},{"hc-key":"us-mi-055","value":2},{"hc-key":"us-mi-079","value":3},{"hc-key":"us-la-003","value":2},{"hc-key":"us-la-011","value":2},{"hc-key":"us-sd-069","value":0},{"hc-key":"us-sc-027","value":2},{"hc-key":"us-mt-095","value":0},{"hc-key":"us-ky-219","value":0},{"hc-key":"us-tn-183","value":1},{"hc-key":"us-tn-017","value":3},{"hc-key":"us-mo-077","value":2},{"hc-key":"us-ne-185","value":1},{"hc-key":"us-ne-159","value":0},{"hc-key":"us-ca-099","value":2},{"hc-key":"us-ca-047","value":2},{"hc-key":"us-ca-043","value":2},{"hc-key":"us-al-103","value":2},{"hc-key":"us-ky-027","value":1},{"hc-key":"us-ms-043","value":3},{"hc-key":"us-ky-213","value":1},{"hc-key":"us-vt-013","value":2},{"hc-key":"us-va-125","value":3},{"hc-key":"us-fl-093","value":3},{"hc-key":"us-fl-097","value":3},{"hc-key":"us-ar-053","value":3},{"hc-key":"us-ga-175","value":1},{"hc-key":"us-ar-007","value":1},{"hc-key":"us-ar-143","value":1},{"hc-key":"us-fl-127","value":2},{"hc-key":"us-fl-083","value":2},{"hc-key":"us-va-135","value":3},{"hc-key":"us-nj-041","value":1},{"hc-key":"us-ny-021","value":1},{"hc-key":"us-ok-023","value":0},{"hc-key":"us-ks-041","value":0},{"hc-key":"us-tx-427","value":1},{"hc-key":"us-ga-103","value":0},{"hc-key":"us-ok-129","value":1},{"hc-key":"us-wi-103","value":1},{"hc-key":"us-tx-079","value":1},{"hc-key":"us-al-075","value":3},{"hc-key":"us-tn-117","value":1},{"hc-key":"us-ar-087","value":1},{"hc-key":"us-pa-041","value":1},{"hc-key":"us-pa-055","value":0},{"hc-key":"us-pa-079","value":1},{"hc-key":"us-mi-039","value":1},{"hc-key":"us-al-113","value":1},{"hc-key":"us-va-065","value":2},{"hc-key":"us-il-097","value":2},{"hc-key":"us-ga-195","value":0},{"hc-key":"us-ar-039","value":0},{"hc-key":"us-co-057","value":1},{"hc-key":"us-id-001","value":2},{"hc-key":"us-wi-141","value":2},{"hc-key":"us-ny-013","value":0},{"hc-key":"us-tn-091","value":3},{"hc-key":"us-nc-009","value":0},{"hc-key":"us-va-195","value":2},{"hc-key":"us-mo-035","value":3},{"hc-key":"us-ok-013","value":0},{"hc-key":"us-ia-033","value":1},{"hc-key":"us-ky-205","value":0},{"hc-key":"us-mo-031","value":1},{"hc-key":"us-mo-133","value":2},{"hc-key":"us-ky-075","value":1},{"hc-key":"us-mo-143","value":2},{"hc-key":"us-ar-139","value":0},{"hc-key":"us-ar-013","value":3},{"hc-key":"us-ar-025","value":0},{"hc-key":"us-la-073","value":1},{"hc-key":"us-ia-007","value":1},{"hc-key":"us-mo-197","value":2},{"hc-key":"us-al-059","value":3},{"hc-key":"us-ky-041","value":3},{"hc-key":"us-tx-267","value":3},{"hc-key":"us-tx-095","value":3},{"hc-key":"us-tx-307","value":3},{"hc-key":"us-tx-217","value":1},{"hc-key":"us-ks-055","value":3},{"hc-key":"us-sc-039","value":1},{"hc-key":"us-mn-091","value":0},{"hc-key":"us-tn-155","value":3},{"hc-key":"us-tx-263","value":0},{"hc-key":"us-mt-099","value":1},{"hc-key":"us-ne-069","value":1},{"hc-key":"us-il-117","value":3},{"hc-key":"us-oh-141","value":0},{"hc-key":"us-il-195","value":3},{"hc-key":"us-tx-141","value":2},{"hc-key":"us-tx-229","value":2},{"hc-key":"us-tx-301","value":2},{"hc-key":"us-oh-167","value":3},{"hc-key":"us-mo-091","value":3},{"hc-key":"us-ks-047","value":1},{"hc-key":"us-in-061","value":0},{"hc-key":"us-in-043","value":0},{"hc-key":"us-ky-111","value":0},{"hc-key":"us-ky-029","value":0},{"hc-key":"us-in-019","value":3},{"hc-key":"us-il-129","value":0},{"hc-key":"us-wi-139","value":1},{"hc-key":"us-ks-079","value":2},{"hc-key":"us-ks-155","value":3},{"hc-key":"us-wv-091","value":3},{"hc-key":"us-ky-093","value":2},{"hc-key":"us-sd-057","value":2},{"hc-key":"us-wi-069","value":0},{"hc-key":"us-ny-033","value":0},{"hc-key":"us-wv-081","value":0},{"hc-key":"us-ca-065","value":2},{"hc-key":"us-ca-059","value":2},{"hc-key":"us-ca-073","value":0},{"hc-key":"us-az-023","value":3},{"hc-key":"us-il-101","value":3},{"hc-key":"us-in-125","value":0},{"hc-key":"us-ky-047","value":0},{"hc-key":"us-mi-045","value":0},{"hc-key":"us-ms-157","value":3},{"hc-key":"us-ms-079","value":1},{"hc-key":"us-ms-025","value":1},{"hc-key":"us-in-029","value":3},{"hc-key":"us-in-175","value":3},{"hc-key":"us-la-125","value":1},{"hc-key":"us-fl-063","value":3},{"hc-key":"us-ga-253","value":0},{"hc-key":"us-va-115","value":2},{"hc-key":"us-tx-283","value":0},{"hc-key":"us-mt-075","value":1},{"hc-key":"us-wi-091","value":1},{"hc-key":"us-mn-157","value":1},{"hc-key":"us-mo-121","value":2},{"hc-key":"us-tx-279","value":2},{"hc-key":"us-ks-097","value":3},{"hc-key":"us-tx-015","value":0},{"hc-key":"us-mo-061","value":1},{"hc-key":"us-ga-087","value":0},{"hc-key":"us-ga-201","value":1},{"hc-key":"us-mi-101","value":1},{"hc-key":"us-tx-195","value":0},{"hc-key":"us-tx-233","value":0},{"hc-key":"us-ks-129","value":1},{"hc-key":"us-ok-139","value":2},{"hc-key":"us-ok-153","value":2},{"hc-key":"us-ia-003","value":0},{"hc-key":"us-ca-101","value":3},{"hc-key":"us-ca-113","value":1},{"hc-key":"us-pa-105","value":2},{"hc-key":"us-ks-077","value":3},{"hc-key":"us-ia-133","value":3},{"hc-key":"us-ny-017","value":2},{"hc-key":"us-il-143","value":3},{"hc-key":"us-ia-091","value":3},{"hc-key":"us-pa-003","value":2},{"hc-key":"us-mo-205","value":3},{"hc-key":"us-oh-097","value":3},{"hc-key":"us-ks-201","value":1},{"hc-key":"us-id-087","value":2},{"hc-key":"us-mi-113","value":2},{"hc-key":"us-nd-093","value":2},{"hc-key":"us-mo-203","value":2},{"hc-key":"us-tx-423","value":3},{"hc-key":"us-ok-143","value":2},{"hc-key":"us-ok-091","value":3},{"hc-key":"us-tx-395","value":0},{"hc-key":"us-tx-171","value":1},{"hc-key":"us-mi-125","value":0},{"hc-key":"us-nd-033","value":1},{"hc-key":"us-wy-027","value":1},{"hc-key":"us-wa-025","value":2},{"hc-key":"us-mn-129","value":1},{"hc-key":"us-fl-001","value":2},{"hc-key":"us-ne-091","value":3},{"hc-key":"us-ut-007","value":1},{"hc-key":"us-pa-017","value":1},{"hc-key":"us-nj-019","value":0},{"hc-key":"us-sd-059","value":3},{"hc-key":"us-pa-081","value":1},{"hc-key":"us-id-057","value":3},{"hc-key":"us-va-640","value":3},{"hc-key":"us-nm-059","value":2},{"hc-key":"us-ok-025","value":3},{"hc-key":"us-tx-111","value":1},{"hc-key":"us-tx-269","value":3},{"hc-key":"us-tx-451","value":2},{"hc-key":"us-fl-049","value":2},{"hc-key":"us-ky-037","value":2},{"hc-key":"us-oh-025","value":1},{"hc-key":"us-tx-501","value":3},{"hc-key":"us-or-071","value":0},{"hc-key":"us-ia-115","value":2},{"hc-key":"us-oh-121","value":1},{"hc-key":"us-oh-119","value":2},{"hc-key":"us-pa-075","value":0},{"hc-key":"us-mn-087","value":1},{"hc-key":"us-nj-031","value":0},{"hc-key":"us-tx-033","value":1},{"hc-key":"us-ok-073","value":2},{"hc-key":"us-tx-051","value":2},{"hc-key":"us-ms-133","value":2},{"hc-key":"us-mn-053","value":3},{"hc-key":"us-pa-027","value":0},{"hc-key":"us-tx-025","value":1},{"hc-key":"us-ia-171","value":0},{"hc-key":"us-ne-147","value":2},{"hc-key":"us-il-021","value":0},{"hc-key":"us-fl-089","value":1},{"hc-key":"us-ga-007","value":1},{"hc-key":"us-ga-275","value":1},{"hc-key":"us-ky-005","value":3},{"hc-key":"us-ky-185","value":0},{"hc-key":"us-tn-109","value":1},{"hc-key":"us-vt-007","value":3},{"hc-key":"us-ia-139","value":0},{"hc-key":"us-ky-223","value":2},{"hc-key":"us-ga-055","value":1},{"hc-key":"us-sc-051","value":1},{"hc-key":"us-ks-131","value":0},{"hc-key":"us-ne-083","value":1},{"hc-key":"us-ks-179","value":3},{"hc-key":"us-ne-065","value":0},{"hc-key":"us-ky-009","value":2},{"hc-key":"us-nd-025","value":0},{"hc-key":"us-ca-005","value":2},{"hc-key":"us-tn-049","value":3},{"hc-key":"us-mi-023","value":0},{"hc-key":"us-va-031","value":0},{"hc-key":"us-oh-105","value":2},{"hc-key":"us-ok-053","value":3},{"hc-key":"us-sd-115","value":2},{"hc-key":"us-tx-455","value":0},{"hc-key":"us-ga-205","value":1},{"hc-key":"us-ky-147","value":1},{"hc-key":"us-ky-191","value":3},{"hc-key":"us-ky-215","value":1},{"hc-key":"us-ne-175","value":3},{"hc-key":"us-tx-069","value":1},{"hc-key":"us-md-043","value":2},{"hc-key":"us-ky-133","value":3},{"hc-key":"us-va-680","value":3},{"hc-key":"us-ar-125","value":2},{"hc-key":"us-wv-053","value":0},{"hc-key":"us-ms-063","value":2},{"hc-key":"us-nd-079","value":0},{"hc-key":"us-mt-103","value":1},{"hc-key":"us-ky-049","value":1},{"hc-key":"us-mi-005","value":1},{"hc-key":"us-tn-005","value":1},{"hc-key":"us-mn-015","value":0},{"hc-key":"us-va-109","value":0},{"hc-key":"us-in-145","value":2},{"hc-key":"us-oh-049","value":0},{"hc-key":"us-ks-093","value":3},{"hc-key":"us-ia-117","value":3},{"hc-key":"us-ky-239","value":3},{"hc-key":"us-sc-053","value":2},{"hc-key":"us-sc-065","value":2},{"hc-key":"us-sc-087","value":1},{"hc-key":"us-mn-103","value":1},{"hc-key":"us-wi-075","value":0},{"hc-key":"us-wa-011","value":2},{"hc-key":"us-wa-059","value":2},{"hc-key":"us-wi-051","value":3},{"hc-key":"us-la-123","value":1},{"hc-key":"us-fl-077","value":1},{"hc-key":"us-ks-083","value":3},{"hc-key":"us-ks-069","value":1},{"hc-key":"us-ks-057","value":1},{"hc-key":"us-ks-119","value":1},{"hc-key":"us-ms-001","value":2},{"hc-key":"us-ms-057","value":1},{"hc-key":"us-ky-073","value":3},{"hc-key":"us-ga-217","value":2},{"hc-key":"us-sd-049","value":3},{"hc-key":"us-mi-061","value":1},{"hc-key":"us-tn-127","value":3},{"hc-key":"us-la-029","value":1},{"hc-key":"us-la-063","value":2},{"hc-key":"us-ma-009","value":1},{"hc-key":"us-wv-009","value":0},{"hc-key":"us-wv-013","value":2},{"hc-key":"us-va-021","value":0},{"hc-key":"us-wv-043","value":2},{"hc-key":"us-ca-033","value":3},{"hc-key":"us-ga-131","value":0},{"hc-key":"us-co-021","value":0},{"hc-key":"us-ut-055","value":3},{"hc-key":"us-in-133","value":0},{"hc-key":"us-id-047","value":1},{"hc-key":"us-tn-167","value":1},{"hc-key":"us-tn-075","value":2},{"hc-key":"us-tn-097","value":3},{"hc-key":"us-mo-171","value":0},{"hc-key":"us-nd-059","value":3},{"hc-key":"us-nc-041","value":1},{"hc-key":"us-nc-021","value":1},{"hc-key":"us-nc-089","value":1},{"hc-key":"us-ca-063","value":1},{"hc-key":"us-ar-115","value":2},{"hc-key":"us-tx-183","value":1},{"hc-key":"us-ar-089","value":0},{"hc-key":"us-mo-187","value":2},{"hc-key":"us-co-025","value":2},{"hc-key":"us-tx-487","value":1},{"hc-key":"us-or-015","value":1},{"hc-key":"us-ut-051","value":2},{"hc-key":"us-ar-011","value":0},{"hc-key":"us-tx-193","value":1},{"hc-key":"us-mn-041","value":0},{"hc-key":"us-mt-077","value":3},{"hc-key":"us-mt-043","value":1},{"hc-key":"us-mt-007","value":3},{"hc-key":"us-mt-023","value":3},{"hc-key":"us-mt-039","value":2},{"hc-key":"us-mt-001","value":1},{"hc-key":"us-tn-001","value":3},{"hc-key":"us-oh-077","value":3},{"hc-key":"us-ga-005","value":0},{"hc-key":"us-de-001","value":0},{"hc-key":"us-il-095","value":1},{"hc-key":"us-mt-107","value":3},{"hc-key":"us-tn-015","value":0},{"hc-key":"us-ky-165","value":3},{"hc-key":"us-sd-089","value":1},{"hc-key":"us-co-051","value":2},{"hc-key":"us-mi-047","value":0},{"hc-key":"us-mn-151","value":1},{"hc-key":"us-ga-267","value":3},{"hc-key":"us-tx-429","value":0},{"hc-key":"us-nc-117","value":2},{"hc-key":"us-mt-081","value":0},{"hc-key":"us-ga-317","value":1},{"hc-key":"us-ca-081","value":0},{"hc-key":"us-oh-159","value":1},{"hc-key":"us-oh-035","value":0},{"hc-key":"us-oh-153","value":1},{"hc-key":"us-oh-103","value":0},{"hc-key":"us-tx-285","value":2},{"hc-key":"us-ok-105","value":2},{"hc-key":"us-ga-125","value":2},{"hc-key":"us-or-059","value":0},{"hc-key":"us-sd-113","value":2},{"hc-key":"us-ks-117","value":1},{"hc-key":"us-la-065","value":1},{"hc-key":"us-ga-313","value":3},{"hc-key":"us-ks-203","value":2},{"hc-key":"us-ne-027","value":2},{"hc-key":"us-il-111","value":3},{"hc-key":"us-mt-069","value":2},{"hc-key":"us-mo-033","value":1},{"hc-key":"us-wa-021","value":0},{"hc-key":"us-wa-075","value":0},{"hc-key":"us-ne-169","value":2},{"hc-key":"us-tn-033","value":0},{"hc-key":"us-tn-053","value":1},{"hc-key":"us-ia-079","value":1},{"hc-key":"us-ga-255","value":2},{"hc-key":"us-ks-107","value":3},{"hc-key":"us-mo-217","value":2},{"hc-key":"us-in-121","value":3},{"hc-key":"us-ia-069","value":0},{"hc-key":"us-ga-035","value":1},{"hc-key":"us-tx-035","value":3},{"hc-key":"us-nd-069","value":3},{"hc-key":"us-il-015","value":3},{"hc-key":"us-or-031","value":3},{"hc-key":"us-al-051","value":1},{"hc-key":"us-ga-187","value":2},{"hc-key":"us-oh-083","value":3},{"hc-key":"us-tx-175","value":1},{"hc-key":"us-fl-061","value":3},{"hc-key":"us-tx-065","value":0},{"hc-key":"us-tx-379","value":0},{"hc-key":"us-ks-067","value":1},{"hc-key":"us-va-105","value":2},{"hc-key":"us-nm-051","value":0},{"hc-key":"us-ne-123","value":0},{"hc-key":"us-ar-109","value":2},{"hc-key":"us-co-077","value":3},{"hc-key":"us-mo-065","value":2},{"hc-key":"us-va-185","value":0},{"hc-key":"us-pa-103","value":3},{"hc-key":"us-pa-115","value":0},{"hc-key":"us-tx-343","value":1},{"hc-key":"us-pa-125","value":3},{"hc-key":"us-vt-001","value":3},{"hc-key":"us-vt-023","value":3},{"hc-key":"us-ks-103","value":2},{"hc-key":"us-ks-087","value":1},{"hc-key":"us-mi-121","value":0},{"hc-key":"us-ms-049","value":0},{"hc-key":"us-tx-281","value":0},{"hc-key":"us-va-107","value":1},{"hc-key":"us-mo-185","value":2},{"hc-key":"us-mt-071","value":1},{"hc-key":"us-mo-129","value":3},{"hc-key":"us-nc-127","value":2},{"hc-key":"us-mo-159","value":1},{"hc-key":"us-mt-063","value":1},{"hc-key":"us-va-047","value":0},{"hc-key":"us-wv-069","value":1},{"hc-key":"us-va-165","value":0},{"hc-key":"us-in-183","value":3},{"hc-key":"us-ca-093","value":0},{"hc-key":"us-sd-037","value":3},{"hc-key":"us-la-071","value":1},{"hc-key":"us-al-127","value":0},{"hc-key":"us-mo-063","value":0},{"hc-key":"us-fl-039","value":1},{"hc-key":"us-ga-083","value":3},{"hc-key":"us-va-187","value":2},{"hc-key":"us-tn-113","value":2},{"hc-key":"us-co-033","value":2},{"hc-key":"us-oh-089","value":0},{"hc-key":"us-al-101","value":0},{"hc-key":"us-al-041","value":3},{"hc-key":"us-ms-067","value":3},{"hc-key":"us-wi-021","value":3},{"hc-key":"us-mn-029","value":0},{"hc-key":"us-ks-163","value":3},{"hc-key":"us-in-041","value":0},{"hc-key":"us-ne-079","value":0},{"hc-key":"us-ne-081","value":0},{"hc-key":"us-ne-115","value":3},{"hc-key":"us-al-007","value":1},{"hc-key":"us-al-021","value":0},{"hc-key":"us-ms-075","value":3},{"hc-key":"us-mn-011","value":1},{"hc-key":"us-mn-013","value":2},{"hc-key":"us-va-091","value":0},{"hc-key":"us-nh-017","value":1},{"hc-key":"us-mi-041","value":0},{"hc-key":"us-ar-131","value":1},{"hc-key":"us-ga-207","value":2},{"hc-key":"us-mo-069","value":2},{"hc-key":"us-ga-117","value":1},{"hc-key":"us-tx-249","value":1},{"hc-key":"us-mi-073","value":1},{"hc-key":"us-tx-421","value":0},{"hc-key":"us-tx-341","value":1},{"hc-key":"us-sc-017","value":0},{"hc-key":"us-wy-035","value":2},{"hc-key":"us-ky-069","value":0},{"hc-key":"us-wi-019","value":3},{"hc-key":"us-sc-091","value":2},{"hc-key":"us-pa-119","value":0},{"hc-key":"us-va-570","value":1},{"hc-key":"us-va-670","value":0},{"hc-key":"us-va-730","value":0},{"hc-key":"us-la-001","value":1},{"hc-key":"us-sd-123","value":1},{"hc-key":"us-ia-077","value":2},{"hc-key":"us-ia-183","value":0},{"hc-key":"us-sd-111","value":3},{"hc-key":"us-ok-051","value":0},{"hc-key":"us-wi-109","value":3},{"hc-key":"us-pa-121","value":2},{"hc-key":"us-tx-479","value":0},{"hc-key":"us-nc-123","value":3},{"hc-key":"us-me-003","value":2},{"hc-key":"us-ms-065","value":1},{"hc-key":"us-tx-031","value":2},{"hc-key":"us-va-025","value":0},{"hc-key":"us-tn-045","value":3},{"hc-key":"us-ar-075","value":2},{"hc-key":"us-nd-027","value":0},{"hc-key":"us-wi-057","value":3},{"hc-key":"us-wi-081","value":1},{"hc-key":"us-tn-151","value":2},{"hc-key":"us-sd-031","value":1},{"hc-key":"us-nc-181","value":2},{"hc-key":"us-sc-011","value":3},{"hc-key":"us-tn-039","value":3},{"hc-key":"us-al-055","value":1},{"hc-key":"us-id-025","value":1},{"hc-key":"us-tn-119","value":2},{"hc-key":"us-il-077","value":0},{"hc-key":"us-sc-043","value":1},{"hc-key":"us-ut-019","value":3},{"hc-key":"us-ut-045","value":1},{"hc-key":"us-va-650","value":1},{"hc-key":"us-wi-011","value":2},{"hc-key":"us-va-069","value":2},{"hc-key":"us-wv-041","value":3},{"hc-key":"us-mn-119","value":1},{"hc-key":"us-mn-127","value":0},{"hc-key":"us-tn-047","value":1},{"hc-key":"us-la-089","value":1},{"hc-key":"us-ar-111","value":2},{"hc-key":"us-fl-045","value":2},{"hc-key":"us-tn-063","value":2},{"hc-key":"us-tx-351","value":2},{"hc-key":"us-tx-219","value":0},{"hc-key":"us-nc-175","value":1},{"hc-key":"us-nc-171","value":0},{"hc-key":"us-al-031","value":3},{"hc-key":"us-id-027","value":0},{"hc-key":"us-fl-073","value":2},{"hc-key":"us-ok-119","value":1},{"hc-key":"us-al-039","value":2},{"hc-key":"us-ky-045","value":0},{"hc-key":"us-ky-169","value":0},{"hc-key":"us-ky-207","value":2},{"hc-key":"us-me-019","value":1},{"hc-key":"us-me-025","value":1},{"hc-key":"us-ky-179","value":3},{"hc-key":"us-ky-187","value":2},{"hc-key":"us-mo-155","value":0},{"hc-key":"us-ne-125","value":3},{"hc-key":"us-tx-365","value":1},{"hc-key":"us-ky-011","value":2},{"hc-key":"us-ia-167","value":2},{"hc-key":"us-ca-021","value":0},{"hc-key":"us-mo-510","value":2},{"hc-key":"us-sd-091","value":2},{"hc-key":"us-ne-107","value":1},{"hc-key":"us-mt-079","value":3},{"hc-key":"us-ia-005","value":1},{"hc-key":"us-tx-339","value":2},{"hc-key":"us-tx-319","value":1},{"hc-key":"us-nc-043","value":1},{"hc-key":"us-ny-093","value":2},{"hc-key":"us-ks-197","value":0},{"hc-key":"us-ky-181","value":1},{"hc-key":"us-wi-071","value":3},{"hc-key":"us-nc-047","value":0},{"hc-key":"us-oh-053","value":2},{"hc-key":"us-oh-031","value":1},{"hc-key":"us-tx-205","value":1},{"hc-key":"us-ia-189","value":1},{"hc-key":"us-tx-505","value":2},{"hc-key":"us-ky-019","value":1},{"hc-key":"us-mn-019","value":1},{"hc-key":"us-ne-037","value":2},{"hc-key":"us-sc-025","value":3},{"hc-key":"us-tn-101","value":1},{"hc-key":"us-or-067","value":1},{"hc-key":"us-va-810","value":2},{"hc-key":"us-mt-057","value":0},{"hc-key":"us-mt-093","value":1},{"hc-key":"us-va-103","value":1},{"hc-key":"us-mo-013","value":2},{"hc-key":"us-nh-001","value":0},{"hc-key":"us-va-159","value":3},{"hc-key":"us-ks-173","value":3},{"hc-key":"us-tx-453","value":2},{"hc-key":"us-fl-057","value":0},{"hc-key":"us-nj-021","value":1},{"hc-key":"us-pa-129","value":1},{"hc-key":"us-ia-029","value":1},{"hc-key":"us-ky-115","value":0},{"hc-key":"us-ri-003","value":2},{"hc-key":"us-vt-019","value":3},{"hc-key":"us-ks-085","value":0},{"hc-key":"us-la-041","value":2},{"hc-key":"us-ms-155","value":0},{"hc-key":"us-ma-011","value":3},{"hc-key":"us-ar-057","value":0},{"hc-key":"us-mo-029","value":2},{"hc-key":"us-va-117","value":2},{"hc-key":"us-ky-233","value":3},{"hc-key":"us-ok-047","value":2},{"hc-key":"us-mi-091","value":3},{"hc-key":"us-ga-123","value":1},{"hc-key":"us-ms-089","value":2},{"hc-key":"us-tx-227","value":0},{"hc-key":"us-wv-099","value":1},{"hc-key":"us-va-760","value":1},{"hc-key":"us-ca-079","value":0},{"hc-key":"us-tx-001","value":0},{"hc-key":"us-nd-071","value":1},{"hc-key":"us-co-029","value":0},{"hc-key":"us-nm-021","value":0},{"hc-key":"us-co-083","value":3},{"hc-key":"us-mt-049","value":0},{"hc-key":"us-tx-457","value":2},{"hc-key":"us-wa-077","value":3},{"hc-key":"us-tx-347","value":2},{"hc-key":"us-mt-089","value":0},{"hc-key":"us-nm-047","value":0},{"hc-key":"us-sd-103","value":3},{"hc-key":"us-ia-149","value":0},{"hc-key":"us-id-005","value":1},{"hc-key":"us-az-025","value":2},{"hc-key":"us-az-012","value":0},{"hc-key":"us-sd-065","value":3},{"hc-key":"us-ok-077","value":0},{"hc-key":"us-ny-067","value":1},{"hc-key":"us-mn-097","value":0},{"hc-key":"us-wa-001","value":3},{"hc-key":"us-co-039","value":2},{"hc-key":"us-al-073","value":1},{"hc-key":"us-nm-061","value":0},{"hc-key":"us-nv-009","value":1},{"hc-key":"us-or-011","value":3},{"hc-key":"us-ca-107","value":3},{"hc-key":"us-az-007","value":0},{"hc-key":"us-ks-003","value":2},{"hc-key":"us-wa-009","value":2},{"hc-key":"us-mi-083","value":2},{"hc-key":"us-mt-035","value":1},{"hc-key":"us-nm-005","value":2},{"hc-key":"us-mt-047","value":1},{"hc-key":"us-ca-025","value":2},{"hc-key":"us-mt-085","value":1},{"hc-key":"us-nd-065","value":1},{"hc-key":"us-me-021","value":0},{"hc-key":"us-mt-061","value":3},{"hc-key":"us-mt-021","value":3},{"hc-key":"us-mn-125","value":2},{"hc-key":"us-wy-017","value":2},{"hc-key":"us-hi-003","value":1},{"hc-key":"us-hi-007","value":3},{"hc-key":"us-hi-009","value":0},{"hc-key":"us-hi-001","value":0},{"hc-key":"us-hi-005","value":3},{"hc-key":"us-ak-110","value":1},{"hc-key":"us-ak-261","value":0},{"hc-key":"us-ak-070","value":3},{"hc-key":"us-ak-013","value":0},{"hc-key":"us-ak-180","value":1},{"hc-key":"us-ak-016","value":3},{"hc-key":"us-ak-150","value":0},{"hc-key":"us-ak-290","value":0},{"hc-key":"us-ak-170","value":2},{"hc-key":"us-ak-068","value":2},{"hc-key":"us-ak-105","value":2},{"hc-key":"us-ak-164","value":3},{"hc-key":"us-ak-060","value":0},{"hc-key":"us-ak-130","value":0},{"hc-key":"us-ak-195","value":3},{"hc-key":"us-ak-220","value":1},{"hc-key":"us-ak-090","value":2},{"hc-key":"us-ak-020","value":1},{"hc-key":"us-ak-198","value":3},{"hc-key":"us-ak-100","value":1},{"hc-key":"us-ak-122","value":0},{"hc-key":"us-ak-050","value":1},{"hc-key":"us-ak-230","value":1},{"hc-key":"us-ak-240","value":1},{"hc-key":"us-ak-188","value":2},{"hc-key":"us-ak-270","value":3},{"hc-key":"us-ak-185","value":1},{"hc-key":"us-ak-282","value":1},{"hc-key":"us-ak-275","value":3}];


Highcharts.mapChart('product105', {
        chart: {
            borderWidth: 0,
			      height:600,
            marginRight: 20, // for the legend
              backgroundColor: '#292929',
        },
        title: {
            text: ''
        },
        credits: false,
        legend: {
            layout: 'vertical',
            align: 'right',
            floating: true,
            itemStyle: {
              color: '#ffffff'
            },
            itemHoverStyle: {
              color: '#ffffff'
            },
            useHTML:true,
            backgroundColor: '#292929',
            borderWidth: 1,
            borderColor: '#fff',
        },
        mapNavigation: {
            enabled: true,
            enableButtons: true,
            enableDoubleClickZoomTo: false, 
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },
        colorAxis: {
            dataClassColor: 'category',
            dataClasses: [{
                name: 'HIDTA RISK',
                color: '#222',
                from: 0,
                to: 0
            }, {
                name: 'HIFCA RISK',
                color: '#444',
                from: 1,
                to: 1
            }, {
                name: 'HIDTA/HIFCA RISK',
                color: '#666',
                from: 2,
                to: 2
            }, {
                name: 'NO HIDTA or HIFCA RISK',
                color: '#888',
                from: 3,
                to: 3
            }]
        },
        tooltip: {
            formatter: function(){          
              if(this.point.value == 0){ return '<b>'+this.point.name+'</b><br/><br/><b>HIDTA:</b>YES<br/><b>HIFCA:</b>NO'; }
              if(this.point.value == 1){ return '<b>'+this.point.name+'</b><br/><br/><b>HIDTA:</b>NO<br/><b>HIFCA:</b>YES'; }
              if(this.point.value == 2){ return '<b>'+this.point.name+'</b><br/><br/><b>HIDTA:</b>YES<br/><b>HIFCA:</b>YES'; }
              if(this.point.value == 3){ return '<b>'+this.point.name+'</b><br/><br/><b>HIDTA:</b>NO<br/><b>HIFCA:</b>NO'; }
            }
        },
        plotOptions: {
            mapline: {
                showInLegend: false,
                enableMouseTracking: false
            },
            series: {
                point: {
                    events: {
                        select: function (a) {
                            $('.data-selection').hide();

                            var cData = {
                              county: a.target.name,
                              hidtaRisk: 'YES',
                              hifcaRisk: 'NO',
                              bothRisk: 'NO',
                              noRisk: 'NO',
                              zipCode: '89658', //**need to clarify
                              latLong: '35.25, -105.24',
                              region: 'Region IX',
                              specialNote: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt possimus non necessitatibus voluptate, maiores quaerat id illum sapiente autem blanditiis praesentium suscipit omnis?'
                            };

                            if(a.target.name == "Maricopa, AZ"){
                              cData.hidtaRisk = 'YES'; cData.hifcaRisk = 'YES'; cData.bothRisk = 'YES'; cData.noRisk = 'NO'; cData.zipCode = '85001, 85002, 85003, 85004, 85005, 85006, 85007, 85008, 85009, 85010, 85011, 85012, 85013, 85014, 85015, 85016, 85017, 85018, 85019, 85020, 85021, 85022, 85023, 85024, 85025, 85026, 85027, 85028, 85029, 85030, 85031, 85032, 85033, 85034, 85035, 85036, 85037, 85038, 85039, 85040, 85041, 85042, 85043, 85044, 85045, 85046, 85048, 85050, 85051, 85053, 85054, 85060, 85061, 85062, 85063, 85064, 85065, 85066, 85067, 85068, 85069, 85070, 85071, 85072, 85073, 85074, 85075, 85076, 85078, 85079, 85080, 85082, 85083, 85085, 85086, 85087, 85097, 85098, 85142, 85201, 85202, 85203, 85204, 85205, 85206, 85207, 85208, 85209, 85210, 85211, 85212, 85213, 85214, 85215, 85216, 85224, 85225, 85226, 85233, 85234, 85236, 85244, 85246, 85248, 85249, 85250, 85251, 85252, 85253, 85254, 85255, 85256, 85257, 85258, 85259, 85260, 85261, 85262, 85263, 85264, 85266, 85267, 85268, 85269, 85271, 85274, 85275, 85277, 85280, 85281, 85282, 85283, 85284, 85285, 85286, 85287, 85295, 85296, 85297, 85298, 85299, 85301, 85302, 85303, 85304, 85305, 85306, 85307, 85308, 85309, 85310, 85311, 85312, 85318, 85320, 85322, 85323, 85326, 85327, 85329, 85331, 85335, 85337, 85338, 85339, 85340, 85342, 85343, 85345, 85351, 85353, 85354, 85355, 85358, 85361, 85363, 85372, 85373, 85374, 85375, 85376, 85377, 85378, 85379, 85380, 85381, 85382, 85383, 85385, 85387, 85388, 85390, 85392, 85395, 85396,'; cData.latLong = '33.468, -112.3947'; cData.region = 'Region IX'; cData.specialNote = ' Maricopa County is a highly populated metro area (the city of Phoenix is located in Maricopa County and is a top 10 US city and Arizona’s capital) with close proximity to the US/Mexico border. As such, Maricopa County is disproportionally affected by illegal immigration, human smuggling, and drug trafficking.'; 
                            }
                            if(a.target.name == "Palm Beach, FL"){
                              cData.hidtaRisk = 'YES'; cData.hifcaRisk = 'YES'; cData.bothRisk = 'YES'; cData.noRisk = 'NO'; cData.zipCode = '33401, 33402, 33403, 33404, 33405, 33406, 33407, 33408, 33409, 33410, 33411, 33412, 33413, 33414, 33415, 33416, 33417, 33418, 33419, 33420, 33421, 33422, 33424, 33425, 33426, 33427, 33428, 33429, 33430, 33431, 33432, 33433, 33434, 33435, 33436, 33437, 33438, 33444, 33445, 33446, 33448, 33449, 33454, 33458, 33459, 33460, 33461, 33462, 33463, 33464, 33465, 33466, 33467, 33468, 33469, 33470, 33472, 33473, 33474, 33476, 33477, 33478, 33480, 33481, 33482, 33483, 33484, 33486, 33487, 33488, 33493, 33496, 33497, 33498, 33499'; cData.latLong = '26.7165, -80.0679'; cData.region = 'Region IV'; cData.specialNote = 'Palm Beach County stretches along Florida’s Atlantic coast and includes metro areas like West Palm Beach and Boca Raton. Palm Beach is the wealthiest county in Florida and has high profile high net worth individuals as residents. As such certain financial crimes are prevalent in this area.'; 
                            }
                            if(a.target.name == "New York, NY"){
                              cData.hidtaRisk = 'YES'; cData.hifcaRisk = 'YES'; cData.bothRisk = 'YES'; cData.noRisk = 'NO'; cData.zipCode = '10001, 10002, 10003, 10004, 10005, 10006, 10007, 10008, 10009, 10010, 10011, 10012, 10013, 10014, 10016, 10017, 10018, 10019, 10020, 10021, 10022, 10023, 10024, 10025, 10026, 10027, 10028, 10029, 10030, 10031, 10032, 10033, 10034, 10035, 10036, 10037, 10038, 10039, 10040, 10041, 10043, 10044, 10045, 10055, 10060, 10065, 10069, 10075, 10080, 10081, 10087, 10090, 10101, 10102, 10103, 10104, 10105, 10106, 10107, 10108, 10109, 10110, 10111, 10112, 10113, 10114, 10115, 10116, 10117, 10118, 10119, 10120, 10121, 10122, 10123, 10124, 10125, 10126, 10128, 10129, 10130, 10131, 10132, 10133, 10138, 10150, 10151, 10152, 10153, 10154, 10155, 10156, 10157, 10158, 10159, 10160, 10161, 10162, 10163, 10164, 10165, 10166, 10167, 10168, 10169, 10170, 10171, 10172, 10173, 10174, 10175, 10176, 10177, 10178, 10179, 10185, 10199, 10203, 10211, 10212, 10213, 10242, 10249, 10256, 10258, 10259, 10260, 10261, 10265, 10268, 10269, 10270, 10271, 10272, 10273, 10274, 10275, 10276, 10277, 10278, 10279, 10280, 10281, 10282, 10285, 10286'; cData.latLong = '26.7165, -80.0679'; cData.region = 'Region IV'; cData.specialNote = 'New York County encompasses New York City, NY and is considered a world financial center. The area is heavily populated by foreign banks operating in the US and other types of international systems and conduits (trade/logistics/ports). As such money laundering and other types of financial crimes (fraud and corruption) are prevalent in this area.'; 
                            }

                            var cardHtml = card105tmpl.render(cData);
              							$("#county-card").html(cardHtml);
              							$('#select-a-card').hide();
                            $('.data-selection').show();
                            $('#content').addClass('show');

                            a.target.zoomTo();
                            Highcharts.charts[0].mapZoom(5);
                            $("#country-search105").val(a.target.properties["hc-key"]).trigger("change");



                        }
                    }
                }
            }
        },
        series: [{
            mapData: countiesMap,
            data: cData,
            allowPointSelect: true,
            name: 'Risk Index',
            borderWidth: 0.5,
            states: {
                hover: {
                    color: '#d65527'
                },
                select: {
                    color: '#d65527'
                },
            }
        }, /*{
            type: 'mapline',
            name: 'State borders',
            data: borderLines,
            color: 'white'
        }, {
            type: 'mapline',
            name: 'Separator',
            data: separatorLines,
            color: 'gray'
        }*/]
    });
}

if(window.location.href.split('/').pop() == "product-106.php"){


    var p106Data = [{"id":"","orig":"","text":""},{"id":"AF","orig":"Afghanistan","text":"Afghanistan"},{"id":"AL","orig":"Albania","text":"Albania"},{"id":"DZ","orig":"Algeria","text":"Algeria"},{"id":"AS","orig":"American Samoa","text":"American Samoa"},{"id":"AD","orig":"Andorra","text":"Andorra"},{"id":"AO","orig":"Angola","text":"Angola"},{"id":"AG","orig":"Antigua and Barbuda","text":"Antigua and Barbuda"},{"id":"AR","orig":"Argentina","text":"Argentina"},{"id":"AM","orig":"Armenia","text":"Armenia"},{"id":"AU","orig":"Australia","text":"Australia"},{"id":"AT","orig":"Austria","text":"Austria"},{"id":"AZ","orig":"Azerbaijan","text":"Azerbaijan"},{"id":"BH","orig":"Bahrain","text":"Bahrain"},{"id":"BU","orig":"Bajo Nuevo Bank (Petrel Is.)","text":"Bajo Nuevo Bank (Petrel Is.)"},{"id":"BD","orig":"Bangladesh","text":"Bangladesh"},{"id":"BB","orig":"Barbados","text":"Barbados"},{"id":"BY","orig":"Belarus","text":"Belarus"},{"id":"BE","orig":"Belgium","text":"Belgium"},{"id":"BZ","orig":"Belize","text":"Belize"},{"id":"BJ","orig":"Benin","text":"Benin"},{"id":"BT","orig":"Bhutan","text":"Bhutan"},{"id":"BO","orig":"Bolivia","text":"Bolivia"},{"id":"BA","orig":"Bosnia and Herzegovina","text":"Bosnia and Herzegovina"},{"id":"BW","orig":"Botswana","text":"Botswana"},{"id":"BR","orig":"Brazil","text":"Brazil"},{"id":"BN","orig":"Brunei","text":"Brunei"},{"id":"BG","orig":"Bulgaria","text":"Bulgaria"},{"id":"BF","orig":"Burkina Faso","text":"Burkina Faso"},{"id":"BI","orig":"Burundi","text":"Burundi"},{"id":"KH","orig":"Cambodia","text":"Cambodia"},{"id":"CM","orig":"Cameroon","text":"Cameroon"},{"id":"CA","orig":"Canada","text":"Canada"},{"id":"CV","orig":"Cape Verde","text":"Cape Verde"},{"id":"CF","orig":"Central African Republic","text":"Central African Republic"},{"id":"TD","orig":"Chad","text":"Chad"},{"id":"CL","orig":"Chile","text":"Chile"},{"id":"CN","orig":"China","text":"China"},{"id":"CO","orig":"Colombia","text":"Colombia"},{"id":"KM","orig":"Comoros","text":"Comoros"},{"id":"CR","orig":"Costa Rica","text":"Costa Rica"},{"id":"HR","orig":"Croatia","text":"Croatia"},{"id":"CU","orig":"Cuba","text":"Cuba"},{"id":"CY","orig":"Cyprus","text":"Cyprus"},{"id":null,"orig":"Cyprus No Mans Area","text":"Cyprus No Mans Area"},{"id":"CZ","orig":"Czech Republic","text":"Czech Republic"},{"id":"CD","orig":"Democratic Republic of the Congo","text":"Democratic Republic of the Congo"},{"id":"DK","orig":"Denmark","text":"Denmark"},{"id":"DJ","orig":"Djibouti","text":"Djibouti"},{"id":"DM","orig":"Dominica","text":"Dominica"},{"id":"DO","orig":"Dominican Republic","text":"Dominican Republic"},{"id":"TL","orig":"East Timor","text":"East Timor"},{"id":"EC","orig":"Ecuador","text":"Ecuador"},{"id":"EG","orig":"Egypt","text":"Egypt"},{"id":"SV","orig":"El Salvador","text":"El Salvador"},{"id":"GQ","orig":"Equatorial Guinea","text":"Equatorial Guinea"},{"id":"ER","orig":"Eritrea","text":"Eritrea"},{"id":"EE","orig":"Estonia","text":"Estonia"},{"id":"ET","orig":"Ethiopia","text":"Ethiopia"},{"id":"FO","orig":"Faroe Islands","text":"Faroe Islands"},{"id":"FM","orig":"Federated States of Micronesia","text":"Federated States of Micronesia"},{"id":"FJ","orig":"Fiji","text":"Fiji"},{"id":"FI","orig":"Finland","text":"Finland"},{"id":"FR","orig":"France","text":"France"},{"id":"GA","orig":"Gabon","text":"Gabon"},{"id":"GM","orig":"Gambia","text":"Gambia"},{"id":"GZ","orig":"Gaza Strip","text":"Gaza Strip"},{"id":"GE","orig":"Georgia","text":"Georgia"},{"id":"DE","orig":"Germany","text":"Germany"},{"id":"GH","orig":"Ghana","text":"Ghana"},{"id":"GR","orig":"Greece","text":"Greece"},{"id":"GL","orig":"Greenland","text":"Greenland"},{"id":"GD","orig":"Grenada","text":"Grenada"},{"id":"GU","orig":"Guam","text":"Guam"},{"id":"GT","orig":"Guatemala","text":"Guatemala"},{"id":"GN","orig":"Guinea","text":"Guinea"},{"id":"GW","orig":"Guinea Bissau","text":"Guinea Bissau"},{"id":"GY","orig":"Guyana","text":"Guyana"},{"id":"HT","orig":"Haiti","text":"Haiti"},{"id":"HN","orig":"Honduras","text":"Honduras"},{"id":"HU","orig":"Hungary","text":"Hungary"},{"id":"IS","orig":"Iceland","text":"Iceland"},{"id":"IN","orig":"India","text":"India"},{"id":"ID","orig":"Indonesia","text":"Indonesia"},{"id":"IR","orig":"Iran","text":"Iran"},{"id":"IQ","orig":"Iraq","text":"Iraq"},{"id":"IE","orig":"Ireland","text":"Ireland"},{"id":"IL","orig":"Israel","text":"Israel"},{"id":"IT","orig":"Italy","text":"Italy"},{"id":"CI","orig":"Ivory Coast","text":"Ivory Coast"},{"id":"JM","orig":"Jamaica","text":"Jamaica"},{"id":"JP","orig":"Japan","text":"Japan"},{"id":"JO","orig":"Jordan","text":"Jordan"},{"id":"KZ","orig":"Kazakhstan","text":"Kazakhstan"},{"id":"KE","orig":"Kenya","text":"Kenya"},{"id":"KI","orig":"Kiribati","text":"Kiribati"},{"id":"KV","orig":"Kosovo","text":"Kosovo"},{"id":"KW","orig":"Kuwait","text":"Kuwait"},{"id":"KG","orig":"Kyrgyzstan","text":"Kyrgyzstan"},{"id":"LA","orig":"Laos","text":"Laos"},{"id":"LV","orig":"Latvia","text":"Latvia"},{"id":"LB","orig":"Lebanon","text":"Lebanon"},{"id":"LS","orig":"Lesotho","text":"Lesotho"},{"id":"LR","orig":"Liberia","text":"Liberia"},{"id":"LY","orig":"Libya","text":"Libya"},{"id":"LI","orig":"Liechtenstein","text":"Liechtenstein"},{"id":"LT","orig":"Lithuania","text":"Lithuania"},{"id":"LU","orig":"Luxembourg","text":"Luxembourg"},{"id":"MK","orig":"Macedonia","text":"Macedonia"},{"id":"MG","orig":"Madagascar","text":"Madagascar"},{"id":"MW","orig":"Malawi","text":"Malawi"},{"id":"MY","orig":"Malaysia","text":"Malaysia"},{"id":"MV","orig":"Maldives","text":"Maldives"},{"id":"ML","orig":"Mali","text":"Mali"},{"id":"MT","orig":"Malta","text":"Malta"},{"id":"MH","orig":"Marshall Islands","text":"Marshall Islands"},{"id":"MR","orig":"Mauritania","text":"Mauritania"},{"id":"MU","orig":"Mauritius","text":"Mauritius"},{"id":"MX","orig":"Mexico","text":"Mexico"},{"id":"MD","orig":"Moldova","text":"Moldova"},{"id":"MC","orig":"Monaco","text":"Monaco"},{"id":"MN","orig":"Mongolia","text":"Mongolia"},{"id":"ME","orig":"Montenegro","text":"Montenegro"},{"id":"MA","orig":"Morocco","text":"Morocco"},{"id":"MZ","orig":"Mozambique","text":"Mozambique"},{"id":"MM","orig":"Myanmar","text":"Myanmar"},{"id":"NA","orig":"Namibia","text":"Namibia"},{"id":"NR","orig":"Nauru","text":"Nauru"},{"id":"NP","orig":"Nepal","text":"Nepal"},{"id":"NL","orig":"Netherlands","text":"Netherlands"},{"id":"NZ","orig":"New Zealand","text":"New Zealand"},{"id":"NI","orig":"Nicaragua","text":"Nicaragua"},{"id":"NE","orig":"Niger","text":"Niger"},{"id":"NG","orig":"Nigeria","text":"Nigeria"},{"id":"KP","orig":"North Korea","text":"North Korea"},{"id":"NC","orig":"Northern Cyprus","text":"Northern Cyprus"},{"id":"MP","orig":"Northern Mariana Islands","text":"Northern Mariana Islands"},{"id":"NO","orig":"Norway","text":"Norway"},{"id":"OM","orig":"Oman","text":"Oman"},{"id":"PK","orig":"Pakistan","text":"Pakistan"},{"id":"PW","orig":"Palau","text":"Palau"},{"id":"PA","orig":"Panama","text":"Panama"},{"id":"PG","orig":"Papua New Guinea","text":"Papua New Guinea"},{"id":"PY","orig":"Paraguay","text":"Paraguay"},{"id":"PE","orig":"Peru","text":"Peru"},{"id":"PH","orig":"Philippines","text":"Philippines"},{"id":"PL","orig":"Poland","text":"Poland"},{"id":"PT","orig":"Portugal","text":"Portugal"},{"id":"PR","orig":"Puerto Rico","text":"Puerto Rico"},{"id":"QA","orig":"Qatar","text":"Qatar"},{"id":"CG","orig":"Republic of Congo","text":"Republic of Congo"},{"id":"RS","orig":"Republic of Serbia","text":"Republic of Serbia"},{"id":"RO","orig":"Romania","text":"Romania"},{"id":"RU","orig":"Russia","text":"Russia"},{"id":"RW","orig":"Rwanda","text":"Rwanda"},{"id":"KN","orig":"Saint Kitts and Nevis","text":"Saint Kitts and Nevis"},{"id":"LC","orig":"Saint Lucia","text":"Saint Lucia"},{"id":"VC","orig":"Saint Vincent and the Grenadines","text":"Saint Vincent and the Grenadines"},{"id":"WS","orig":"Samoa","text":"Samoa"},{"id":"SM","orig":"San Marino","text":"San Marino"},{"id":"ST","orig":"Sao Tome and Principe","text":"Sao Tome and Principe"},{"id":"SA","orig":"Saudi Arabia","text":"Saudi Arabia"},{"id":"SH","orig":"Scarborough Reef","text":"Scarborough Reef"},{"id":"SN","orig":"Senegal","text":"Senegal"},{"id":"SW","orig":"Serranilla Bank","text":"Serranilla Bank"},{"id":"SC","orig":"Seychelles","text":"Seychelles"},{"id":"JK","orig":"Siachen Glacier","text":"Siachen Glacier"},{"id":"SL","orig":"Sierra Leone","text":"Sierra Leone"},{"id":"SG","orig":"Singapore","text":"Singapore"},{"id":"SK","orig":"Slovakia","text":"Slovakia"},{"id":"SI","orig":"Slovenia","text":"Slovenia"},{"id":"SB","orig":"Solomon Islands","text":"Solomon Islands"},{"id":"SO","orig":"Somalia","text":"Somalia"},{"id":"SX","orig":"Somaliland","text":"Somaliland"},{"id":"ZA","orig":"South Africa","text":"South Africa"},{"id":"KR","orig":"South Korea","text":"South Korea"},{"id":"SS","orig":"South Sudan","text":"South Sudan"},{"id":"ES","orig":"Spain","text":"Spain"},{"id":"SP","orig":"Spratly Islands","text":"Spratly Islands"},{"id":"LK","orig":"Sri Lanka","text":"Sri Lanka"},{"id":"SD","orig":"Sudan","text":"Sudan"},{"id":"SR","orig":"Suriname","text":"Suriname"},{"id":"SZ","orig":"Swaziland","text":"Swaziland"},{"id":"SE","orig":"Sweden","text":"Sweden"},{"id":"CH","orig":"Switzerland","text":"Switzerland"},{"id":"SY","orig":"Syria","text":"Syria"},{"id":"TW","orig":"Taiwan","text":"Taiwan"},{"id":"TJ","orig":"Tajikistan","text":"Tajikistan"},{"id":"TH","orig":"Thailand","text":"Thailand"},{"id":"BS","orig":"The Bahamas","text":"The Bahamas"},{"id":"TG","orig":"Togo","text":"Togo"},{"id":"TO","orig":"Tonga","text":"Tonga"},{"id":"TT","orig":"Trinidad and Tobago","text":"Trinidad and Tobago"},{"id":"TN","orig":"Tunisia","text":"Tunisia"},{"id":"TR","orig":"Turkey","text":"Turkey"},{"id":"TM","orig":"Turkmenistan","text":"Turkmenistan"},{"id":"TV","orig":"Tuvalu","text":"Tuvalu"},{"id":"UG","orig":"Uganda","text":"Uganda"},{"id":"UA","orig":"Ukraine","text":"Ukraine"},{"id":"AE","orig":"United Arab Emirates","text":"United Arab Emirates"},{"id":"GB","orig":"United Kingdom","text":"United Kingdom"},{"id":"TZ","orig":"United Republic of Tanzania","text":"United Republic of Tanzania"},{"id":"UM","orig":"United States Minor Outlying Islands","text":"United States Minor Outlying Islands"},{"id":"VI","orig":"United States Virgin Islands","text":"United States Virgin Islands"},{"id":"US","orig":"United States of America","text":"United States of America"},{"id":"UY","orig":"Uruguay","text":"Uruguay"},{"id":"UZ","orig":"Uzbekistan","text":"Uzbekistan"},{"id":"VU","orig":"Vanuatu","text":"Vanuatu"},{"id":"VA","orig":"Vatican","text":"Vatican"},{"id":"VE","orig":"Venezuela","text":"Venezuela"},{"id":"VN","orig":"Vietnam","text":"Vietnam"},{"id":"WE","orig":"West Bank","text":"West Bank"},{"id":"EH","orig":"Western Sahara","text":"Western Sahara"},{"id":"YE","orig":"Yemen","text":"Yemen"},{"id":"ZM","orig":"Zambia","text":"Zambia"},{"id":"ZW","orig":"Zimbabwe","text":"Zimbabwe"}];

    $("#country-search106").select2({
        placeholder: "Select a country",
        data: p106Data
    }).on('select2:select', function (e) {
        var data = e.params.data;
        for(var i = 0; i <  Highcharts.charts[0].series[0].points.length; i++){

           if(Highcharts.charts[0].series[0].points[i].properties["iso-a2"] == data.id){
            console.log(Highcharts.charts[0].series[0].points[i]);
              Highcharts.charts[0].series[0].points[i].zoomTo();

                  $('.data-selection').hide();

                  var riskClass = 'low-risk';
                  var riskText = 'LOW RISK';
                  if(Highcharts.charts[0].series[0].points[i].value > 33){ riskClass = 'medium-risk'; riskText = 'MID RISK'; }
                  if(Highcharts.charts[0].series[0].points[i].value > 66){ riskClass = 'high-risk'; riskText = 'HIGH RISK'; }


                    var cData = {
                      country: data.orig,
                      region: Highcharts.charts[0].series[0].points[i].properties["region-wb"],
                      capital: 'Capital',
                      riskIndex: Highcharts.charts[0].series[0].points[i].value,
                      riskText: riskText,
                      riskClass: riskClass,
                      cdml:"YES",
                      kyc:"YES",
                      rst:"YES",
                      fiu:"YES",
                      ssot:"NO",
                      illicit_activities:[{name:"Real Estate (Coastal Areas)"},{name:"Business Development Projects"}, {name:"Fraud"}],
                      fatf: "FATF-APG",
                      last_eval: "April 2012",
                      eval_results: "In ac justo dignissim, suscipit dui et, malesuada orci. Vivamus porta, odio a bibendum porta, diam libero laoreet justo, in ultrices orci lectus sit amet diam. Nulla facilisis auctor pellentesque. Aenean ut erat sapien. Aenean venenatis nisi placerat magna condimentum commodo. Donec facilisis, tellus at venenatis commodo, metus metus condimentum leo, vitae ultrices urna turpis ut est. Donec in venenatis ipsum.",
                    };

                    if(data.orig == "Albania"){
                      cData.capital = "Tirana";
                      cData.illicit_activities = [{name:"Real Estate (Coastal Areas)"},{name:"Business Development Projects"}, {name:"Gaming"}];
                      cData.riskIndex = 39.96; cData.riskClass = 'medium-risk'; cData.riskText = 'MODERATE RISK';
                      cData.fatf = "MONEYVAL";
                      cData.last_eval = "April 2011";
                      cData.eval_results = "Although Albania has made considerable progress to tackle money laundering (ML) and terrorist financing (TF) the risk of ML remains high. Albania has a history of organised crime with clan-based and hierarchically organised networks that are mainly involved in drug trafficking. The relative size of the cash-based informal economy facilitates the laundering and integration of proceeds of crime. The number of sectors identified with illegal practices, including illegal gambling establishments and exchange bureaus, as well as the vulnerabilities that relate to cross-border transportation of currency, also make Albania at risk for ML activity.";
                    }
                    if(data.orig == "Australia"){
                      cData.capital = "Canberra";
                      cData.illicit_activities = [{name:"Fraud"}];
                      cData.riskIndex = 22.18; cData.riskClass = 'low-risk'; cData.riskText = 'LOW RISK';
                      cData.fatf = "FATF-APG";
                      cData.last_eval = "April 2015";
                      cData.eval_results = "Overall, Australian authorities have a good understanding of most of Australia’s main money laundering (ML) risks but need to develop their understanding further in certain areas. They coordinate very well activities to address key aspects of the ML / terrorist financing (TF) risks but some key risks remain unaddressed, and an underlying concern remains that the authorities are addressing predicate crime rather than ML.";
                    }
                    if(data.orig == "Egypt"){
                      cData.capital = "Cairo";
                      cData.illicit_activities = [{name:"Major Hub for Money Laundering"}, {name:"Remains vulnerable by virtue of its large informal cash-based economy"},{name:"Estimated as much as 90% of the population does not have bank accounts"}];
                      cData.riskIndex = 50.24; cData.riskClass = 'medium-risk'; cData.riskText = 'MODERATE RISK';
                      cData.fatf = "MENAFATF";
                      cData.last_eval = "November 2014";
                      cData.eval_results = "The Government of Egypt has taken significant steps to set up an AML/CFT regime, compared to 2002, when none existed. The AML/CFT Law criminalizes money laundering in Egypt, the material elements are broadly in line with the Palermo and Vienna Conventions, but participation in some forms of organized crime and adult human trafficking are not criminalized. Terrorism financing is criminalized in the Penal code, but its provisions capture neither the financing of an individual terrorist, nor the collection of funds with the unlawful intention that they should be used or in the knowledge that they are to be used to carry out a terrorist act or acts. With regard to implementation of United Nations Security Council Resolutions, information provided to support the authorities’ claim of implementing the Resolutions did not fully meet the requirements set out by the methodology.";
                    }
                    if(data.orig == "Ghana"){
                      cData.capital = "Accra";
                      cData.illicit_activities = [{name:"Money Laundering"}, {name:"Narcotics trafficking"},{name:"Fraud and Public Corruption"},{name:"Romance Scams"},{name:"Advance-fee-fraud or similar schemes"}];
                      cData.riskIndex = 38.28; cData.riskClass = 'medium-risk'; cData.riskText = 'MODERATE RISK';
                      cData.fatf = "GIABA";
                      cData.last_eval = "May 2017";
                      cData.eval_results = "Relevant competent authorities in Ghana have a sound understanding of most of the ML/TF risks it faces which is informed by the good quality of the National Risk Assessment (NRA). Though a National Risk Assessment Action Plan (NRAAP) which seeks to prioritize the deficiencies identified in the NRA has been developed, a comprehensive national AML/CFT policy based on the risk identified in the NRA is yet to be developed. Overall, the banks in Ghana have a good understanding of ML/TF risks they face and the larger banks are strongest in their mitigation efforts. However, there is a significant difference in the level of understanding of the ML/TF risks and application of preventive measures between the banks and other institutions within the financial sector. More robust risk-based AML/CFT controls are needed to ensure that AML/CFT obligations are being adequately applied across the financial sector.";
                    }
                    if(data.orig == "Maldives"){
                      cData.capital = "Male";
                      cData.fiu = "NO";
                      cData.illicit_activities = [{name:"Forced labor"},{name:"Sex trafficking"}];
                      cData.riskIndex = 31.27; cData.riskClass = 'medium-risk'; cData.riskText = 'MODERATE RISK';
                      cData.fatf = "APG";
                      cData.last_eval = "January 2012";
                      cData.eval_results = "The financial sector of the Maldives, although small and not very developed, is susceptible to both money laundering and, to a lesser extent, terrorist financing. While the authorities do not have estimates of the size of the crime economy, anecdotal evidence suggest that trafficking in illegal drugs and corruption alone produce significant amounts of illegal funds. There are also indications that resources have been raised in the country to fund terrorists and terrorist activities abroad. 3. Over the last years, the Maldives has taken steps to lay down the foundations of an AML/CFT framework. Institutional measures have been taken to set up a financial intelligence unit (FIU), and laws have been passed to criminalize, albeit insufficiently, the laundering of the proceeds of drug-related offenses and impose basic AML/CFT preventive measures on banks. Regulations were adopted to address some AML/CFT aspects in the securities sector.";
                    }

                  var cardHtml = card106tmpl.render(cData);
                  $("#country-card").html(cardHtml);
				       $('#select-a-card').hide();
               $('.data-selection').show();

               $('#content').addClass('show');


           }
        }
    });






var data = [{"hc-key":"gl","value":63},{"hc-key":"sh","value":96},{"hc-key":"bu","value":57},{"hc-key":"lk","value":21},{"hc-key":"as","value":78},{"hc-key":"dk","value":94},{"hc-key":"fo","value":5},{"hc-key":"gu","value":99},{"hc-key":"mp","value":30},{"hc-key":"pr","value":64},{"hc-key":"um","value":66},{"hc-key":"us","value":11},{"hc-key":"vi","value":5},{"hc-key":"ca","value":0},{"hc-key":"st","value":83},{"hc-key":"jp","value":73},{"hc-key":"cv","value":9},{"hc-key":"dm","value":42},{"hc-key":"sc","value":35},{"hc-key":"jm","value":70},{"hc-key":"ws","value":35},{"hc-key":"om","value":27},{"hc-key":"in","value":93},{"hc-key":"vc","value":36},{"hc-key":"sb","value":36},{"hc-key":"lc","value":49},{"hc-key":"fr","value":68},{"hc-key":"nr","value":66},{"hc-key":"no","value":15},{"hc-key":"fm","value":74},{"hc-key":"kn","value":6},{"hc-key":"cn","value":76},{"hc-key":"bh","value":69},{"hc-key":"to","value":7},{"hc-key":"id","value":35},{"hc-key":"mu","value":14},{"hc-key":"se","value":21},{"hc-key":"tt","value":53},{"hc-key":"sw","value":84},{"hc-key":"bs","value":15},{"hc-key":"pw","value":78},{"hc-key":"ec","value":8},{"hc-key":"au","value":22.18},{"hc-key":"tv","value":70},{"hc-key":"mh","value":29},{"hc-key":"cl","value":42},{"hc-key":"ki","value":6},{"hc-key":"ph","value":29},{"hc-key":"gd","value":77},{"hc-key":"ee","value":23},{"hc-key":"ag","value":0},{"hc-key":"es","value":42},{"hc-key":"bb","value":57},{"hc-key":"it","value":86},{"hc-key":"mt","value":5},{"hc-key":"mv","value":31.27},{"hc-key":"sp","value":36},{"hc-key":"pg","value":16},{"hc-key":"vu","value":99},{"hc-key":"sg","value":52},{"hc-key":"gb","value":94},{"hc-key":"cy","value":51},{"hc-key":"gr","value":44},{"hc-key":"km","value":14},{"hc-key":"fj","value":12},{"hc-key":"ru","value":80},{"hc-key":"va","value":11},{"hc-key":"sm","value":66},{"hc-key":"am","value":30},{"hc-key":"az","value":58},{"hc-key":"ls","value":88},{"hc-key":"tj","value":12},{"hc-key":"ml","value":91},{"hc-key":"dz","value":71},{"hc-key":"co","value":55},{"hc-key":"tw","value":42},{"hc-key":"uz","value":12},{"hc-key":"tz","value":55},{"hc-key":"ar","value":47},{"hc-key":"sa","value":61},{"hc-key":"nl","value":39},{"hc-key":"ye","value":33},{"hc-key":"ae","value":97},{"hc-key":"bd","value":13},{"hc-key":"ch","value":6},{"hc-key":"pt","value":0},{"hc-key":"my","value":35},{"hc-key":"vn","value":66},{"hc-key":"br","value":61},{"hc-key":"pa","value":16},{"hc-key":"ng","value":35},{"hc-key":"tr","value":71},{"hc-key":"ir","value":16},{"hc-key":"ht","value":31},{"hc-key":"do","value":21},{"hc-key":"sl","value":11},{"hc-key":"sn","value":78},{"hc-key":"gw","value":28},{"hc-key":"hr","value":92},{"hc-key":"th","value":44},{"hc-key":"mx","value":36},{"hc-key":"tn","value":4},{"hc-key":"kw","value":4},{"hc-key":"de","value":60},{"hc-key":"mm","value":43},{"hc-key":"gq","value":36},{"hc-key":"cnm","value":53},{"hc-key":"nc","value":64},{"hc-key":"ie","value":22},{"hc-key":"kz","value":90},{"hc-key":"pl","value":43},{"hc-key":"lt","value":32},{"hc-key":"eg","value":50.24},{"hc-key":"ug","value":76},{"hc-key":"cd","value":72},{"hc-key":"mk","value":21},{"hc-key":"al","value":39.96},{"hc-key":"cm","value":92},{"hc-key":"bj","value":26},{"hc-key":"ge","value":95},{"hc-key":"tl","value":98},{"hc-key":"tm","value":33},{"hc-key":"kh","value":64},{"hc-key":"pe","value":28},{"hc-key":"mw","value":91},{"hc-key":"mn","value":70},{"hc-key":"ao","value":81},{"hc-key":"mz","value":37},{"hc-key":"za","value":79},{"hc-key":"cr","value":15},{"hc-key":"sv","value":39},{"hc-key":"ly","value":12},{"hc-key":"sd","value":75},{"hc-key":"kp","value":17},{"hc-key":"kr","value":13},{"hc-key":"gy","value":26},{"hc-key":"hn","value":37},{"hc-key":"ga","value":88},{"hc-key":"ni","value":21},{"hc-key":"et","value":33},{"hc-key":"so","value":72},{"hc-key":"ke","value":13},{"hc-key":"gh","value":38.28},{"hc-key":"si","value":23},{"hc-key":"gt","value":68},{"hc-key":"bz","value":44},{"hc-key":"ba","value":6},{"hc-key":"jo","value":79},{"hc-key":"we","value":10},{"hc-key":"il","value":49},{"hc-key":"zm","value":63},{"hc-key":"mc","value":30},{"hc-key":"uy","value":67},{"hc-key":"rw","value":7},{"hc-key":"bo","value":44},{"hc-key":"cg","value":80},{"hc-key":"eh","value":27},{"hc-key":"rs","value":57},{"hc-key":"me","value":93},{"hc-key":"tg","value":63},{"hc-key":"la","value":93},{"hc-key":"af","value":23},{"hc-key":"jk","value":96},{"hc-key":"pk","value":52},{"hc-key":"bg","value":69},{"hc-key":"ua","value":33},{"hc-key":"ro","value":48},{"hc-key":"qa","value":76},{"hc-key":"li","value":73},{"hc-key":"at","value":89},{"hc-key":"sk","value":36},{"hc-key":"sz","value":1},{"hc-key":"hu","value":53},{"hc-key":"ne","value":52},{"hc-key":"lu","value":27},{"hc-key":"ad","value":90},{"hc-key":"ci","value":40},{"hc-key":"lr","value":85},{"hc-key":"bn","value":54},{"hc-key":"mr","value":44},{"hc-key":"be","value":87},{"hc-key":"iq","value":99},{"hc-key":"gm","value":51},{"hc-key":"ma","value":84},{"hc-key":"td","value":6},{"hc-key":"kv","value":63},{"hc-key":"lb","value":6},{"hc-key":"sx","value":36},{"hc-key":"dj","value":53},{"hc-key":"er","value":55},{"hc-key":"bi","value":57},{"hc-key":"sr","value":33},{"hc-key":"gn","value":95},{"hc-key":"zw","value":78},{"hc-key":"py","value":1},{"hc-key":"by","value":42},{"hc-key":"lv","value":16},{"hc-key":"sy","value":87},{"hc-key":"bt","value":84},{"hc-key":"na","value":32},{"hc-key":"bf","value":27},{"hc-key":"cf","value":41},{"hc-key":"md","value":35},{"hc-key":"gz","value":27},{"hc-key":"ss","value":49},{"hc-key":"cz","value":63},{"hc-key":"nz","value":63},{"hc-key":"cu","value":97},{"hc-key":"fi","value":82},{"hc-key":"mg","value":3},{"hc-key":"ve","value":90},{"hc-key":"is","value":51},{"hc-key":"np","value":20},{"hc-key":"kg","value":44},{"hc-key":"bw","value":60}];

$('.data-selection').hide();
// Create the chart
Highcharts.mapChart('product106', {
	    legend: {
		margin:40,
		itemStyle: {
			color: '#ffffff'
		},
		itemHoverStyle: {
			color: '#ffffff'
		},
		//useHTML:true,
    title: 'RISK INDEX SCALE'
		
	},

    chart: {
        map: 'custom/world-palestine',
		backgroundColor: '#292929',
		height:600
    },

    title: {
        text: ''
    },
    credits:false,
    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },

    plotOptions: {
        series: {
            point: {
                events: {
                    select: function (a) {
                      a.target.zoomTo();
                      $("#country-search106").val(a.target.properties["iso-a2"]).trigger("change");

                  $('.data-selection').hide();
                  var riskClass = 'low-risk';
                  var riskText = 'LOW RISK';
                  if(a.target.value > 33){ riskClass = 'medium-risk'; riskText = 'MODERATE RISK';}
                  if(a.target.value > 66){ riskClass = 'high-risk'; riskText = 'HIGH RISK';}

                    var cData = {
                      country: a.target.name,
                      region: a.target.properties["region-wb"],
                      capital: 'Capital',
                      riskIndex: a.target.value,
                      riskText: riskText,
                      riskClass: riskClass,
                      cdml:"YES",
                      kyc:"YES",
                      rst:"YES",
                      fiu:"YES",
                      ssot:"NO",
                      illicit_activities:[{name:"Real Estate (Coastal Areas)"},{name:"Business Development Projects"}, {name:"Fraud"}],
                      fatf: "FATF-APG",
                      last_eval: "April 2012",
                      eval_results: "In ac justo dignissim, suscipit dui et, malesuada orci. Vivamus porta, odio a bibendum porta, diam libero laoreet justo, in ultrices orci lectus sit amet diam. Nulla facilisis auctor pellentesque. Aenean ut erat sapien. Aenean venenatis nisi placerat magna condimentum commodo. Donec facilisis, tellus at venenatis commodo, metus metus condimentum leo, vitae ultrices urna turpis ut est. Donec in venenatis ipsum.",
                    };

                    if(a.target.name == "Albania"){
                      cData.capital = "Tirana";
                      cData.illicit_activities = [{name:"Real Estate (Coastal Areas)"},{name:"Business Development Projects"}, {name:"Gaming"}];
                      cData.riskIndex = 39.96; cData.riskClass = 'medium-risk'; cData.riskText = 'MODERATE RISK';
                      cData.fatf = "MONEYVAL";
                      cData.last_eval = "April 2011";
                      cData.eval_results = "Although Albania has made considerable progress to tackle money laundering (ML) and terrorist financing (TF) the risk of ML remains high. Albania has a history of organised crime with clan-based and hierarchically organised networks that are mainly involved in drug trafficking. The relative size of the cash-based informal economy facilitates the laundering and integration of proceeds of crime. The number of sectors identified with illegal practices, including illegal gambling establishments and exchange bureaus, as well as the vulnerabilities that relate to cross-border transportation of currency, also make Albania at risk for ML activity.";
                    }
                    if(a.target.name == "Australia"){
                      cData.capital = "Canberra";
                      cData.illicit_activities = [{name:"Fraud"}];
                      cData.riskIndex = 22.18; cData.riskClass = 'low-risk'; cData.riskText = 'LOW RISK';
                      cData.fatf = "FATF-APG";
                      cData.last_eval = "April 2015";
                      cData.eval_results = "Overall, Australian authorities have a good understanding of most of Australia’s main money laundering (ML) risks but need to develop their understanding further in certain areas. They coordinate very well activities to address key aspects of the ML / terrorist financing (TF) risks but some key risks remain unaddressed, and an underlying concern remains that the authorities are addressing predicate crime rather than ML.";
                    }
                    if(a.target.name == "Egypt"){
                      cData.capital = "Cairo";
                      cData.illicit_activities = [{name:"Major Hub for Money Laundering"}, {name:"Remains vulnerable by virtue of its large informal cash-based economy"},{name:"Estimated as much as 90% of the population does not have bank accounts"}];
                      cData.riskIndex = 50.24; cData.riskClass = 'medium-risk'; cData.riskText = 'MODERATE RISK';
                      cData.fatf = "MENAFATF";
                      cData.last_eval = "November 2014";
                      cData.eval_results = "The Government of Egypt has taken significant steps to set up an AML/CFT regime, compared to 2002, when none existed. The AML/CFT Law criminalizes money laundering in Egypt, the material elements are broadly in line with the Palermo and Vienna Conventions, but participation in some forms of organized crime and adult human trafficking are not criminalized. Terrorism financing is criminalized in the Penal code, but its provisions capture neither the financing of an individual terrorist, nor the collection of funds with the unlawful intention that they should be used or in the knowledge that they are to be used to carry out a terrorist act or acts. With regard to implementation of United Nations Security Council Resolutions, information provided to support the authorities’ claim of implementing the Resolutions did not fully meet the requirements set out by the methodology.";
                    }
                    if(a.target.name == "Ghana"){
                      cData.capital = "Accra";
                      cData.illicit_activities = [{name:"Money Laundering"}, {name:"Narcotics trafficking"},{name:"Fraud and Public Corruption"},{name:"Romance Scams"},{name:"Advance-fee-fraud or similar schemes"}];
                      cData.riskIndex = 38.28; cData.riskClass = 'medium-risk'; cData.riskText = 'MODERATE RISK';
                      cData.fatf = "GIABA";
                      cData.last_eval = "May 2017";
                      cData.eval_results = "Relevant competent authorities in Ghana have a sound understanding of most of the ML/TF risks it faces which is informed by the good quality of the National Risk Assessment (NRA). Though a National Risk Assessment Action Plan (NRAAP) which seeks to prioritize the deficiencies identified in the NRA has been developed, a comprehensive national AML/CFT policy based on the risk identified in the NRA is yet to be developed. Overall, the banks in Ghana have a good understanding of ML/TF risks they face and the larger banks are strongest in their mitigation efforts. However, there is a significant difference in the level of understanding of the ML/TF risks and application of preventive measures between the banks and other institutions within the financial sector. More robust risk-based AML/CFT controls are needed to ensure that AML/CFT obligations are being adequately applied across the financial sector.";
                    }
                    if(a.target.name == "Maldives"){
                      cData.capital = "Male";
                      cData.fiu = "NO";
                      cData.illicit_activities = [{name:"Forced labor"},{name:"Sex trafficking"}];
                      cData.riskIndex = 31.27; cData.riskClass = 'medium-risk'; cData.riskText = 'MODERATE RISK';
                      cData.fatf = "APG";
                      cData.last_eval = "January 2012";
                      cData.eval_results = "The financial sector of the Maldives, although small and not very developed, is susceptible to both money laundering and, to a lesser extent, terrorist financing. While the authorities do not have estimates of the size of the crime economy, anecdotal evidence suggest that trafficking in illegal drugs and corruption alone produce significant amounts of illegal funds. There are also indications that resources have been raised in the country to fund terrorists and terrorist activities abroad. 3. Over the last years, the Maldives has taken steps to lay down the foundations of an AML/CFT framework. Institutional measures have been taken to set up a financial intelligence unit (FIU), and laws have been passed to criminalize, albeit insufficiently, the laundering of the proceeds of drug-related offenses and impose basic AML/CFT preventive measures on banks. Regulations were adopted to address some AML/CFT aspects in the securities sector.";
                    }


                  var cardHtml = card106tmpl.render(cData);
                  $("#country-card").html(cardHtml);
				      $('#select-a-card').hide();
               $('.data-selection').show();
               $('#content').addClass('show');



                    }
                }
            }
        }
    },
    tooltip: {
        backgroundColor: {
            linearGradient: [0, 0, 0, 60],
            stops: [
                [0, '#FFFFFF'],
                [1, '#E0E0E0']
            ]
        },
        borderWidth: 1,
        borderColor: '#AAA',
        borderRadius: 2,
        formatter: function () {
            if(this.point.value <= 33){ return '<b>'+this.point.name+':</b> LOW RISK '+this.point.value+'/100'; }
            if(this.point.value >= 34 || this.point.value <= 66){ return '<b>'+this.point.name+':</b> MODERATE RISK '+this.point.value+'/100'; }
            if(this.point.value >= 66 || this.point.value <= 100){ return '<b>'+this.point.name+':</b> HIGH RISK '+this.point.value+'/100'; }
        }
    },
            colorAxis: {
                dataClassColor: 'category',
                dataClasses: [{
                    name: 'LOW RISK (0-33)',
                    color: '#66af46',
                    from: 0,
                    to: 33
                }, {
                    name: 'MODERATE RISK (34-66)',
                    color: '#ffa500',
                    from: 34,
                    to: 66
                }, {
                    name: 'HIGH RISK (66-100)',
                    color: '#ff0000',
                    from: 66,
                    to: 100
                }]
            },

    series: [{
        data: data,
        name: 'Risk Index',
        allowPointSelect: true,
        states: {
            hover: {
                color: '#d65527'
            },
            select: {
                color: '#d65527'
            }
        },
        dataLabels: {
            enabled: true,
            format: '{point.name}'
        }
    }]
});

}

	
$(function(){
    

	
	


});	
	
	
	
	
});
	
	

</script>


</body>
</html>