<?php include("header.php"); ?>

<div id="content">
	<div class="col-1">
		<div class="inner">
			<div id="welcome-dashboard" class="panel image-left" style="display:none;">
					<img src="img/data.png" alt="">
					<div>
						<h1>Intelligent Insights, Delivered</h1>
						<p>Welcome to THE DATA INITIATIVE. This application was built for you, by people just like you. We understand the information problems faced by financial crimes risk and compliance professionals. <br/><br/>
							THE DATA INITIATIVE eliminates the laborious and costly efforts of identifying, collecting, and analyzing data spread across the public domain. THE DATA INITIATIVE is a platform that unifies volumes of related but fragmented data, presents meaningful insights, and supports your daily decisions.
						</p>
						<a href="" class="btn"><i class="fa fa-play"></i> TAKE THE TOUR</a>					
					</div>
					<span>  
						<div class="checkbox">
						<label>
						  <input type="checkbox"> Hide this message on next login
						</label>
					  </div>

  </span>
			</div>
	
			<h3 class="head">100 Series - MONEY LAUNDERING DATA PACK</h3>

			<ul class="numbered-tiles">
				<li>
					<div class="inner">
						<a href="product-101.php">
							<table class="table">
								<tr>
									<td><span class="no">101</span></td>
									<td><h4>Global Money Laundering Statistics</h4></td>
								</tr>
							</table>
							
						</a>
					</div>
				</li>
				<li>
					<div class="inner">
						<a href="product-102.php">
							<table class="table">
								<tr>
									<td><span class="no">102</span></td>
									<td><h4>Global Money Laundering Laws and Regulations</h4></td>
								</tr>
							</table>
							
						</a>
					</div>
				</li>
				<li>
					<div class="inner">
						<a href="product-103.php">
							<table class="table">
								<tr>
									<td><span class="no">103</span></td>
									<td><h4>US Regulatory Guidance (FinCEN)</h4></td>
								</tr>
							</table>
							
						</a>
					</div>
				</li>
				<li>
					<div class="inner">
						<a href="product-104.php">
							<table class="table">
								<tr>
									<td><span class="no">104</span></td>
									<td><h4>US Money Laundering Enforcement Actions</h4></td>
								</tr>
							</table>
							
						</a>
					</div>
				</li>
				<li>
					<div class="inner">
						<a href="product-105.php">
							<table class="table">
								<tr>
									<td><span class="no">105</span></td>
									<td><h4>HIDTA HIFCA Risk Index</h4></td>
								</tr>
							</table>
						</a>
					</div>
				</li>
				<li>
					<div class="inner">
						<a href="product-106.php">
							<table class="table">
								<tr>
									<td><span class="no">106</span></td>
									<td><h4>Global Money Laundering Risk Index</h4></td>
								</tr>
							</table>
							
						</a>
					</div>
				</li>
			</ul>

			<h3 class="head">200 Series - GLOBAL SANCTIONS DATA PACK</h3>
			<span class="callout">Coming Soon</span>
		</div>	
	</div>
	<div id="dashboard-sidebar" class="col-2">
			<h3 class="head">RECENT BOOKMARKS</h3>
			<ul id="bookmark-notifications">
				<li><a href="">South Sudan Risk Index</a></li>
				<li><a href="">OFAC South Sudan Sanctions Program</a></li>
				<li><a href="">Advisory on Political Corruption Risks in South Sudan</a></li>
				<li><a href="">Laws of South Sudan</a></li>
				<li><a href="">Basel AML Index Report</a></li>
			</ul>
		<a href="" class="btn viewall">View all Bookmarks <i class="fa fa-long-arrow-right"></i></a>	
			<h3 class="head">UPDATES</h3>
			<ul id="posts-notification">
				<li>
					<span class="date">06.28.18</span>
					<a href=""><h4>Update: Scheduled Release of v.1.4 </h4></a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro voluptate et modi quos repellendus maiores, natus animi architecto quisquam ea. Quibusdam quasi amet totam.</p>
				</li>
				<li>
					<span class="date">06.23.18</span>
					<a href=""><h4>Update: 200 Series Data Pack Coming in 2019</h4></a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro voluptate et modi quos repellendus maiores, natus animi architecto quisquam ea. Quibusdam quasi amet totam.</p>
				</li>
				<li>
					<span class="date">06.21.18</span>
					<a href=""><h4>Update: 300 Series Data Pack Coming in 2019</h4></a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro voluptate et modi quos repellendus maiores, natus animi architecto quisquam ea. Quibusdam quasi amet totam.</p>
				</li>				

			</ul>		
			<a href="" class="btn viewall">View All Posts <i class="fa fa-long-arrow-right"></i></a>	
			
	</div>
</div>


<?php include("footer.php"); ?>

