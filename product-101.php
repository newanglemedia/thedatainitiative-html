<?php include("header.php"); ?>

<div id="breadcrumb">
	<a class="back" href="javascript:history.back();"><i class="fa fa-chevron-left"></i></a>
	<ul>
		<li><a href="">Dashboard</a>
		</li><li><a href="">Product 101</a></li>
	</ul>
</div>

<div id="content" class="prod-104">

	<div class="page-header">
		<div class="title">
			<span>Product 101</span>
			<h1>Global Money Laundering Statistics</h1>							
		</div>
	</div>

  <div id="prod-104-filters">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel">
          <div id="filters" class="pad">

            <span class="small-cap">FILTERS</span>

            <select class="option-set clearfix"  data-filter-group="institution" style="float:left !important;">
              <option value="*" data-filter-value="" class="selected">Select a dataset</option>
              <option value="" data-filter-value="">Global Money Laundering Trends</option>
              <option value="" data-filter-value="">Tactics, Techniques, Procedures</option>
            </select>

            <select class="filter option-set clearfix"  data-filter-group="bsa" style="float:left !important;">
              <option value="*" data-filter-value="" class="selected">Select an insight</option>
              <option value="" data-filter-value="">Illicit Financial Flows</option>
              <option value="" data-filter-value="">Human Trafficking</option>
              <option value="" data-filter-value="">Drug Trafficking</option>
              <option value="" data-filter-value="">Transparency</option>
              <option value="" data-filter-value="">Corruption</option>
              <option value="" data-filter-value="">Transnational Organized Crime</option>
              <option value="" data-filter-value="">Terrorist Financing</option>
            </select>

            <div class="search select2box" style="float:left !important;">
              <form>
                <select id="country-search101"></select>
              </form>           
            </div>

            <div class="date-range">
              <span>Dataset Range</span>
              <select name="" id="gtDate">
              </select>
              -
              <select name="" id="ltDate">
              </select>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

		
  <div class="inner">

    <div class="col-2" style="">
      <div class="inner">
        <div class="row">

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product101-gml'></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product101-iff'></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product101-ht'></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product101-dt'></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product101-tc'></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product101-fs'></div>
            </div>
          </div>

        </div>
      </div>	
    </div>

    <div class="col-1"  style="min-height:2500px !important;">
      <div class="row">
        <div class="col-sm-12">
          <div class="panel">
            <div id="sort">					
              SORT BY
              <select class="button-group sort-by-button-group">
                <option data-sort-by="year">Most Recent</option>
              </select>
            </div>	
            <h3 style="margin-top:10px;">DATASET - GLOBAL MONEY LAUNDERING TRENDS (57)</h3>
            <div class="grid"> </div>
          </div>
        </div>		
      </div>
    </div>

    <div id="inspector">
      <div id="report-card" class="data-selection ea-1">

      </div>
    </div>


  </div>
	
</div>

<?php include("footer.php"); ?>

