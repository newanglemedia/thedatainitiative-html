<?php include("header.php"); ?>

<div id="breadcrumb">
	<a class="back" href="javascript:history.back();"><i class="fa fa-chevron-left"></i></a>
	<ul>
		<li><a href="">Dashboard</a>
		</li><li><a href="https://newanglemedia.com/?utm_source=google&utm_medium=organic">Product 104</a></li>
	</ul>
</div>

<div id="content" class="prod-104">

	<div class="page-header">
		<div class="title">
			<span>Product 104</span>
			<h1>US Money Laundering Enforcement Actions</h1>						
		</div>
	</div>


	<div id="prod-104-filters">
			<div class="row">
				<div class="col-sm-12">
					<div class="panel">
<div id="filters" class="pad">

<span class="small-cap">FILTERS</span>
<!-- casinos,depository_institutions,money_services_businesses,precious_metals_jewelry_industry,securities_and_futures,no_information -->
<select class="option-set clearfix"  data-filter-group="entity">
    <option value="*" data-filter-value="" class="selected">All Entity Types</option>
    <option value="#filter-institution-di" data-filter-value=".depository_institutions">Depository Institutions</option>
    <option value="#filter-institution-casinos" data-filter-value=".casinos">Casinos</option>
    <option value="#filter-institution-msbs" data-filter-value=".money_services_businesses">Money Services Businesses</option>
    <option value="#filter-institution-precious-metals" data-filter-value=".precious_metalsjewelry_industry">Precious Metals/Jewelry Industry</option>
    <option value="#filter-institution-securities" data-filter-value=".securities_and_futures">Securities and Futures</option>
    <option value="#filter-institution-no-information" data-filter-value=".no_information">No Information</option>
</select>

<!-- "System of Internal Controls,Independent Testing of BSA Compliance,Use of All Available Information,Designation of a Qualified BSA Officer,BSA Training Program,Use of All Automated Systems,Indepdendent Testing of BSA Compliance,Recordkeeping,N/A,Designation of Qualified BSA Officer,Use of All Available Systems,Systen of Internal Controls" -->
<select class="filter option-set clearfix"  data-filter-group="pillars">
    <option value="*" data-filter-value="" class="selected">All Pillars Violated</option>
    <option value="" data-filter-value=".internal_controls">Internal Controls</option>
    <option value="" data-filter-value=".independent_testing">Independent Testing</option>
    <option value="" data-filter-value=".qualified_bsa_officer">Qualified BSA Officer</option>
    <option value="" data-filter-value=".training">Training</option>
</select>

<select class="filter option-set clearfix"  data-filter-group="bsa">
    <option value="*" data-filter-value="" class="selected">All BSA Program Areas</option>
    <option value="#filter-bsa-kyc" data-filter-value=".kyc">KYC</option>
    <option value="#filter-bsa-ctr" data-filter-value=".ctr">CTR</option>
    <option value="#filter-bsa-sar" data-filter-value=".sar">SAR</option>
    <option value="#filter-bsa-wlm" data-filter-value=".wlm">WLM</option>
</select>

<select class="filter option-set clearfix"  data-filter-group="subarea">
    <option value="*" data-filter-value="" class="selected">All BSA Sub-Program Area</option>
    <option value="" data-filter-value=".cip">Customer Identification Program</option>
    <option value="" data-filter-value=".cdd">Customer Due Diligence</option>
    <option value="" data-filter-value=".crr">Customer Risk Rating</option>
    <option value="" data-filter-value=".edd">Enhanced Due Diligence</option>
    <option value="" data-filter-value=".tranmon">Transaction Monitoring</option>
    <option value="" data-filter-value=".sarfile">SAR Filings</option>
    <option value="" data-filter-value=".ctrfile">CTR Filings</option>
    <option value="" data-filter-value=".redflag">Red Flags</option>
    <option value="" data-filter-value=".recordkeeping">Recordkeeping</option>
    <option value="" data-filter-value=".314a">314(a)</option>
    <option value="" data-filter-value=".314b">314(b)</option>
    <option value="" data-filter-value=".sanctions">Sanctions</option>
    <option value="" data-filter-value=".riskassesment">Risk Assessment</option>
    <option value="" data-filter-value=".coraccs">Correspondent Accounts</option>
</select>

<select class="filter option-set clearfix"  data-filter-group="assetsize">
    <option value="*" data-filter-value="" class="selected">All Asset Sizes</option>
    <option value="" data-filter-value=".Tier_I">Tier I</option>
    <option value="" data-filter-value=".Tier_II">Tier II</option>
    <option value="" data-filter-value=".Tier_III">Tier III</option>
    <option value="" data-filter-value=".Tier_IV">Tier IV</option>
    <option value="" data-filter-value=".Tier_V">Tier V</option>
</select>

<select class="filter option-set clearfix"  data-filter-group="penamont">
    <option value="*" data-filter-value="" class="selected">All Penalty Amounts</option>
    <option value="" data-filter-value=".pen1">Less than $25,000,000</option>
    <option value="" data-filter-value=".pen2">$25,000,000-$100,000,000</option>
    <option value="" data-filter-value=".pen3">Greater than $100,000,000</option>
</select>


<select class="filter option-set clearfix"  data-filter-group="state">
    <option value="*" data-filter-value="" class="selected">All States</option>
  <option data-filter-value=".AL">Alabama</option>
  <option data-filter-value=".AK">Alaska</option>
  <option data-filter-value=".AZ">Arizona</option>
  <option data-filter-value=".AR">Arkansas</option>
  <option data-filter-value=".CA">California</option>
  <option data-filter-value=".CO">Colorado</option>
  <option data-filter-value=".CT">Connecticut</option>
  <option data-filter-value=".DE">Delaware</option>
  <option data-filter-value=".DC">District Of Columbia</option>
  <option data-filter-value=".FL">Florida</option>
  <option data-filter-value=".GA">Georgia</option>
  <option data-filter-value=".HI">Hawaii</option>
  <option data-filter-value=".ID">Idaho</option>
  <option data-filter-value=".IL">Illinois</option>
  <option data-filter-value=".IN">Indiana</option>
  <option data-filter-value=".IA">Iowa</option>
  <option data-filter-value=".KS">Kansas</option>
  <option data-filter-value=".KY">Kentucky</option>
  <option data-filter-value=".LA">Louisiana</option>
  <option data-filter-value=".ME">Maine</option>
  <option data-filter-value=".MD">Maryland</option>
  <option data-filter-value=".MA">Massachusetts</option>
  <option data-filter-value=".MI">Michigan</option>
  <option data-filter-value=".MN">Minnesota</option>
  <option data-filter-value=".MS">Mississippi</option>
  <option data-filter-value=".MO">Missouri</option>
  <option data-filter-value=".MT">Montana</option>
  <option data-filter-value=".NE">Nebraska</option>
  <option data-filter-value=".NV">Nevada</option>
  <option data-filter-value=".NH">New Hampshire</option>
  <option data-filter-value=".NJ">New Jersey</option>
  <option data-filter-value=".NM">New Mexico</option>
  <option data-filter-value=".NY">New York</option>
  <option data-filter-value=".NC">North Carolina</option>
  <option data-filter-value=".ND">North Dakota</option>
  <option data-filter-value=".OH">Ohio</option>
  <option data-filter-value=".OK">Oklahoma</option>
  <option data-filter-value=".OR">Oregon</option>
  <option data-filter-value=".PA">Pennsylvania</option>
  <option data-filter-value=".RI">Rhode Island</option>
  <option data-filter-value=".SC">South Carolina</option>
  <option data-filter-value=".SD">South Dakota</option>
  <option data-filter-value=".TN">Tennessee</option>
  <option data-filter-value=".TX">Texas</option>
  <option data-filter-value=".UT">Utah</option>
  <option data-filter-value=".VT">Vermont</option>
  <option data-filter-value=".VA">Virginia</option>
  <option data-filter-value=".WA">Washington</option>
  <option data-filter-value=".WV">West Virginia</option>
  <option data-filter-value=".WI">Wisconsin</option>
  <option data-filter-value=".WY">Wyoming</option>

</select>

			<div class="date-range">
				<span>Dataset Range</span>
				<select name="" id="gtDate">
					<option value="1999" data-filter-value="greaterThanDate">1999</option>
					<option value="2000" data-filter-value="greaterThanDate">2000</option>
					<option value="2001" data-filter-value="greaterThanDate">2001</option>
					<option value="2002" data-filter-value="greaterThanDate">2002</option>
          <option value="2003" data-filter-value="greaterThanDate">2003</option>
          <option value="2004" data-filter-value="greaterThanDate">2004</option>          
          <option value="2005" data-filter-value="greaterThanDate">2005</option>
          <option value="2006" data-filter-value="greaterThanDate">2006</option>
          <option value="2007" data-filter-value="greaterThanDate">2007</option>
          <option value="2008" data-filter-value="greaterThanDate">2008</option>
          <option value="2009" data-filter-value="greaterThanDate">2009</option>
          <option value="2010" data-filter-value="greaterThanDate">2010</option>
          <option value="2011" data-filter-value="greaterThanDate">2011</option>   
          <option value="2012" data-filter-value="greaterThanDate">2012</option>   
          <option value="2013" data-filter-value="greaterThanDate">2013</option>   
          <option value="2014" data-filter-value="greaterThanDate">2014</option>   
          <option value="2015" data-filter-value="greaterThanDate">2015</option>   
          <option value="2016" data-filter-value="greaterThanDate">2016</option>   
          <option value="2017" data-filter-value="greaterThanDate">2017</option>   
          <option value="2018" data-filter-value="greaterThanDate">2018</option>   
				</select>
				-
				<select class="filter" name="" id="ltDate" data-filter-group="ltDate">
          <option value="1999" data-filter-value="lessThanDate">1999</option>
          <option value="2000" data-filter-value="lessThanDate">2000</option>
          <option value="2001" data-filter-value="lessThanDate">2001</option>
          <option value="2002" data-filter-value="lessThanDate">2002</option>
          <option value="2003" data-filter-value="lessThanDate">2003</option>
          <option value="2004" data-filter-value="lessThanDate">2004</option>          
          <option value="2005" data-filter-value="lessThanDate">2005</option>
          <option value="2006" data-filter-value="lessThanDate">2006</option>
          <option value="2007" data-filter-value="lessThanDate">2007</option>
          <option value="2008" data-filter-value="lessThanDate">2008</option>
          <option value="2009" data-filter-value="lessThanDate">2009</option>
          <option value="2010" data-filter-value="lessThanDate">2010</option>
          <option value="2011" data-filter-value="lessThanDate">2011</option>   
          <option value="2012" data-filter-value="lessThanDate">2012</option>   
          <option value="2013" data-filter-value="lessThanDate">2013</option>   
          <option value="2014" data-filter-value="lessThanDate">2014</option>   
          <option value="2015" data-filter-value="lessThanDate">2015</option>   
          <option value="2016" data-filter-value="lessThanDate">2016</option>   
          <option value="2017" data-filter-value="lessThanDate">2017</option>   
          <option value="2018" data-filter-value="lessThanDate" class="selected" selected="true">2018</option>   
				</select>
			</div>

</div>
						

					</div></div>
				</div>
			</div>

		
	<div class="inner">
		<div class="col-2" style="">
			<div class="inner">
				<div class="row">
          
					<div class="col-sm-12">
						<div class="stat-box bar panel">
							<div id='product104-number'></div>
							<div class="stat">
								<h3>Number of Actions</h3>
								<span class="actionCount">97</span>
							</div>
						</div>
					</div>				

					<div class="col-sm-12">
						<div class="stat-box panel">
							<div id='product104-fi'></div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="stat-box panel">
							<div id='product104-pillars'></div>
						</div>
					</div>					

					<div class="col-sm-12">
						<div class="stat-box panel">
							<div id='product104-bsa'></div>
						</div>
					</div>						

					<div class="col-sm-12">
						<div class="stat-box panel">
							<div id='product104-subprograms'></div>
						</div>
					</div>		

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product104-assetsize'></div>
            </div>
          </div>  

          <div class="col-sm-12">
            <div class="stat-box panel">
              <div id='product104-penaltyamount'></div>
            </div>
          </div> 

				</div>
			</div>	
		</div>
		<div class="col-1" style="min-height:2500px !important;">
				<div class="row">
				<div class="col-sm-12">
					<div class="panel">
					
					
					<div id="sort">					
	SORT BY
	<select class="button-group sort-by-button-group">
	  <option data-sort-by="year">Year</option>
	  <option data-sort-by="state">State</option>
	</select>
</div>	
					<h3 style="margin-top:10px;">DATASET - ENFORCEMENT ACTIONS (<span class="actionCount">97</span>)</h3>



<div class="grid">

</div>

					</div>
				</div>		
			</div>

	
	
	
	
	</div>
	
	
	<div id="inspector">
	
		<div id="action-card" class="data-selection ea-1">

		</div>		
	</div>
	
	
	</div>
	
	
	
</div>

<?php include("footer.php"); ?>

