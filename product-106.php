<?php include("header.php"); ?>

<div id="breadcrumb">
	<a class="back" href="javascript:history.back();"><i class="fa fa-chevron-left"></i></a>
	<ul>
		<li><a href="">Dashboard</a>
		</li><li><a href="">Product 106</a></li>
	</ul>
</div>


<div id="content" class="prod-map">

	<div class="page-header">
		<div class="title">
			<span>Product 106</span>
			<h1>Global Money Laundering Risk Index</h1>							
		</div>
	</div>

	<div class="inner">
		<div class="col-1">
			<div class="row">
				<div class="col-sm-12">
					<div class="panel">
						<div id="select-filters">
							<div class="inner">
								<h4>COUNTRY EXPLORER</h4>
								<div class="search select2box">
									<form>
										<select id="country-search106"></select>
									</form>						
								</div>
								<div id="product106"></div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>		
		<div class="col-2">
		
			<div id="select-a-card">
				<div class="inner">
					<i class="fa fa-search-plus"></i>
					<p>Select a country on the map to view more information</p>
				</div>
			</div>		
		
			<div id="country-card" class="data-selection" style="display:none;"></div>
		</div>	
	</div>	
</div>

<?php include("footer.php"); ?>

