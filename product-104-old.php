<?php include("header.php"); ?>

<div id="breadcrumb">
	<a class="back" href="javascript:history.back();"><i class="fa fa-chevron-left"></i></a>
	<ul>
		<li><a href="">Dashboard</a>
		</li><li><a href="https://newanglemedia.com/?utm_source=google&utm_medium=organic">Product 104</a></li>
	</ul>
</div>

<div id="content">
	<div class="col-1" style="">
		<div class="inner">
		
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header">

						<div class="date-range">
							<span>Dataset Range</span>
							<select name="" id="">
								<option value="">1999</option>
								<option value="">2000</option>
								<option value="">2001</option>
								<option value="">2002</option>
								<option value="">2003</option>
							</select>
							-
							<select name="" id="">
								<option value="">2018</option>
								<option value="">2017</option>
								<option value="">2016</option>
								<option value="">2015</option>
								<option value="">2014</option>
							</select>
						</div>
						<div class="title">
							<span>Product 104</span>
							<h1>US Money Laundering Enforcement Actions</h1>						
						</div>

					</div>
				</div>
			</div>

		

			<div class="row">
				<div class="col-sm-12">
					<div class="panel">
						<h3>97 Enforcement Actions</h3>
<div id="filters" class="pad">
<select class="option-set clearfix"  data-filter-group="institution">
    <option value="*" data-filter-value="" class="selected">All Institutions</option>
    <option value="#filter-institution-casinos" data-filter-value=".casinos">Casinos</option>
    <option value="#filter-institution-msbs" data-filter-value=".msbs">MSBs</option>
    <option value="#filter-institution-precious-metals" data-filter-value=".precious-metals">Precious Metals/Jewelry</option>
    <option value="#filter-institution-securities" data-filter-value=".securities">Securities and Futures</option>
    <option value="#filter-institution-no-information" data-filter-value=".no-information">No Information</option>
</select>

<select class="filter option-set clearfix"  data-filter-group="bsa">
    <option value="*" data-filter-value="" class="selected">All BSA Programs</option>
    <option value="#filter-bsa-kyc" data-filter-value=".kyc">KYC</option>
    <option value="#filter-bsa-ctr" data-filter-value=".ctr">CTR</option>
    <option value="#filter-bsa-sar" data-filter-value=".sar">SAR</option>
</select>
   
<select class="filter option-set clearfix"  data-filter-group="bsa">
    <option value="*" data-filter-value="" class="selected">By State</option>
</select>


</div>
						
<div id="sort">					
	SORT BY:
	<select class="button-group sort-by-button-group">
	  <option data-sort-by="year">Most Recent</option>
	  <option data-sort-by="penalty">Penalty Amount</option>
	  <option data-sort-by="name">Alpha</option>
	</select>
</div>	
					</div>
				</div>
			</div>
		


<!--
			<h3 class="head">HIGHLIGHTS</h3>
-->

			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="stat-box panel">
						<div id='product104-pe'></div>

					

						<div class="stat">
							<h3>Sum of Penalties</h3>
							<span>$1,907,577,000</span>
						</div>

					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="stat-box panel">
						<div id='product104-fi'></div>
						<!--
						<div class="stat">
							<h3>Financial Institutions</h3>
							<ul>
								<li>Casinos</li>
								<li>MSBs</li>
								<li>Depository Institutions</li>
								<li>Precious Metals/Jewelry</li>
								<li>Securities and Futures</li>
								<li>No Information</li>
							</ul>
						</div>
						-->
					</div>
					
				</div>
				<!--
				<div class="col-sm-6 col-lg-4">
					<div class="stat-box panel">
						<div class="pie-chart">
							<img src="http://via.placeholder.com/400x400" alt="">
						</div>
						<div class="stat">
							<h3>AML Pillars Violated</h3>
							<ul>
								<li>System of Internal</li>
								<li>All Available Information</li>
								<li>Independent Testing</li>
								<li>Designation of BSA</li>
							</ul>
						</div>

					</div>
					
				</div>
				-->
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="stat-box panel">
						<div id='product104-bsa'></div>
						<!--
						<div class="stat">
							<h3>BSA Program Area</h3>
							<ul>
								<li>KYC</li>
								<li>CTR</li>
								<li>SAR</li>
							</ul>
						</div>
						-->

					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="panel">


<div class="grid">
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc casinos"><div class="inner">
    <p class="year">2009</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">11,000</span></p>
  <div class="legend-circle casino"></div><div class="legend-circle sar"></div></div></div>
  
  <div class="grid-item kyc casinos" ><div class="inner">
    <p class="year">2010</p>
    <h3 class="name">Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h3>
    <p>$<span class="penalty">801,000</span></p>
 <div class="legend-circle msb"></div><div class="legend-circle kyc"></div></div></div>
 
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">701,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar depository"><div class="inner">
    <p class="year">2012</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>
  <div class="legend-circle no-information"></div><div class="legend-circle ctr"></div></div></div>
  
  
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle ktr"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">101,000</span></p>

  <div class="legend-circle depository"></div><div class="legend-circle sar"></div></div></div>
  <div class="grid-item sar msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">200,000</span></p>

  <div class="legend-circle securities"></div><div class="legend-circle ctr"></div></div></div>
  <div class="grid-item kyc msbs"><div class="inner">
    <p class="year">2007</p>
    <h3 class="name">Gulfside Casino Partnership, d/b/a Copa Casino</h3>
    <p>$<span class="penalty">30,000</span></p>

  <div class="legend-circle casino"></div><div class="legend-circle ctr"></div></div></div>

</div>

					</div>
				</div>		
			</div>
		</div>	
	</div>
	
	<div class="col-2">
		<div class="data-selection ea-1">
			<div class="inner">
				<h2>In the Matter of Gulfside Casino Partnership, d/b/a Copa Casino</h2>
				<table>
					<tr>
						<td>Financial Institution</td>
						<td>No Information</td>
					</tr>
					<tr>
						<td>BSA Program Area</td>
						<td>CTR</td>
					</tr>
					<tr>
						<td>Fine/Penalty</td>
						<td>$101,000</td>
					</tr>
				</table>
				<div class="moreinfo">
					<h3 class="head">Specific Conduct</h3>
					<p>FinCEN determined that between September 24, 1993, through December 31, 1994, Copa failed to timely file at least 28 Currency Transaction Report by Casinos (“CTRC”) forms for currency transactions in an amount greater than $10,000, as required by the Bank Secrecy Act.</p>
					<h3 class="head">Information 2</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt possimus non necessitatibus voluptate, maiores quaerat id illum sapiente autem blanditiis praesentium suscipit omnis?</p>
				</div>
				<span class="btn">Show Less</span>
			</div>
		</div>
		<div class="data-selection ea-2">
			<div class="inner">
				<h2>In the Matter of GBiloxi Casino Corp., d/b/a/ Casino Magic-Biloxi</h2>
				<table>
					<tr>
						<td>Financial Institution</td>
						<td>No Information</td>
					</tr>
					<tr>
						<td>BSA Program Area</td>
						<td>CTR</td>
					</tr>
					<tr>
						<td>Fine/Penalty</td>
						<td>$145,000</td>
					</tr>
				</table>
				<div class="moreinfo">
					<h3 class="head">Specific Conduct</h3>
					<p>FinCEN determined that between September 24, 1993, through December 31, 1994, Copa failed to timely file at least 28 Currency Transaction Report by Casinos (“CTRC”) forms for currency transactions in an amount greater than $10,000, as required by the Bank Secrecy Act.</p>
					<h3 class="head">Information 2</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt possimus non necessitatibus voluptate, maiores quaerat id illum sapiente autem blanditiis praesentium suscipit omnis?</p>
				</div>
				<span class="btn">Show Less</span>
			</div>
		</div>
		<div class="data-selection ea-3">
			<div class="inner">
				<h2>In the Matter of Lady Luck Mississippi, Inc., d/b/a Lady Luck Natchez</h2>
				<table>
					<tr>
						<td>Financial Institution</td>
						<td>No Information</td>
					</tr>
					<tr>
						<td>BSA Program Area</td>
						<td>CTR</td>
					</tr>
					<tr>
						<td>Fine/Penalty</td>
						<td>$85,000</td>
					</tr>
				</table>
				<div class="moreinfo">
					<h3 class="head">Specific Conduct</h3>
					<p>FinCEN determined that between September 24, 1993, through December 31, 1994, Copa failed to timely file at least 28 Currency Transaction Report by Casinos (“CTRC”) forms for currency transactions in an amount greater than $10,000, as required by the Bank Secrecy Act.</p>
					<h3 class="head">Information 2</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt possimus non necessitatibus voluptate, maiores quaerat id illum sapiente autem blanditiis praesentium suscipit omnis?</p>
				</div>
				<span class="btn">Show Less</span>
			</div>
		</div>
	</div>

</div>

<?php include("footer.php"); ?>

