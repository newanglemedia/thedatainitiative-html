<?php include("header.php"); ?>

<div id="breadcrumb">
	<a class="back" href="javascript:history.back();"><i class="fa fa-chevron-left"></i></a>
	<ul>
		<li><a href="">Dashboard</a>
		</li><li><a href="">Search Results for 'FinCEN'</a></li>
	</ul>
</div>

<div id="content" class="reverse">


	<div class="col-1">
		<div id="search-layout">
			<span class="count">2,730,000,000 Results - Results matching 'FinCEN'</span>
			<div class="sort">Sort by:
				<select name="" id="">
					<option value="">Most Relevant</option>
				</select>
			</div>
			<hr>
			<ul id="cards">
				<li>
				<a href="preview.php">
				<span class="inner">
					<span class="type">PDF</span>
					<h3>Advisory on Human Rights Abuses Enabled by
Corrupt Senior Foreign Political Figures and their Financial Facilitators</h3>
					
					<ul>
						<li>Advisory</li>
						<li>Financial</li>
						<li>Politics</li>
						<li>Corruption</li>
						<li>Laws</li>
						<li>FinCEN</li>
					</ul>
				</span>	
				</a>
				</li>
				<li>
				<a href="preview.php">
				<span class="inner">
					<span class="type">PDF</span>
					<h3>Advisory to Financial Institutions Regarding

Disaster-Related Fraud</h3>
					
					<ul>
						<li>Advisory</li>
						<li>Financial</li>
						<li>Politics</li>
						<li>Corruption</li>
						<li>Laws</li>
						<li>FinCEN</li>
					</ul>
				</span>	
				</a>
				</li>
				<li>
				<a href="preview.php">
				<span class="inner">
					<span class="type">PDF</span>
					<h3>Advisory on Widespread Public Corruption in Venezuela</h3>
					
					<ul>
						<li>Advisory</li>
						<li>Financial</li>
						<li>Politics</li>
						<li>Corruption</li>
						<li>Laws</li>
						<li>FinCEN</li>
					</ul>
				</span>	
				</a>
				</li>
				<li>
				<a href="preview.php">
				<span class="inner">
					<span class="type">PDF</span>
					<h3>Advisory on the FATF-Identified Jurisdictions with

AML/CFT Deficiencies</h3>
					
					<ul>
						<li>Advisory</li>
						<li>Financial</li>
						<li>Politics</li>
						<li>Corruption</li>
						<li>Laws</li>
						<li>FinCEN</li>
					</ul>
				</span>	
				</a>
				</li>
				<li>
				<a href="preview.php">
				<span class="inner">
					<span class="type">PDF</span>
					<h3>Advisory on Political Corruption Risks in South Sudan</h3>
					
					<ul>
						<li>Advisory</li>
						<li>Financial</li>
						<li>Politics</li>
						<li>Corruption</li>
						<li>Laws</li>
						<li>FinCEN</li>
					</ul>
				</span>	
				</a>
				</li>
				<li>
				<a href="preview.php">
				<span class="inner">
					<span class="type">PDF</span>
					<h3>Advisory on Political Corruption Risks in South Sudan</h3>
					
					<ul>
						<li>Advisory</li>
						<li>Financial</li>
						<li>Politics</li>
						<li>Corruption</li>
						<li>Laws</li>
						<li>FinCEN</li>
					</ul>
				</span>	
				</a>
				</li>
				<li>
				<a href="preview.php">
				<span class="inner">
					<span class="type">PDF</span>
					<h3>Advisory on the FATF-Identified Jurisdictions with AML/CFT Deficiencies</h3>
					
					<ul>
						<li>Advisory</li>
						<li>Financial</li>
						<li>Politics</li>
						<li>Corruption</li>
						<li>Laws</li>
						<li>FinCEN</li>
					</ul>
				</span>	
				</a>
				</li>
				<li>
				<a href="preview.php">
				<span class="inner">
					<span class="type">PDF</span>
					<h3>Advisory on the FATF-Identified Jurisdictions with AML/CFT Deficiencies</h3>
					
					<ul>
						<li>Advisory</li>
						<li>Financial</li>
						<li>Politics</li>
						<li>Corruption</li>
						<li>Laws</li>
						<li>FinCEN</li>
					</ul>
				</span>	
				</a>
				</li>
				<li>
				<a href="preview.php">
				<span class="inner">
					<span class="type">PDF</span>
					<h3>Advisory on the FATF-Identified Jurisdictions with AML/CFT Deficiencies</h3>
					
					<ul>
						<li>Advisory</li>
						<li>Financial</li>
						<li>Politics</li>
						<li>Corruption</li>
						<li>Laws</li>
						<li>FinCEN</li>
					</ul>
				</span>	
				</a>
				</li>

			</ul>
		</div>		

	</div>	
	
	<div class="col-2">
				<div id="search-filters">
					<div class="inner panel">
					
						<h2>Filters</h2>

						<h3>Product Group</h3>
						<ul class="product-group">

						<li >
							<div class="inner">
								  <div class="form-group">
									<div class="col-sm-12">
									  <div class="checkbox">
										<label>
										   <input type="checkbox"> 

											<table class="table">
												<tr>
													<td><span class="no">101</span></td>
													<td>Global Money Laundering Statistics</td>
												</tr>
											</table>					  
										</label>
									  </div>
									</div>
								  </div>
							</div>
						</li>
						<li>
							<div class="inner">
								  <div class="form-group">
									<div class="col-sm-12">
									  <div class="checkbox">
										<label>
										   <input type="checkbox"> 

											<table class="table">
												<tr>
													<td><span class="no">102</span></td>
													<td>Global Money Laundering Laws and Regulations</td>
												</tr>
											</table>					  
										</label>
									  </div>
									</div>
								  </div>
							</div>
						</li>
						
						
						<li>
							<div class="inner">
								  <div class="form-group">
									<div class="col-sm-12">
									  <div class="checkbox">
										<label>
										   <input type="checkbox"> 

											<table class="table">
												<tr>
													<td><span class="no">103</span></td>
													<td>US Regulatory Guidance (FinCEN)</td>
												</tr>
											</table>					  
										</label>
									  </div>
									</div>
								  </div>
							</div>
						</li>
						<li>
							<div class="inner">
								  <div class="form-group">
									<div class="col-sm-12">
									  <div class="checkbox">
										<label>
										   <input type="checkbox"> 

											<table class="table">
												<tr>
													<td><span class="no">104</span></td>
													<td>US Money Laundering Enforcement Actions</td>
												</tr>
											</table>					  
										</label>
									  </div>
									</div>
								  </div>
							</div>
						</li>
						<li>
							<div class="inner">
								  <div class="form-group">
									<div class="col-sm-12">
									  <div class="checkbox">
										<label>
										   <input type="checkbox"> 

											<table class="table">
												<tr>
													<td><span class="no">105</span></td>
													<td>HIDTA HIFCA Risk Index</td>
												</tr>
											</table>					  
										</label>
									  </div>
									</div>
								  </div>
							</div>
						</li>
						<li>
							<div class="inner">
								  <div class="form-group">
									<div class="col-sm-12">
									  <div class="checkbox">
										<label>
										   <input type="checkbox"> 

											<table class="table">
												<tr>
													<td><span class="no">106</span></td>
													<td>Global Money Laundering Risk Index</td>
												</tr>
											</table>					  
										</label>
									  </div>
									</div>
								  </div>
							</div>
						</li>

					</ul>
					
					<h3>Data Type</h3>
					<ul class="data-type">
						<li>
			
								  <div class="checkbox">
									<label>
										<input type="checkbox"> <span>Spreadsheet</span>
									</label>
								  </div>

						</li>
						<li>
							
								  <div class="checkbox">
									<label>
										<input type="checkbox"> <span>PDF</span>
									</label>
								  </div>
								
						</li>
						<li>
							 
								  <div class="checkbox">
									<label>
										<input type="checkbox"> <span>Document</span>
									</label>
								  </div>
								
						</li>
						<li>
							
								  <div class="checkbox">
									<label>
										<input type="checkbox"> <span>Chart</span>
									</label>
								  </div>
								
						</li>
						<li>
							
								  <div class="checkbox">
									<label>
										<input type="checkbox"> <span>Spreadsheet</span>
									</label>
								  </div>
								
						</li>
					</ul>
					
					<a href="" class="update">Update</a>
					</div>
				</div>
	</div>
	
</div>


<?php include("footer.php"); ?>

