<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="css/styles.css">    
    <link rel="stylesheet" href="css/demo.css">    
    <link rel="stylesheet" href="css/component.css">    
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <style type="text/css">
    	.twitter-typeahead{ width:100%; }
    	.tt-menu{background:#111;opacity: 0.8;padding: 20px;}
    	.select2-selection__placeholder{ color:white!important;}
    	.select2-container--default .select2-selection--single .select2-selection__arrow b { border-color: #fff transparent transparent transparent!important;border-width: 8px 7px 0 7px!important;}
    	.select2-container--default .select2-selection--single .select2-selection__arrow {    height: 35px!important;    width: 40px!important;}
    </style>
</head>
<body>




<header>
	<div class="row">
		<div class="col-1">
			<div id="logo">
				<a href="dashboard.php"><img src="img/logo.png" alt=""></a>
			</div>
			<div id="search">
				<div class="inner">
					<form action="">
						<input id="search-input" placeholder="Enter a keyword or data tag" type="text">
						<input class="submit" type="submit" value="">
					</form>
				
					<div class="dropdown">
						All Products <i class="fa fa-chevron-down"></i>
						<div class="dropdown-menu">
							<ul>
								<li></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="nav">
				<ul>
					<li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="#">Products <i class="fa fa-chevron-down"></i></a>
					<div class="dropdown-menu">
						<div class="dropdown-nav">
							<ul>
								<li class="active"><a href="">100 SERIES</a></li>
								<li><a href="">200 SERIES <span>Coming Soon</span></a></li>
							</ul>
						</div>
						<div class="dropdown-content">
							<ul>
							<li>
								<div class="inner">
									<a href="product-101.php">
										<table class="table">
											<tr>
												<td><span class="no">101</span></td>
												<td><h4>Global Money Laundering Statistics</h4></td>
											</tr>
										</table>

									</a>
								</div>
							</li>
							<li>
								<div class="inner">
									<a href="product-102.php">
										<table class="table">
											<tr>
												<td><span class="no">102</span></td>
												<td><h4>Global Money Laundering Laws and Regulations</h4></td>
											</tr>
										</table>

									</a>
								</div>
							</li>
							<li>
								<div class="inner">
									<a href="product-103.php">
										<table class="table">
											<tr>
												<td><span class="no">103</span></td>
												<td><h4>US Regulatory Guidance (FinCEN)</h4></td>
											</tr>
										</table>

									</a>
								</div>
							</li>
							<li>
								<div class="inner">
									<a href="product-104.php">
										<table class="table">
											<tr>
												<td><span class="no">104</span></td>
												<td><h4>US Money Laundering Enforcement Actions</h4></td>
											</tr>
										</table>

									</a>
								</div>
							</li>
							<li>
								<div class="inner">
									<a href="product-105.php">
										<table class="table">
											<tr>
												<td><span class="no">105</span></td>
												<td><h4>HIDTA HIFCA Risk Index</h4></td>
											</tr>
										</table>
									</a>
								</div>
							</li>
							<li>
								<div class="inner">
									<a href="product-106.php">
										<table class="table">
											<tr>
												<td><span class="no">106</span></td>
												<td><h4>Global Money Laundering Risk Index</h4></td>
											</tr>
										</table>

									</a>
								</div>
							</li>
							</ul>
						</div>
					</div>
					</li>
					<li><a href="#">About</a></li>
					<li><a href="#">Contact Us</a></li>
				</ul>
			</div>
		</div>
		<div class="col-2">
			<div id="nav-profile">
				<ul>
					<li><a href="#"><i class="fa fa-bookmark"></i></a></li>
					<li><a href="#"><i class="fa fa-user"></i></a></li>
					<li><a href="#"><i class="fa fa-search"></i></a></li>
					<li><a href="#"><i class="fa fa-bars"></i></a></li>
				</ul>
				<a href=""></a>
			</div>
		</div>
	</div>	
</header>